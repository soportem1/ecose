var base_url = $("#base_url").val();
$(document).ready(function () {
    $("#save").on("click",function(){
        guardar();
    });
});
function guardar(){
    var form_register = $('#insert_encuesta');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
          resp1: "required",
          resp2: "required",
          resp3: "required",
          resp4: "required",
          resp5: "required",
          resp6: "required",
          resp7: "required",
          resp8: "required",
          resp9: "required",
          resp10: "required"
        }, 
        messages: {
            resp1: "Requerido",
        },    
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var valid = $("#insert_encuesta").valid();
    if ($("#tabla_pregs tbody > tr").length>0 && valid) {
        var DATA  = [];
        var TABLA   = $("#tabla_pregs tbody > tr");
        TABLA.each(function(){  
            var id_preg = $(this).find("input[id*='id_preg']").val();       
            item = {};
            //item ["id"] = $(this).find("input[id*='id']").val();
            item ["id"] = 0;
            item ["id_pregunta"] = $(this).find("input[id*='id_preg']").val();
            item["id_cliente"] = $("#id_cli").val()
            if($(this).find("input[id*='tipo']").val()==1){ //escala
                if($(this).find("input[id*='resp"+id_preg+"_4']").is(":checked")==true){
                    resp = 4;
                }else if($(this).find("input[id*='resp"+id_preg+"_3']").is(":checked")==true){
                    resp = 3;
                }else if($(this).find("input[id*='resp"+id_preg+"_2']").is(":checked")==true){
                    resp = 2;
                }else if($(this).find("input[id*='resp"+id_preg+"_1']").is(":checked")==true){
                    resp = 1;
                }
            }else{
                if($(this).find("input[id*='resp"+id_preg+"_2']").is(":checked")==true){ //no
                    resp = 2;
                }else if($(this).find("input[id*='resp"+id_preg+"_1']").is(":checked")==true){ //si
                    resp = 1;
                }
            }
            item["respuesta"] = resp;
            item ["sugerencias"] = $("#sugerencias").val();
            item ["serv_inclu"] = $("#serv_inclu").val();
            item ["empresa"] = $("#empresa").val();
            item ["nombre"] = $("#nombre").val();
            item ["serv"] = $("#serv").val();
            item ["area"] = $("#area").val();
            item ["tel"] = $("#tel").val();
            item ["correo"] = $("#correo").val();
            DATA.push(item);
            //console.log("resp: "+resp);
        });
        INFO  = new FormData();
        aInfo   = JSON.stringify(DATA);
        INFO.append('data', aInfo);
        $.ajax({
            data: INFO,
            type: 'POST',
            url : base_url+'index.php/Cliente/submit_pregs',
            processData: false, 
            contentType: false,
            async: false,
            beforeSend: function(){
                $("#save").attr("disabled",true);
             },
            success: function(data_docs){
                swal("¡Agradecemos su colaboración!", "Enviado correctamente", "success");

                setTimeout(function () { 
                    if($("#idemp").val()==1 || $("#idemp").val()==2 || $("#idemp").val()==3 || $("#idemp").val()==6)
                        window.location.href="http://ecose.com.mx";
                    else if($("#idemp").val()==4)
                        window.location.href="https://ahisa.mx"; 
                    else if($("#idemp").val()==5)
                        window.location.href="https://auven.com.mx"; 
                }, 2000);
            }
        }); 
    }
}