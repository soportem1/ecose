var base_url=$("#base_url").val();
$(document).ready(function () {   
    $('.pickadate').pickadate({
        monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dec'],
        weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        today: 'Hoy',
        clear: 'Limpiar',
        close: 'Cerrar',
        format: "yyyy-mm-dd"
    });

    $('.timepicker').pickatime({
        format: "HH:i:00",
        formatLabel: "HH:i a ",
        min: [7, 0],
        max: [23, 30]
    });
    
    loadTable($("#tipostock option:selected").val());

    $("#Fecha1").on("change",function(){
        if($("#Fecha1").val()!="")
            loadTable($("#tipostock option:selected").val());
    });
    $("#Fecha2").on("change",function(){
        if($("#Fecha1").val()!="" && $("#Fecha2").val()!="")
            loadTable($("#tipostock option:selected").val());
    });
    /*$("#tipostock").on("change",function(){
        loadTable($("#tipostock option:selected").val());
    });*/
    $("#empresa").on("change",function(){
        loadTable($("#tipostock option:selected").val());
    });
    $("#export").on("click",function(){
        estatus = $('#tipostock option:selected').val();
        fecha1= $('#Fecha1').val();
        fecha2= $('#Fecha2').val();
        empresa= $('#empresa').val();

        if(estatus=="")
            estatus=0;
        if(fecha1=="")
            fecha1=0;
        if(fecha2=="")
            fecha2=0;
        /*if(empresa!=0){
            window.open(base_url+"Ordenes/ExportOrdenes/"+fecha1+"/"+fecha2+"/"+estatus+"/"+empresa, '_blank');
        }else{

        }*/
        window.open(base_url+"Ordenes/ExportOrdenes/"+fecha1+"/"+fecha2+"/"+estatus+"/"+empresa, '_blank');
    }); 


    $("#file_repor").fileinput({
        showCaption: false,
        showUpload: false,
        allowedFileExtensions: ["pdf","zip","rar"],
        browseLabel: 'Seleccionar archivo',
        uploadUrl: base_url+'Ordenes/cargaReporte',
        maxFilePreviewSize: 358400, //350mb
        maxFileSize: 358400,
        showUpload: true,
        elErrorContainer: '#kv-avatar-errors-1',
        /*msgErrorClass: 'alert alert-block alert-danger',
        initialPreview: [
            ''+previewe+'',    
        ],
        initialPreviewAsData: "true",
        initialPreviewFileType: 'image', // image is the default and can be overridden in config below
        initialPreviewConfig: [
            {type:"pdf", url: ""+base_url+"Ordenes/deleteReporte/"+id_orden+"/"+carp_cli+"/"+carp+"/"+file, caption: file, key:1}
        ],*/
        previewFileIconSettings: {
            'pdf': '<i class="fa fa-file-pdf-o text-warning"></i>',
            'zip': '<i class="fa fa-file-archive-o text-warning"></i>',
            'rar': '<i class="fa fa-file-archive-o text-warning"></i>'
        },
        uploadExtraData: function (previewId, index) {
            var info = {   
                    id_orden:$("#id_orden_aux").val(),
                    carp:$("#carp_aux").val(),
                    carp_prin:$("#carp_cl").val(),
                    id_cli:$("#id_cli_aux").val()
                };
            return info;
        }
    }).on('fileuploaded', function(event, files, extra) {
        $("#modal_carga").modal("hide");
        $('#file_repor').fileinput('clear');
        cont_msj++;
        if(cont_msj==1){
            toastr.success('Éxito', 'Guardado correctamente');
        }
    }).on('filebatchuploadsuccess', function(event, files, extra) {
        //$("#modal_carga").modal("hide");
        //$('#file_repor').fileinput('clear');
        //toastr.success('Éxito', 'Guardado correctamente');
    }).on('filedeleted', function(event, files, extra) {
        $("#modal_carga").modal("hide");
        toastr.success('Éxito', 'Eliminado correctamente');
        //verificarReportes(id_orden);
    });

});

    function verProveedor(id,id_emp){
        //console.log("id_emp: "+id_emp);
        //$("#modal_empresa").modal("show");
        var prov="";
        $.ajax({
            type: "POST",
            url: base_url+"Ordenes/verEmpresaOrden",
            data: { id:id, id_emp:id_emp},
            async:false,
            success: function (data) {
                //console.log(data);
                //$("#cont_emp").html(data);
                //return data;
                prov=data;
            }
        });
        return prov;
    }

    /*function verEmpresa(id){
        $("#modal_empresa").modal("show");
        $.ajax({
            type: "POST",
            url: base_url()+"Ordenes/verEmpresaOrden",
            data: { id:id},
            success: function (data) {
                //console.log(data);
                $("#cont_emp").html(data);
            }
        });
    }*/

    function loadTable(status){
        url = base_url+"Ordenes/getData_ordenes";
        var table = $('#tabla').DataTable({
            responsive: true,
            "bProcessing": true,
            "serverSide": true,
            stateSave:true,
            destroy:true,
            "order": [[ 0, "desc" ]],
            "ajax": {
                url: url,
                data: { fechai:$("#Fecha1").val(), fechaf:$("#Fecha2").val(), emp:$("#empresa option:selected").val(),status:status },
                type: "post",
                error: function () {
                    //$("#tabla").css("display", "none");
                }
            },
            "columns": [
                {"data": "id_orden"},
                {"data": "nomVendedor"},
                {"data": "cotizacion_id"},
                //{"data": "nombre"},
                {"data": "fecha_creacion"},
                {"data": "alias"},
                //{"data": "proveedor"},
                {"data": null,
                    "render": function (data, type, row, meta) {
                        return verProveedor(row.cotizacion_id,row.id_empresa);
                        //return row.empresa;
                    }
                },
                {"data": "status",
                    "render": function (data, type, row, meta) {
                        //console.log("id_orden: "+row.id_orden);
                        //console.log("status: "+row.status);
                        return estatus(row.status, row.fecha_inicio);
                    }
                },
                {"data": "id_orden",
                    "render": function (data, type, row, meta) {
                        return acciones(data,row.fecha_inicio,row.status,row.file_repor,row.carpeta,row.cliente_id,row.carpeta_prin,row.id_empresa);
                    }
                }
            ]
        });
        $('#tabla').on('click', 'a.prog', function () {
            var tr = $(this).closest('tr');
            var data = table.row(tr).data();
            //console.log("cotizacion_id: "+data.cotizacion_id);
            $("#servicio").val(data.cotizacion_id);
            $("#cliente_orden").val(data.cliente_id);
            $("#hora_inicio").val(data.hora_inicio);
            $("#hora_fin").val(data.hora_fin);
            if(data.fecha_inicio!="" && data.fecha_inicio!="0"){
                $("#fecha_inicio").val(data.fecha_inicio);
            }
            $("#fecha_fin").val(data.fecha_fin);
            
            if(data.id_tecnico>0 || data.id_tecnico!=""){
                //console.log("id_tecnico: "+data.id_tecnico);
                $("#tecnico").val(data.id_tecnico);
            }
            $("#comentarios").val(data.comentarios);
            //$("#tecnico").val(data.id_tecnico);

            $("#modal_program").modal("show");
            /*$("#tecnico").select2({
                width: '100%',
                minimumInputLength: 3,
                minimumResultsForSearch: 10,
                placeholder: 'Buscar un técnico',
                language: {
                    noResults: function() {
                      return "No hay resultado";        
                    },
                    searching: function() {
                      return "Buscando..";
                    }
                }
            });*/

        });
        /*$('#tabla').on('click', 'a.print', function () {
            var tr = $(this).closest('tr');
            var data = table.row(tr).data();
            console.log("id_orden: "+data.id_orden);
            imprimir(data.id_orden);
        });*/

        $('#tabla').on('click', 'a.env_mail', function () {
            var tr = $(this).closest('tr');
            var data = table.row(tr).data();
            enviarEmail(data.cliente_id,data.cotizacion_id,data.hora_inicio,data.hora_fin,data.fecha_inicio,data.fecha_fin,data.comentarios);
        });
    }
    function acciones(data,fechai,status,file_repor,carpeta,id_cli,carp_prin,id_empresa) {
        if($("#idus").val()=="1"){ //super admin
            var opcion = "";
            var opcion2 = "";
            var opcion3 = "";
            var opcion4 = "";
            opcion = '<a class="dropdown-item prog" >Programación</a>'; 
            opcion2 = '<a class="dropdown-item print" onclick="imprimir('+data+')">Ver Orden</a>';
            if(status!=2){
                opcion3 = '<a class="dropdown-item" onclick="cambiar_estatus('+data+',2)">Cambiar a "Realizada"</a>'; //administracion no puede 
            }else{
                if(file_repor==""){
                    file_repor=0;
                }
                if(carpeta==""){
                    carpeta=0;
                }
                opcion3 = '<a data-idc="'+id_cli+'" data-cli="'+id_empresa+'" class="dropdown-item datacli_'+data+'" onclick="updateDoc('+data+',\''+file_repor+'\',\''+carpeta+'\','+id_cli+',\''+carp_prin+'\')">Cargar Documento</a>';
            }
            if(fechai!="" && fechai!="0" && fechai!=null){
                opcion4 = '<a title="Envíar email a cliente de confirmación" class="dropdown-item env_mail" >Enviar Programación</a>';   
            }

            var btn='<div class="btn-group ml-1 mb-0">\
                <button type="button" class="btn btn-sm btn-raised mb-0 btn-dark dropdown-toggle" data-toggle="dropdown" ><i class="fa fa-bars"></i></button>\
                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">\
                    '+opcion+'\
                    '+opcion4+'\
                    '+opcion2+'\
                    '+opcion3+'\
                </div>\
            </div>';
        } 
        else if($("#idus").val()!="1"){
            var opcion = "";
            var opcion2 = "";
            var opcion3 = "";
            var opcion4 = "";    
            opcion = '<a class="dropdown-item prog" >Programación</a>'; 
            opcion2 = '<a class="dropdown-item print" onclick="imprimir('+data+')">Ver Orden</a>';
            if(status!=2){
                opcion3 = '<a class="dropdown-item" onclick="cambiar_estatus('+data+',2)">Cambiar a "Realizada"</a>'; //administracion no puede realizar esta accion, comentar despues de pruebas
            }
            if($('#permiso_program_env').val()==1){
                if(fechai!="" && fechai!="0" && fechai!=null){
                    opcion4 = '<a title="Envíar email a cliente de confirmación" class="dropdown-item env_mail" >Enviar Programación</a>';   
                }
            } 
            var btn='<div class="btn-group ml-1 mb-0">\
                <button type="button" class="btn btn-sm btn-raised mb-0 btn-dark dropdown-toggle" data-toggle="dropdown" ><i class="fa fa-bars"></i></button>\
                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">\
                    '+opcion+'\
                    '+opcion4+'\
                    '+opcion2+'\
                    '+opcion3+'\
                </div>\
            </div>';
        } 
        else{
            var btn='<div class="btn-group ml-1 mb-0">\
                    <button type="button" class="btn btn-sm btn-raised mb-0 btn-dark dropdown-toggle" data-toggle="dropdown" ><i class="fa fa-bars"></i></button>\
                    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">\
                        <a class="dropdown-item" href="../ordenes/orden/'+data+'">Datos de la Orden</a>\
                    </div>\
                </div>';
            
        }
        //<a class="dropdown-item" onclick="cambiar_estatus('+data+',2)">Cancelar</a>\
        return btn;
    }

    function get_namcp1(id_cli){
        //console.log("id_cli: "+id_cli)
        $.ajax({
            type: "POST",
            async:false,
            url: base_url+"Ordenes/creaNameCP1",
            data: {id_cli: id_cli},
            success: function (result) {
                $("#carp_cl").val(result);
                //console.log("carpeta_cli creada: "+result);
            }
        });
    }
    var cont_msj=0;
    function updateDoc(id_orden,file,carp,id_cli,carp_cli){
        if(carp=="0"){
            //get_namcp1(id_cli);
            new Promise(function(resolve) {
                resolve(get_namcp1(id_cli));
            }).then(function(result) {
                verificarCarpetas(id_orden);
            });
        }else{
            //verificarCarpetas(id_orden);
        }
        $("#modal_carga").modal();
        $("#file_aux").val(file);
        $("#carp_aux").val(carp);
        $("#carp_cl").val(carp_cli);
        $("#id_orden_aux").val(id_orden);
        $("#id_cli_aux").val(id_cli);

        if($(".datacli_"+id_orden).data("cli")!=4 && $(".datacli_"+id_orden).data("cli")!=5){
            empresa="Ecose";
        }if($(".datacli_"+id_orden).data("cli")==4){
            empresa="Ahisa";
        }if($(".datacli_"+id_orden).data("cli")==5){
            empresa="Auven";
        }
        $("#datacliente").html("<b>ID cliente:</b> "+$(".datacli_"+id_orden).data("idc") +" / <b>Empresa del cliente:</b> "+empresa);
        //var carp_cli=$("#carp_cl").val();
        //console.log("file: "+file);
        //console.log("carp: "+carp);
        //console.log("carp_cli: "+carp_cli);
        //console.log("id_orden: "+id_orden);
        //console.log("id_orden: "+$("#id_orden_aux").val());
        var id_orden_aux=$("#id_orden_aux").val();
        var previewe ="";
        /*if(file!="0"){
            previewe=''+base_url+"reportes/cliente/"+carp_cli+"/"+carp+"/"+file+'';
        }*/
        
        verificarReportes(id_orden);        
    }

    function verificarCarpetas(id_orden){
        //console.log("verificarCarpetas");
        $.ajax({
            type: "POST",
            url: base_url+"Ordenes/verificarCarpetasOrd",
            data: { "id_orden": id_orden },
            async: false,
            success: function (result) {
                var array = $.parseJSON(result);
                if(array.cont>0){
                    $("#carp_aux").val(array.carpeta);
                    $("#carp_cl").val(array.carpeta_prin);
                    $("#id_orden_aux").val(array.id_orden);
                    //console.log("consulta trae id_orden: "+array.id_orden);
                    //console.log("carpeta verificarReportes: "+array.carpeta);
                    //console.log("carpeta_prin verificarReportes: "+array.carpeta_prin);
                }
            }
        });
    }

    function verificarReportes(id_orden){
        $.ajax({
            type: "POST",
            url: base_url+"Ordenes/verificarReportes",
            data: { "id_orden": id_orden },
            async: false,
            success: function (result) {
                var array = $.parseJSON(result);
                $("#cont_files").html(array.html);
                cont_msj=0;
                /*$("#carp_aux").val(array.carpeta);
                $("#carp_cl").val(array.carpeta_prin);
                console.log("carpeta verificarReportes: "+array.carpeta);
                console.log("carpeta_prin verificarReportes: "+array.carpeta_prin);*/
            }
        });
    }
    
    function estatus(data, fechai){
        var str='<span class="badge badge-warning width-100">Pendiente</span>';
        if(data==1 && fechai!="" && fechai!="0" && fechai!=null && fechai!="0000:00:00"){
            str='<span class="badge badge-primary width-100">En Proceso</span>';
        }
        if(data==2){
            str='<span class="badge badge-success width-100">Realizado</span>';
        }
        if(data==3){
            str='<span class="badge badge-danger width-100">Cancelado</span>';
        }
        return str;
    }
    
    function cambiar_estatus(id,estatus){
        //$("#modal_program").modal("show");
        swal({
            title: '',
            text: "¿Desea cambiar el estatus de la Orden?",
            type: 'info',
            showCancelButton: true,
            allowOutsideClick: false
        }).then(function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: base_url+"Ordenes/cambiarEstatus",
                    data: {id: id, status: estatus},
                    success: function (data) {
                        swal("", "Se cambió el estatus de la cotización", "success");
                        //table.ajax.reload();
                        loadTable($("#tipostock option:selected").val());
                    }
                });
            }
        }).catch(swal.noop);
        
    }
    
    function programacion(){
        //console.log("servicio: "+$("#servicio").val());
        var datos={
            hora_inicio: $("#hora_inicio").val(),
            hora_fin: $("#hora_fin").val(),
            fecha_inicio: $("#fecha_inicio").val(),
            fecha_fin: $("#fecha_fin").val(),
            comentarios: $("#comentarios").val(),
            tecnico: $("#tecnico").val()
        };
        if($("#hora_inicio").val()!="" && $("#hora_fin").val()!="" && $("#fecha_inicio").val()!="" && $("#fecha_fin").val()!="" && $("#tecnico").val()!=""){
            $.ajax({
                type: "POST",
                url: base_url+"Ordenes/programacion/"+$("#servicio").val()+"/"+$("#cliente_orden").val(),
                data: datos,
                success: function (data) {
                    swal("", "Se ha actualizado el evento", "success");
                    $("#modal_program").modal("hide");
                    //table.ajax.reload();
                    loadTable($("#tipostock option:selected").val());
                }
            });
        }else{
            swal("Álerta", "Ingresar los datos necesarios", "warning");
        }
    } 
    
    function imprimir(id) {
        window.open(base_url+'Formatos/orden/' + id,
                'imprimir',
                'width=700,height=800');
    }

    function enviarEmail(id_cli,servicio,hora_inicio,hora_fin,fecha_inicio,fecha_fin,comentarios){
        /*console.log("hora_inicio: "+hora_inicio);
        console.log("hora_fin: "+hora_fin);
        console.log("fecha_inicio: "+fecha_inicio);
        console.log("fecha_fin: "+fecha_fin);
        console.log("comentarios: "+comentarios);
        console.log("servicio: "+servicio);
        console.log("id_cli: "+id_cli);*/
        var datos={
            hora_inicio: hora_inicio,
            hora_fin: hora_fin,
            fecha_inicio: fecha_inicio,
            fecha_fin: fecha_fin,
            comentarios: comentarios,
            servicio: servicio,
            id_cli: id_cli,
        };
        $.ajax({
            type: "POST",
            url: base_url+"Ordenes/avisarMailCli",
            data: datos,
            success: function (data) {
                //console.log("data: "+data);
                swal("Éxito", "Email de confirmación enviado correctamente", "success");;
            }
        });
    } 

    function asignaServRep(id_repor,id_chs){
        swal({
            title: '',
            text: "¿Desea asignar el reporte al servicio seleccionado?",
            type: 'info',
            showCancelButton: true,
            allowOutsideClick: false
        }).then(function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: base_url+"Ordenes/asignarServicioRep",
                    data: {id_repor: id_repor, id_chs: id_chs},
                    success: function (data) {
                        swal("Éxito", "Se asignó correctamente", "success");
                        //table.ajax.reload();
                        //loadTable($("#tipostock option:selected").val());
                    }
                });
            }
        }).catch(swal.noop);   
    }

    function saveCargas(){
        $("#modal_carga").modal("hide");
        swal("Éxito", "Guardado correctamente", "success");
        loadTable($("#tipostock option:selected").val());
    }