ALTER TABLE `clientes` ADD `usuario` VARCHAR(75) NOT NULL AFTER `tipo_cli`, ADD `password` TEXT NOT NULL AFTER `usuario`;
ALTER TABLE `ordenes` ADD `file_repor` TEXT NOT NULL AFTER `id_tecnico`, ADD `carpeta` TEXT NOT NULL AFTER `file_repor`;
ALTER TABLE `ordenes` ADD `carpeta_prin` TEXT NOT NULL AFTER `carpeta`;
ALTER TABLE `clientes` ADD `id_empresa_user` INT NOT NULL AFTER `password`;

DROP TABLE IF EXISTS `bitacora_eliminacli`;
CREATE TABLE IF NOT EXISTS `bitacora_eliminacli` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
