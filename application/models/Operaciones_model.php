<?php

class Operaciones_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }
    
    //funcion generica de obtencion de istado de un catalogo
    public function getCatalogo($catalogo) {
        $sql = "SELECT * FROM $catalogo";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    //funcion generica
    public function getItemCatalogo($catalogo,$id) {
        $sql = "SELECT * FROM $catalogo WHERE id=$id";
        $query = $this->db->query($sql);
        return $query->row();
    }
    
    //funcion generica de actualizacion de tablas
    public function updateTable($id,$data,$tabla) {
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update(''.$tabla);
    }
    
    public function deleteCatalogo($id, $catalogo) {
        $this->db->where('id', $id);
        return $this->db->delete(''.$catalogo);
    }
    
    public function noRegistros($tabla) {
        return $this->db->count_all("$tabla");
    }
    
    
    //funcion generica de insercion en un catalogo
    public function insertToCatalogo($data, $catalogo) {
        $this->db->insert('' . $catalogo, $data);
        return $this->db->insert_id();
    }
    
    
    // FUNCIONES DE LA AGENDA ------------------------------------------------------
    
    public function getEventos(){
        $sql = "SELECT id,nombre as title,color,CONCAT(fecha_inicio,'T',hora_inicio) as start, CONCAT(fecha_fin,'T',hora_fin) as end, 'eventos' as tabla"
                . " FROM eventos";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    public function getEventosServicios(){
        /*$sql = "SELECT o.id, cotizaciones_has_servicios.id as idcs, CONCAT('Orden ',o.id) as title,'#058e1c' as color,CONCAT(fecha_inicio,'T',hora_inicio) as start, CONCAT(fecha_fin,'T',hora_fin) as end, 'ordenes' as tabla"
                . " FROM cotizaciones_has_servicios
                INNER JOIN ordenes as o on o.cotizacion_id=cotizaciones_has_servicios.cotizacion_id
                WHERE fecha_inicio >  '0000-00-00'
                group by o.id";*/

        $where="";
        if($this->session->userdata("empresa")==1 || $this->session->userdata("empresa")==2 || $this->session->userdata("empresa")==3 || $this->session->userdata("empresa")==6){
            $where="and (c.id_empresa=1 or c.id_empresa=2 or c.id_empresa=3 or c.id_empresa=6)"; 
        }else if($this->session->userdata("empresa")==4){
            $where="and c.id_empresa=4"; 
        }else if($this->session->userdata("empresa")==5){
            $where="and c.id_empresa=5"; 
        }

        $sql = "SELECT max(o.id) as id_ultimo, o.id, cs.id as idcs, CONCAT('Orden ',max(o.id)) as title,'#058e1c' as color,CONCAT(fecha_inicio,'T',hora_inicio) as start, CONCAT(fecha_fin,'T',hora_fin) as end, 'ordenes' as tabla"
                . " FROM ordenes as o
                INNER JOIN cotizaciones_has_servicios as cs on cs.cotizacion_id=o.cotizacion_id
                INNER JOIN cotizaciones as c on c.id=o.cotizacion_id
                WHERE fecha_inicio > '0000-00-00' ".$where."
                group by o.cotizacion_id";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    public function getEvento($id,$tabla){
        if($tabla=="eventos"){
            $sql = "SELECT eventos.nombre as evento,CONCAT('Inicio: ',fecha_inicio,' ',hora_inicio,' Fin: ',fecha_fin,' ',hora_fin) as horario,eventos.observaciones"
                . " FROM eventos "
                . "WHERE eventos.id=$id";
        }
        else{
            /*$sql = "SELECT CONCAT(' Orden ',o.id,'<br>',s.nombre) as evento,CONCAT('Inicio: ',fecha_inicio,' ',hora_inicio,' Fin: ',fecha_fin,' ',hora_fin) as horario,comentarios as observaciones, clientes.empresa, clientes.calle, clientes.no_ext, clientes.colonia, clientes.cp, clientes.poblacion, clientes.estado, empleados.nombre, empleados.correo, empleados.telefono"
                . " FROM cotizaciones_has_servicios cs "
                    . "INNER JOIN servicios s ON cs.servicio_id=s.id "
                    . "INNER JOIN ordenes o ON o.cotizacion_id = cs.cotizacion_id "
                    . "INNER JOIN cotizaciones ON cotizaciones.id = cs.cotizacion_id "
                    . "INNER JOIN clientes ON clientes.id = cotizaciones.cliente_id "
                    . "INNER JOIN empleados ON empleados.empresa = s.proveedor "
                . "AND cs.id=$id  ";*/

                /*$sql = "SELECT CONCAT(' Orden ',o.id,'<br>',s.nombre) as evento,CONCAT('Inicio: ',fecha_inicio,' ',hora_inicio,' Fin: ',fecha_fin,' ',hora_fin) as horario,comentarios as observaciones, clientes.empresa, clientes.calle, clientes.no_ext, clientes.colonia, clientes.cp, clientes.poblacion, clientes.estado, empleados.nombre, empleados.correo, empleados.telefono"
                . " FROM cotizaciones_has_servicios cs "
                    . "INNER JOIN servicios s ON cs.servicio_id=s.id "
                    . "INNER JOIN ordenes o ON o.cotizacion_id = cs.cotizacion_id "
                    . "INNER JOIN cotizaciones ON cotizaciones.id = cs.cotizacion_id "
                    . "INNER JOIN clientes ON clientes.id = cotizaciones.cliente_id "
                    . "INNER JOIN empleados ON empleados.empresa = s.proveedor "
                . "AND cs.id=$id  ";*/

                $sql = "SELECT CONCAT(' Orden ',o.id,'<br>',s.nombre) as evento, CONCAT(' Orden ',o.id,'<br>',s2.nombre) as evento2,
                    CONCAT(' Orden ',o.id,'<br>',s3.nombre) as evento3, cs.id_empresa_serv,
                    CONCAT('Inicio: ',fecha_inicio,' ',hora_inicio,' Fin: ',fecha_fin,' ',hora_fin) as horario,comentarios as observaciones, clientes.empresa, clientes.calle, clientes.no_ext, clientes.colonia, clientes.cp, clientes.poblacion, clientes.estado, IFNULL(empleados.nombre,'') as nombre, IFNULL(empleados.correo,'') as correo, IFNULL(empleados.telefono,'') as telefono"
                . " FROM ordenes o "
                    . "INNER JOIN cotizaciones_has_servicios cs ON cs.cotizacion_id = o.cotizacion_id "
                    . "left JOIN servicios s ON cs.servicio_id=s.id "
                    . "left JOIN servicios_ahisa s2 ON cs.servicio_id=s2.id "
                    . "left JOIN servicios_auven s3 ON cs.servicio_id=s3.id "
                    . "INNER JOIN cotizaciones ON cotizaciones.id = cs.cotizacion_id "
                    . "INNER JOIN clientes ON clientes.id = cotizaciones.cliente_id "
                    . "LEFT JOIN empleados ON empleados.id = o.id_tecnico "
                . "where o.id=$id  ";
        }
        
        $query = $this->db->query($sql);
        return $query->row();
    }
    
    
    //---------- FUNCIONES PARA ENCUESTAS Y FACTURAS ----------------------------------------
    
    public function getFacturas($id_cliente){
        $sql = "SELECT * "
                . " FROM facturas WHERE cliente_id=$id_cliente";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    public function getEncuestas($id_cliente){
        $sql = "SELECT * "
                . " FROM respuestas_encuesta as re 
                inner join preguntas_encuesta pe on pe.id=re.id_pregunta
                WHERE id_cliente=$id_cliente
                group by id_cliente
                order by re.id asc";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getEncuestasTotal($id_cliente){
        $sql = "SELECT * "
                . " FROM respuestas_encuesta as re 
                inner join preguntas_encuesta pe on pe.id=re.id_pregunta
                WHERE id_cliente=$id_cliente
                order by re.id asc";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    /*  funcion de clientes-email */
    public function getClienteContac($id) {
        $this->db->select('pc.*, c.empresa_id');
        $this->db->from('personas_contacto pc');
        $this->db->join('clientes c','c.id=pc.cliente_id');
        $this->db->where('cliente_id',$id);
        $query = $this->db->get();
        return $query->result();
    }

    public function getClienteContac2($id) {
        $this->db->select('*');
        $this->db->from('personas_contacto');
        $this->db->where('id',$id);
        $query = $this->db->get();
        return $query->result();
    }

    public function eliminaCliente($id){
        $this->db->where('id', $id);
        $this->db->set('status' , 0);
        $this->db->update('clientes');
    }

    public function getClienteUsuario($user) {
        $this->db->select('usuario');
        $this->db->from('clientes');
        $this->db->where('usuario',$user);
        $this->db->where('status',1);
        $query = $this->db->get();
        return $query;
    }
   
}