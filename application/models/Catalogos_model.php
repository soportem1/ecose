<?php

class Catalogos_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    //funcion generica de obtencion de istado de un catalogo

    public function getPerfilPermisos($id)
    {  
        $sql = "SELECT * FROM perfiles
                INNER JOIN permisos_perfil 
                ON permisos_perfil.perfiles_id=perfiles.id
                WHERE perfiles.id=$id";
        $query = $this->db->query($sql);
        return $query->row(); 
    }
 /*   
    public function getItemEmpleados($id)
    {  
        $sql = "SELECT * FROM empleados
                INNER JOIN perfiles 
                ON perfiles.id=empleados.perfil
                WHERE perfiles.id=$id";
        $query = $this->db->query($sql);
        return $query->row(); 
    }
*/
    public function getPefil($catalogo) {
        $sql = "SELECT * FROM $catalogo WHERE status=1";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getCatalogo($catalogo) {
        $sql = "SELECT * FROM $catalogo";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getData_perfiles($catalogo) {
        $sql = "SELECT * FROM $catalogo WHERE status=1" ;
        $query = $this->db->query($sql);
        return $query->result();
    }
    //funcion generica de consulta con condicion
    public function getCatalogoWhere($catalogo, $condicion) {
        $sql = "SELECT * FROM $catalogo WHERE $condicion";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //funcion generica que obtiene un registro por ID
    
    public function getItemCatalogo($catalogo, $id) {
        $sql = "SELECT * FROM $catalogo WHERE id=$id";
        $query = $this->db->query($sql);
        return $query->row();
    }
    
    //funcion generica de insercion en un catalogo
    public function insertToCatalogo($data, $catalogo) {
        return $this->db->insert('' . $catalogo, $data);
    }

    //funcion generica de edicion en un catalogo
    public function updateCatalogo($data, $id, $catalogo) {
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('' . $catalogo);
    }

    public function updateDescrip($descrip,$id,$id_emp) {
        if($id_emp==1 || $id_emp==2 || $id_emp==3 || $id_emp==6){
            $from="servicios";
        }else if($id_emp==4){
            $from="servicios_ahisa";
        }
        else if($id_emp==5){
            $from="servicios_auven";
        }
        $result="UPDATE $from SET descripcion='$descrip' WHERE id=$id";
        $this->db->query($result);  
    }

    public function deleteCatalogo($id, $catalogo) {
        $this->db->where('id', $id);
        $this->db->delete(''.$catalogo);
    }

    public function insertPerfil($data) {
        $this->db->insert('perfiles', $data);
        return $this->db->insert_id();
    }

    public function insertEmpleado($data) {
        $this->db->insert('empleados', $data);
        return $this->db->insert_id();
    }

    public function updatePermisos_perfil($data, $id) {
        $this->db->set($data);
        $this->db->where('perfiles_id', $id);
        return $this->db->update('permisos_perfil');
    }
    public function updatePermisos($data, $id) {
        $this->db->set($data);
        $this->db->where('empleados_id', $id);
        return $this->db->update('permisos');
    }
	
	public function getServicios($prov,$id){
        $where="";
        $from="";
        if($prov>0){
            $where=" and proveedor=$prov";
        }
        if($id==1 || $id==2 || $id==3 || $id==6){
            $from="servicios";
        }else if($id==4){
            $from="servicios_ahisa";
        }
        else if($id==5){
            $from="servicios_auven";
        }
		$sql = "SELECT $from.*, familias.nombre as familia 
                FROM $from 
                INNER JOIN familias ON familia=familias.id 
                WHERE status=1";
        $query = $this->db->query($sql);
        return $query->result();
        
	}

    public function getClientes() {
        $id_usuario = $this->session->userdata("id_usuario"); 
        /*if($this->session->userdata("perfil")!='1'){
            $where = "and id_usuario = $id_usuario"; 
        }
        else
            $where="";*/
        /*if($id_usuario>1){
            //$where = "and id_usuario = $id_usuario"; 
        }
        if(!$this->session->userdata("administrador")){
           //$where = "and id_usuario = $id_usuario"; 
        }
        else
            $where="";*/
        $sql = "SELECT *, clientes.empresa, clientes.alias, clientes.id as cliente_id, "
                . "GROUP_CONCAT(personas_contacto.nombre SEPARATOR '<br>') as contacto "
                . "FROM clientes "
                . "INNER JOIN personas_contacto ON cliente_id=clientes.id "
                //. "WHERE clientes.estatus=1 $where "
                . "WHERE clientes.status=1 "
                . "GROUP BY clientes.id";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function getDataClientes(){
        $this->db->select("*");
        $this->db->from('clientes');
        $this->db->where('clientes.status','1');
        
        if($this->session->userdata("empresa")=="1" || $this->session->userdata("empresa")=="2" || $this->session->userdata("empresa")=="3" || $this->session->userdata("empresa")=="6") {
            $this->db->where("(empresa_id=1 or empresa_id=2 or empresa_id=3 or empresa_id=6)");
        }
        else if($this->session->userdata("empresa")=="4") {
            $this->db->where('empresa_id',4);
        }
        else if($this->session->userdata("empresa")=="5") {
            $this->db->where('empresa_id',5); 
        }
        $query=$this->db->get();
        return $query->result();
    }

    function getClientesData($params){
        $columns = array( 
            0=>'empresa',
            1=>'alias',
            2=>'giro',
            3=>'representa',
            4=>'sucursal',
            5=>'calle',
            6=>'no_ext',
            7=>'no_int',
            8=>'colonia',
            9=>'referencia',
            9=>'poblacion',
            10=>"cp",
            11=>"estado",
            12=>'coordenadas',
            13=>'razon_social',
            14=>'rfc',
            15=>'cp_fiscal',
            16=>'direccion_fiscal',
            17=>'curp',
            18=>"cuenta",
            19=>"observaciones",
            20=>'turno1_inicio',
            21=>'turno1_fin',
            22=>'turno2_inicio',
            23=>'turno2_fin',
            24=>"turno3_inicio",
            25=>"turno3_fin",
            26=>"id_usuario",
            27=>"clientes.id as cliente_id",
            //28=>"GROUP_CONCAT(personas_contacto.nombre SEPARATOR '<br>') as contacto",
        );
        $columnsSearch = array( 
            0=>'empresa',
            1=>'alias',
            2=>'giro',
            3=>'representa',
            4=>'sucursal',
            5=>'calle',
            6=>'no_ext',
            7=>'no_int',
            8=>'colonia',
            9=>'referencia',
            9=>'poblacion',
            10=>"cp",
            11=>"estado",
            12=>'coordenadas',
            13=>'razon_social',
            14=>'rfc',
            15=>'cp_fiscal',
            16=>'direccion_fiscal',
            17=>'curp',
            18=>"cuenta",
            19=>"observaciones",
            20=>'turno1_inicio',
            21=>'turno1_fin',
            22=>'turno2_inicio',
            23=>'turno2_fin',
            24=>"turno3_inicio",
            25=>"turno3_fin",
            26=>"id_usuario",
            27=>"clientes.id",
            //28=>"personas_contacto.nombre",
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        
        $this->db->select($select);
        $this->db->from('clientes');
        //$this->db->join('personas_contacto', 'personas_contacto.cliente_id=clientes.id','left');
        $this->db->where('clientes.status','1');
        
        if($this->session->userdata("empresa")=="1" || $this->session->userdata("empresa")=="2" || $this->session->userdata("empresa")=="3" || $this->session->userdata("empresa")=="6") {
            $this->db->where("(empresa_id=1 or empresa_id=2 or empresa_id=3 or empresa_id=6)");
        }
        else if($this->session->userdata("empresa")=="4") {
            $this->db->where('empresa_id',4);
        }
        else if($this->session->userdata("empresa")=="5") {
            $this->db->where('empresa_id',5); 
        }
        if($params["edo"]!="0"){
            $this->db->where('clientes.estado',$params["edo"]);
        }
        if($params["tipo_cli"]!="0"){
            $this->db->where('clientes.tipo_cli',$params["tipo_cli"]);
        }
        $this->db->group_by('clientes.id');

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columnsSearch as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
        }
        
        $this->db->order_by($columnsSearch[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_clientes($params){
        $columnsSearch = array( 
            0=>'empresa',
            1=>'alias',
            2=>'giro',
            3=>'representa',
            4=>'sucursal',
            5=>'calle',
            6=>'no_ext',
            7=>'no_int',
            8=>'colonia',
            9=>'referencia',
            9=>'poblacion',
            10=>"cp",
            11=>"estado",
            12=>'coordenadas',
            13=>'razon_social',
            14=>'rfc',
            15=>'cp_fiscal',
            16=>'direccion_fiscal',
            17=>'curp',
            18=>"cuenta",
            19=>"observaciones",
            20=>'turno1_inicio',
            21=>'turno1_fin',
            22=>'turno2_inicio',
            23=>'turno2_fin',
            24=>"turno3_inicio",
            25=>"turno3_fin",
            26=>"id_usuario",
            27=>"clientes.id",
            //28=>"personas_contacto.nombre",
        );
        $this->db->select('count(1)');
        $this->db->from('clientes');
        //$this->db->join('personas_contacto', 'personas_contacto.cliente_id=clientes.id','left');
        $this->db->where('clientes.status','1'); 

        if($this->session->userdata("empresa")=="1" || $this->session->userdata("empresa")=="2" || $this->session->userdata("empresa")=="3" || $this->session->userdata("empresa")=="6") {
            $this->db->where("(empresa_id=1 or empresa_id=2 or empresa_id=3 or empresa_id=6)"); 
        }
        else if($this->session->userdata("empresa")=="4") {
            $this->db->where('clientes.empresa_id',4);
        }
        else if($this->session->userdata("empresa")=="5") {
            $this->db->where('clientes.empresa_id',5); 
        }
        if($params["edo"]!="0"){
            $this->db->where('clientes.estado',$params["edo"]);
        }
        if($params["tipo_cli"]!="0"){
            $this->db->where('clientes.tipo_cli',$params["tipo_cli"]);
        }
        $this->db->group_by('clientes.id');

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsSearch as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }   
        return $this->db->count_all_results();
    }
    
   
    
   
    // Funcion de Elimanar
    public function deleteContactos($id) {
        $this->db->where('cliente_id', $id);
        $this->db->delete('personas_contacto');
    }
    
    public function deleteCategorias($id) {
        $this->db->where('proveedor_id', $id);
        $this->db->delete('proveedores_has_categorias');
    }
    
    public function insertCliente($data) {
        $this->db->insert('clientes', $data);
        return $this->db->insert_id();
    }
    
    public function updateCliente($data, $id) {
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('clientes');
    }
    
    public function deleteDirecciones($id) {
        $this->db->where('cliente_id', $id);
        $this->db->delete('direcciones');
    }
    
    public function removeFamiliaElements($id){
        $this->db->where('familia_id', $id);
        $this->db->delete('familias_has_documentos');
        $this->db->where('familia_id', $id);
        $this->db->delete('alcances');
    }

    public function deletePermisos($data, $id) {
        $this->db->set($data);
        $this->db->where('empleados_id', $id);
        return $this->db->update('permisos');
    }


    public function deletePerfil($id) {  
        $result="UPDATE perfiles SET status=0 WHERE id=$id";
        $datos = $this->db->query($result);
        return $datos;
    }

    // Funciones de logueo
    public function userLogin($usuario,$pass) {
        $sql = "SELECT * FROM empleados WHERE usuario='$usuario' and activo=1";
        $query = $this->db->query($sql); 
        return $query->row();
    }

    public function getPermisos($id) {
        $sql = "SELECT * FROM perfiles
                INNER JOIN permisos_perfil 
                ON permisos_perfil.perfiles_id=perfiles.id
                WHERE perfiles.id=$id";
        $query = $this->db->query($sql);
        return $query->row(); 
    }
/*
    public function getPermisos($id) {
        $sql = "SELECT * FROM permisos WHERE empleados_id=$id";
        $query = $this->db->query($sql);
        return $query->row();
    }
*/    
    

    public function getDescuentos(){
        $sql = "SELECT FORMAT(limite_inf,2) as inf,FORMAT(limite_sup,2) as sup,id,descuento FROM descuentos_act WHERE estatus=1";
        $query = $this->db->query($sql); 
        return $query->result();
    }
    function getselectwheren($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query;
    }
    function Insert($Tabla,$data){
        $this->db->insert($Tabla, $data);
        $id=$this->db->insert_id();
        return $id;
    }
    function updateCatalogon($Tabla,$data,$where){
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($Tabla);
        //return $id;
    }

    public function selectWhere($tabla,$where){
        $this->db->select("*");
        $this->db->from($tabla);
        $this->db->where($where);
        $query=$this->db->get();
        return $query->result();
    }

    public function getDocsServicio($where,$id_emp){
        $this->db->select("sd.*, d.nombre as documento");
        if($id_emp==1 || $id_emp==2 || $id_emp==3 || $id_emp==6){
            $this->db->from("servicios_documentos sd");
        }else if($id_emp==4){
            $this->db->from('servicios_documentos_ahisa sd');
        }else if($id_emp==5){
            $this->db->from('servicios_documentos_auven sd');
        }if(!isset($id_emp)){
            $this->db->from("servicios_documentos sd");
        }
        $this->db->join("documentos d","d.id=sd.id_documento");
        $this->db->where($where);
        $query=$this->db->get();
        return $query->result();
    }

    public function selectZona(){
        $this->db->select('DISTINCT(zona)');
        $this->db->from("empleados");
        $this->db->where(array("zona!="=>"")); 
        $query=$this->db->get();
        return $query->result();
    }

    public function selectWhereOrder($tabla,$where,$order,$tip){
        $this->db->select("*");
        $this->db->from($tabla);
        $this->db->where($where);
        $this->db->order_by($order,$tip);
        $query=$this->db->get();
        return $query->result();
    }

    public function getServiciosEmpresa($where,$id_emp){
        $this->db->select("p.nombre, p.id");
        $this->db->from("cotizaciones_has_servicios cs");
        //$this->db->join("servicios_has_sucursales ss","ss.servicio_id=cs.servicio_id");
        //$this->db->join("sucursales s","s.id=ss.sucursal_id");
        if($id_emp=="1" || $id_emp=="2" || $id_emp=="3" || $id_emp=="6") {
            $this->db->join("servicios s","s.id=cs.servicio_id");
        }
        else if($id_emp=="4") {
            $this->db->join("servicios_ahisa s","s.id=cs.servicio_id");
        }
        else if($id_emp=="5") {
            $this->db->join("servicios_auven s","s.id=cs.servicio_id"); 
        }
        $this->db->join("proveedores p","p.id=s.proveedor");
        $this->db->where($where);
        $this->db->group_by("p.id");
        $query=$this->db->get();
        return $query->result();
    }

    public function getEmpleEmpresa($id){
        $this->db->select("*");
        $this->db->from("empleados");
        $this->db->where('activo',1); 
        if($id=="1" || $id=="2" || $id=="3" || $id=="6") {
            $this->db->where("(empresa=1 or empresa=2 or empresa=3 or empresa=6)"); 
        }
        else if($id=="4") {
            $this->db->where('empresa',4);
        }
        else if($id=="5") {
            $this->db->where('empresa',5); 
        }
        $query=$this->db->get();
        return $query->result();
    }

    public function getCatalogoEmpresa($id) {
        $this->db->select("*");
        $this->db->from("familias");
        if($id=="1" || $id=="2" || $id=="3" || $id=="6") {
            $this->db->where("(id_empresa=1 or id_empresa=2 or id_empresa=3 or id_empresa=6)"); 
        }
        else if($id=="4") {
            $this->db->where('id_empresa',4);
        }
        else if($id=="5") {
            $this->db->where('id_empresa',5); 
        }
        $query=$this->db->get();
        return $query->result();
    }

    public function getUsuarios(){
        $this->db->select("id, nombre");
        $this->db->from("empleados");
        $this->db->where('activo',1);
        $this->db->order_by("nombre","asc"); 
        $query=$this->db->get();
        return $query->result();
    }
    
    public function getListaClientes($id,$array){
        $this->db->select("*");
        $this->db->from("clientes");
        if($id=="1" || $id=="2" || $id=="3" || $id=="6") {
            $this->db->where("(empresa_id=1 or empresa_id=2 or empresa_id=3 or empresa_id=6)"); 
        }
        else if($id=="4") {
            $this->db->where('empresa_id',4);
        }
        else if($id=="5") {
            $this->db->where('empresa_id',5); 
        }
        $this->db->where($array);
        $query=$this->db->get();
        return $query->result();
    }
    
}
