<?php

class Cotizaciones_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }
    
    //funcion generica de obtencion de istado de un catalogo
    public function getCatalogo($catalogo) {    //select de empresa para jalar el contacta dinamico
        $id_usuario = $this->session->userdata('id_usuario');
        /*if($this->session->userdata("perfil")!='1'){
            $where = "WHERE id_usuario = $id_usuario";
        }
        else
            $where="";*/
        //if($id_usuario>1){//si no es admin
            //$where = "WHERE id_usuario = $id_usuario";
        /*}
        else
            $where="";*/
        //$sql = "SELECT * FROM $catalogo $where";
        $sql = "SELECT * FROM $catalogo ";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    function getselectwheren($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query->result();
    }

    public function searchContactos($cliente){
        $sql = "SELECT * FROM personas_contacto WHERE cliente_id=$cliente AND cotizacion=1 and estatus=1";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    public function searchFamilias($tamaño,$id_emp){
        $where="";
        if($id_emp!=4 && $id_emp!=5){
            $tabla="servicios";
        }
        if($id_emp==4){
            $tabla="servicios_ahisa";
            $where="and f.id_empresa=$id_emp";
        }
        if($id_emp==5){
            $tabla="servicios_auven";
            $where="and f.id_empresa=$id_emp";
        }
        $sql = "SELECT f.nombre, f.id FROM familias f INNER JOIN $tabla s ON f.id=s.familia "
                . "WHERE tamano=$tamaño $where GROUP BY s.familia order by nombre asc";

        $query = $this->db->query($sql);
        return $query->result();
    }

    public function searchFamiliasUnion($tamaño){
        $this->db->select("f.nombre, f.id, 1 as empresa");
        $this->db->from("familias f");
        $this->db->join("servicios s","f.id=s.familia");
        $this->db->where("tamano",$tamaño);
        $this->db->group_by("nombre","asc");

        $query1 = $this->db->get_compiled_select(); 

        $this->db->select("f.nombre, f.id, 4 as empresa");
        $this->db->from("familias f");
        $this->db->join("servicios_ahisa sa","f.id=sa.familia");
        $this->db->where("tamano",$tamaño);
        $this->db->group_by("nombre","asc");
        
        $query2 = $this->db->get_compiled_select(); 

        $this->db->select("f.nombre, f.id, 5 as empresa");
        $this->db->from("familias f");
        $this->db->join("servicios_auven sau","f.id=sau.familia");
        $this->db->where("tamano",$tamaño);
        $this->db->group_by("nombre","asc");
        
        $query3 = $this->db->get_compiled_select(); 
        
        $queryunions=$query1." UNION ".$query2." UNION ".$query3;
        return $this->db->query($queryunions)->result();
    }
    
    public function searchServicios($familia,$tamaño,$empresa){
        if($empresa==1 || $empresa==2 || $empresa==3 || $empresa==6){
            $sql = "SELECT * FROM servicios WHERE familia=$familia and tamano=$tamaño /*and proveedor=$empresa*/ and servicios.status=1
            order by nombre ASC";
        }else if($empresa==4){
            $sql = "SELECT * FROM servicios_ahisa WHERE familia=$familia and tamano=$tamaño and status=1
            order by nombre ASC";
        }
        else if($empresa==5){
            $sql = "SELECT * FROM servicios_auven WHERE familia=$familia and tamano=$tamaño and status=1
            order by nombre ASC";
        }
        
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    public function getNRow($table,$id){
        $sql = "SELECT * FROM $table WHERE id=$id";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function getServicio($id){
        $sql = "SELECT * FROM servicios WHERE id=$id";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function getEmailTecnico($empresa){
        $sql = "SELECT correo FROM empleados WHERE empresa=$empresa";
        $query = $this->db->query($sql);
        return $query->row();
    }
    
    public function getCategoria($id){
        $sql = "SELECT * FROM familias WHERE id=$id";
        $query = $this->db->query($sql);
        return $query->row();
    }
    
    public function getDescuentos(){
        $sql = "SELECT * FROM descuentos";
        $query = $this->db->query($sql);
        return $query->row();
    }
    
    public function getPrecio($id_servicio,$cantidad, $sucursal,$id_empresa){ //aqui hay que cambiar la forma de traer el precio si es edicion de cotizacion
        $por_puntos=$this->getTipoPrecio($id_servicio,$id_empresa);

        if($sucursal==0){
            $sucursal=$this->getSucursal();
        }

        $precio=0;
        if($por_puntos){
            $sql = "SELECT precio FROM servicios_has_precios WHERE servicio_id=$id_servicio AND sucursal_id=$sucursal AND $cantidad >= rango_inicio AND $cantidad<= rango_fin";
            $query = $this->db->query($sql);
            if(isset($query->row()->precio))
                $precio=$query->row()->precio;
            else
                $precio=0;
        }
        else{
            $sql = "SELECT precio FROM servicios_has_sucursales WHERE servicio_id=$id_servicio AND sucursal_id=$sucursal";
            $query = $this->db->query($sql);
            if(isset($query->row()->precio))
                $precio=$query->row()->precio;
            else
                $precio=0;
        }

        return $precio;
    }

    public function getPrecioOtra($id_servicio,$cantidad,$tipo,$id_emp){ //aqui hay que cambiar la forma de traer el precio si es edicion de cotizacion
        $precio=0;
        $tabla="servicios_ahisa";
        if($tipo==1){ //consultor
            $precio="precio_consultor";
        }else if($tipo==2){ //directo
            $precio="precio_directo";
        }
        if($id_emp==4){ //ahisa
            $tabla = "servicios_ahisa";
        }else if($id_emp==5){ //auven
            $tabla="servicios_auven";
        }
        $sql = "SELECT $precio as precio FROM $tabla WHERE id=$id_servicio";
        $query = $this->db->query($sql);
        if(isset($query->row()->precio))
            $precio=$query->row()->precio;
        else
            $precio=0;
        
        return $precio;
    }

    private function getTipoPrecio($id_servicio,$id_empresa){
        if($id_empresa==1 || $id_empresa==2 || $id_empresa==3 || $id_empresa==6){ //cotizacion para ecose
            $sql = "SELECT por_puntos FROM servicios WHERE id=$id_servicio";
        }
        if($id_empresa==4){ //cotizacion para ahisa
            $sql = "SELECT por_puntos FROM servicios_ahisa WHERE id=$id_servicio";
        }
        if($id_empresa==5){ //cotizacion para auven
            $sql = "SELECT por_puntos FROM servicios_auven WHERE id=$id_servicio";
        }

        $query = $this->db->query($sql);
        $result=$query->row()->por_puntos;
        return $result;
    }

    private function getSucursal(){
        $id_usuario=$this->session->userdata("id_usuario"); 
        $sql = "SELECT sucursal FROM empleados WHERE id=$id_usuario";
        $query = $this->db->query($sql);
        $sucursal=$query->row()->sucursal;
        return $sucursal;
    }

    public function insertCotizacion($data){
        $this->db->insert('cotizaciones', $data);
        return $this->db->insert_id();
    }
    
    public function updateCotizacion($id,$data){
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('cotizaciones');
    }
    
    public function deleteServicios($id){
        $this->db->where('cotizacion_id', $id);
        $this->db->delete('cotizaciones_has_servicios');
    }
    
    public function insertServicioCotiza($data){
        $this->db->insert('cotizaciones_has_servicios', $data);
        return $this->db->insert_id();
    }

    public function getModificaciones($id_padre){
        $sql = "SELECT COUNT(1) as no FROM cotizaciones WHERE padre=$id_padre";
        $query = $this->db->query($sql);
        $no=$query->row()->no;
        return $no+1;
    }

    public function getConsecutivo(){
        $sql = "SELECT consecutivo FROM cotizaciones ORDER BY consecutivo desc limit 0,1";
        $query = $this->db->query($sql);
        $no=$query->row()->consecutivo;
        return $no+1;
    }
    
    public function getCotizaciones($params){
        $sta=$params["status"];
        $a=date('y');
        //$status = $params['estatus'];
        $columns = array( 
            0 => "c.id",
            1 => "(CONCAT(clave,consecutivo,'-','".$a."',IF(no_modificacion=0, '', CONCAT('-',no_modificacion)))) as folio",
            2 => 'alias', 
            3 => 'fecha_creacion',
            4 => 'FORMAT(total,2) as importe',
            5 => 'descuento',
            6 => 'forma_pago',
            7 => 'e.nombre',
            8 => 'c.status',
            //9 => 'c.id'
            10 => 'c.id_empresa',
            11 => 'total as importe_num',
            12 => 'subtotal'
        );
        $columns2 = array( 
            0 => "(CONCAT(clave,consecutivo,'-','".$a."',IF(no_modificacion=0, '', CONCAT('-',no_modificacion))))",
            1 => 'alias', 
            2 => 'fecha_creacion',
            3 => 'total',
            4 => 'descuento',
            5 => 'forma_pago',
            6 => 'e.nombre',
            7 => 'c.status',
            8 => 'c.id'
        );
        /*$select="c.id,cl.alias,c.fecha_creacion,c.forma_pago,c.status,e.nombre,e.clave,descuento,no_modificacion,consecutivo";
        $select.=", FORMAT(total,2) as importe, (CONCAT(clave,consecutivo,'-',$a,IF(no_modificacion=0, '', CONCAT('-',no_modificacion)))) as folio";*/
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('cotizaciones c');
        $this->db->join('clientes cl', 'cl.id = c.cliente_id','left');
        $this->db->join('empleados e', 'e.id = c.vendedor_id','left');
        //$this->db->order_by('c.id', 'DESC');
        
        /*if(!$this->session->userdata("administrador")){
            $this->db->where("vendedor_id",$this->session->userdata("id_usuario"));
        }*/
        if($this->session->userdata("perfil")!='1'){
            $this->db->where("vendedor_id",$this->session->userdata("id_usuario")); 
        }
        if($this->session->userdata("id_usuario")!='1' && $this->session->userdata("id_usuario")!='12' && $this->session->userdata("id_usuario")!="12"){
            if($this->session->userdata("empresa")==1 || $this->session->userdata("empresa")==2 || $this->session->userdata("empresa")==3 || $this->session->userdata("empresa")==6){
                $this->db->where("(c.id_empresa=1 or c.id_empresa=2 or c.id_empresa=3 or c.id_empresa=6)"); 
            }else if($this->session->userdata("empresa")==4){
                $this->db->where("c.id_empresa",4); 
            }else if($this->session->userdata("empresa")==5){
                $this->db->where("c.id_empresa",5); 
            }
        }
        
        if($sta!=0){
            $this->db->where("c.status",$sta);
        }else{
            $this->db->where("c.status !=","0");
        }
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns2 as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();   
        }

        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        
        $query=$this->db->get();
        return $query;
    }
    
    public function totalCotizaciones($params){
        $sta=$params["status"];
        $a=date('y');
        $columns = array( 
            0 => "(CONCAT(clave,consecutivo,'-','".$a."',IF(no_modificacion=0, '', CONCAT('-',no_modificacion))))",
            1 => 'alias', 
            2 => 'fecha_creacion',
            3 => 'total',
            4 => 'descuento',
            5 => 'forma_pago',
            6 => 'e.nombre',
            7 => 'c.status',
            8 => 'c.id'
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }

        $this->db->select('COUNT(1) as total');
        $this->db->from('cotizaciones c');
        $this->db->join('clientes cl', 'cl.id = c.cliente_id','left');
        $this->db->join('empleados e', 'e.id = c.vendedor_id','left');
        /*if(!$this->session->userdata("administrador")){
            $this->db->where("vendedor_id",$this->session->userdata("id_usuario"));
        }*/
        if($this->session->userdata("perfil")!='1'){
            $this->db->where("vendedor_id",$this->session->userdata("id_usuario")); 
        }
        if($this->session->userdata("id_usuario")!='1' && $this->session->userdata("id_usuario")!='12' && $this->session->userdata("id_usuario")!="15"){
            if($this->session->userdata("empresa")==1 || $this->session->userdata("empresa")==2 || $this->session->userdata("empresa")==3 || $this->session->userdata("empresa")==6){
                $this->db->where("(c.id_empresa=1 or c.id_empresa=2 or c.id_empresa=3 or c.id_empresa=6)"); 
            }else if($this->session->userdata("empresa")==4){
                $this->db->where("c.id_empresa",4); 
            }else if($this->session->userdata("empresa")==5){
                $this->db->where("c.id_empresa",5); 
            }
        }
        if($sta!=0){
            $this->db->where("c.status",$sta);
        }else{
            $this->db->where("c.status !=","0");
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }   
        $query=$this->db->get();
        return $query->row()->total;

    }
    public function totalCotizacionesEstatus($params,$status){
        $columns = array( 
            0 => "(CONCAT(clave,consecutivo,'-','".$a."',IF(no_modificacion=0, '', CONCAT('-',no_modificacion))))",
            1 => 'alias', 
            2 => 'fecha_creacion',
            3 => 'total',
            4 => 'descuento',
            5 => 'forma_pago',
            6 => 'e.nombre',
            7 => 'c.status',
            8 => 'c.id'
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('cotizaciones c');
        $this->db->join('clientes cl', 'cl.id = c.cliente_id','left');
        $this->db->join('empleados e', 'e.id = c.vendedor_id','left');

        /*if(!$this->session->userdata("administrador")){
            $this->db->where("vendedor_id",$this->session->userdata("id_usuario"));
        }*/
        if($this->session->userdata("perfil")!='1'){
            $this->db->where("vendedor_id",$this->session->userdata("id_usuario")); 
        }
        if($this->session->userdata("id_usuario")=='1' || $this->session->userdata("id_usuario")=='12' || $this->session->userdata("id_usuario")=="15"){
            if($this->session->userdata("empresa")==1 || $this->session->userdata("empresa")==2 || $this->session->userdata("empresa")==3 || $this->session->userdata("empresa")==6){
                $this->db->where("(c.id_empresa=1 or c.id_empresa=2 or c.id_empresa=3 or c.id_empresa=6)"); 
            }else if($this->session->userdata("empresa")==4){
                $this->db->where("c.id_empresa",4); 
            }else if($this->session->userdata("empresa")==5){
                $this->db->where("c.id_empresa",5); 
            }
        }
        
        $this->db->where("c.status",$status);

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }   
        $query=$this->db->get();
        return $query->row()->total;
    }
    
    public function getCotizacion($id){
        $a="";
        $sub="(SELECT nombre FROM personas_contacto WHERE cotizacion=1 AND cliente_id=clientes.id LIMIT 0,1) as p_contacto";
        $sub2="(SELECT puesto FROM personas_contacto WHERE cotizacion=1 AND cliente_id=clientes.id LIMIT 0,1) as p_contacto_puesto";
        $this->db->select("cotizaciones.*,clientes.empresa,cliente_id,calle,no_ext,colonia,poblacion,estado,cp,empleados.clave,empleados.nombre as vendedor,empleados.sucursal,empleados.empresa as empre_emp,$sub,$sub2,(CONCAT(clave,consecutivo,'-',$a,IF(no_modificacion=0, '', CONCAT('-',no_modificacion)))) as folio, cotizaciones.id_empresa");
        $this->db->from("cotizaciones");
        $this->db->join('clientes', 'clientes.id = cliente_id');
        $this->db->join('empleados', 'empleados.id = cotizaciones.vendedor_id');
        $this->db->where("cotizaciones.id",$id);
        /*$this->db->where("cotizaciones.status!=",0);
        $this->db->where("clientes.status",1);
        $this->db->where("empleados.estatus",1);*/
        $query=$this->db->get();
        return $query->row();
    }

    public function getContactoCotizaciones($cont_id){
        $this->db->select("nombre,puesto,personas_contacto.id");
        $this->db->from("personas_contacto");
        $this->db->join('cotizaciones', 'cotizaciones.contacto_id = personas_contacto.id');
        $this->db->where("personas_contacto.id",$cont_id);
        $query=$this->db->get();
        return $query->row();
    }
    
    public function getServiciosCotizacion($id,$id_emp){
        $this->db->select("cotizaciones_has_servicios.*,s.clave,s.nombre,s.descripcion,prov.nombre as empresa,s.familia as categoria,
            s2.clave as clave3, s2.nombre as nombre2, s2.descripcion as descripcion2, prov2.nombre as empresa2, s2.familia as categoria2,
            s3.clave as clave3, s3.nombre as nombre3, s3.descripcion as descripcion3, prov3.nombre as empresa3, s3.familia as categoria3");

        $this->db->join('servicios s', 's.id = servicio_id and (id_empresa_serv=1 or id_empresa_serv=2 or id_empresa_serv=3 or id_empresa_serv=6)','left');
        $this->db->join('servicios_ahisa s2', 's2.id = servicio_id and id_empresa_serv=4','left');
        $this->db->join('servicios_auven s3', 's3.id = servicio_id and id_empresa_serv=5','left');

        $this->db->join('proveedores as prov', 's.proveedor = prov.id','left');
        $this->db->join('proveedores as prov2', 's2.proveedor = prov2.id','left');
        $this->db->join('proveedores as prov3', 's3.proveedor = prov3.id','left');

        /*if($id_emp==1 || $id_emp==2 || $id_emp==3 || $id_emp==6){
            $this->db->join('servicios s', 's.id = servicio_id');
        }else if($id_emp==4){
            $this->db->join('servicios_ahisa s', 's.id = servicio_id');
        }else if($id_emp==5){
            $this->db->join('servicios_auven s', 's.id = servicio_id');
        }
        $this->db->join('proveedores as prov', 'proveedor = prov.id');*/

        $this->db->where("cotizacion_id",$id);
        //$this->db->where("servicios.status",1);
        //$this->db->where("prov.status",1);
        $this->db->where("cotizaciones_has_servicios.status",1);
        $this->db->group_by("id");
        $query=$this->db->get("cotizaciones_has_servicios");
        return $query->result();
    }

    public function getServiciosCotizacion2($id,$id_emp){
        $this->db->select("cotizaciones_has_servicios.*,s.clave,s.nombre,s.descripcion,prov.nombre as empresa,s.familia as categoria");
        if($id_emp==1 || $id_emp==2 || $id_emp==3 || $id_emp==6){
            $this->db->join('servicios s', 's.id = servicio_id');
        }else if($id_emp==4){
            $this->db->join('servicios_ahisa s', 's.id = servicio_id');
        }else if($id_emp==5){
            $this->db->join('servicios_auven s', 's.id = servicio_id');
        }
        $this->db->join('proveedores as prov', 'proveedor = prov.id');

        $this->db->where("cotizacion_id",$id);
        //$this->db->where("servicios.status",1);
        //$this->db->where("prov.status",1);
        $this->db->where("cotizaciones_has_servicios.status",1);
        $this->db->group_by("id");
        $query=$this->db->get("cotizaciones_has_servicios");
        return $query->result();
    }
    
    public function getContactoCotizacion($id){
        
        $this->db->join('clientes', 'clientes.id = cotizaciones.cliente_id');
        $this->db->join('personas_contacto', 'personas_contacto.id = contacto_id');
        $this->db->where("cotizaciones.id",$id);
        $this->db->where("contacto_id ",'!=0');
        $this->db->where("clientes.status",'0');
        
        $query=$this->db->get("cotizaciones");
        if($query->num_rows()>0){
            return $query->row();
        }
        else{
            $this->db->join('clientes', 'clientes.id = cotizaciones.cliente_id');
            $this->db->join('personas_contacto', 'clientes.id = personas_contacto.cliente_id');
            $this->db->where("cotizaciones.id",$id);
            $this->db->where("cotizacion",1);

            $query=$this->db->get("cotizaciones");
            return $query->row();
        }
    }
    
    public function getMail($id){
        $this->db->join('clientes', 'clientes.id = cotizaciones.cliente_id');
        $this->db->join('personas_contacto', 'clientes.id = personas_contacto.cliente_id');
        $this->db->where("cotizaciones.id",$id);
        $this->db->where("cotizacion",1);
        $query=$this->db->get("cotizaciones");
        return $query->row()->email;
    }

    public function getDescuentoPrecio($total){
       $sql = "SELECT descuento FROM descuentos_act WHERE $total>=limite_inf AND $total<=limite_sup AND estatus=1";
        $query = $this->db->query($sql);
        if(isset($query->row()->descuento))
            return $query->row()->descuento;
        else
            return "";
    }


    
   
}