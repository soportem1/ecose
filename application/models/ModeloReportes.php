<?php

class ModeloReportes extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    //funcion generica de obtencion de istado de un catalogo

    public function GetVendedores(){
        $sql="SELECT * FROM empleados WHERE activo=1 and perfil = 1 OR activo=1 and perfil=2 order by nombre asc";
        $resutl= $this->db->query($sql);
        return $resutl;
    }

    public function GetCientes(){
        $sql="SELECT * FROM clientes where status=1 order by alias asc";
        $resutl= $this->db->query($sql);
        return $resutl;
    }

    function GetData($params){
        //$params['vendedor']=null;
        /*$fechas="";
        $cliente="";
        $vendedor="";

        if ($data!="") {
            if ($data['fecha1']!="" && $data['fecha2']!="") {
                $fechas=" and co.fecha_creacion between '".$data['fecha1']."' and '".$data['fecha2']."' ";
            }else{
                $fechas="";
            }
            if ($data['cliente']!=""){
                $cliente=" and cli.id=".$data['cliente'];
            }else{
                $cliente="";
            }
            if ($data['vendedor']!="") {
                $vendedor=" and em.id=".$data['vendedor'];
            }else{
                $vendedor="";
            }
            
        }else{
            $fechas="";
            $cliente="";
            $vendedor="";
        }*/
        /*$sql="SELECT co.id, co.total as monto, co.forma_pago, cli.alias ,co.status, em.nombre, concat(em.clave,co.id,co.fecha_creacion) as clave FROM cotizaciones as co inner join empleados as em inner join clientes as cli where co.cliente_id = cli.id and co.vendedor_id=em.id ".$vendedor.$cliente.$fechas." and co.status!=0 ";*/
        $columns = array( 
            0 => "co.id",
            1 => 'co.total', 
            2 => 'co.forma_pago',
            3 => 'cli.alias',
            4 => 'co.status',
            5 => 'em.nombre',
            6 => 'em.clave',
            7 => 'co.fecha_creacion',
            8 => 'co.status'
        );
        for($i=0; $i<count($params['tiposta']); $i++){
            //log_message('error', 'status en model: '.$params['tiposta'][$i]);
            if($params['tiposta'][$i]=="2"){ 
                $tipo="1 as tipo";
            }else{
                $tipo="2 as tipo";
            }
        }
        
        $select="ord.*, IFNULL(ord.id,'0') as id_ord, ord.fecha_creacion as fecha_creacion_ord, co.id, co.total as monto, co.forma_pago, cli.alias ,co.status, em.nombre, co.fecha_creacion as fecha_creacion_cot";
        $select.=", concat(em.clave,co.id,co.fecha_creacion) as clave, em.zona, ".$tipo."";
        //$select.=", FORMAT(total,2) as importe, (CONCAT(clave,consecutivo,'-',$a,IF(no_modificacion=0, '', CONCAT('-',no_modificacion)))) as folio";
        $this->db->select($select);
        $band_ord=0;
        $aux=0;
        for($i=0; $i<count($params['tiposta']); $i++){
            $aux++;
            if($params['tiposta'][$i]=="2"){ //aceptada
                $this->db->from('ordenes ord');
                $this->db->join('cotizaciones co', 'co.id = ord.cotizacion_id','left');
                $band_ord++;
            }
            else{
                if($aux==1){
                    $this->db->from('cotizaciones co');
                    $this->db->join('ordenes ord', 'ord.cotizacion_id = co.id','left'); 
                    /*$this->db->where("co.no_modificacion",1);
                    $this->db->where("co.padre",0);*/
                }
                $band_ord=0;
            }
        }
        
        $this->db->join('clientes cli', 'cli.id = co.cliente_id','left');
        $this->db->join('empleados em', 'em.id = co.vendedor_id','left');
        /*$this->db->join('cotizaciones_has_servicios cs', 'cs.cotizacion_id = co.id','left');
        $this->db->join('servicios s', 's.id = cs.servicio_id','left');*/
        
        //$this->db->where("co.status!=",0);
        if ($params['sucursal']!="") {
            $this->db->where("em.sucursal", $params['sucursal']);
        }
        if ($params['zona']!="") {
            $this->db->where("em.zona", $params['zona']);
        }
        if ($params['vendedor']!="") {
            $this->db->where("em.id", $params['vendedor']);
        }
        if ($params['cliente']!=""){
            $this->db->where("cli.id", $params['cliente']);
        }
        if ($params['fecha1']!="" && $params['fecha2']!="") {
            $aux2=0;
            for($i=0; $i<count($params['tiposta']); $i++){
                $aux2++;
                if($params['tiposta'][$i]=="2"){
                    $this->db->where("ord.fecha_creacion between '{$params['fecha1']} 00:00:00' AND '{$params['fecha2']} 23:59:59' ");
                    //$this->db->where("ord.status",1);
                    //$this->db->or_where("ord.status",2);
                }
                else{
                    if($aux2==1){
                        $this->db->where("co.fecha_creacion between '{$params['fecha1']} 00:00:00' AND '{$params['fecha2']} 23:59:59' ");
                    }
                } 
            }
        }
        $aux_3=0;
        for($i=0; $i<count($params['tiposta']); $i++){
            $aux_3++;
        }
        $aux_4=0; $band_imp=0;
        for($i=0; $i<count($params['tiposta']); $i++){
            $aux_4++;
            if ($params['tiposta'][$i]!="0"){ 
                $band_imp++;
                /*if($aux_4==1){
                    $this->db->where("(co.status", $params['tiposta'][$i]);
                }else if($aux_4>1){
                    //$this->db->where("co.status ".$params['tiposta'][$i]."");
                    $cierre="";
                    if($aux_4==$aux_3){
                        $cierre= ')';
                    }
                    $this->db->or_where("co.status",$params['tiposta'][$i]." ".$cierre);
                }*/
                /*else{
                    $this->db->group_start()
                    ->where('co.status', $params['tiposta'][$i])
                    ->or_group_start()
                            ->where('co.status', $params['tiposta'][$i])
                    ->group_end()
                    ->group_end();
                }*/
            }
            else{
                //$this->db->where("co.status!=",0);
                $this->db->where("(co.status=1 or co.status=2 or co.status=3 or co.status=4)");
            }
        }
        if($aux_4==1 && $band_imp>0){
            $this->db->where("co.status", $params['tiposta'][0]);
        }
        else if($aux_4==2 && $band_imp>0){
            $this->db->where("(co.status=".$params['tiposta'][0]." or co.status=".$params['tiposta'][1].")");
        }else if($aux_4>2 && $band_imp>0){
            $this->db->where("(co.status=".$params['tiposta'][0]." or co.status=".$params['tiposta'][1]." or co.status=".$params['tiposta'][2].")");
        }

        /*if ($params['tiposta'][0]!="0" || isset($params['tiposta'][1]) && $params['tiposta'][1]!="0" || isset($params['tiposta'][2]) && $params['tiposta'][2]!="0"){ 
            if($aux_4==1){
                $this->db->where("co.status", $params['tiposta'][0]);
            }
            else if($aux_4==2){
                $this->db->where("(co.status=".$params['tiposta'][0]." or co.status=".$params['tiposta'][1].")");
            }else if($aux_4>2){
                $this->db->where("(co.status=".$params['tiposta'][0]." or co.status=".$params['tiposta'][1]." or co.status=".$params['tiposta'][2].")");
            }
        }*/
        /*for($i=0; $i<count($params['tiposta']); $i++){
            $aux_4++;
            if ($params['tiposta'][$i]!="0"){ 
                if($aux_3==1){
                    log_message('error', 'aux3 = 1: '.$aux_3);
                    $this->db->where("co.status", $params['tiposta'][$i]);
                }
                if($aux_4==1 && $aux_3>1){
                    log_message('error', 'aux3> 1 y aux_4=1: '.$aux_3);
                    $this->db->where("(co.status", $params['tiposta'][$i]);
                }else if($aux_4>1 && $aux_3==2){
                    //$this->db->where("co.status ".$params['tiposta'][$i]."");
                    $this->db->or_where("co.status = ".$params['tiposta'][$i].")");
                }else if($aux_4>1 && $aux_3>2){
                    $cierre="";
                    if($aux_4==$aux_3){
                        $cierre=")";
                    }
                    //$this->db->where("co.status ".$params['tiposta'][$i]."");
                    $this->db->or_where("co.status = ".$params['tiposta'][$i]." ".$cierre." ");
                }

            }
            else{
                $this->db->where("co.status !=",0);
                //$this->db->where("(co.status=1 or co.status=3 or co.status=4)");
            }
        }*/
        
        //if($params['tiposta']=="2"){ //aceptada
            //$this->db->where("ord.status",1);
            //$this->db->where("co.status",2);
        //}
        //$this->db->where(array('em.activo'=>1));
        //$this->db->where("cs.status",1);
        $this->db->group_by("co.id");
        //$this->db->order_by("max(co.id)","desc");

        //si hay busqueda con el campo de busqueda
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            //$search.=$sta;
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
            
        }
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
        /*$res=$this->db->query($sql);
        return $res;*/
    }

    public function totalCotizaciones($params,$da){
        $band_ord=0;
        $columns = array( 
            0 => "co.id",
            1 => 'co.total', 
            2 => 'co.forma_pago',
            3 => 'cl.alias',
            4 => 'co.status',
            5 => 'e.nombre',
            6 => 'e.clave',
            7 => 'co.fecha_creacion',
            8 => 'co.status'
        );
        $aux=0;
        for($i=0; $i<count($params['tiposta']); $i++){
            $aux++;
            if($params['tiposta'][$i]=="2"){ //aceptada
                $this->db->select('COUNT(ord.id) as total, ord.fecha_creacion as fecha_creacion_ord');
                $this->db->from('ordenes ord');
                $this->db->join('cotizaciones co', 'co.id = ord.cotizacion_id','left');
                $band_ord++;
            }
            else{ //diferentes de aceptadas
                if($aux==1){
                    $this->db->select('COUNT(co.id) as total, co.fecha_creacion as fecha_creacion_cot');
                    $this->db->from('cotizaciones co');
                    $this->db->join('ordenes ord', 'ord.cotizacion_id = co.id','left'); 
                    /*$this->db->where("co.no_modificacion",1);
                    $this->db->where("co.padre",0);*/
                }
                $band_ord=0;
            }
        }

        /*$this->db->from('ordenes ord');
        $this->db->join('cotizaciones co', 'co.id = ord.cotizacion_id','left');*/
        $this->db->join('clientes cl', 'cl.id = co.cliente_id','left');
        $this->db->join('empleados e', 'e.id = co.vendedor_id','left');
        /*$this->db->join('cotizaciones_has_servicios cs', 'cs.cotizacion_id = co.id','left');
        $this->db->join('servicios s', 's.id = cs.servicio_id','left');*/

        //$this->db->where(array('e.activo'=>1));
        //$this->db->where('c.status!=','0');
        if($this->session->userdata("id_usuario")>1){
            $this->db->where("vendedor_id",$this->session->userdata("id_usuario"));
        }
        //if($da==1){
            if ($params['sucursal']!="") {
                $this->db->where("e.sucursal", $params['sucursal']);
            }
            if ($params['zona']!="") {
                $this->db->where("e.zona", $params['zona']);
            }
            if ($params['vendedor']!="") {
                $this->db->where("co.vendedor_id", $params['vendedor']);
            }
            if ($params['cliente']!=""){
                $this->db->where("cl.id", $params['cliente']);
            }
            if ($params['fecha1']!="" && $params['fecha2']!="") {
                $aux2=0;
                for($i=0; $i<count($params['tiposta']); $i++){
                    $aux2++;
                    if($params['tiposta'][$i]=="2"){
                        $this->db->where("ord.fecha_creacion between '{$params['fecha1']} 00:00:00' AND '{$params['fecha2']} 23:59:59' ");
                        //$this->db->where("ord.status",1);
                        //$this->db->or_where("ord.status",2);
                    }
                    else{
                        if($aux2==1){
                            $this->db->where("co.fecha_creacion between '{$params['fecha1']} 00:00:00' AND '{$params['fecha2']} 23:59:59' ");
                        }
                    }
                }
                //$this->db->where("ord.fecha_creacion between '{$params['fecha1']} 00:00:00' AND '{$params['fecha2']} 23:59:59' ");
            }
            $aux_4=0;
            for($i=0; $i<count($params['tiposta']); $i++){
                $aux_4++;
            }
            $aux_3=0; $band_imp=0;
            for($i=0; $i<count($params['tiposta']); $i++){
                $aux_3++;
                if ($params['tiposta'][$i]!="0"){ 
                    $band_imp++;
                }
                else{
                    //$this->db->where("co.status!=",0);
                    $this->db->where("(co.status=1 or co.status=2 or co.status=3 or co.status=4)");
                }
            }
            if($aux_4==1 && $band_imp>0){
                $this->db->where("co.status", $params['tiposta'][0]);
            }
            else if($aux_4==2 && $band_imp>0){
                $this->db->where("(co.status=".$params['tiposta'][0]." or co.status=".$params['tiposta'][1].")");
            }else if($aux_4>2 && $band_imp>0){
                $this->db->where("(co.status=".$params['tiposta'][0]." or co.status=".$params['tiposta'][1]." or co.status=".$params['tiposta'][2].")");
            }      
            
        //}
        //if($params['tiposta']=="2"){ //aceptada
            //$this->db->where("ord.status",1);
            //$this->db->where("co.status",2);
        //}
        //$this->db->where("cs.status",1);
        //$this->db->order_by("max(co.id)","desc");
        $this->db->group_by("co.id");

        //si hay busqueda con el campo de busqueda
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            //$search.=$sta;
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
            
        }
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        $get=$query->result();
        $total=0;
        foreach($get as $k){
            $total++;
        }
        //log_message('error', 'total: '.$total);
        return $total;
    }

    public function get_select_vendedor($buscar,$sucursal,$zonas) {
        $w1=""; $w2="";
        if($zonas!="")
            $w1=" and zona='$zonas'";
        if($sucursal!="")
            $w2=" and sucursal=$sucursal";

        $strq = "SELECT * FROM empleados WHERE activo=1 and perfil = 1 and nombre like '%$buscar%' $w1 $w2 or activo=1 and perfil=2 and nombre like '%$buscar%' $w1 $w2 order by nombre asc";
        $query = $this->db->query($strq);
        return $query->result();
    }


    function get_reportes_data($fecha1=0,$fecha2=0,$sucursal=0,$zona=0,$cliente=0,$tiposta=0,$vendedor=0){
        /*log_message('error', 'fecha1 MODEL: '.$fecha1);
        log_message('error', 'fecha2: '.$fecha2);
        log_message('error', 'sucursal: '.$sucursal);
        log_message('error', 'zona: '.$zona);
        log_message('error', 'cliente: '.$cliente);
        log_message('error', 'tiposta: '.$tiposta);
        log_message('error', 'vendedor: '.$vendedor);*/
        
        for($i=0; $i<count($tiposta); $i++){
            //log_message('error', 'status model: '.$tiposta[$i]);
            if($tiposta[$i]=="2"){ 
                $tipo="1 as tipo";
            }else{
                $tipo="2 as tipo";
            }
        }
        
        $select="ord.*, IFNULL(ord.id,'0') as id_ord, ord.fecha_creacion as fecha_creacion_ord, co.id, co.total as monto, co.forma_pago, cli.alias, cli.empresa, co.status, em.nombre, co.fecha_creacion as fecha_creacion_cot";
        $select.=", concat(em.clave,co.id,co.fecha_creacion) as clave, em.zona, ".$tipo.", cs.servicio_id, cs.precio, cs.cantidad, cs.descuento, s.nombre as servicio, s2.nombre as servicio2, s3.nombre as servicio3, id_empresa_serv";

        $this->db->select($select);
        $aux=0;
        for($i=0; $i<count($tiposta); $i++){
            $aux++;
           if($tiposta[$i]=="2"){ //aceptada
                $this->db->from('ordenes ord');
                $this->db->join('cotizaciones co', 'co.id = ord.cotizacion_id','left');
            }
            else{
                if($aux==1){
                    $this->db->from('cotizaciones co');
                    $this->db->join('ordenes ord', 'ord.cotizacion_id = co.id','left'); 
                }
            }
        }
        
        $this->db->join('clientes cli', 'cli.id = co.cliente_id','left');
        $this->db->join('empleados em', 'em.id = co.vendedor_id','left');
        $this->db->join('cotizaciones_has_servicios cs', 'cs.cotizacion_id = co.id','left');
        $this->db->join('servicios s', 's.id = cs.servicio_id','left');
        $this->db->join('servicios_ahisa s2', 's2.id = cs.servicio_id','left');
        $this->db->join('servicios_auven s3', 's3.id = cs.servicio_id','left');

        if ($sucursal!=0) {
            $this->db->where("em.sucursal", $sucursal);
        }
        if (strlen($zona)>1) {
            $this->db->where("em.zona", $zona);
        }
        if ($vendedor!=0) {
            $this->db->where("em.id", $vendedor);
        }
        if ($cliente!=0){
            $this->db->where("cli.id", $cliente);
        }
        if ($fecha1!=0 && $fecha2!=0) {
            $aux2=0;
            for($i=0; $i<count($tiposta); $i++){
                $aux2++;
                if($tiposta[$i]=="2"){
                    $this->db->where("ord.fecha_creacion between '{$fecha1} 00:00:00' AND '{$fecha2} 23:59:59' ");
                    //$this->db->where("ord.status",1);
                    //$this->db->or_where("ord.status",2);
                }
                else{
                    if($aux2==1){
                        $this->db->where("co.fecha_creacion between '{$fecha1} 00:00:00' AND '{$fecha2} 23:59:59' ");
                    }
                }
            } 
        }
        $aux_3=0; $band_imp=0;
        //$datos = explode(',', $tiposta);
        //$tiposta = array($tiposta);
        //log_message('error', 'sizeof tiposta: '.sizeof($tiposta));
        for($i=0; $i<count($tiposta); $i++){ 
            $datos = explode(',', $tiposta[$i]);
            //log_message('error', 'datos: '.$datos[$i]);
            //log_message('error', 'datos count: '.count($datos));

            if ($tiposta[$i]!="0"){ //por status y no todas
                //log_message('error', 'status model co.status: '.$tiposta[$i]);
                for($j=0; $j<count($datos); $j++){
                    $aux_3++;
                    //log_message('error', 'aux_3: '.$aux_3);
                    if ($datos[$j]!="0"){ // por status
                        $band_imp++;
                    }
                    if($aux_3==1){
                        //$this->db->where("co.status", $datos[$j]);
                    }else if($aux_3>1){
                        //$this->db->or_where("co.status = ".$datos[$j]."");
                    }
                }
            }
            else{
                //$this->db->where("co.status!=",0);
                $this->db->where("(co.status=1 or co.status=2 or co.status=3 or co.status=4)");
            }
        } 
        if($aux_3==1 && $band_imp>0){
            $this->db->where("co.status", $tiposta[0]);
        }
        else if($aux_3==2 && $band_imp>0){
            $this->db->where("(co.status=".$tiposta[0]." or co.status=".$tiposta[1].")");
        }else if($aux_3>2 && $band_imp>0){
            $this->db->where("(co.status=".$tiposta[0]." or co.status=".$tiposta[1]." or co.status=".$tiposta[2].")");
        }

        //if($tiposta=="2"){ //aceptada
            //$this->db->where("ord.status",1);
            //$this->db->where("co.status",2);
        //}

        $aux_4=0;
        for($i=0; $i<count($tiposta); $i++){
            $aux_4++;
            if($tiposta[$i]=="2"){ //orden
                $this->db->group_by("cs.id");
                $this->db->order_by("ord.id","DESC");
            }else{
                if($aux_4==1){
                    $this->db->group_by("cs.id");
                    $this->db->order_by("co.id","DESC");
                }
            }
        }
        
        $this->db->where("cs.status",1);
        $query=$this->db->get();
        return $query->result();
    }

    function get_reportes_data_zona($fecha1,$fecha2,$sucursal,$zona,$cliente,$tiposta,$vendedor,$where){
        if($tiposta=="2"){ 
            $tipo="1 as tipo";
        }else{
            $tipo="2 as tipo";
        }
        $select="ord.*, IFNULL(ord.id,'0') as id_ord, ord.fecha_creacion as fecha_creacion_ord, co.id, co.total as monto, co.forma_pago, cli.alias, cli.empresa, co.status, em.nombre, co.fecha_creacion as fecha_creacion_cot";
        $select.=", concat(em.clave,co.id,co.fecha_creacion) as clave, em.zona, ".$tipo."";

        $this->db->select("sum(co.total), co.total, co.id, ord.id as idord");
        $this->db->from('ordenes ord');
        $this->db->join('cotizaciones co', 'co.id = ord.cotizacion_id','left');        
        $this->db->join('clientes cli', 'cli.id = co.cliente_id','left');
        $this->db->join('empleados em', 'em.id = co.vendedor_id','left');

        if ($sucursal!=0) {
            $this->db->where("em.sucursal", $sucursal);
        }
        if ($zona!=0) {
            $this->db->where("em.zona", $zona);
        }
        if ($vendedor!=0) {
            $this->db->where("em.id", $vendedor);
        }
        if ($cliente!=0){
            $this->db->where("cli.id", $cliente);
        }
        if ($fecha1!=0 && $fecha2!=0) {
            $this->db->where("ord.fecha_creacion between '{$fecha1} 00:00:00' AND '{$fecha2} 23:59:59' ");
            $this->db->where("ord.status",1);
            $this->db->or_where("ord.status",2);
        }
        if ($tiposta!=0){ 
            $this->db->where("co.status", $tiposta);
        }
        else{
            $this->db->where("co.status!=",0);
        }
        if($tiposta=="2"){ //orden
            $this->db->group_by("ord.id");
            $this->db->order_by("ord.id","DESC");
        }
        $this->db->where($where);
        $query=$this->db->get();
        return $query->result();
    }

    function get_total_sucursal($paras){
        $this->db->select("sum(DISTINCT IF(ser.familia!=12 or ser2.familia!=24 or ser3.familia!=32, (chs.cantidad*chs.precio), 0)) as total_sucursal, sum(DISTINCT IF(chs.descuento>0 && ser.familia!=12 or ser2.familia!=24 or ser3.familia!=32, (chs.precio*chs.descuento/100), 0)) as descuentos,
            s.nombre as sucursal, s.id as id_suc");
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones co', 'co.id = o.cotizacion_id');        
        $this->db->join('cotizaciones_has_servicios chs', 'chs.cotizacion_id = co.id');
        $this->db->join('servicios ser', 'ser.id=chs.servicio_id and (id_empresa_serv=1 or id_empresa_serv=2 or id_empresa_serv=3 or id_empresa_serv=6)','left');
        $this->db->join('servicios_ahisa ser2', 'ser2.id=chs.servicio_id and id_empresa_serv=4','left');
        $this->db->join('servicios_auven ser3', 'ser3.id=chs.servicio_id and id_empresa_serv=5','left');
        $this->db->join('empleados e', 'e.id=co.vendedor_id');
        $this->db->join('sucursales s', 's.id=e.sucursal');
        $this->db->where("chs.status",1);
        //$this->db->where("o.status",1);
        //$this->db->or_where("o.status",2);
        $this->db->where("co.status",2);
        //$this->db->where("ser.familia!=",12);
        $this->db->where("(ser.familia!=12 or ser2.familia!=24 or ser3.familia!=32)");
        //$this->db->group_by("e.sucursal");
        $this->db->group_by("chs.id");
        $this->db->order_by("e.sucursal");
        if($paras["fecha1"]!="" && $paras["fecha2"]!="")
            $this->db->where("o.fecha_creacion between '{$paras['fecha1']} 00:00:00' AND '{$paras['fecha2']} 23:59:59' ");

        $query=$this->db->get();
        return $query->result();
    }

    function get_total_viatico_sucu($paras,$id){
        $this->db->select("sum(DISTINCT IF(ser.familia=12 or ser2.familia=24 or ser3.familia=32, (chs.cantidad*chs.precio), 0))  as total_viat");
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones co', 'co.id = o.cotizacion_id');        
        $this->db->join('cotizaciones_has_servicios chs', 'chs.cotizacion_id = co.id');
        $this->db->join('servicios ser', 'ser.id=chs.servicio_id and (id_empresa_serv=1 or id_empresa_serv=2 or id_empresa_serv=3 or id_empresa_serv=6)','left');
        $this->db->join('servicios_ahisa ser2', 'ser2.id=chs.servicio_id and id_empresa_serv=4','left');
        $this->db->join('servicios_auven ser3', 'ser3.id=chs.servicio_id and id_empresa_serv=5','left');
        $this->db->join('empleados e', 'e.id=co.vendedor_id');
        $this->db->where("chs.status",1);
        //$this->db->where("ser.familia",12);
        $this->db->where("(ser.familia=12 or ser2.familia=24 or ser3.familia=32)");
        //$this->db->where("o.status",1);
        //$this->db->or_where("o.status",2);
        $this->db->where("co.status",2);
        $this->db->where("e.sucursal",$id);
        //$this->db->group_by("e.id");
        if($paras["fecha1"]!="" && $paras["fecha2"]!="")
            $this->db->where("o.fecha_creacion between '{$paras['fecha1']} 00:00:00' AND '{$paras['fecha2']} 23:59:59' ");

        $query=$this->db->get();
        return $query->row();
    }

    function get_count_sucu($paras,$id){
        $this->db->select("COUNT(DISTINCT chs.id) as tot_chs");
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones co', 'co.id = o.cotizacion_id');        
        $this->db->join('cotizaciones_has_servicios chs', 'chs.cotizacion_id = co.id');
        $this->db->join('servicios ser', 'ser.id=chs.servicio_id and (id_empresa_serv=1 or id_empresa_serv=2 or id_empresa_serv=3 or id_empresa_serv=6)','left');
        $this->db->join('servicios_ahisa ser2', 'ser2.id=chs.servicio_id and id_empresa_serv=4','left');
        $this->db->join('servicios_auven ser3', 'ser3.id=chs.servicio_id and id_empresa_serv=5','left');
        $this->db->join('empleados e', 'e.id=co.vendedor_id');
        $this->db->where("chs.status",1);
        //$this->db->where("ser.familia!=",12);
        $this->db->where("(ser.familia!=12 or ser2.familia!=24 or ser3.familia!=32)");
        //$this->db->where("o.status",1);
        //$this->db->or_where("o.status",2);
        $this->db->where("co.status",2);
        $this->db->where("e.sucursal",$id);
        //$this->db->group_by("e.id");
        if($paras["fecha1"]!="" && $paras["fecha2"]!="")
            $this->db->where("o.fecha_creacion between '{$paras['fecha1']} 00:00:00' AND '{$paras['fecha2']} 23:59:59' ");

        $query=$this->db->get();
        return $query->row();
    }

    /*function get_total_sucursal($paras){
        $this->db->select("sum(IF(ser.familia!=12 or ser2.familia!=12 or ser3.familia!=12, (chs.cantidad*chs.precio), 0)) as total_sucursal, sum(DISTINCT IF(chs.descuento>0, (chs.precio*chs.descuento/100), 0)) as descuentos,
            s.nombre as sucursal, s.id as id_suc,
            IFNULL((select sum(DISTINCT IF(ser1.familia=12 or ser22.familia=12 or ser33.familia=12, (chs2.cantidad*chs2.precio), 0))
               FROM ordenes o
                JOIN cotizaciones co ON co.id = o.cotizacion_id
                JOIN cotizaciones_has_servicios chs2 ON chs2.cotizacion_id=co.id
                LEFT JOIN servicios ser1 ON ser1.id=chs2.servicio_id and id_empresa_serv=1 or id_empresa_serv=2 or id_empresa_serv=3 or id_empresa_serv=6
                LEFT JOIN servicios_ahisa ser22 ON ser22.id=chs2.servicio_id and id_empresa_serv=4
                LEFT JOIN servicios_auven ser33 ON ser33.id=chs2.servicio_id and id_empresa_serv=5
                JOIN empleados e ON e.id=co.vendedor_id
                JOIN sucursales s2 ON s2.id=e.sucursal
                WHERE chs2.status = 1
                and (ser1.familia=12 or ser22.familia=12 or ser33.familia=12)
                and o.status=1
                and co.status=2
                and s2.id=s.id
                AND o.fecha_creacion between '".$paras['fecha1']." 00:00:00' AND '".$paras['fecha2']." 23:59:59' 
                GROUP BY e.sucursal 
               ),0) as total_sucursal_viat, '0' as descuentos_viat, s.nombre as sucursal, s.id as id_suc,
               ( sum(IF(ser.familia!=12 or ser2.familia!=12 or ser3.familia!=12, (chs.cantidad*chs.precio), 0)) - sum(DISTINCT IF(chs.descuento>0, (chs.precio*chs.descuento/100), 0))+ 
                IFNULL((select sum(DISTINCT IF(ser11.familia=12 or ser222.familia=12 or ser333.familia=12, (chs2.cantidad*chs2.precio), 0))
                               FROM ordenes o
                                JOIN cotizaciones co ON co.id=o.cotizacion_id
                                JOIN cotizaciones_has_servicios chs2 ON chs2.cotizacion_id=co.id
                                LEFT JOIN servicios ser11 ON ser11.id=chs2.servicio_id and id_empresa_serv=1 or id_empresa_serv=2 or id_empresa_serv=3 or id_empresa_serv=6
                                LEFT JOIN servicios_ahisa ser222 ON ser222.id=chs2.servicio_id and id_empresa_serv=4
                                LEFT JOIN servicios_auven ser333 ON ser333.id=chs2.servicio_id and id_empresa_serv=5
                                JOIN empleados e ON e.id=co.vendedor_id
                                JOIN sucursales s2 ON s2.id=e.sucursal
                                WHERE chs2.status = 1
                                and (ser11.familia=12 or ser222.familia=12 or ser333.familia=12)
                                and o.status=1
                                and co.status=2
                                and s2.id=s.id
                                AND o.fecha_creacion between '".$paras['fecha1']." 00:00:00' AND '".$paras['fecha2']." 23:59:59' 
                                GROUP BY e.sucursal 
                               ), 0)
                ) as tot_fin");
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones co', 'co.id = o.cotizacion_id');        
        $this->db->join('cotizaciones_has_servicios chs', 'chs.cotizacion_id = co.id');
        $this->db->join('servicios ser', 'ser.id=chs.servicio_id and id_empresa_serv=1 or id_empresa_serv=2 or id_empresa_serv=3 or id_empresa_serv=6','left');
        $this->db->join('servicios_ahisa ser2', 'ser2.id=chs.servicio_id and id_empresa_serv=4','left');
        $this->db->join('servicios_auven ser3', 'ser3.id=chs.servicio_id and id_empresa_serv=5','left');
        $this->db->join('empleados e', 'e.id=co.vendedor_id');
        $this->db->join('sucursales s', 's.id=e.sucursal');
        $this->db->where("chs.status",1);
        $this->db->where("o.status",1);
        $this->db->where("co.status",2);
        //$this->db->where("ser.familia!=",12);
        $this->db->where("(ser.familia!=12 or ser3.familia!=12 or ser3.familia!=12)");
        $this->db->group_by("e.sucursal");
        if($paras["fecha1"]!="" && $paras["fecha2"]!="")
            $this->db->where("o.fecha_creacion between '{$paras['fecha1']} 00:00:00' AND '{$paras['fecha2']} 23:59:59' ");

        $query=$this->db->get();
        return $query->result();
    }*/

    /*function get_total_empresa($paras){
        $this->db->select("sum(DISTINCT IF(ser.familia!=12, (chs.cantidad*chs.precio), 0)) as total_sucursal, sum(IF(chs.descuento>0, (chs.precio*chs.descuento/100), 0)) as descuentos,
            p.nombre as empresa,
            IFNULL((select sum(DISTINCT IF(ser.familia=12, (chs2.cantidad*chs2.precio), 0))
               FROM ordenes o
                JOIN cotizaciones co ON co.id = o.cotizacion_id
                JOIN cotizaciones_has_servicios chs2 ON chs2.cotizacion_id=co.id
                JOIN servicios ser ON ser.id=chs2.servicio_id
                JOIN empleados e ON e.id=co.vendedor_id
                JOIN proveedores p2 ON p2.id=e.empresa
                WHERE chs2.status = 1
                AND ser.familia=12
                and p2.id=p.id
                AND o.fecha_creacion between '".$paras['fecha1']." 00:00:00' AND '".$paras['fecha2']." 23:59:59' 
                GROUP BY e.empresa 
               ),0) as total_sucursal_viat, '0' as descuentos_viat, p.nombre as empresa,
               ( sum(DISTINCT IF(ser.familia!=12, (chs.cantidad*chs.precio), 0)) - sum(IF(chs.descuento>0, (chs.precio*chs.descuento/100), 0))+ 
                IFNULL((select sum(DISTINCT IF(ser.familia=12, (chs2.cantidad*chs2.precio), 0))
                               FROM ordenes o
                                JOIN cotizaciones co ON co.id=o.cotizacion_id
                                JOIN cotizaciones_has_servicios chs2 ON chs2.cotizacion_id=co.id
                                JOIN servicios ser ON ser.id=chs2.servicio_id
                                JOIN empleados e ON e.id=co.vendedor_id
                                JOIN proveedores p2 ON p2.id=e.empresa
                                WHERE chs2.status = 1
                                AND ser.familia = 12
                                and p2.id=p.id
                                AND o.fecha_creacion between '".$paras['fecha1']." 00:00:00' AND '".$paras['fecha2']." 23:59:59' 
                                GROUP BY e.empresa 
                               ), 0)
                ) as tot_fin");
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones co', 'co.id = o.cotizacion_id');        
        $this->db->join('cotizaciones_has_servicios chs', 'chs.cotizacion_id = co.id');
        $this->db->join('servicios ser', 'ser.id=chs.servicio_id');
        $this->db->join('empleados e', 'e.id=co.vendedor_id');
        $this->db->join('proveedores p', 'p.id=e.empresa');
        $this->db->where("chs.status",1);
        //$this->db->where("ser.familia!=",12);
        $this->db->group_by("e.empresa");
        if($paras["fecha1"]!="" && $paras["fecha2"]!="")
            $this->db->where("o.fecha_creacion between '{$paras['fecha1']} 00:00:00' AND '{$paras['fecha2']} 23:59:59' ");

        $query=$this->db->get();
        return $query->result();
    }*/

    function get_total_empresa($paras){
        $this->db->select("sum(DISTINCT IF(ser.familia!=12 or ser2.familia!=24 or ser3.familia!=32, (chs.cantidad*chs.precio), 0)) as total_sucursal, sum(DISTINCT IF(chs.descuento>0 && ser.familia!=12 or ser2.familia!=24 or ser3.familia!=32, (chs.precio*chs.descuento/100), 0)) as descuentos,
            p.nombre as empresa, e.empresa as id_empresa");
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones co', 'co.id = o.cotizacion_id');        
        $this->db->join('cotizaciones_has_servicios chs', 'chs.cotizacion_id = co.id');
        $this->db->join('servicios ser', 'ser.id=chs.servicio_id and (id_empresa_serv=1 or id_empresa_serv=2 or id_empresa_serv=3 or id_empresa_serv=6)','left');
        $this->db->join('servicios_ahisa ser2', 'ser2.id=chs.servicio_id and id_empresa_serv=4','left');
        $this->db->join('servicios_auven ser3', 'ser3.id=chs.servicio_id and id_empresa_serv=5','left');
        $this->db->join('empleados e', 'e.id=co.vendedor_id');
        $this->db->join('proveedores p', 'p.id=e.empresa');
        $this->db->where("chs.status",1);
        //$this->db->where("ser.familia!=",12);
        $this->db->where("(ser.familia!=12 or ser2.familia!=24 or ser3.familia!=32)");
        //$this->db->where("o.status",1);
        //$this->db->or_where("o.status",2);
        $this->db->where("co.status",2);
        //$this->db->group_by("e.empresa");
        $this->db->group_by("chs.id");
        $this->db->order_by("e.empresa");
        if($paras["fecha1"]!="" && $paras["fecha2"]!="")
            $this->db->where("o.fecha_creacion between '{$paras['fecha1']} 00:00:00' AND '{$paras['fecha2']} 23:59:59' ");

        $query=$this->db->get();
        return $query->result();
    }

    function get_total_viatico_empre($paras,$id){
        $this->db->select("sum(DISTINCT IF(ser.familia=12 or ser2.familia=24 or ser3.familia=32, (chs.cantidad*chs.precio), 0))  as total_viat");
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones co', 'co.id = o.cotizacion_id');        
        $this->db->join('cotizaciones_has_servicios chs', 'chs.cotizacion_id = co.id');
        $this->db->join('servicios ser', 'ser.id=chs.servicio_id and (id_empresa_serv=1 or id_empresa_serv=2 or id_empresa_serv=3 or id_empresa_serv=6)','left');
        $this->db->join('servicios_ahisa ser2', 'ser2.id=chs.servicio_id and id_empresa_serv=4','left');
        $this->db->join('servicios_auven ser3', 'ser3.id=chs.servicio_id and id_empresa_serv=5','left');
        $this->db->join('empleados e', 'e.id=co.vendedor_id');
        $this->db->where("chs.status",1);
        //$this->db->where("ser.familia",12);
        $this->db->where("(ser.familia=12 or ser2.familia=24 or ser3.familia=32)");
        //$this->db->where("o.status",1);
        //$this->db->or_where("o.status",2);
        $this->db->where("co.status",2);
        $this->db->where("e.empresa",$id);
        //$this->db->group_by("e.id");
        if($paras["fecha1"]!="" && $paras["fecha2"]!="")
            $this->db->where("o.fecha_creacion between '{$paras['fecha1']} 00:00:00' AND '{$paras['fecha2']} 23:59:59' ");

        $query=$this->db->get();
        return $query->row();
    }

    function get_count_empre($paras,$id){
        $this->db->select("COUNT(DISTINCT chs.id) as tot_chs");
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones co', 'co.id = o.cotizacion_id');        
        $this->db->join('cotizaciones_has_servicios chs', 'chs.cotizacion_id = co.id');
        $this->db->join('servicios ser', 'ser.id=chs.servicio_id and (id_empresa_serv=1 or id_empresa_serv=2 or id_empresa_serv=3 or id_empresa_serv=6)','left');
        $this->db->join('servicios_ahisa ser2', 'ser2.id=chs.servicio_id and id_empresa_serv=4','left');
        $this->db->join('servicios_auven ser3', 'ser3.id=chs.servicio_id and id_empresa_serv=5','left');
        $this->db->join('empleados e', 'e.id=co.vendedor_id');
        $this->db->where("chs.status",1);
        //$this->db->where("ser.familia!=",12);
        $this->db->where("(ser.familia!=12 or ser2.familia!=24 or ser3.familia!=32)");
        //$this->db->where("o.status",1);
        //$this->db->or_where("o.status",2);
        $this->db->where("co.status",2);
        $this->db->where("e.empresa",$id);
        //$this->db->group_by("e.id");
        if($paras["fecha1"]!="" && $paras["fecha2"]!="")
            $this->db->where("o.fecha_creacion between '{$paras['fecha1']} 00:00:00' AND '{$paras['fecha2']} 23:59:59' ");

        $query=$this->db->get();
        return $query->row();
    }

    function get_total_vende($paras){
        $this->db->select("sum(DISTINCT IF(ser.familia!=12 or ser2.familia!=24 or ser3.familia!=32, (chs.cantidad*chs.precio), 0)) as total_sucursal, sum(DISTINCT IF(chs.descuento>0 && ser.familia!=12 or ser2.familia!=24 or ser3.familia!=32, (chs.precio*chs.descuento/100), 0)) as descuentos,
            e.nombre as empleado, e.id as id_emplea, e.zona");
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones co', 'co.id = o.cotizacion_id');        
        $this->db->join('cotizaciones_has_servicios chs', 'chs.cotizacion_id = co.id');
        $this->db->join('servicios ser', 'ser.id=chs.servicio_id and (id_empresa_serv=1 or id_empresa_serv=2 or id_empresa_serv=3 or id_empresa_serv=6)','left');
        $this->db->join('servicios_ahisa ser2', 'ser2.id=chs.servicio_id and id_empresa_serv=4','left');
        $this->db->join('servicios_auven ser3', 'ser3.id=chs.servicio_id and id_empresa_serv=5','left');
        $this->db->join('empleados e', 'e.id=co.vendedor_id');
        $this->db->where("chs.status",1);
        //$this->db->where("o.status",1);
        //$this->db->or_where("o.status",2);
        $this->db->where("co.status",2);
        //$this->db->where("ser.familia!=",12);
        $this->db->where("(ser.familia!=12 or ser2.familia!=24 or ser3.familia!=32)");
        //$this->db->group_by("e.id");
        $this->db->group_by("chs.id");
        $this->db->order_by("e.id");
        if($paras["fecha1"]!="" && $paras["fecha2"]!="")
            $this->db->where("o.fecha_creacion between '{$paras['fecha1']} 00:00:00' AND '{$paras['fecha2']} 23:59:59' ");

        $query=$this->db->get();
        return $query->result();
    }

    function get_count_vende($paras,$id){
        $this->db->select("COUNT(DISTINCT chs.id) as tot_chs");
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones co', 'co.id = o.cotizacion_id');        
        $this->db->join('cotizaciones_has_servicios chs', 'chs.cotizacion_id = co.id');
        $this->db->join('servicios ser', 'ser.id=chs.servicio_id and (id_empresa_serv=1 or id_empresa_serv=2 or id_empresa_serv=3 or id_empresa_serv=6)','left');
        $this->db->join('servicios_ahisa ser2', 'ser2.id=chs.servicio_id and id_empresa_serv=4','left');
        $this->db->join('servicios_auven ser3', 'ser3.id=chs.servicio_id and id_empresa_serv=5','left');
        $this->db->join('empleados e', 'e.id=co.vendedor_id');
        $this->db->where("chs.status",1);
        //$this->db->where("o.status",1);
        //$this->db->or_where("o.status",2);
        $this->db->where("co.status",2);
        //$this->db->where("ser.familia!=",12);
        $this->db->where("(ser.familia!=12 or ser2.familia!=24 or ser3.familia!=32)");
        $this->db->where("e.id",$id);
        //$this->db->group_by("e.id");
        if($paras["fecha1"]!="" && $paras["fecha2"]!="")
            $this->db->where("o.fecha_creacion between '{$paras['fecha1']} 00:00:00' AND '{$paras['fecha2']} 23:59:59' ");

        $query=$this->db->get();
        return $query->row();
    }

    function get_total_viatico_vende($paras,$id){
        $this->db->select("sum(DISTINCT IF(ser.familia=12 or ser2.familia=24 or ser3.familia=32, (chs.cantidad*chs.precio), 0))  as total_viat, e.nombre as empleado");
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones co', 'co.id = o.cotizacion_id');        
        $this->db->join('cotizaciones_has_servicios chs', 'chs.cotizacion_id = co.id');
        $this->db->join('servicios ser', 'ser.id=chs.servicio_id and (id_empresa_serv=1 or id_empresa_serv=2 or id_empresa_serv=3 or id_empresa_serv=6)','left');
        $this->db->join('servicios_ahisa ser2', 'ser2.id=chs.servicio_id and id_empresa_serv=4','left');
        $this->db->join('servicios_auven ser3', 'ser3.id=chs.servicio_id and id_empresa_serv=5','left');
        $this->db->join('empleados e', 'e.id=co.vendedor_id');
        $this->db->where("chs.status",1);
        //$this->db->where("o.status",1);
        //$this->db->or_where("o.status",2);
        $this->db->where("co.status",2);
        //$this->db->where("ser.familia",12);
        $this->db->where("(ser.familia=12 or ser2.familia=24 or ser3.familia=32)");
        $this->db->where("e.id",$id);
        //$this->db->group_by("e.id");
        if($paras["fecha1"]!="" && $paras["fecha2"]!="")
            $this->db->where("o.fecha_creacion between '{$paras['fecha1']} 00:00:00' AND '{$paras['fecha2']} 23:59:59' ");

        $query=$this->db->get();
        return $query->row();
    }

    function getVentasGralMes($paras){
        //$this->db->set("lc_time_names = 'es_ES'");
        $this->db->select("MONTH(o.fecha_creacion) AS mes, sum(DISTINCT (chs.cantidad*chs.precio)) as total_sucursal, sum(DISTINCT IF(chs.descuento>0, (chs.precio*chs.descuento/100), 0)) as descuentos");
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones co', 'co.id = o.cotizacion_id');        
        $this->db->join('cotizaciones_has_servicios chs', 'chs.cotizacion_id = co.id');
        $this->db->where("chs.status",1);
        //$this->db->where("o.status",1);
        //$this->db->or_where("o.status",2);
        $this->db->where("co.status",2);
        //$this->db->group_by("chs.id");
        $this->db->group_by("mes");
        $this->db->order_by("mes","asc");

        if($paras["fecha1"]!="" && $paras["fecha2"]!="")
            $this->db->where("o.fecha_creacion between '{$paras['fecha1']} 00:00:00' AND '{$paras['fecha2']} 23:59:59' "); 

        $query=$this->db->get();
        return $query->result();
    }

    function getVentasGral($paras){
        //$this->db->set("lc_time_names = 'es_ES'");
        $this->db->select("MONTH(o.fecha_creacion) AS mes, sum(DISTINCT (chs.cantidad*chs.precio)) as total_sucursal, sum(DISTINCT IF(chs.descuento>0, (chs.precio*chs.descuento/100), 0)) as descuentos, o.fecha_creacion");
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones co', 'co.id = o.cotizacion_id');        
        $this->db->join('cotizaciones_has_servicios chs', 'chs.cotizacion_id = co.id');
        $this->db->where("chs.status",1);
        //$this->db->where("o.status",1);
        //$this->db->or_where("o.status",2);
        $this->db->where("co.status",2);
        $this->db->group_by("chs.id");
        //$this->db->group_by("mes");
        $this->db->order_by("mes","asc");

        if($paras["fecha1"]!="" && $paras["fecha2"]!="")
            $this->db->where("o.fecha_creacion between '{$paras['fecha1']} 00:00:00' AND '{$paras['fecha2']} 23:59:59' "); 

        $query=$this->db->get();
        return $query->result();
    }

    function get_count_total_mes($paras,$mes,$year){
        $this->db->select("COUNT(DISTINCT chs.id) as tot_chs");
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones co', 'co.id = o.cotizacion_id');        
        $this->db->join('cotizaciones_has_servicios chs', 'chs.cotizacion_id = co.id');

        $this->db->where("chs.status",1);
        //$this->db->where("o.status",1);
        //$this->db->or_where("o.status",2);
        $this->db->where("co.status",2);
        $this->db->where("MONTH(o.fecha_creacion)",$mes);
        //$this->db->where("YEAR(o.fecha_creacion)",$year);

        if($paras["fecha1"]!="" && $paras["fecha2"]!="")
            $this->db->where("o.fecha_creacion between '{$paras['fecha1']} 00:00:00' AND '{$paras['fecha2']} 23:59:59' ");

        $query=$this->db->get();
        return $query->row();
    }

}   

?>