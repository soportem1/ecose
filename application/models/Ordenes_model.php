<?php

class Ordenes_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }
    
    //funcion generica de obtencion de istado de un catalogo
    public function getCatalogo($catalogo) {
        $sql = "SELECT * FROM $catalogo";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    public function getOrden($id){
        //$this->db->set("lc_time_names = 'es_ES'");
        //$this->db->select("SET o.fecha_creacion = 'es_ES'");
        $this->db->select("o.*, MONTHNAME(o.fecha_creacion) as mes, MONTH(o.fecha_creacion) as mesn, YEAR(o.fecha_creacion) as anio, DAY(o.fecha_creacion) as dia, cs.hora_inicio, hora_fin, fecha_inicio, fecha_fin, e.nombre as vendedor, c.observaciones, IFNULL(pc.nombre,'') as contacto, cli.empresa as cliente, c.id_empresa");
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones c', 'c.id = o.cotizacion_id');
        $this->db->join('empleados e', 'e.id = c.vendedor_id');
        $this->db->join('cotizaciones_has_servicios cs', 'cs.cotizacion_id=o.cotizacion_id');
        $this->db->join('clientes cli', 'cli.id=c.cliente_id');
        $this->db->join('personas_contacto pc', 'pc.id=c.contacto_id','left');
        $this->db->where("o.id",$id);
        $query=$this->db->get();
        return $query->row();
    }
    
    public function getCliente($id){
        $this->db->select("*");
        $this->db->from('clientes c');
        $this->db->join('personas_contacto p', 'c.id = p.cliente_id');
        $this->db->where("c.id",$id);
        $this->db->where("c.estatus",1);
        $query=$this->db->get();
        return $query->row();
    }
    
    public function updateOrden($id,$data){
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('ordenes');
    }
    
    public function getOrdenes($params){
        $sta=$params["status"];
        $id_usuario=$this->session->userdata("id_usuario");
        $columns = array( 
            0 => 'o.id',
            1 => 'e.usuario as nomVendedor',
            2 => 'o.cotizacion_id',
            3 => 'o.fecha_creacion',
            4 => 'alias',
            //5 => "GROUP_CONCAT(IFNULL(p.nombre, ''), ' ', IFNULL(p2.nombre, ''),' ',IFNULL(p3.nombre, ''),' ' SEPARATOR '<br>') as empresa",
            6 => 'o.status',
            7 => 'c.cliente_id',
            8 => 'cs.hora_inicio',
            9 => 'cs.hora_fin',
            10 => 'cs.fecha_inicio',
            11 => 'cs.fecha_fin',
            12 => 'o.id_tecnico',
            13 => 'cs.comentarios',
            14 => 'c.id_empresa',
            15 => 'max(o.id) as id_orden',
            16 => 'file_repor',
            17 => 'carpeta',
            18 => 'carpeta_prin'
        );
        $columns2 = array( 
            0 => 'o.id',
            1 => 'e.usuario',
            2 => 'o.cotizacion_id',
            3 => 'o.fecha_creacion',
            4 => 'alias',
            //5 => "GROUP_CONCAT(IFNULL(p.nombre, ''), ' ', IFNULL(p2.nombre, ''),' ',IFNULL(p3.nombre, ''),' ' SEPARATOR '<br>')",
            6 => 'o.status',
            7 => 'c.cliente_id',
            8 => 'cs.hora_inicio',
            9 => 'cs.hora_fin',
            10 => 'cs.fecha_inicio',
            11 => 'cs.fecha_fin',
            12 => 'o.id_tecnico',
            13 => 'cs.comentarios',
            14 => 'c.id_empresa',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }

        /*if(!$this->session->userdata("administrador")){
            $columns[2]='CONCAT(s.nombre,"<br>",s.descripcion) as nombre';
            //$where = "and id_usuario = $id_usuario"; 
        }
        $select = "o.id,cl.alias,o.fecha_creacion,o.cotizacion_id,o.status, e.usuario as nomVendedor, o.cotizacion_id as nombre, IFNULL(cs.fecha_inicio,'0') as fecha_inicio, cs.hora_inicio, cs.fecha_fin, cs.hora_fin, cs.comentarios, c.id_empresa, ";
        $select.="o.cotizacion_id as nombre, c.cliente_id, , max(o.id) as id_orden, o.id_tecnico";*/
      
        $this->db->select($select);
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones c', 'c.id = o.cotizacion_id');
        $this->db->join('clientes cl', 'cl.id = c.cliente_id','left');
        $this->db->join('empleados e', 'e.id = c.vendedor_id','left');
        //$this->db->join('empleados e2', 'e2.id = o.id_tecnico','left');
        $this->db->join('cotizaciones_has_servicios cs', 'cs.cotizacion_id = c.id and cs.status!=0');

        /*$this->db->join("servicios s","s.id=cs.servicio_id and c.id_empresa!=4 and c.id_empresa!=5","left");
        $this->db->join("servicios_ahisa s2","s2.id=cs.servicio_id and c.id_empresa=4","left");
        $this->db->join("servicios_auven s3","s3.id=cs.servicio_id and c.id_empresa=5","left"); 
        $this->db->join("proveedores p","p.id=s.proveedor","left");
        $this->db->join("proveedores p2","p2.id=s2.proveedor","left");
        $this->db->join("proveedores p3","p3.id=s3.proveedor","left");*/
        
        if($this->session->userdata("perfil")!='1' && $this->session->userdata("perfil")!='3'){ //admin y programacion
            //$this->db->join('cotizaciones_has_servicios cs', 'cs.cotizacion_id = c.id');
            //$this->db->join('servicios s', 's.id = cs.servicio_id');
            //$this->db->where("s.proveedor",$this->session->userdata("empresa"));

            //$id_emp = $this->session->userdata("empresa");
            $id_emp = $params["emp"];
            if($id_emp==1 || $id_emp==2 || $id_emp==3 || $id_emp==6){
                $this->db->join('servicios s', 's.id = cs.servicio_id');     
                //$this->db->where("(cs.id_empresa_serv=1 or cs.id_empresa_serv=2 or cs.id_empresa_serv=3 or cs.id_empresa_serv=6)");
                //$this->db->where("s.proveedor",$this->session->userdata("empresa"));
            }
            else{
                if($id_emp==4){
                    $this->db->join('servicios_ahisa s', 's.id = cs.servicio_id');
                }else if($id_emp==5){
                    $this->db->join('servicios_auven s', 's.id = cs.servicio_id');
                }
                //$this->db->where("s.proveedor",$this->session->userdata("empresa"));
            }
            //$this->db->where('s.status!=', 0);
        }
        //$this->db->join('servicios s', 's.id = cs.servicio_id');    
        //$this->db->join("proveedores p","p.id=s.proveedor","left");
        //$this->db->join("proveedores p","p.id=s.proveedor");
        if($params["emp"]!="0" /*&& $this->session->userdata("perfil")!='1'*/ /* && $this->session->userdata("perfil")!='3'*/){ //admin y programacion
            $this->db->where('c.id_empresa',$params["emp"]);
        }

        /*if($id_usuario>1 && $this->session->userdata("departamento")!="DEPARTAMENTO TECNICO"){  //descomentar despues de que asignen a c/usu
            $this->db->where("o.id_usuario",$this->session->userdata("id_usuario"));
        }*/
        /*if($id_usuario>1 && $this->session->userdata("departamento")!="DEPARTAMENTO TECNICO"){  //descomentar despues de que asignen a c/usu
            $this->db->where("o.id_usuario",$this->session->userdata("id_usuario"));
        }*/
        if($this->session->userdata("perfil")!='1' && $this->session->userdata("perfil")=='2'){ //!=super admin e igual a vendedor
            //$this->db->where("o.id_usuario",$this->session->userdata("id_usuario")); 
            $this->db->where("c.vendedor_id",$this->session->userdata("id_usuario")); 
        }

        if($sta!=0 && $sta!=4){
            $this->db->where("o.status",$sta); //por estatus modificada, aceptada, terminada
            if($sta==1){
                $this->db->where("cs.fecha_inicio IS NULL");  //pendiente
            }
        }else if($sta==0){ //todas
            $this->db->where("o.status!=","0");
        }
        if($sta==4){ //en proceso
           $this->db->where("o.status","1"); 
           $this->db->where("cs.fecha_inicio!=",""); 
        }
        if($params["fechai"]!="" && $params["fechaf"]!=""){
            $this->db->where('o.fecha_creacion BETWEEN '.'"'.$params["fechai"].' 00:00:00"'.' AND '.'"'.$params["fechaf"].' 23:59:59"');
        }else if($params["fechai"]!="" && $params["fechaf"]==""){
            $this->db->where('o.fecha_creacion >= '.'"'.$params["fechai"].' 00:00:00"');
        }
        //$this->db->where('cl.status!=',0);
        $this->db->where('c.status!=',0);
        $this->db->group_by('c.id');
        /*$this->db->group_by('p.id');
        $this->db->group_by('p2.id');
        $this->db->group_by('p3.id');*/

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns2 as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();   
        }

        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        
        $query=$this->db->get();
        return $query;
    }

    public function getTotalOrdenes($params){
        $sta=$params["status"];
        $columns = array( 
            0 => 'o.id',
            1 => 'e.usuario',
            2 => 'o.cotizacion_id',
            3 => 'o.fecha_creacion',
            4 => 'alias',
            //5 => "GROUP_CONCAT(IFNULL(p.nombre, ''), ' ', IFNULL(p2.nombre, ''),' ',IFNULL(p3.nombre, ''),' ' SEPARATOR '<br>')",
            6 => 'o.status',
            7 => 'c.cliente_id',
            8 => 'cs.hora_inicio',
            9 => 'cs.hora_fin',
            10 => 'cs.fecha_inicio',
            11 => 'cs.fecha_fin',
            12 => 'o.id_tecnico',
            13 => 'cs.comentarios',
            14 => 'c.id_empresa'
        );
        $id_usuario=$this->session->userdata("id_usuario");
        /*$select = "o.id,cl.alias,o.fecha_creacion,o.cotizacion_id,o.status, e.usuario as nomVendedor, o.cotizacion_id as nombre, IFNULL(cs.fecha_inicio,'0') as fecha_inicio, cs.hora_inicio, cs.fecha_fin, cs.hora_fin, cs.comentarios, ";
        
        if(!$this->session->userdata("administrador") && $this->session->userdata("perfil")!='3'){
            $select.="CONCAT(s.nombre,'<br>',s.descripcion) as nombre,cs.id as servicio_id,IFNULL(cs.fecha_inicio,'0') as fecha_inicio,fecha_fin,hora_inicio,hora_fin,cs.comentarios, c.cliente_id, , max(o.id) as id_orden,";
        }
        else{
            $select.="o.cotizacion_id as nombre, c.cliente_id, , max(o.id) as id_orden, e2.correo as mail_tec, ";
            $select.="";
        }*/
        $this->db->select("COUNT(1) as total");
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones c', 'c.id = o.cotizacion_id');
        $this->db->join('clientes cl', 'cl.id = c.cliente_id','left');
        $this->db->join('empleados e', 'e.id = c.vendedor_id','left');
        //$this->db->join('empleados e2', 'e2.id = o.id_tecnico','left');
        $this->db->join('cotizaciones_has_servicios cs', 'cs.cotizacion_id = c.id and cs.status!=0');

        /*$this->db->join("servicios s","s.id=cs.servicio_id and c.id_empresa!=4 and c.id_empresa!=5","left");
        $this->db->join("servicios_ahisa s2","s2.id=cs.servicio_id and c.id_empresa=4","left");
        $this->db->join("servicios_auven s3","s3.id=cs.servicio_id and c.id_empresa=5","left"); 
        $this->db->join("proveedores p","p.id=s.proveedor","left");
        $this->db->join("proveedores p2","p2.id=s2.proveedor","left");
        $this->db->join("proveedores p3","p3.id=s3.proveedor","left");*/
        
        if($this->session->userdata("perfil")!='1' && $this->session->userdata("perfil")!='3'){ //admin y programacion
            //$this->db->join('cotizaciones_has_servicios cs', 'cs.cotizacion_id = c.id');
            //$this->db->join('servicios s', 's.id = cs.servicio_id');
            //$this->db->where("s.proveedor",$this->session->userdata("empresa"));

            //$id_emp = $this->session->userdata("empresa");
            $id_emp = $params["emp"];
            if($id_emp==1 || $id_emp==2 || $id_emp==3 || $id_emp==6){
                $this->db->join('servicios s', 's.id = cs.servicio_id');     
                //$this->db->where("(cs.id_empresa_serv=1 or cs.id_empresa_serv=2 or cs.id_empresa_serv=3 or cs.id_empresa_serv=6)");
                //$this->db->where("s.proveedor",$this->session->userdata("empresa"));
            }
            else{
                if($id_emp==4){
                    $this->db->join('servicios_ahisa s', 's.id = cs.servicio_id');
                }else if($id_emp==5){
                    $this->db->join('servicios_auven s', 's.id = cs.servicio_id');
                }
                //$this->db->where("s.proveedor",$this->session->userdata("empresa"));
            }
            //$this->db->where('s.status!=', 0);
        }
        //$this->db->join('servicios s', 's.id = cs.servicio_id');    
        //$this->db->join("proveedores p","p.id=s.proveedor","left");
        //$this->db->join("proveedores p","p.id=s.proveedor");
        if($params["emp"]!="0" /*&& $this->session->userdata("perfil")!='1'*/ /* && $this->session->userdata("perfil")!='3'*/){ //admin y programacion
            $this->db->where('c.id_empresa',$params["emp"]);
        }

        /*if($id_usuario>1 && $this->session->userdata("departamento")!="DEPARTAMENTO TECNICO"){  //descomentar despues de que asignen a c/usu
            $this->db->where("o.id_usuario",$this->session->userdata("id_usuario"));
        }*/
        /*if($id_usuario>1 && $this->session->userdata("departamento")!="DEPARTAMENTO TECNICO"){  //descomentar despues de que asignen a c/usu
            $this->db->where("o.id_usuario",$this->session->userdata("id_usuario"));
        }*/
        if($this->session->userdata("perfil")!='1' && $this->session->userdata("perfil")=='2'){ //!=super admin e igual a vendedor
            //$this->db->where("o.id_usuario",$this->session->userdata("id_usuario")); 
            $this->db->where("c.vendedor_id",$this->session->userdata("id_usuario")); 
        }

        if($sta!=0 && $sta!=4){
            $this->db->where("o.status",$sta); //por estatus modificada, aceptada, terminada
            if($sta==1){
                $this->db->where("cs.fecha_inicio IS NULL");  //pendiente
            }
        }else if($sta==0){ //todas
            $this->db->where("o.status!=","0");
        }
        if($sta==4){ //en proceso
           $this->db->where("o.status","1"); 
           $this->db->where("cs.fecha_inicio!=",""); 
        }
        if($params["fechai"]!="" && $params["fechaf"]!=""){
            $this->db->where('o.fecha_creacion BETWEEN '.'"'.$params["fechai"].' 00:00:00"'.' AND '.'"'.$params["fechaf"].' 23:59:59"');
        }else if($params["fechai"]!="" && $params["fechaf"]==""){
            $this->db->where('o.fecha_creacion >= '.'"'.$params["fechai"].' 00:00:00"');
        }

        //$this->db->where('cl.status!=',0);
        $this->db->where('c.status!=',0);

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        } 
        $this->db->group_by('c.id');
        $query=$this->db->get();
        $get=$query->result();
        $total=0;
        foreach($get as $k){
            $total++;
        }
        return $total;
    }

    public function getTotalOrdenesEstatus($params,$sta){
        $id_usuario=$this->session->userdata("id_usuario");
        $columns = array( 
            0 => 'o.id',
            1 => 'o.cotizacion_id',
            2 => 'o.cotizacion_id',
            3 => 'o.fecha_creacion',
            4 => 'alias',
            5 => 'empleados.usuario',
            6 => 'o.status',
            7 => 'o.id',
            8=> 'c.cliente_id',
            //9=> 'p.nombre'
        );
        $this->db->select("o.id");
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones c', 'c.id = o.cotizacion_id');
        $this->db->join('clientes cl', 'cl.id = c.cliente_id','left');
        $this->db->join('empleados', 'empleados.id = c.vendedor_id','left');
        $this->db->join('cotizaciones_has_servicios cs', 'cs.cotizacion_id = c.id');
        //if(!$this->session->userdata("administrador")){
        if($this->session->userdata("perfil")!='1' && $this->session->userdata("perfil")!='3'){ //admin y programacion
            //$this->db->join('cotizaciones_has_servicios cs', 'cs.cotizacion_id = c.id');
            //$this->db->join('servicios s', 's.id = cs.servicio_id');
            //$this->db->where("s.proveedor",$this->session->userdata("empresa"));
            //$this->db->where("o.status",$sta);
            //$this->db->where("o.status!=",0);
            //$this->db->where("o.id_usuario",$id_usuario);

            //$id_emp = $this->session->userdata("empresa");
            $id_emp = $params["emp"];
            if($id_emp==1 || $id_emp==2 || $id_emp==3 || $id_emp==6){
                $this->db->join('servicios s', 's.id = cs.servicio_id');
                
                //$this->db->where("(cs.id_empresa_serv=1 or cs.id_empresa_serv=2 or cs.id_empresa_serv=3 or cs.id_empresa_serv=6)");
                //$this->db->where("s.proveedor",$this->session->userdata("empresa")); //comentado porque ecose addmin puede crear varios servicios de otras empresas
            }
            else{
                if($id_emp==4){
                    $this->db->join('servicios_ahisa s', 's.id = cs.servicio_id');
                }else if($id_emp==5){
                    $this->db->join('servicios_auven s', 's.id = cs.servicio_id');
                }
                //$this->db->where("cs.id_empresa_serv",$this->session->userdata("empresa"));
                //$this->db->where("s.proveedor",$this->session->userdata("empresa")); 
            }
            if($params["emp"]==0){
                /*$this->db->join('servicios s', 's.id = cs.servicio_id','left');
                $this->db->join('servicios_ahisa s2', 's2.id = cs.servicio_id','left');
                $this->db->join('servicios_auven s3', 's3.id = cs.servicio_id','left');*/
            }

        }
        if($this->session->userdata("perfil")=='1' || $this->session->userdata("perfil")=='3'){ //admin y programacion
            $id_empses = $this->session->userdata("empresa");
            $id_emp = $params["emp"];
            if($id_emp==1 || $id_emp==2 || $id_emp==3 || $id_emp==6){
                //$this->db->join('servicios s', 's.id = cs.servicio_id');
                
                //$this->db->where("(cs.id_empresa_serv=1 or cs.id_empresa_serv=2 or cs.id_empresa_serv=3 or cs.id_empresa_serv=6)");
                if($this->session->userdata("perfil")=='3'){ //programacion
                    //$this->db->where("s.proveedor",$this->session->userdata("empresa"));
                }
            }
            else{
                if($id_emp==4){
                    //$this->db->join('servicios_ahisa s', 's.id = cs.servicio_id');
                }else if($id_emp==5){
                    //$this->db->join('servicios_auven s', 's.id = cs.servicio_id');
                }
                if($this->session->userdata("perfil")=='3'){ //programacion
                    //$this->db->where("s.proveedor",$this->session->userdata("empresa"));
                }
            }
            if($params["emp"]==0){
                /*$this->db->join('servicios s', 's.id = cs.servicio_id','left');
                $this->db->join('servicios_ahisa s2', 's2.id = cs.servicio_id','left');
                $this->db->join('servicios_auven s3', 's3.id = cs.servicio_id','left');*/
            }
            $this->db->where('cs.status!=', 0);
        }
        /*if($id_usuario>1 && $this->session->userdata("departamento")!="DEPARTAMENTO TECNICO"){ //descomentar despues de que asignen a c/usu
            $this->db->where("o.id_usuario",$this->session->userdata("id_usuario"));
        }*/

        //$this->db->join('servicios s', 's.id = cs.servicio_id');
        
        //$this->db->join("proveedores p","p.id=s.proveedor");
        
        if($params["emp"]!="0" /* && $this->session->userdata("perfil")!='1' */ /*&& $this->session->userdata("perfil")!='3'*/){
            //$this->db->where('e.empresa',$params["emp"]);
            $this->db->where('c.id_empresa',$params["emp"]);
        }
        if($sta!=0 && $sta!=4){
            $this->db->where("o.status",$sta); //por estatus modificada, aceptada, terminada
            if($sta==1){
                $this->db->where("cs.fecha_inicio IS NULL");  //pendiente
            }
        }else if($sta==0){ //todas
            $this->db->where("o.status!=","0");
        }
        if($sta==4){ //en proceso
           $this->db->where("o.status","1"); 
           $this->db->where("cs.fecha_inicio!=",""); 
        }
        if($params["fechai"]!="" && $params["fechaf"]!=""){
            $this->db->where('o.fecha_creacion BETWEEN '.'"'.$params["fechai"].' 00:00:00"'.' AND '.'"'.$params["fechaf"].' 23:59:59"');
        }
        $this->db->where('c.status!=',0);

        if($this->session->userdata("perfil")!='1' && $this->session->userdata("perfil")!='3'){
            //$this->db->where("o.id_usuario",$this->session->userdata("id_usuario"));
            $this->db->where("c.vendedor_id",$this->session->userdata("id_usuario")); 
        }
        if($this->session->userdata("id_usuario")=='1' || $this->session->userdata("id_usuario")=='12' || $this->session->userdata("id_usuario")=="15"){
            /*if($this->session->userdata("empresa")==1 || $this->session->userdata("empresa")==2 || $this->session->userdata("empresa")==3 || $this->session->userdata("empresa")==6){
                $this->db->where("(c.id_empresa=1 or c.id_empresa=2 or c.id_empresa=3 or c.id_empresa=6)"); 
            }else if($this->session->userdata("empresa")==4){
                $this->db->where("c.id_empresa",4); 
            }else if($this->session->userdata("empresa")==5){
                $this->db->where("c.id_empresa",5); 
            }*/
        }
        //$this->db->where("o.status",$status);

        //si hay busqueda con el campo de busqueda
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();   
        }

        $this->db->group_by('c.id');
        $query=$this->db->get();
        //return $query->row()->total;
        //return $this->db->count_all_results();
        $get=$query->result();
        $total=0;
        foreach($get as $k){
            $total++;
        }
        return $total;
    }
    
    public function getServiciosCotizacion($id,$id_emp){
        $this->db->select("cotizaciones_has_servicios.*,s.clave,s.nombre,s.descripcion,prov.nombre as empresa,
            s2.clave as clave2, s2.nombre as nombre2, s2.descripcion as descripcion2,prov2.nombre as empresa2,
            s3.clave clave3, s3.nombre as nombre3, s3.descripcion as descripcion3, prov3.nombre as empresa3");
        $this->db->join('servicios s', 's.id = servicio_id and (id_empresa_serv=1 or id_empresa_serv=2 or id_empresa_serv=3 or id_empresa_serv=6)','left');
        $this->db->join('servicios_ahisa s2', 's2.id = servicio_id and id_empresa_serv=4','left');
        $this->db->join('servicios_auven s3', 's3.id = servicio_id and id_empresa_serv=5','left');

        $this->db->join('proveedores as prov', 's.proveedor = prov.id','left');
        $this->db->join('proveedores as prov2', 's2.proveedor = prov2.id','left');
        $this->db->join('proveedores as prov3', 's3.proveedor = prov3.id','left');

        /*if($id_emp==1 || $id_emp==2 || $id_emp==3 || $id_emp==6){
            $this->db->join('servicios s', 's.id = servicio_id');
        }else if($id_emp==4){
            $this->db->join('servicios_ahisa s', 's.id = servicio_id');
        }else if($id_emp==5){
            $this->db->join('servicios_auven s', 's.id = servicio_id');
        }
        $this->db->join('proveedores as prov', 'proveedor = prov.id');*/
        $this->db->where("cotizacion_id",$id);
        $this->db->where("cotizaciones_has_servicios.status",1);
        if(!$this->session->userdata("administrador")){
            //$this->db->where("s.proveedor",$this->session->userdata("empresa"));
        }
        $query=$this->db->get("cotizaciones_has_servicios");
        return $query->result();
    }
    
    public function getServicio($id,$id_emp){
        //$this->db->select("servicios.nombre as nombre, empleados.correo");
        $this->db->select("s.nombre as nombre, prov.correo, e.correo as mail_tec");
        if($id_emp==1 || $id_emp==2 || $id_emp==3 || $id_emp==6){
            $this->db->join('servicios s', 's.id = servicio_id');
        }else if($id_emp==4){
            $this->db->join('servicios_ahisa s', 's.id = servicio_id');
        }else if($id_emp==5){
            $this->db->join('servicios_auven s', 's.id = servicio_id');
        }

        $this->db->join('ordenes o', 'o.cotizacion_id = cotizaciones_has_servicios.cotizacion_id and cotizaciones_has_servicios.status=1');
        $this->db->join('empleados e', 'e.id = o.id_tecnico');
        $this->db->join('proveedores prov', 'prov.id = s.proveedor');
        $this->db->where("cotizaciones_has_servicios.cotizacion_id",$id);
        $query=$this->db->get("cotizaciones_has_servicios");
        return $query->row();
    }
    public function getDocsFam($id){
        $this->db->select("ordenes.familia_id, familias_has_documentos.documento");
        //$this->db->from("ordenes");
        $this->db->join('familias_has_documentos', 'familias_has_documentos.familia_id = ordenes.familia_id');
        $this->db->where("ordenes.id",$id);
        $query=$this->db->get("ordenes");
        return $query->row()->documento;
    }

    public function getDocsConfig(){
        $this->db->select("*");
        $this->db->from("documentos");
        $query=$this->db->get();
        return $query->result();
    }

    public function getDocsServicio($id,$id_emp){
        $this->db->select("d.nombre");
        $this->db->from("cotizaciones_has_servicios chs");
        if($id_emp==1 || $id_emp==2 || $id_emp==3 || $id_emp==6){
            $this->db->join("servicios_documentos sd","sd.id_servicio=chs.servicio_id");
        }else if($id_emp==4){
            $this->db->join('servicios_documentos_ahisa sd', 'sd.id_servicio = chsservicio_id');
        }else if($id_emp==5){
            $this->db->join('servicios_documentos_auven sd', 'sd.id_servicio = chsservicio_id');
        }
        $this->db->join("documentos d","d.id=sd.id_documento");
        $this->db->where("chs.cotizacion_id",$id);
        $this->db->where("sd.estatus",1);
        $this->db->group_by("sd.id_documento");
        $query=$this->db->get();
        return $query->result();
    }
    
    public function getMail($id){
        $this->db->select("email");
        $this->db->join('cotizaciones', 'cotizaciones.id = cotizacion_id');
        $this->db->join('personas_contacto', 'cotizaciones.cliente_id = personas_contacto.cliente_id');
        $this->db->where("cotizaciones_has_servicios.cotizacion_id",$id);
        $this->db->where("orden",1);
        $query=$this->db->get("cotizaciones_has_servicios");
        return $query->row()->email;
    }

    public function getMailContacto($id){
        $this->db->select("email");
        $this->db->join('cotizaciones', 'cotizaciones.id = cotizacion_id');
        $this->db->join('personas_contacto', 'cotizaciones.contacto_id = personas_contacto.id');
        $this->db->where("cotizaciones_has_servicios.cotizacion_id",$id);
        //$this->db->where("orden",1);
        $query=$this->db->get("cotizaciones_has_servicios");
        return $query->result();
    }

    public function getOrdenesPend(){
        $fecha = date("Y-m-d", strtotime("-2 months")); 
        $this->db->select("o.id,cl.alias,o.fecha_creacion, c.id as id_cot, c.id_empresa, cs.id_empresa_serv");
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones c', 'c.id = o.cotizacion_id');
        $this->db->join('clientes cl', 'cl.id = c.cliente_id','left');
        $this->db->join('cotizaciones_has_servicios cs', 'cs.cotizacion_id = c.id');

        $this->db->where("cs.fecha_inicio IS NULL");  //pendiente
        $this->db->where("o.status",1);  //pendiente
        $this->db->where('c.status',2);   //aceptada

        $this->db->where('o.fecha_creacion >=',''.$fecha.' 00:00:00');   //aceptada
        $this->db->group_by('c.id');
        $this->db->order_by('o.id',"asc");
        $query=$this->db->get();
        return $query->result();
    }

    public function getOrdenesExcel($fecha1,$fecha2,$sta,$emp){
        $id_usuario=$this->session->userdata("id_usuario");   
        if($emp==0){
            $sel_nombre = 'CONCAT(s.nombre,"<br>",s.descripcion) as nombre, CONCAT(s2.nombre,"<br>",s2.descripcion) as nombre2, CONCAT(s3.nombre,"<br>",s3.descripcion) as nombre3, ';
        }else if($emp==1 || $emp==2 || $emp==4 || $emp==6){
           $sel_nombre = 'CONCAT(s.nombre,"<br>",s.descripcion) as nombre, ';
        }else if($emp==4){
            $sel_nombre = 'CONCAT(s2.nombre,"<br>",s2.descripcion) as nombre2, ';
        }else if($emp==5){
            $sel_nombre = 'CONCAT(s3.nombre,"<br>",s3.descripcion) as nombre3, ';
        }

        /*if(!$this->session->userdata("administrador")){
            $columns[2]='CONCAT(s.nombre,"<br>",s.descripcion) as nombre, CONCAT(s2.nombre,"<br>",s2.descripcion) as nombre2, CONCAT(s3.nombre,"<br>",s3.descripcion) as nombre3';
        }*/
        $select = "o.id,cl.alias,o.fecha_creacion,o.cotizacion_id,o.status, empleados.usuario as nomVendedor, o.cotizacion_id as nombre, IFNULL(cs.fecha_inicio,'0') as fecha_inicio, cs.hora_inicio, cs.fecha_fin, cs.hora_fin, cs.comentarios, c.id as id_cot, ";
        
        if(!$this->session->userdata("administrador") && $this->session->userdata("perfil")!='3'){
            $select.="cs.id as servicio_id,IFNULL(cs.fecha_inicio,'0') as fecha_inicio,fecha_fin,hora_inicio,hora_fin,cs.comentarios, c.cliente_id, , max(o.id) as id_orden, c.id as id_cot, 
            ".$sel_nombre.", c.id_empresa, ";
        }
        else{
            $select.="o.cotizacion_id as nombre, c.cliente_id, , max(o.id) as id_orden, ".$sel_nombre.", c.id_empresa,";
        }
             
        $this->db->select($select);
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones c', 'c.id = o.cotizacion_id');
        $this->db->join('clientes cl', 'cl.id = c.cliente_id','left');
        $this->db->join('empleados', 'empleados.id = c.vendedor_id','left');
        $this->db->join('cotizaciones_has_servicios cs', 'cs.cotizacion_id = c.id');

        if($emp==0){
            $this->db->join('servicios s', 's.id=cs.servicio_id and (c.id_empresa=1 or c.id_empresa=2 or c.id_empresa=3 or c.id_empresa=6)','left');
            $this->db->join('servicios_ahisa s2', 's2.id=cs.servicio_id and c.id_empresa=4','left');
            $this->db->join('servicios_auven s3', 's3.id=cs.servicio_id and c.id_empresa=5','left');
        }else if($emp==1 || $emp==2 || $emp==4 || $emp==6){
            $this->db->join('servicios s', 's.id = cs.servicio_id');
        }else if($emp==4){
            $this->db->join('servicios_ahisa s2', 's2.id=cs.servicio_id');
        }else if($emp==5){
            $this->db->join('servicios_auven s3', 's3.id=cs.servicio_id');
        }

        //if($this->session->userdata("perfil")!='1' && $this->session->userdata("perfil")!='3'){ //admin y tecnico
            //$this->db->join('servicios s', 's.id = cs.servicio_id'); ahora se dividieron los servicios por tabla de empresa
            $this->db->where('cs.status!=', 0);
        //}
        /*if($emp!="0"){ //ya no va porque se dividieron las tablas se servicios
            $this->db->where('s.proveedor',$emp);
        }*/

        if($this->session->userdata("perfil")!='1' && $this->session->userdata("perfil")!='3'){
            //$this->db->where("o.id_usuario",$this->session->userdata("id_usuario")); 
            $this->db->where("c.vendedor_id",$this->session->userdata("id_usuario")); 
        }
        if($sta!=0 && $sta!=4){
            $this->db->where("o.status",$sta); //por estatus modificada, aceptada, terminada
            if($sta==1){
                $this->db->where("cs.fecha_inicio IS NULL");  //pendiente
            }
        }else if($sta==0){ //todas
            $this->db->where("o.status!=","0");
        }
        if($sta==4){ //en proceso
           $this->db->where("o.status","1"); 
           $this->db->where("cs.fecha_inicio!=",""); 
        }
        if($fecha1!="0" && $fecha2!="0"){
            $this->db->where('o.fecha_creacion BETWEEN '.'"'.$fecha1.' 00:00:00"'.' AND '.'"'.$fecha2.' 23:59:59"');
        }
        $this->db->where('c.status!=',0);
        $this->db->group_by('c.id');
        $this->db->order_by('o.id',"desc");

        $query=$this->db->get();
        return $query->result();
    }

    public function getOrdenesReportes($id_orden){           
        $this->db->select("IFNULL(o.carpeta_prin,'') as carpeta_prin, IFNULL(o.carpeta,'') as carpeta, ord.id, ord.file_repor, ord.id_serv_repor");
        $this->db->from('ordenes o');
        $this->db->join('ordenes_reportes ord', 'ord.id_orden=o.id and ord.estatus=1');
        //$this->db->join('cotizaciones_has_servicios chs', 'chs.cotizacion_id=o.cotizacion_id and chs.status=1');
        $this->db->where("o.id",$id_orden);
        $query=$this->db->get();
        return $query->result();
    }

    public function getServsReportes($id_orden){           
        $this->db->select("chs.id_empresa_serv");
        $this->db->from('cotizaciones_has_servicios chs');
        $this->db->join('ordenes o', 'o.cotizacion_id=chs.cotizacion_id');
        $this->db->where("o.id",$id_orden);
        $this->db->where("chs.status",1);
        $query=$this->db->get();
        return $query->result();
    }

    public function getServiciosOrden($id,$id_serv_repor){
        $this->db->select("chs.id, chs.servicio_id, s.nombre, s2.nombre as nombre2, s3.nombre as nombre3, chs.id_empresa_serv");
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones_has_servicios chs', 'chs.cotizacion_id=o.cotizacion_id and chs.status=1');
        //$this->db->join('servicios s', 's.id = chs.servicio_id');
        
        $this->db->join('servicios s', 's.id=chs.servicio_id and (chs.id_empresa_serv=1 or chs.id_empresa_serv=2 or chs.id_empresa_serv=3 or chs.id_empresa_serv=6)','left');
        $this->db->join('servicios_ahisa s2', 's2.id=chs.servicio_id and chs.id_empresa_serv=4','left');
        $this->db->join('servicios_auven s3', 's3.id=chs.servicio_id and chs.id_empresa_serv=5','left');
        
        $this->db->where("o.id",$id);
        //$this->db->where("chs.id",$id_serv_repor);
        $query=$this->db->get();
        return $query->result();
    }

    /*public function getServiciosOrden($id,$id_serv_repor=0){
        $this->db->select("chs.id, chs.servicio_id, s.nombre");
        $this->db->from('cotizaciones_has_servicios chs');
        $this->db->join('servicios s', 's.id = chs.servicio_id');
        $this->db->where("chs.id",$id);
        $query=$this->db->get();
        return $query->result();
    }*/


}