<?php

class Servicios_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function deletePreciosServicio($id){
        $this->db->where('servicio_id', $id);
        $this->db->delete('servicios_has_precios');
        $this->db->where('servicio_id', $id);
        $this->db->delete('servicios_has_sucursales');
    }

    public function getPreciosSucursal($id){
        $sql = "SELECT * FROM servicios_has_sucursales WHERE servicio_id=$id";
        $query = $this->db->query($sql);
        if($query->num_rows()>0){
             return $query->result();
        }
        else{
            return null;
        }
       
    }

    public function getPreciosPuntos($id){
        $sql = "SELECT * FROM servicios_has_precios WHERE servicio_id=$id";
        $query = $this->db->query($sql);
        if($query->num_rows()>0){
             return $query->result();
        }
        else{
            return null;
        }
       
    }


}