<?php

class Dashboard_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function getLastMonths($meses){ //obtiene la sumatoria de cotizaciones de los ultimos 6 meses 
    	$meses=$meses-1; 
        $where="";
        if($this->session->userdata("empresa")==1 || $this->session->userdata("empresa")==2 || $this->session->userdata("empresa")==3 || $this->session->userdata("empresa")==6){
            $where="and (id_empresa=1 or id_empresa=2 or id_empresa=3 or id_empresa=6)"; 
        }else if($this->session->userdata("empresa")==4){
            $where="and id_empresa=4"; 
        }else if($this->session->userdata("empresa")==5){
            $where="and id_empresa=5"; 
        }
    	$sql="SELECT year(fecha_creacion) as anio, month(fecha_creacion) as mes, sum(1) AS no_cotizaciones 
        FROM cotizaciones 
        WHERE fecha_creacion >= curdate() - interval $meses month ".$where."
        GROUP BY year(fecha_creacion), month(fecha_creacion)";
    	$query = $this->db->query($sql);
        return $query->result();

    }

}