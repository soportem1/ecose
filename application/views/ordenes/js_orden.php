<script>
     $(document).ready(function () {
		 
        $('.pickadate').pickadate({
            monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dec'],
            weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
            today: 'Hoy',
            clear: 'Limpiar',
            close: 'Cerrar',
            format: "yyyy-mm-dd"
        });
        
	$('.timepicker').timepicker({
            'timeFormat': 'H:i:s',
            'minTime': '8:00am',
            'maxTime': '6:00pm'
        }); 
        
        <?php
            if($this->session->userdata("departamento")!="ADMINISTRADOR"){
                echo '$("input").attr("disabled",true); ';
                echo '$("#obs").attr("disabled",true); ';
            }
        ?>
        
        // VALIDACION

        $("#form_orden")
                .on('success.form.fv', function (e) {
                    // Prevent form submission
                    e.preventDefault();
                    var $form = $(e.target);
                    // Use Ajax to submit form data
                    var fd = new FormData($form[0]);
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/ordenes/update/<?php echo $orden->id; ?>",
                        cache: false,
                        data: fd,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            if (data==1)
                            {
				                swal("Exito!", "Se actualizaron los datos", "success")
                            }
                            else{
                                swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error")
                            }
                        }
                    }); // fin ajax
                })
                .formValidation({
                    framework: 'bootstrap',
                    icon: {
                        valid: 'fa fa-check',
                        invalid: 'fa fa-times',
                        validating: 'fa fa-refresh'
                    },
                    fields: {
                        
                    }
                });
                // FIN VALIDADOR
    });
    
    function cancelar_servicio(id,btn){
        swal({
            title: '',
            text: "¿Desea cancelar este servicio en la Orden?",
            type: 'info',
            showCancelButton: true,
            allowOutsideClick: false
        }).then(function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>index.php/ordenes/cancelarServicio",
                    data: {id: id},
                    success: function (data) {
                        swal("", "Se canceló el servicio", "success");
                        btn.closest("tr").find("td:nth-child(6)").html('<span class="badge badge-danger width-100">Cancelado</span>');
                    }
                });
            }
        }).catch(swal.noop);
    }

</script>