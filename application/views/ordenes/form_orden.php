
<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">
                        Orden de Trabajo</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <form class="form" id="form_orden" method="post">
                            <div class="row mt-4">
                                <div class="col-6">
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <p>Cotización:</p>
                                        </div>
                                        <div class="col-sm-8">
                                            <input class="form-control form-control-sm" value="<?php echo $orden->cotizacion_id; ?>" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <p>Vendedor:</p>
                                        </div>
                                        <div class="col-sm-8">
                                            <input class="form-control form-control-sm" value="<?php echo $orden->usuario; ?>" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    
                                </div>
                            </div>
                            <h4 class="form-section"><i class="fa fa-building-o"></i> Datos de la empresa</h4>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group row">
                                        <div class="col-sm-2">
                                            <p>Dirección:</p>
                                        </div>
                                        <div class="col-sm-10">
                                            <textarea style="resize: none;" class="form-control" rows="2" readonly><?php
                                                echo "$cliente->calle $cliente->no_ext, $cliente->colonia"
                                                . "\n$cliente->poblacion, $cliente->estado. CP: $cliente->cp";
                                                ?>
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group row">
                                        <div class="col-sm-2">
                                            <p>Razón Social:</p>
                                        </div>
                                        <div class="col-sm-10">
                                            <input class="form-control form-control-sm" value="<?php echo $cliente->empresa; ?>" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h4 class="form-section"><i class="fa fa-user-circle"></i> Persona de Contacto</h4>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <p>Nombre:</p>
                                        </div>
                                        <div class="col-sm-8">
                                            <input class="form-control form-control-sm" value="<?php echo $cliente->nombre; ?>" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <p>Teléfono:</p>
                                        </div>
                                        <div class="col-sm-8">
                                            <input class="form-control form-control-sm" value="<?php echo $cliente->telefono; ?>" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-6">
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <p>Correo:</p>
                                        </div>
                                        <div class="col-sm-8">
                                            <input class="form-control form-control-sm" value="<?php echo $cliente->email; ?>" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h4 class="form-section"><i class="fa fa-sign-in"></i> Requisitos de ingreso</h4>
                            <div class="row">
                                <div class="col-sm-4">
                                    <p>¿Curso de Seguridad?
                                        <label class="radio-inline"><input type="radio" name="curso_seguridad"> SI</label>
                                        <label class="radio-inline"><input type="radio" name="curso_seguridad"> NO</label>
                                    </p>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <p>Hora de inicio:</p>
                                        </div>
                                        <div class="col-sm-8">
                                            <input class="form-control form-control-sm timepicker" pattern="^((?:[01]\d|2[0-3]):[0-5]\d:[0-5]\d$)" name="hora_inicio_ingreso" value="<?php echo $orden->hora_inicio_ingreso; ?>" >
                                        </div>

                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <p>Formato de ingreso:</p>
                                        </div>
                                        <div class="col-sm-8">
                                            <input class="form-control form-control-sm" name="formato_ingreso" value="<?php echo $orden->formato_ingreso; ?>" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p>Turnos de trabajo</p>
                            <div align="center">
                                <table style="width: 70%" class="table table-sm">
                                    <tr>
                                        <td>1º Turno</td>
                                        <td><div class="form-group"><input class="form-control form-control-sm timepicker" pattern="^((?:[01]\d|2[0-3]):[0-5]\d:[0-5]\d$)" placeholder="Inicio" name="turno1_inicio" value="<?php echo $orden->turno1_inicio; ?>" ></div></td>
                                        <td><div class="form-group"><input class="form-control form-control-sm timepicker" pattern="^((?:[01]\d|2[0-3]):[0-5]\d:[0-5]\d$)" placeholder="Fin" name="turno1_fin" value="<?php echo $orden->turno1_fin; ?>"></div></td>
                                    </tr>
                                    <tr>
                                        <td>2º Turno</td>
                                        <td><div class="form-group"><input class="form-control form-control-sm timepicker" pattern="^((?:[01]\d|2[0-3]):[0-5]\d:[0-5]\d$)" placeholder="Inicio" name="turno2_inicio" value="<?php echo $orden->turno2_inicio; ?>"></div></td>
                                        <td><div class="form-group"><input class="form-control form-control-sm timepicker" pattern="^((?:[01]\d|2[0-3]):[0-5]\d:[0-5]\d$)" placeholder="Fin" name="turno2_fin" value="<?php echo $orden->turno2_fin; ?>"></div></td>
                                    </tr>
                                    <tr>
                                        <td>3º Turno</td>
                                        <td><div class="form-group"><input class="form-control form-control-sm timepicker" pattern="^((?:[01]\d|2[0-3]):[0-5]\d:[0-5]\d$)" placeholder="Inicio" name="turno3_inicio" value="<?php echo $orden->turno3_inicio; ?>"></div></td>
                                        <td><div class="form-group"><input class="form-control form-control-sm timepicker" pattern="^((?:[01]\d|2[0-3]):[0-5]\d:[0-5]\d$)" placeholder="Fin"name="turno3_fin" value="<?php echo $orden->turno3_fin; ?>"></div></td>
                                    </tr>
                                </table>
                            </div>

                            <h4 class="form-section"><i class="fa fa-search"></i> Observaciones para recipientes sujetos a presión</h4>


                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <p class="text-bold-600">Observaciones:</p>
                                        <textarea id="obs" style="resize: none;" class="form-control" rows="2" ><?php echo $orden->observaciones; ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <br><hr>
                            <div class="row">
                                <button type="submit" onclick="save_cotizacion()" class="btn btn-success shadow-z-1" style="width: 40%; margin-left: 30%;">Guardar <i class="ft-save"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <form>
                        <h4 class="form-section"><i class="fa fa-briefcase"></i> Servicios a realizar</h4>
                            <table class="table table-sm">
                                <thead>
                                    <tr>
                                        <th>Partida</th>
                                        <th>Cantidad de Análisis</th>
                                        <th>Clave</th>
                                        <th>Descripción</th>
                                        <th>Empresa</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="table_servicios">
                                        <?php $i = 1;
                                        foreach ($servicios as $s) { ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $s->cantidad; ?></td>
                                            <td><?php echo $s->clave; ?></td>
                                            <td><?php echo $s->nombre; ?></td>
                                            <td><?php echo $s->empresa; ?></td>
                                            <td>
                                                <?php
                                                if($s->status==1){ 
                                                 $logueo = $this->session->userdata();
                                                 if($logueo['cancelacion']){
                                                    echo "<button class='btn btn-sm btn-danger' onclick='cancelar_servicio($s->id,$(this))' type='button'>Cancelar</button>";
                                                 }
                                                }
                                                else{
                                                    echo '<span class="badge badge-danger width-100">Cancelado</span>';
                                                }
                                                ?>
                                            </td>
                                        </tr>
    <?php $i++;
} ?>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
            
        </section>
    </div>
</div>


<?php
include 'js_orden.php';
