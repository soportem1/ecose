<?php
    $id_usuario = $this->session->userdata("id_usuario");
    $empresa = $this->session->userdata("empresa");
    $perfil = $this->session->userdata("perfil");
    $permiso_program_env=$this->session->userdata("permiso_program_env");
?>
<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">Ordenes de trabajo</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <input type="hidden" id="idus" value="<?php echo $id_usuario; ?>">
                        <h5><i class="ft-mail"></i> Listado de ordenes</h5><hr>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="modal-title text-text-bold-600">Busqueda por estatus</label>
                                <div class="position-relative has-icon-right">
                                    <select class="form-control" id="tipostock" name="tipostock" onchange="loadTable(this.value)">
                                        <option value="0">Todas:</option>
                                        <option value="1">Pendiente</option>
                                        <option value="4">En Proceso</option>
                                        <option value="2">Realizado</option>
                                        <option value="3">Cancelado</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 form-group">
                                <div class="position-relative has-icon-right">
                                    <label for="Fecha1">Desde</label>
                                    <input id="Fecha1" type="date" class="form-control" value="<?php echo date("Y-m-d", strtotime("-1 months")); ?>" max="<?php echo date("Y-m-d"); ?>">
                                </div>
                            </div>
                            <div class="col-md-3 form-group">
                                <div class="position-relative has-icon-right">
                                    <label for="Fecha2">Hasta</label>
                                    <input id="Fecha2" type="date" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3 form-group">
                                <label class="modal-title text-text-bold-600">Empresa</label>
                                <div class="position-relative has-icon-right">
                                    <select type="text" id="empresa" class="form-control">
                                        <?php //if($perfil!=2){
                                            echo '<option value="0">Todos:</option>';
                                        //}
                                        if($id_usuario=="1" || $id_usuario=="12" || $id_usuario=="15"){ ?>
                                            <!--<option value="0">Todos:</option>-->
                                        <?php }                                          
                                        foreach ($proveedores as $p) {
                                            if($perfil!=2){
                                                echo "<option value='$p->id'>$p->nombre</option>";
                                            }
                                            else if($perfil==2){
                                                if($empresa=="1" && $p->id!=4 && $p->id!="5"|| $empresa=="2" && $p->id!=4 && $p->id!="5" || $empresa=="3" && $p->id!=4 && $p->id!="5"|| $empresa=="6" && $p->id!=4 && $p->id!="5"){  //ecose
                                                echo "<option data-eco='ecose' value='$p->id'>$p->nombre</option>";
                                                }else if($empresa=="4" && $p->id==4 ){ //ahisa
                                                    echo "<option value='$p->id'>$p->nombre</option>";
                                                }else if($empresa=="5" && $p->id==5){ //auven
                                                    echo "<option value='$p->id'>$p->nombre</option>";
                                                }  
                                            }
                                            /*if($id_usuario=="1" || $id_usuario=="12"){
                                                echo "<option value='$p->id'>$p->nombre</option>";
                                            }
                                            else if($id_usuario!="1" && $empresa=="1" && $p->id==1 || $id_usuario!="12" && $empresa=="1" && $p->id==1
                                                || $id_usuario!="1" && $empresa=="2" && $p->id==2 || $id_usuario!="12" && $empresa=="2" && $p->id==2 
                                                || $id_usuario!="1" && $empresa=="3" && $p->id==3 || $id_usuario!="12" && $empresa=="3" && $p->id==3
                                                || $id_usuario!="1" && $empresa=="6" && $p->id==6 || $id_usuario!="12" && $empresa=="6" && $p->id==6){  //ecose
                                                echo "<option value='$p->id'>$p->nombre</option>";
                                            }else if($id_usuario!="1" && $empresa=="4" && $p->id==4 || $id_usuario!="12" && $empresa=="4" && $p->id==4){ //ahisa
                                                echo "<option value='$p->id'>$p->nombre</option>";
                                            }else if($id_usuario!="1" && $empresa=="5" && $p->id==5 || $id_usuario!="12" && $empresa=="5" && $p->id==5){ //auven
                                                echo "<option value='$p->id'>$p->nombre</option>";
                                            }*/
                                        } ?>
                                    </select>
                                </div>
                            </div>
                            

                            <div class="col-md-2 form-group">
                                <button class="btn btn-success" type="button" id="export" >Exportar Excel</button>
                            </div>
                        </div>
                        <br>
                        <div class="table-responsive">
                            <table class="table table-striped" id="tabla">
                                <thead>
                                    <tr>
                                        <th># Orden</th>
                                        <th>Vendedor</th>
                                        <th>Cotización</th>
                                        <!--<th>Servicio</th>-->
                                        <th>Fecha creación de la Orden</th>
                                        <th>Cliente</th>
                                        <th>Empresa</th>
                                        <th>Estatus</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<!--MODAL Programacion ---------------------------------------------------------------------->
<div class="modal fade text-left" id="modal_program" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label class="modal-title text-text-bold-600">Fecha de programación</label>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Inicio:</p>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar-o"></span>
                                </span>
                                <input type='text' id="fecha_inicio" class="form-control form-control-sm pickadate" placeholder="Selecciona la fecha" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                                <input type='text' id="hora_inicio" class="form-control form-control-sm timepicker" placeholder="Selecciona la hora" />
                            </div>
                        </div>
                    </div>
                </div>
                <p>Fin:</p>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar-o"></span>
                                </span>
                                <input type='text' id="fecha_fin" class="form-control form-control-sm pickadate" placeholder="Selecciona la fecha" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                                <input type='text' id="hora_fin" class="form-control form-control-sm timepicker" placeholder="Selecciona la hora" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        <label>Técnico asignado</label>
                        <input type='text' id="tecnico" class="form-control form-control-sm" placeholder="Ingrese el técnico" />

                    </div>
                </div>
                <p>Comentarios:</p>
                <textarea class="form-control" id="comentarios" rows="3"></textarea>
                <input id="servicio" type="hidden" value="0">
                <input id="cliente_orden" type="hidden" value="0">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <?php if($this->session->userdata("id_usuario")=="1" || $this->session->userdata("perfil")=="3"){ ?>
                    <button type="button" onclick="programacion()" class="btn btn-success">Guardar</button>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<!--MODAL DE PRODUCTOs ---------------------------------------------------------------------->

<div class="modal fade text-left" id="modal_empresa" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label class="modal-title text-text-bold-600">Empresa</label>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="cont_emp"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal_carga" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content xl">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Cargar Reporte</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="content-header mb-3" id="datacliente"></div>
                        <div class="content-header mb-3"><b>Recordatorio:</b> El usuario y contraseña se debe crear justamente en ese ID de cliente (catálogo)</div>
                        <div class="mb-3 row">
                            <label class="col-sm-4 col-form-label">Subir archivo</label>
                            <hr>
                            <div class="col-md-12">
                                <input class="form-control" type="hidden" id="carp_cl" value="0">
                                <input class="form-control" type="hidden" id="carp_aux" value="0">
                                <input class="form-control" type="hidden" id="file_aux" value="0">
                                <input class="form-control" type="hidden" id="id_orden_aux" value="0">
                                <input class="form-control" type="hidden" id="id_cli_aux" value="0">
                                <input class="form-control" type="file" name="file_repor" id="file_repor" accept=".pdf,.zip,.rar">
                            </div>
                            <!--<button class="btn btn-primary sav_cont" style="margin-top: 5px; " type="button" onclick="save_contrato()">Guardar</button>-->
                            <div class="col-md-12">
                                <h5><i class="ft-file-text"></i> Reportes Cargados</h5>
                                <div class="col-md-12">
                                    <hr style="background-color: blue;">
                                </div>
                            </div>
                            <div id="cont_files"></div>
                        </div>
                    </div>
                </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-light" data-dismiss="modal">Cerrar</button>
            <button class="btn btn-primary sav_cont" type="button" onclick="saveCargas()">Guardar</button>
          </div>
        </div>
    </div>
</div>