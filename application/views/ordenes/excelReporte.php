<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=ordenes".date('Y-m-d').".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table border="1" id="tabla" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
        	<th># Orden</th>
          <th>Vendedor</th>
          <th>Cotización</th>
          <!--<th>Servicio</th>-->
          <th>Fecha creación de la Orden</th>
          <th>Cliente</th>
          <th>Empresa</th>
          <th>Estatus</th>
        </tr>
    </thead>
    <tbody>
    	<?php 
        foreach ($o as $key) {
          $getcs=$this->Catalogos_model->getServiciosEmpresa(array("cotizacion_id"=>$key->id_cot,"cs.status"=>1),$key->id_empresa);
          $cont_serv=0;
          $prov=""; $class="";
          foreach ($getcs as $k) {
            if($k->id==1 || $k->id==2 || $k->id==3 || $k->id==6){
                $class="class='btn btn-success'";
            }else if($k->id==4){
                $class="class='btn btn-warning' style='border-radius:20px'";
            }else if($k->id==5){
                $class="class='btn btn-info' style='border-radius:20px'";
            }
            $prov.='<p '.$class.'> '.$k->nombre.' </p>';
          }
          $str='Pendiente';
          if($key->status==1 && $key->fecha_inicio!="" && $key->fecha_inicio!="0" && $key->fecha_inicio!=null && $key->fecha_inicio!="0000:00:00"){
            $str='En Proceso';
          }
          if($key->status==2){
            $str='Realizado';
          }
          if($key->status==3){
            $str='Cancelado';
          }
        	echo '
            <tr>
                <td >'.$key->id_orden.'</td>
                <td >'.($key->nomVendedor).'</td>
                <td >'.$key->cotizacion_id.'</td>
                <td >'.$key->fecha_creacion.'</td>
                <td >'.$key->alias.'</td>
                <td >'.$prov.'</td>
                <td >'.$str.'</td>';
        }
        echo '</tr>';

        ?>
    </tbody>
</table>
