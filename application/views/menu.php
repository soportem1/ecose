<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if($this->session->userdata('logeado_ecose') == true){ 

    /*if($this->session->userdata('perfil_permiso') != $this->session->userdata('perfil')){
        redirect(base_url()."index.php/main/inicio");
    } */   

    /*echo "perfil permiso  ". $this->session->userdata('perfil_permiso');
    echo "perfil" .$this->session->userdata('perfil');*/
        ?>
<body data-col="2-columns" class=" 2-columns ">
    <!-- ////////////////////////////////////////// //////////////////////////////////-->
    <div class="wrapper">
 
 
        <!-- main menu-->
        <!--.main-menu(class="#{menuColor} #{menuOpenType}", class=(menuShadow == true ? 'menu-shadow' : ''))-->
        <?php $logo =  base_url()."app-assets/img/logo2.png"; $color="harmonic-energy";
            if($this->session->userdata("empresa")=="1" || $this->session->userdata("empresa")=="2" || $this->session->userdata("empresa")=="3" || $this->session->userdata("empresa")=="6") {
                $logo =  base_url()."app-assets/img/logo2.png"; 
                $color='data-background-color="harmonic-energy"';
            }
            else if($this->session->userdata("empresa")=="4") {
                $logo =  base_url()."app-assets/img/ahisa_blanco.png"; 
                //$color='style="background-color=rgb(233,98,0);!important"';
                $color='data-background-color="ibiza-sunset"';
            }
            else if($this->session->userdata("empresa")=="5") {
                $logo =  base_url()."app-assets/img/logo_auvenbco2.png";
                $color='data-background-color="king-yna"';
            }
        ?>
        <div data-active-color="white" <?php echo $color; ?> data-image="<?php echo base_url(); ?>app-assets/img/back.jpg" class="app-sidebar"> <!--data-image="../app-assets/img/sidebar-bg/04.jpg"-->
            <!-- main menu header-->
            <!-- Sidebar Header starts-->
            <div class="sidebar-header">
                <div class="logo clearfix">
                    <a href="#" class="logo-text float-left">
                        <div class="logo-img">
                        </div>
                        
                        <span class="text pull-left"><img width="120px" src=<?php echo $logo; ?> /></span>
                    </a>
                    <a id="sidebarToggle" href="#" class="nav-toggle d-none d-sm-none d-md-none d-lg-block"><i data-toggle="expanded" class="ft-toggle-right toggle-icon"></i></a><a id="sidebarClose" href="javascript:;" class="nav-close d-block d-md-block d-lg-none d-xl-none"><i class="ft-x"></i></a>
                </div>
            </div>

            <!-- Sidebar Header Ends--> 
            <!-- / main menu header-->
            <!-- main menu content-->
            <?php
            $logueo = $this->session->userdata();
                //print_r($logueo);
            ?>
            <div class="sidebar-content">
                <div class="nav-container">
                    <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
                        <li><a href="<?php echo base_url(); ?>index.php/main/inicio"><i class="ft-home"></i><span data-i18n="" class="menu-title">Inicio</span></a>
                        </li>
                        <?php if($this->session->userdata("id_usuario")=="1" || $this->session->userdata("id_usuario")=="12" || $this->session->userdata("id_usuario")=="15"){ //este ultimo es usuario ana de ecose merida ?>
                        <li class="has-sub nav-item"><a href="#"><i class="fa fa-building"></i><span data-i18n="" class="menu-title">Empresas</span></a>
                            <ul class="menu-content">
                                <li><a href="<?php echo base_url(); ?>index.php/Main/inicio/1" class="menu-item">Grupo Ecose</a>
                                </li>
                                <li><a href="<?php echo base_url(); ?>index.php/Main/inicio/4" class="menu-item">Ahisa</a>
                                </li>
                                <li><a href="<?php echo base_url(); ?>index.php/Main/inicio/5" class="menu-item">Auven</a>
                                </li>
                            </ul>
                        </li>
                        <?php } ?>
            
                        <?php if($logueo['catalogos'] || $logueo['clientes']){ ?>
                        <li class="has-sub nav-item"><a href="#"><i class="ft-clipboard"></i><span data-i18n="" class="menu-title">Catálogos</span></a>
                            <ul class="menu-content">
                                <li><a href="<?php echo base_url(); ?>index.php/catalogos/clientes" class="menu-item">Clientes</a>
                                </li>
                                 <?php if($logueo['catalogos']){ ?>
                                <li><a href="<?php echo base_url(); ?>index.php/catalogos/perfiles" class="menu-item">Perfiles</a>
                                </li>
                                <li><a href="<?php echo base_url(); ?>index.php/catalogos/empleados" class="menu-item">Empleados</a>
                                </li>
                                <li><a href="<?php echo base_url(); ?>index.php/catalogos/empresas" class="menu-item">Proveedores</a>
                                </li>
                                <li><a href="<?php echo base_url(); ?>index.php/catalogos/familias" class="menu-item">Categorías</a>
                                </li>
                                <li><a href="<?php echo base_url(); ?>index.php/servicios/" class="menu-item">Servicios</a>
                                </li>
                                <li><a href="<?php echo base_url(); ?>index.php/catalogos/sucursales" class="menu-item">Sucursales</a>
                                </li>
                                 <?php }  ?>
                            </ul>
                        </li>
                
                        <?php } if($logueo['configuraciones']){ ?>
                        <li class="has-sub nav-item"><a href="#"><i class="ft-settings"></i><span data-i18n="" class="menu-title">Configuraciones</span></a>
                            <ul class="menu-content">
                                <li><a href="<?php echo base_url(); ?>index.php/configuraciones/descuentos" class="menu-item">Descuentos</a>
                                </li>
                                <li><a href="<?php echo base_url(); ?>index.php/configuraciones/departamentos" class="menu-item">Departamentos</a>
                                </li>
                                <li><a href="<?php echo base_url(); ?>index.php/configuraciones/documentos" class="menu-item">Documentos</a>
                                </li>
                                <?php if($this->session->userdata("id_usuario")==1){ ?>
                                <li><a href="<?php echo base_url(); ?>index.php/Configuraciones/encuestas" class="menu-item">Encuestas</a>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <?php }  ?>
                        <?php if(1){ ?>
                       <li class="has-sub nav-item"><a href="#"><i class="ft-trending-up"></i><span data-i18n="" class="menu-title">Operaciones</span></a>
                            <ul class="menu-content">
                                <?php if($logueo['creacion'] || $logueo['edicion'] || $logueo['estatus'] || $logueo['envio']){ ?>
                                <li><a href="<?php echo base_url(); ?>index.php/cotizaciones/" class="menu-item">Cotizaciones</a></li>
                                <?php } if($logueo['datos'] || $logueo['cancelacion'] || $logueo['programacion'] || $logueo['estatus_servicio'] || $logueo['pdf']){ ?>
                                <li><a href="<?php echo base_url(); ?>index.php/ordenes" class="menu-item">Ordenes de Trabajo</a></li>
                                <?php } if($logueo['agenda']){ ?>
                                <li><a href="<?php echo base_url(); ?>index.php/agenda" class="menu-item">Agenda</a></li>
                                <?php }  ?>
                                <?php if($this->session->userdata("perfil")==1) { ?>
                                <li><a href="<?php echo base_url(); ?>index.php/GenerarPass" class="menu-item">Generar Pass</a></li>
                                <?php }  ?>
                                 <li><a href="<?php echo base_url(); ?>index.php/reportes" class="menu-item">Reportes</a></li>
                            </ul>
                        </li>
                        <?php }  ?>
                    </ul>
                    
                </div>
            </div>
            <!-- main menu content-->
            <div class="sidebar-background"></div>
            <!-- main menu footer-->
            <!-- include includes/menu-footer-->
            <!-- main menu footer-->
        </div>
        <!-- / main menu-->

        <div class="main-panel">

            <!-- Navbar (Header) Starts-->
            <nav class="navbar navbar-expand-lg navbar-light bg-faded">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" data-toggle="collapse" class="navbar-toggle d-lg-none float-left"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                        <!--                        <form role="search" class="navbar-form navbar-right mt-1">
                                                    <div class="position-relative has-icon-right">
                                                        <input type="text" placeholder="Search" class="form-control round"/>
                                                        <div class="form-control-position"><i class="ft-search"></i></div>
                                                    </div>
                                                </form>-->
                    </div>
                    <div class="navbar-container">
                        <div id="navbarSupportedContent" class="collapse navbar-collapse">
                            <ul class="navbar-nav">
                               <!--  <li class="nav-item mr-2"><a id="navbar-fullscreen" href="javascript:;" class="nav-link apptogglefullscreen"><i class="ft-maximize font-medium-3 blue-grey darken-4"></i>
                                        <p class="d-none">fullscreen</p></a></li> -->
                                <li class=" nav-item"><p class="mt-1">Bienvenido <strong><?php echo strtoupper($logueo["usuario"]); ?></strong></p>
                                </li>
                                <li class="dropdown nav-item"><a id="dropdownBasic2" href="#" data-toggle="dropdown" class="nav-link position-relative dropdown-toggle"><i class="ft-bell font-medium-3 blue-grey darken-4"></i>
                                    <?php $cont_ord=0; $html='';
                                    if($this->session->userdata("id_usuario")==1 || $this->session->userdata("perfil")==3){
                                        $get_ord = $this->Ordenes_model->getOrdenesPend();
                                        foreach ($get_ord as $k) {
                                            $cont_ord++;
                                            /*$getcs=$this->Catalogos_model->getServiciosEmpresa(array("cotizacion_id"=>$k->id_cot,"cs.status"=>1),$k->id_empresa);*/
                                            $getcs=$this->Catalogos_model->getServiciosEmpresa(array("cotizacion_id"=>$k->id_cot,"cs.status"=>1),$k->id_empresa_serv);
                                            $class=""; $prov="";
                                            foreach ($getcs as $c) {
                                                if($c->id==1 || $c->id==2 || $c->id==3 || $c->id==6){
                                                    $class="class='btn btn-success' style='font-size:12px'";
                                                }else if($c->id==4){
                                                    $class="class='btn btn-warning' style='border-radius:20px; font-size:12px'";
                                                }else if($c->id==5){
                                                    $class="class='btn btn-info' style='border-radius:20px; font-size:12px'";
                                                }
                                                $prov.= "<p ".$class."> ".$c->nombre." </p>";
                                            }
                                            $html .='
                                            <a class="dropdown-item noti-container py-3 border-bottom border-bottom-blue-grey border-bottom-lighten-4"><i class="ft-bell info float-left d-block font-large-1 mt-1 mr-2"></i><span class="noti-wrapper"><span class="noti-title line-height-1 d-block text-bold-400 info">Nueva Orden Recibida</span>
                                                <span class="float-right ">'.$prov.'</span>
                                                <br>
                                                <span class="noti-text">Orden:  '.$k->id.'</span><br>
                                                <span class="noti-text">Cliente:  '.$k->alias.'</span><br>
                                                <span class="noti-text">Fecha:  '.$k->fecha_creacion.'</span>
                                            </span></a>';
                                        }
                                    } ?>
                                    <span class="notification badge badge-pill badge-danger"><?php echo $cont_ord; ?></span>
                                    <p class="d-none">Notificaciones</p></a>
                                    <div class="notification-dropdown dropdown-menu dropdown-menu-right">
                                        <div class="noti-list">
                                            <?php echo $html; ?>
                                        </div>
                                    </div>
                                </li>
                                <li class="dropdown nav-item"><a id="dropdownBasic3" href="#" data-toggle="dropdown" class="nav-link position-relative dropdown-toggle"><i class="ft-user font-medium-3 blue-grey darken-4"></i>
                                        <p class="d-none">User Settings</p></a>
                                    <div ngbdropdownmenu="" aria-labelledby="dropdownBasic3" class="dropdown-menu dropdown-menu-right">
                                        <a href="javascript:;" class="dropdown-item py-1"><i class="ft-settings mr-2"></i><span>Configuración</span></a>
                                        <div class="dropdown-divider"></div>
                                        <a href="<?php echo base_url(); ?>index.php/main/cerrar_sesion" class="dropdown-item"><i class="ft-power mr-2"></i><span>Cerrar Sesión</span></a>
                                    </div>
                                </li>
<!--                                <li class="nav-item"><a href="javascript:;" class="nav-link position-relative notification-sidebar-toggle"><i class="ft-align-left font-medium-3 blue-grey darken-4"></i>
                                        <p class="d-none">Notifications Sidebar</p></a></li>-->
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <!-- Navbar (Header) Ends-->
            <?php }else{

  redirect(base_url());

  }
?>