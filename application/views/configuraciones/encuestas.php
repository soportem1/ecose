
<div class="main-content">
    <div class="content-wrapper">
        <div class="col-sm-12">
            <div class="content-header">Configuración de encuestas</div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <form class="form" method="post">
                        <h4 class="form-section"><i class="ft-file-text"></i> Preguntas de encuesta</h4>
                        <div class="form-group row">
                            <div class="col-md-4 form-group">
                                <label for="sucursal">Empresa</label>
                                <select id="empresa" class="form-control">
                                    <option value="1">Ecose</option>
                                    <option value="4">Ahisa</option>
                                    <option value="5">Auven</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label style="color:transparent;">Agregar</label><br>
                                <button type="button" title="Agregar Pregunta" onclick="agregar()" class="btn btn-sm btn-raised btn-outline-primary"><i class="ft-plus"></i></button>
                            </div>
                        </div>
                        <table class="table table-condensed" id="tabla_pregs">
                            <thead>
                                <tr>
                                    <td>Preguntas</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody id="table_pre">
                            <?php foreach ($pregs->result() as $d) { ?>
                                <tr id="por_preg">
                                    <td width="90%"><input type="hidden" id="id" value="<?php echo $d->id; ?>"> 
                                        <div class="row">
                                            <div class="col-md-10">
                                                <input class="form-control" type="text" id="pregunta" value="<?php echo $d->pregunta; ?>">
                                            </div>
                                            <div class="col-md-2">
                                                <select id="tipo" class="form-control">
                                                    <option <?php if($d->tipo==1) echo "selected"; ?> value="1">Escala</option>
                                                    <option <?php if($d->tipo==2) echo "selected"; ?> value="2">Si/No</option>
                                                </select>
                                            </div>
                                        </div>
                                    </td>
                                    <td><button type="button" onclick="eliminar(<?php echo $d->id; ?>)" class="btn btn-raised btn-icon btn-pure danger mr-1"><i class="fa fa-times-circle"></i></button></td>
                                </tr>
                            <?php } ?>
                        </table>
                        <div class="col-md-3">
                            <button type="submit" onclick="guardar()" id="save" class="btn gradient-green-teal shadow-z-1 white">Guardar <i class="fa fa-save"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#empresa").on("change",function(){
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php/Configuraciones/get_encuestas",
                cache: false,
                data: { 'id':$("#empresa option:selected").val()},
                success: function (data) {
                    $("#table_pre").html(data);
                }
            }); 
        });
    });
    function agregar() {
        var id_empresa = $("#empresa option:selected").val();
        var html ='<td width="90%"><div class="row"><input type="hidden" id="id" value="0">\
                        <div class="col-md-10">\
                            <input class="form-control" type="text" id="pregunta">\
                        </div>\
                        <div class="col-md-2">\
                            <select id="tipo" class="form-control">\
                                <option value="1">Escala</option>\
                                <option value="2">Si/No</option>\
                            </select>\
                        </div>\
                    </div>\
                        </td>\
                        <td><button type="button" onclick="eliminar($(this))" class="btn btn-raised btn-icon btn-pure danger mr-1"><i class="fa fa-times-circle"></i></button></td>';
        $("#table_pre").append("<tr id='por_preg'>"+html+"</tr>");
    }

    function eliminar(i){
        i.closest("#por_preg").remove();
    }
    function eliminarPreg(){
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>index.php/Configuraciones/eliminar_preg",
            cache: false,
            data: { 'id':$("#empresa option:selected").val()},
            success: function (data) {
                swal("Éxito!", "Pregunta eliminada correctamente", "success");
            }
        }); 
    }

    function guardar(){
        if ($("#tabla_pregs tbody > tr").length>0) {
            var DATA  = [];
            var TABLA   = $("#tabla_pregs tbody > tr");
            TABLA.each(function(){         
                item = {};
                item ["id_empresa"] = $("#empresa option:selected").val();
                item ["id"] = $(this).find("input[id*='id']").val();
                item ["pregunta"] = $(this).find("input[id*='pregunta']").val();
                item ["tipo"] = $(this).find("select[id*='tipo'] option:selected").val();
                DATA.push(item);
            });
            INFO  = new FormData();
            aInfo   = JSON.stringify(DATA);
            INFO.append('data', aInfo);
            $.ajax({
                data: INFO,
                type: 'POST',
                url : '<?php echo base_url(); ?>index.php/Configuraciones/submit_pregs',
                processData: false, 
                contentType: false,
                async: false,
                statusCode:{
                    404: function(data_docs){
                        //toastr.error('Error!', 'No Se encuentra el archivo');
                    },
                    500: function(){
                        //toastr.error('Error', '500');
                    }
                },
                beforeSend: function(){
                    $("#save").attr("disabled",true);
                 },
                success: function(data_docs){
                    swal("Éxito!", "Guardado correctamente", "success");
                    setTimeout(function () { location.reload(); }, 1500);
                    
                }
            }); 
        }
    }
</script>
