
<div class="main-content">
    <div class="content-wrapper">
        <div class="col-sm-12">
            <div class="content-header">Descuentos</div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <form class="form" method="post" id="descuento">
                        <h4 class="form-section"><i class="ft-plus"></i>Agregar descuento</h4>
                        <div class="row">
                            <div class="col-3">
                                <p>Rango Inicio</p>
                                <div class="form-group">
                               <div class="input-group ">
                                    <span class="input-group-addon">$</span>
                                    <input type="number" step="any" class="form-control" name="limite_inf" >
                                </div>
                            </div>
                            </div>
                            <div class="col-3">
                                <p>Rango Fin</p>
                                <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <input type="number" step="any" class="form-control" name="limite_sup" >
                                </div>
                            </div>
                            </div>
                            <div class="col-3">
                                <p>Porcentaje de Descuento</p>
                                <div class="form-group">
                                 <div class="input-group">
                                    <span class="input-group-addon">%</span>
                                    <input type="number" max="100" class="form-control" name="descuento" >
                                    
                                </div>
                            </div>
                            </div>
                            <div class="col-1"><button type="submit"  class="btn btn-success mt-4"><i class="ft-plus"></i> AGREGAR</button></div>
                        </div>
</form>
                        
                        
                        <h4 class="form-section"><i class="ft-layers"></i>Descuentos</h4>
                        
                        <br>
                        <table class="table table-bordered table-hover" id="tabla" style="width: 80%">
                            <thead>
                                <th>Limite Inferior</th>
                                <th>Limite Superior</th>
                                <th>% Descuento</th>
                                <th></th>
                            </thead>
                        </table>
                        
                        
                    
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function load() {
        table.destroy();
        table = $('#tabla').DataTable({
            "ajax": {
                "url": "<?php echo base_url(); ?>index.php/configuraciones/getDescuentos"
            },
            "columns": [
                {"data": "inf"},
                {"data": "sup"},
                {"data": "descuento"},
                {
                    "data": null,
                    "defaultContent": " <button type='button' class='btn btn-sm btn-icon btn-danger white delete mb-0'><i class='ft-trash'></i></button>"
                }
            ]
        });

    }
   $(document).ready(function () {
     

        table = $('#tabla').DataTable();

        //Listener para edicion
        $('#tabla tbody').on('click', 'button.delete', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = row.data();
            swal({
                                    title: '',
                                    text: "¿Desea eliminar el decuento?",
                                    type: 'warning',
                                    showCancelButton: true,
                                    allowOutsideClick: false
                                }).then(function (isConfirm) {
                                    if (isConfirm) {
                                       $.ajax({
                                            type: "POST",
                                            url: "<?php echo base_url(); ?>index.php/configuraciones/deleteDescuento/"+data.id,
                                            cache: false,
                                            success: function (data) {
                                                if (data)
                                                {
                                                    table.ajax.reload();
                                                    swal("Exito!","Se eliminó el porcentaje","success");
                                                }
                                                else{
                                                    swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error")
                                                }
                                            }
                                        }); // fin ajax
                                    }
                                }).catch(swal.noop);
            
        });

        

        $("#descuento")
        .on('success.form.fv', function (e) {
                    // Prevent form submission
                    e.preventDefault();
                    var $form = $(e.target);
                    // Use Ajax to submit form data
                    var fd = new FormData($form[0]);

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/configuraciones/insertUpdateToCatalogo/descuentos_act",
                        cache: false,
                        data: fd,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            if (data==1)
                            {
                                var texto="Se agregó el porcentaje";
                                
                                swal({
                                    title: 'Exito!',
                                    text: texto,
                                    type: 'success',
                                    showCancelButton: false,
                                    allowOutsideClick: false
                                }).then(function (isConfirm) {
                                    if (isConfirm) {
                                       table.ajax.reload();
                                    }
                                }).catch(swal.noop);
                            }
                            else{
                                swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error")
                            }
                        }
                    }); // fin ajax
                })
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
            },
            fields: {
                limite_inf: {
                    validators: {
                                notEmpty: {
                                    message: "Ingrese limite"
                                }
                            }
                },
                limite_sup: {
                    validators: {
                                notEmpty: {
                                    message: "Ingrese limite"
                                },
                                greaterThan:{
                                    value: 'limite_inf',
                                    message: "El limite debe ser mayor"
                                }
                            }
                },
                descuento: {
                    validators: {
                                notEmpty: {
                                    message: "Ingrese descuento"
                                },
                                greaterThan:{
                                    value: 0,
                                    message: "El porcentaje no debe ser negativo"
                                }

                            }
                }
            }
        });
                // FIN VALIDADOR
                load();
            });

        </script>
