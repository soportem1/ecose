<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=reporte_ord_cots".date('Y-m-d').".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table border="1" id="tabla" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
        	<th>#</th>
          <th>Mes de la Venta</th>
          <th>Cotización</th>
          <th>Razón social</th>
          <th>Servicio</th>
          <th>Vendedor</th>
          <th>Importe por servicio</th>
          <th>Importe total</th>
          <th>Por facturar</th>
          <th>Observaciones</th>
          <th>Proveedor del servicio</th>
          <th>Zona</th>
          <th>Estatus</th>
        </tr>
    </thead>
    <tbody>
    	<?php 
        $where= array();
        $cont_serv=0; $monto_zona=""; $cot_ant=""; $total = 0; $zona=""; $nom_serv="";
        foreach ($r as $key) {
          
          if($key->tipo=="2" && $key->id_ord==0){
            $fecha_crea = $key->fecha_creacion_cot;
          }
          else if($key->tipo=="2" && $key->id_ord>0){
            $fecha_crea = $key->fecha_creacion_ord;
          }
          else if($key->tipo=="1"){
            $fecha_crea = $key->fecha_creacion_ord;
          }
          if($key->clave!=null){
            $nueva=explode("-",$key->clave);
            $codigo=$nueva[0];
          }else{
            $codigo='';
          }
          //log_message('error', 'fecha_crea: '.$fecha_crea);
          if($key->id_empresa_serv==1 || $key->id_empresa_serv==2 || $key->id_empresa_serv==3 || $key->id_empresa_serv==6){ 
            $nom_serv=$key->servicio; 
          }
          else if($key->id_empresa_serv==4){
            $nom_serv=$key->servicio2; 
          }else if($key->id_empresa_serv==5){
            $nom_serv=$key->servicio3; 
          }

          setlocale(LC_ALL, 'es_ES');
          $monthNum  = date("m", strtotime($fecha_crea));
          $dateObj   = DateTime::createFromFormat('!m', $monthNum);
          $monthName = strftime('%B', $dateObj->getTimestamp());
          $stat = "Pendiente";
          switch ($key->status) {
            case "2":
                $stat = "Aceptada";
                break;
            case "3":
                $stat = "Rechazada";
                break;
            case "4":
                $stat = "Modificada";
                break;
          }
          $precio_con=0;
          $sub=$key->precio*$key->cantidad;
          if($key->descuento>0){
            $desc=$sub*$key->descuento/100;
            $sub = $sub-$desc;
          }
        	echo '
            <tr>
                <td >'.$key->id.'</td>
                <td >'.($monthName).'</td>
                <td >'.$codigo.'</td>
                <td >'.$key->empresa.'</td>
                <td >'.$nom_serv.'</td>
                <td >'.$key->nombre.'</td>
                <td >'.$sub.'</td>';
              if($cot_ant!=$key->id){
                $total = $total + $key->monto;
                echo '<td >'.$key->monto.'</td>';
                if($zona!=$key->zona){
                  $where = array_merge($where,array($key->zona));
                }
              }else{
                echo '<td ></td>'; 
              }
              echo '
                <td > </td>
                <td ></td>
                <td ></td>
                <td >'.$key->zona.'</td>
                <td >'.$stat.'</td>';
          $cot_ant = $key->id;
          $zona=$key->zona;
        }
        echo '</tr>';

        ?>
    </tbody>
    <tfoot>
      <tr>
        <th colspan="7">
           VENTA TOTAL:
        </th>
        <th>
          <?php echo number_format($total,2); ?>
        </th>
      </tr>
    </tfoot>
</table>

<?php //echo var_dump($where); $this->ModeloReportes->get_reportes_data_zona($fecha1,$fecha2,$sucursal,$zona,$cliente,$tiposta,$vendedor,$where); ?>