<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">Catálogo de Clientes</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <h5><i class="ft-file-text"></i> Listado de Clientes</h5><hr>
                        <div class="row">
                            
                            <div class="col-md-3 form-group">
                                <label for="estado">Estado</label>
                                <select id="estado" class="form-control" onchange="load()">
                                    <option data-id_edo="0" value="0">Todos</option>
                                    <?php foreach ($estados as $e) { ?>
                                    <option data-id_edo="<?php echo $e->id; ?>" value="<?php echo $e->estado; ?>"><?php echo $e->estado; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            
                            <div class="col-md-3 form-group">
                                <label for="estado">Tipo de Cliente</label>
                                <select id="tipo_cli" class="form-control" onchange="load()">
                                    <option value="0">Todos</option>
                                    <option value="1">Consultor</option>
                                    <option value="2">Cliente directo</option>
                                </select>
                            </div>

                            <div class="col-md-3">
                                <label style="color: transparent;">export cliente</label>
                                <button type="button" class="btn gradient-green-teal shadow-z-1 white" id="export_cli" onclick="exportar()"><i class="fa fa-file-excel-o" ></i> Exportar Cliente</button> 
                            </div>
                            <div class="col-md-3">
                                <label style="color: transparent;">agregar cliente</label>
                                <a href="<?php echo base_url(); ?>index.php/catalogos/alta_cliente/0" class="btn gradient-green-teal shadow-z-1 white pull-right"><i class="ft-user-plus"></i> Agregar Cliente</a>       
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-12">
                                <table class="table table-striped table-hover table-responsive" id="tabla">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Empresa</th>
                                            <th>Alias</th>
                                            <!--<th>Personas de Contacto</th>-->
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<script>

    function load() {
        //table.destroy();
        table = $('#tabla').DataTable({
            "bProcessing": true,
            "serverSide": true,
            "searching": true,
            responsive: !0,
            destroy:true,
            "order": [[ 1, "asc" ]],
            "ajax": {
                type: "post",
                "url": "<?php echo base_url(); ?>index.php/Catalogos/getClientes",
                data: { edo: $("#estado option:selected").val(), tipo_cli: $("#tipo_cli option:selected").val() }
            },
            "columns": [
                {"data": "cliente_id"},
                {"data": "empresa"},
                {"data": "alias"},
                //{ "data": "contacto"},
                {
                    "data": "cliente_id",
                    "render": function (data, type, row, meta) {
                        return botoneria(data);
                    }
                }
            ]
        });

    }
    
    $(document).ready(function () {
        table = $('#tabla').DataTable();
        //Listener para edicion
        $('#tabla tbody').on('click', 'button.edit', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = row.data();
            window.location = '<?php echo base_url(); ?>index.php/catalogos/edicion_cliente/'+data.cliente_id;
        });
        load();
    });
    
    function botoneria(data){
        var str='<a href="<?php echo base_url(); ?>index.php/catalogos/edicion_cliente/'+data+'" target="a_blank" class="mb-0 btn btn-sm btn-icon btn-success edit"><i class="ft-edit"></i></a>\
        <div class="btn-group ml-1 mb-0">\
                <button type="button" class="btn btn-sm btn-raised mb-0 btn-dark dropdown-toggle" data-toggle="dropdown" ><i class="fa fa-bars"></i></button>\
                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">\
                    <a class="dropdown-item" href="../cliente/facturas/'+data+'">Facturas</a>\
                    <a class="dropdown-item" href="../cliente/encuestas/'+data+'">Encuestas</a>\
                    <a class="dropdown-item delete" href="#">Eliminar</a>\
                </div>\
            </div>';
        return str;
    }

    $('#tabla').on('click', 'a.delete', function () {
        var tr = $(this).closest('tr');
        var data = table.row(tr).data();
        eliminar(data.cliente_id);
    });

    function eliminar(id){
        swal({
            title: "Eliminar",
            text: "¿Desea eliminar este elemento?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Eliminar"
        }).then(function(t){t &&
            $.ajax({
                     type: "POST",
                     url: '<?php echo base_url(); ?>index.php/Cliente/eliminaCliente/',
                     data: {id:id},
                     success: function (result) {
                        console.log(result);
                        table.ajax.reload();
                        swal("Exito!", "Se ha eliminado correctamente", "success");
                     }
                 });
        });
    }

    function exportar(){
        //var id = $("#estado option:selected").val();
        var id = $("#estado option:selected").data("id_edo");
        //console.log("estado id: "+id);
        window.open('<?php echo base_url(); ?>Export/cliente/'+id, '_blank');  
    }

</script>