
<div class="main-content">
    <div class="content-wrapper">
        <div class="col-sm-12">
            <?php $title="Alta"; 
                if(isset($empleado)){
                    $title="Edición";
                }
            ?>
            <div class="content-header"><?php echo $title; ?> de Empleado</div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <form class="form" id="form-empleado" method="post">
                        <h4 class="form-section"><i class="ft-file-text"></i> Datos del Empleado</h4>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <h5>Nombre <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="nombre" class="form-control form-control-sm toupper" <?php if(isset($empleado)){ echo "value='$empleado->nombre'";} ?> >
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <h5>Clave <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="clave" class="form-control form-control-sm toupper" <?php if(isset($empleado)){ echo "value='$empleado->clave'";} ?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Correo Electrónico <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="email" name="correo" class="form-control form-control-sm" <?php if(isset($empleado)){ echo "value='$empleado->correo'";} ?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Teléfono <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="telefono" class="form-control form-control-sm" <?php if(isset($empleado)){ echo "value='$empleado->telefono'";} ?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Dirección <span class="required">*</span></h5>
                                    <div class="controls">
                                        <textarea name="direccion" class="form-control form-control-sm toupper" rows="3"><?php if(isset($empleado)){ echo $empleado->direccion;} ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Perfil <span class="required">*</span></h5>
                                    <div class="controls">
                                        <select type="text" name="perfil" class="form-control form-control-sm" >
                                            <?php                                           
                                            foreach ($perfiles as $p) {
                                                $sel="";
                                                if(isset($empleado) && $empleado->perfil==$p->id){
                                                    $sel="selected";
                                                }
                                                echo "<option value='$p->id' $sel>$p->nombre</option>";
                                            } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <h5>Empresa <span class="required">*</span></h5>
                                    <div class="controls">
                                        <select type="text" name="empresa" class="form-control form-control-sm" >
                                            <?php 											
											foreach ($proveedores as $p) {
												$sel="";
												if(isset($empleado) && $empleado->empresa==$p->id){
													$sel="selected";
												}
                                                echo "<option value='$p->id' $sel>$p->nombre</option>";
                                            } ?>
                                        </select>
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <h5>Sucursal <span class="required">*</span></h5>
                                    <div class="controls">
                                        <select type="text" name="sucursal" class="form-control form-control-sm" >
                                            <?php                                           
                                            foreach ($sucursales as $p) {
                                                $sel="";
                                                if(isset($empleado) && $empleado->sucursal==$p->id){
                                                    $sel="selected";
                                                }
                                                echo "<option value='$p->id' $sel>$p->nombre</option>";
                                            } ?>
                                        </select>
                                    </div>
                                </div>
								<div class="form-group">
                                    <h5>Departamento <span class="required">*</span></h5>
                                    <div class="controls">
                                        <select type="text" name="departamento" class="form-control form-control-sm" >
                                            <?php 											
											foreach ($departamentos as $d) {
												$sel="";
												if(isset($empleado) && $empleado->departamento==$d->nombre){
													$sel="selected";
												}
                                                echo "<option $sel>$d->nombre</option>";
                                            } ?>
											
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Zona <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="zona" class="form-control form-control-sm toupper" <?php if(isset($empleado)){ echo "value='$empleado->zona'";} ?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Usuario <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="usuario" class="form-control form-control-sm" <?php if(isset($empleado)){ echo "value='$empleado->usuario'";} ?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Password <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="password" name="password" class="form-control form-control-sm" <?php if(isset($empleado)){ echo "value='$empleado->password'";} ?>>
                                    </div>
                                </div>
                            </div>
                        </div>
                 
                       
                        
                        <br><hr>
                        <div class="row">
                            <a href="<?php echo base_url(); ?>index.php/catalogos/empleados" class="btn btn-icon btn-secondary"><i class="ft-chevron-left"></i> Regresar</a>
                            <button type="submit" class="btn gradient-green-teal shadow-z-1 white" style="width: 50%; margin-left: 15%;">Guardar <i class="fa fa-save"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
     $(document).ready(function () {
         $(".toupper").on("change",function(){
            $(this).val($(this).val().toUpperCase()); 
         });
         
        // VALIDACION
        $("#form-empleado")
                .on('success.form.fv', function (e) {
                    // Prevent form submission
                    e.preventDefault();
                    var $form = $(e.target);
                    // Use Ajax to submit form data
                    var fd = new FormData($form[0]);
                    <?php if(isset($empleado)){
                        echo "fd.append('id', $empleado->id);";
                    }?>
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/catalogos/insertUpdateEmpleado",
                        cache: false,
                        data: fd,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            if (data>0)
                            {
                                var texto="Se insertó el Empleado";
                                var url="<?php echo base_url(); ?>index.php/catalogos/empleados";
                                
                                <?php if(isset($empleado)){ // Si es Edicion
                                    echo "texto='Se actualizó el Empleado'; ";
                                    echo "url='".base_url()."index.php/catalogos/empleados';";
                                } ?>
  
                                swal({
                                    title: 'Exito!',
                                    text: texto,
                                    type: 'success',
                                    showCancelButton: false,
                                    allowOutsideClick: false
                                }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        window.location = url;
                                    }
                                }).catch(swal.noop);
                            }
                            else{
                                swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error")
                            }
                        }
                    }); // fin ajax
                })
                .formValidation({
                    framework: 'bootstrap',
                    icon: {
                        valid: 'fa fa-check',
                        invalid: 'fa fa-times',
                        validating: 'fa fa-refresh'
                    },
                    fields: {
                        nombre: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese un Nombre"
                                }
                            }
                        },
                        clave: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese Clave"
                                }
                            }
                        },
                        telefono: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese un Teléfono"
                                }
                            }
                        },
                        direccion: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese una Dirección"
                                }
                            }
                        },
                        correo: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese un Correo"
                                }
                            }
                        },
                        usuario: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese el nombre de Usuario"
                                }
                            }
                        },
                        password: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese la Contraseña"
                                }
                            }
                        }
                    }
                });
                // FIN VALIDADOR
    });

</script>
