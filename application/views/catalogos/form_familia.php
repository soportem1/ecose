
<div class="main-content">
    <div class="content-wrapper">
        <div class="col-sm-12">
            <?php
            $title = "Alta";
            if (isset($familia)) {
                $title = "Edición";
            }
            ?>
            <div class="content-header"><?php echo $title; ?> de Categoría</div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <form class="form" id="form-familia" method="post">
                        <h5><i class="ft-plus-circle"></i> Datos generales</h5>
                        <div style="height: 5px" class="gradient-green-teal"></div><br>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <p>Clave <span class="required">*</span></p>
                                    <div class="controls">
                                        <input type="text" name="clave" class="form-control" <?php if (isset($familia)) {
                                            echo "value='$familia->clave'";
                                        } ?> >
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <p>Nombre <span class="required">*</span></p>
                                    <div class="controls">
                                        <input type="text" name="nombre" class="form-control" <?php if (isset($familia)) {
                                                echo "value='$familia->nombre'";
                                            } ?> >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <p>Descripción <span class="required">*</span></p>
                                    <div class="controls">
                                        <div class="controls">
                                            <textarea name="descripcion" class="form-control" rows="3"><?php if (isset($familia)) {
                                                echo $familia->descripcion;
                                            } ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <!--<div class="col-md-6">
                                    <div class="form-group">
                                        <p>Empresa <span class="required">*</span></p>
                                        <div class="controls">
                                            <select type="text" name="id_empresa" class="form-control">
                                                <?php                                          
                                                foreach ($proveedores as $p) {
                                                    echo "<option value='$p->id'>$p->nombre</option>";
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>-->
                            </div>

                        </div>
                        <h5><i class="ft-file-text"></i> Documentación Obligatoria</h5>
                        <div style="height: 5px" class="gradient-green-teal"></div><br>
                        <div class="form-group row">
                            <div class="col-sm-4">
                                <select id="documento" class="form-control form-control-sm">
                                <?php
                                foreach ($documentos as $d) {
                                    echo "<option>$d->nombre</option>";
                                }
                                ?>
                                </select>
                            </div>
                            <button type="button" onclick="add_documento()" class="btn btn-sm btn-raised btn-outline-primary"><i class="ft-plus"></i> Agregar Doc.</button>
                        </div>
                        <hr>
                        <div class="overflow-auto">
                            <table class="table table-sm table-hover table-striped">
                                <thead>
                                    <tr>
                                        <td>Documentos</td>
                                        <td></td>
                                    </tr>
                                </thead>
                                <tbody id="table_documentos">
                                    <?php 
                                    if(isset($docs)){
                                    foreach($docs as $d){ ?>
                                    <tr>
                                        <td>
                                            <?php echo $d->documento; ?>
                                            <input name="documentos[]" value="<?php echo $d->documento; ?>" hidden />
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-sm btn-raised btn-outline-danger btnRemove"><i class="ft-minus"></i></button>
                                        </td>
                                    </tr>
                                    <?php } } ?>
                                </tbody>
                            </table>
                        </div>
                        <h5><i class="ft-file-text"></i> Alcances</h5>
                        <div style="height: 5px" class="gradient-green-teal"></div><br>
                        <button type="button" class="btn btn-sm btn-raised btn-outline-primary addButton2" onclick="lanzar_modal()"><i class="ft-plus"></i> Agregar Alcance</button>
                       
                        <div class="overflow-auto">
                            <table class="table table-sm table-striped">
                                <thead>
                                    <tr>
                                        <td>Alcances</td>
                                        <td></td>
                                    </tr>
                                </thead>
                                <tbody id="table_alcances">
                                    <?php 
                                    if(isset($alcances)){
                                    foreach($alcances as $a){ ?>
                                    <tr>
                                        <td>
                                            <?php echo $a->alcance; ?>
                                            <input name="alcances[]" value='<?php echo $a->alcance; ?>' hidden />
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-sm btn-raised btn-outline-danger btnRemove"><i class="ft-minus"></i></button>
                                        </td>
                                    </tr>
                                    <?php } } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade text-left" id="modal_alcance" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel1">Alcance</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body height-450">
                                        <div id="summernote">Escribe tu alcance...</div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                                        <button type="button" onclick="add_alcance()" class="btn btn-outline-primary">Guardar</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br><hr>
                        <div class="row">
                            <a href="<?php echo base_url(); ?>index.php/catalogos/familias" class="btn btn-icon btn-secondary"><i class="ft-chevron-left"></i> Regresar</a>
                            <button type="submit" class="btn gradient-green-teal shadow-z-1 white" style="width: 50%; margin-left: 15%;">Guardar <i class="fa fa-save"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $("#template_doc").hide();
        // VALIDACION
        
        $("table").on("click",".btnRemove",function(){
           $(this).closest("tr").remove();
        });

        $("#form-familia")
                .on('success.form.fv', function (e) {
                    // Prevent form submission
                    e.preventDefault();
                    var $form = $(e.target);
                    // Use Ajax to submit form data
                    var fd = new FormData($form[0]);
                    <?php
                    if (isset($familia)) {
                        echo "fd.append('id', $familia->id);";
                    }
                    ?>
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/catalogos/insertUpdateFamila",
                        cache: false,
                        data: fd,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            if (data > 0)
                            {
                                var texto = "Se insertó la Categoría";
                                var url = "<?php echo base_url(); ?>index.php/catalogos/familias";

                            <?php
                            if (isset($familia)) { // Si es Edicion
                                echo "texto='Se actualizó la Categoría'; ";
                                echo "url='" . base_url() . "index.php/catalogos/familias/';";
                            }
                            ?>

                                swal({
                                    title: 'Exito!',
                                    text: texto,
                                    type: 'success',
                                    showCancelButton: false,
                                    allowOutsideClick: false
                                }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        window.location = url;
                                    }
                                }).catch(swal.noop);
                            } else {
                                swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error")
                            }
                        }
                    }); // fin ajax
                })
                .formValidation({
                    framework: 'bootstrap',
                    icon: {
                        valid: 'fa fa-check',
                        invalid: 'fa fa-times',
                        validating: 'fa fa-refresh'
                    },
                    fields: {
                        clave: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese un Clave"
                                }
                            }
                        }
                    }
                });
        // FIN VALIDADOR
//        toolbar= [
//            // [groupName, [list of button]]
//            ['style', ['bold', 'italic', 'underline', 'clear']],
//            ['font', ['superscript', 'subscript']],
//            ['fontsize', ['fontsize']],
//            ['color', ['color']],
//            ['para', ['ul', 'ol', 'paragraph']],
//            ['insert',['table','hr']]
//          ];
          
          $('#summernote').richText({
              imageUpload: false,
              fileUpload: false,
              videoEmbed: false,
              urls: false,
              code: false
        });
    });
    
    function add_alcance(){
        $("#modal_alcance").modal("hide");
        //var markupStr = $('#summernote').summernote('code')+ '<input name="alcances[]" value="'+$('#summernote').summernote('code')+'" hidden />';
        var markupStr=$('#summernote').val();
        //console.log(markupStr);
        var btn='<button type="button" class="btn btn-sm btn-raised btn-outline-danger btnRemove"><i class="ft-minus"></i></button>';
        $("#table_alcances").append("<tr><td>"+markupStr+"<input name='alcances[]' value='"+$('#summernote').val()+"' hidden /></td><td>"+btn+"</td></tr>");
    }
    
    function add_documento(){
        var markupStr = $('#documento').val()+ '<input name="documentos[]" value="'+$('#documento').val()+'" hidden />';
        var btn='<button type="button" class="btn btn-sm btn-raised btn-outline-danger btnRemove"><i class="ft-minus"></i></button>';
        $("#table_documentos").append("<tr><td>"+markupStr+"</td><td>"+btn+"</td></tr>");
    }
    
    function lanzar_modal(){
        $("#modal_alcance").modal("show");
        
//         $('#summernote').summernote({
//            lang: 'es-ES',
//            height: 300,
//            disableDragAndDrop: true,
//            toolbar: toolbar
//        });
    }

</script>
