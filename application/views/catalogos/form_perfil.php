
<div class="main-content">
    <div class="content-wrapper">
        <div class="col-sm-12">
            <?php $title="Alta"; 
                if(isset($perfiles)){
                    $title="Edición";
                }
            ?>
            <div class="content-header"><?php echo $title; ?> de Perfil</div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <form class="form" id="form_perfil" method="post">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <h5>Nombre del Perfil<span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="nombre" class="form-control form-control-sm toupper" <?php if(isset($perfiles)){ echo "value='$perfiles->nombre'";} ?> >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h4 class="form-section"><i class="ft-unlock"></i> Seleccione los Permisos del Perfil</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Administrador</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input name="permiso_administrador" type="checkbox" <?php if(isset($permisos_perfil) && $permisos_perfil->permiso_administrador=="on"){echo "checked"; } ?>>
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                             </div>
                        </div>        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Catálogos</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input name="permiso_catalogos" type="checkbox" <?php if(isset($permisos_perfil) && $permisos_perfil->permiso_catalogos=="on"){echo "checked"; } ?>>
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Clientes</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input name="permiso_clientes" type="checkbox" <?php if(isset($permisos_perfil) && $permisos_perfil->permiso_clientes=="on"){echo "checked"; } ?>>
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">        
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Configuraciones</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input name="permiso_configuraciones" type="checkbox" <?php if(isset($permisos_perfil) && $permisos_perfil->permiso_configuraciones=="on"){echo "checked"; } ?>>
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                         </div>   
                         <br>
                        <div class="row">
                            <div class="col-md-12">    
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 style="font-weight: 600;" class="pull-right">Cotizaciones</h5>  
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <div class="row">
                            <div class="col-md-12">    
                                <div class="row">            
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Creación</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input name="permiso_creacion" type="checkbox" <?php if(isset($permisos_perfil) && $permisos_perfil->permiso_creacion=="on"){echo "checked"; } ?>>
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Edición</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input name="permiso_edicion" type="checkbox" <?php if(isset($permisos_perfil) && $permisos_perfil->permiso_edicion=="on"){echo "checked"; } ?>>
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Cambio estatus</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input name="permiso_estatus" type="checkbox" <?php if(isset($permisos_perfil) && $permisos_perfil->permiso_estatus=="on"){echo "checked"; } ?>>
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Envío a cliente</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input name="permiso_envio" type="checkbox" <?php if(isset($permisos_perfil) && $permisos_perfil->permiso_envio=="on"){echo "checked"; } ?>>
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                         </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12"> 
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 style="font-weight: 600;" class="pull-right">Ordenes de Trabajo</h5>  
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12"> 
                                <div class="row">           
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Datos generales</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input name="permiso_datos" type="checkbox" <?php if(isset($permisos_perfil) && $permisos_perfil->permiso_datos=="on"){echo "checked"; } ?>>
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>           
                         <div class="row">
                            <div class="col-md-12"> 
                                <div class="row">                 
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Cancelación de servicios</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input name="permiso_cancelacion" type="checkbox" <?php if(isset($permisos_perfil) && $permisos_perfil->permiso_cancelacion=="on"){echo "checked"; } ?>>
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                              </div>
                            </div>
                        </div>           
                        <div class="row">
                            <div class="col-md-12"> 
                                <div class="row">  
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Programación</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input name="permiso_programacion" type="checkbox" <?php if(isset($permisos_perfil) && $permisos_perfil->permiso_programacion=="on"){echo "checked"; } ?>>
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                              </div>
                            </div>
                        </div>           
                        <div class="row">
                            <div class="col-md-12"> 
                                <div class="row">              
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Cambio de estatus (Servicio)</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input name="permiso_estatus_servicio" type="checkbox" <?php if(isset($permisos_perfil) && $permisos_perfil->permiso_estatus_servicio=="on"){echo "checked"; } ?>>
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-md-12"> 
                                <div class="row">  
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Enviar Programación</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input name="permiso_program_env" type="checkbox" <?php if(isset($permisos_perfil) && $permisos_perfil->permiso_program_env=="on"){echo "checked"; } ?>>
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                              </div>
                            </div>
                        </div>          

                         <div class="row">
                            <div class="col-md-12"> 
                                <div class="row">              
                                    <div class="col-md-6">
                                        <h5 class="pull-right">PDF orden de trabajo</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input name="permiso_pdf" type="checkbox" <?php if(isset($permisos_perfil) && $permisos_perfil->permiso_pdf=="on"){echo "checked"; } ?>>
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>         
                        <br>
                        <div class="row">
                            <div class="col-md-12"> 
                                <div class="row">         
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Agenda</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input name="permiso_agenda" type="checkbox" <?php if(isset($permisos_perfil) && $permisos_perfil->permiso_agenda=="on"){echo "checked"; } ?>>
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <br><hr>
                        <div class="row">
                            <a href="<?php echo base_url(); ?>index.php/catalogos/perfiles" class="btn btn-icon btn-secondary"><i class="ft-chevron-left"></i> Regresar</a>
                            <button type="submit" class="btn gradient-green-teal shadow-z-1 white" style="width: 50%; margin-left: 15%;">Guardar <i class="fa fa-save"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
     $(document).ready(function () {
         $(".toupper").on("change",function(){
            $(this).val($(this).val().toUpperCase()); 
         });
         
        // VALIDACION
        $("#form_perfil")
                .on('success.form.fv', function (e) {
                    // Prevent form submission
                    e.preventDefault();
                    var $form = $(e.target);
                    // Use Ajax to submit form data
                    var fd = new FormData($form[0]);
                    <?php if(isset($perfiles)){
                        echo "fd.append('id', $perfiles->id);";
                    }?>
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/catalogos/insertUpdatePerfil",
                        cache: false,
                        data: fd,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            if (data==1)
                            {
                                var texto="Se insertó el Perfil";
                                var url="<?php echo base_url(); ?>index.php/catalogos/perfiles";
                                
                                <?php if(isset($perfiles)){ // Si es Edicion
                                    echo "texto='Se actualizó el Perfil'; ";
                                    echo "url='".base_url()."index.php/catalogos/edicion_perfil/".$perfiles->id."';";
                                } ?>
  
                                swal({
                                    title: 'Exito!',
                                    text: texto,
                                    type: 'success',
                                    showCancelButton: false,
                                    allowOutsideClick: false
                                }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        window.location = url;
                                    }
                                }).catch(swal.noop);
                            }
                            else{
                                swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error")
                            }
                        }
                    }); // fin ajax
                })
                .formValidation({
                    framework: 'bootstrap',
                    icon: {
                        valid: 'fa fa-check',
                        invalid: 'fa fa-times',
                        validating: 'fa fa-refresh'
                    },
                    fields: {
                        nombre: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese un Nombre"
                                }
                            }
                        }
                    }
                });
                // FIN VALIDADOR
    });

</script>
