<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">Catálogo de Personal</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <h5><i class="ft-file-text"></i> Listado de Empleados</h5><hr>
                        <a href="<?php echo base_url(); ?>index.php/catalogos/alta_empleado" class=" pull-right btn gradient-green-teal shadow-z-1 white"><i class="ft-user-plus"></i> Agregar Empleado </a><br><br>
                        <table class="table table-striped table-condensed table-hover table-responsive" id="tabla">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Dirección</th>
                                    <th>Teléfono</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<div id="visualModal" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
            <div class="modal-content ">
              <div class="modal-header">
                <h4 class="modal-title">Permisos del Perfil</h4>
              </div>
                   <!---->
                  <div class="col-md-12"> 
                        <form>
                             <form class="form" id="form-empleado" method="post">
                        <h4 class="form-section"><i class="ft-file-text"></i> Datos del Empleado</h4>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <h5>Nombre <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="nombre" class="form-control form-control-sm toupper" <?php if(isset($empleado)){ echo "value='$empleado->nombre'";} ?> >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Correo Electrónico <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="email" name="correo" class="form-control form-control-sm" <?php if(isset($empleado)){ echo "value='$empleado->correo'";} ?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Teléfono <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="telefono" class="form-control form-control-sm" <?php if(isset($empleado)){ echo "value='$empleado->telefono'";} ?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Dirección <span class="required">*</span></h5>
                                    <div class="controls">
                                        <textarea name="direccion" class="form-control form-control-sm toupper" rows="3"><?php if(isset($empleado)){ echo $empleado->direccion;} ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Perfil <span class="required">*</span></h5>
                                    <div class="controls">
                                        <select type="text" name="perfil" class="form-control form-control-sm" >
                                            <?php                                           
                                            foreach ($perfiles as $p) {
                                                $sel="";
                                                if(isset($empleado) && $empleado->perfil==$p->id){
                                                    $sel="selected";
                                                }
                                                echo "<option value='$p->id' $sel>$p->nombre</option>";
                                            } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                 <div class="form-group">
                                    <h5>Clave <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="clave" class="form-control form-control-sm toupper" <?php if(isset($empleado)){ echo "value='$empleado->clave'";} ?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Empresa <span class="required">*</span></h5>
                                    <div class="controls">
                                        <select type="text" name="empresa" class="form-control form-control-sm" >
                                            <?php                                           
                                            foreach ($proveedores as $p) {
                                                $sel="";
                                                if(isset($empleado) && $empleado->empresa==$p->id){
                                                    $sel="selected";
                                                }
                                                echo "<option value='$p->id' $sel>$p->nombre</option>";
                                            } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Departamento <span class="required">*</span></h5>
                                    <div class="controls">
                                        <select type="text" name="departamento" class="form-control form-control-sm" >
                                            <?php                                           
                                            foreach ($departamentos as $d) {
                                                $sel="";
                                                if(isset($empleado) && $empleado->departamento==$d->nombre){
                                                    $sel="selected";
                                                }
                                                echo "<option $sel>$d->nombre</option>";
                                            } ?>
                                            <option <?php if(isset($empleado) && $empleado->departamento=="ADMINISTRADOR"){ echo "selected"; } ?>>ADMINISTRADOR</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Zona <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="zona" class="form-control form-control-sm toupper" <?php if(isset($empleado)){ echo "value='$empleado->zona'";} ?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Usuario <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="usuario" class="form-control form-control-sm" <?php if(isset($empleado)){ echo "value='$empleado->usuario'";} ?>>
                                    </div>
                                </div>
                            </div>
                        </div> 
                         <br>
                             <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                             </div>         
                            </form>  
                        </div>     
       </div>
  </div>
</div>
<script>
    function load() {
        table.destroy();
        table = $('#tabla').DataTable({
            "ajax": {
                "url": "<?php echo base_url(); ?>index.php/catalogos/getEmpleados"
            },
            "columns": [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": "<i class='ft-chevron-down' aria-hidden='true'></i>"
                },
                {"data": "id"},
                {"data": "nombre"},
                {"data": "direccion"},
                {"data": "telefono"},
                {"data": null,
                    render:function(data,type,row){
                        var html='';
                            html+=" <button type='button' class='btn btn-sm btn-icon gradient-green-teal white edit'><i class='ft-edit'></i></button>";
                            <?php  if($id_usuario==1){ ?>
                            html+=" <button type='button' class='btn btn-sm btn-raised gradient-ibiza-sunset white' onclick='deleteemple("+row.id+")'><i class='fa fa-trash-o'></i></button>";
                            <?php  } ?>

                        return html;
                    }
                }
            ]
        });

    }
    $(document).ready(function () {
        table = $('#tabla').DataTable();

        //Listener para edicion
        $('#tabla tbody').on('click', 'button.edit', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = row.data();
            window.location = '<?php echo base_url(); ?>index.php/catalogos/edicion_empleado/'+data.id;
        });

        load();


/*
        $('#tabla').on('click', 'button.visual', function(){
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var dato = row.data();

      $('#visualModal').modal('show'); 
          $.ajax({
          type: 'ajax',
          method: 'get',
          url: '<?php echo base_url() ?>index.php/catalogos/visual_empleado',
          data: {id:dato.id},
          async: false,
          dataType: 'json',
          success: function(data)
          {
            
          $('input[name=nombre]').val(data.nombre);  
          $('input[name=correo]').val(data.correo);  
          $('input[name=telefono]').val(data.telefono);  
          $('input[name=direccion]').val(data.direccion);  
          $('input[name=perfil]').val(data.perfil);  
          $('input[name=clave]').val(data.clave);  
          $('input[name=empresa]').val(data.empresa);  
          $('input[name=departamento]').val(data.nombre);  
          $('input[name=zona]').val(data.zona);  
          $('input[name=usuario]').val(data.usuario);  
          },
          error: function(){
            alert('no se muestran los datos');
          }
      });
      });
 */     
    });
    function deleteemple(id){
        $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar el empleado?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: "<?php echo base_url() ?>index.php/Catalogos/eliminaremple",
                    data: {
                        id:id
                    },
                    success:function(response){  
                        console.log(response);
                        
                            swal("Éxito!", "Se ha Eliminado con exito", "success");
                            load();
                        

                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
    }
</script>