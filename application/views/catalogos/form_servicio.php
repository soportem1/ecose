<?php
    $empresa = $this->session->userdata("empresa");
?>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<div class="main-content">
    <div class="content-wrapper">
        <div class="col-sm-12">
            <?php $title="Alta"; 
                if(isset($servicio)){
                    $title="Edición";
                    echo '<input id="servicio_idin" type="hidden" echo "value='.$servicio->clave.'"; >';
                }
            ?>
            <div class="content-header"><?php echo $title; ?> de Servicio</div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <form class="form" id="form-servicio" method="post">
                        <h4 class="form-section"><i class="ft-plus-circle"></i> Datos del Servicio</h4>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <p>Clave <span class="required">*</span></p>
                                    <div class="controls">
                                        <input type="text" name="clave" class="form-control" <?php if(isset($servicio)){ echo "value='$servicio->clave'";} ?> >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p>Categoría <span class="required">*</span></p>
                                    <div class="controls">
                                        <select name="familia" class="form-control" >
                                            <?php foreach ($familias as $f) {
												$sel="";
												if(isset($servicio) && $servicio->familia==$f->id){
													$sel="selected";
												}
                                                echo "<option value='$f->id' $sel>$f->clave - $f->nombre</option>";
                                            }?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p>Nombre del Servicio <span class="required">*</span></p>
                                    <div class="controls">
                                        <input type="text" name="nombre" class="form-control toupper" <?php if(isset($servicio)){ echo "value='$servicio->nombre'";} ?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p>Empresa <span class="required">*</span></p>
                                    <select name="proveedor" class="form-control" >
                                        <?php 
                                        foreach ($proveedores as $p) {
                                            $sel="";
                                            if(isset($servicio) && $servicio->proveedor==$p->id){
                                                $sel="selected";
                                            }
                                            echo "<option ".$sel." value='$p->id' $sel>$p->nombre</option>";

                                            /*if($empresa=="1" || $empresa=="2" || $empresa=="3" || $empresa=="6"){  //ecose
                                                if($p->id==1 || $p->id==2 || $p->id==3 || $p->id==6){
                                                    echo "<option ".$sel." value='$p->id'>$p->nombre</option>";
                                                }
                                            }else if($empresa=="4" && $p->id==4){ //ahisa
                                                echo "<option ".$sel." value='$p->id'>$p->nombre</option>";
                                            }else if($empresa=="5" && $p->id==5){ //auven
                                                echo "<option ".$sel." value='$p->id'>$p->nombre</option>";
                                            }*/
                                        } 
                                        ?>
                                    </select>
                                </div>
                                 
                            </div>
                            <div class="col-md-6">
                                
                                <div class="form-group">
                                    <p>Tamaño <span class="required">*</span></p>
                                    <select name="tamano" class="form-control" >
                                        <option value="0" <?php if(isset($servicio) && $servicio->tamano==0){ echo "selected"; } ?>>N/A</option>
                                        <option value="1" <?php if(isset($servicio) && $servicio->tamano==1){ echo "selected"; } ?>>Chica</option>
                                        <option value="2" <?php if(isset($servicio) && $servicio->tamano==2){ echo "selected"; } ?>>Mediana</option>
                                        <option value="3" <?php if(isset($servicio) && $servicio->tamano==3){ echo "selected"; } ?>>Grande</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <p>Recipientes sujetos a Presión<span class="required">*</span></p>
                                    <select name="recipientes" class="form-control" >
                                        <option value="0" <?php if(isset($servicio) && $servicio->recipientes==0){ echo "selected"; } ?>>N/A</option>
                                        <option value="1" <?php if(isset($servicio) && $servicio->recipientes==1){ echo "selected"; } ?>>CAT 1</option>
                                        <option value="2" <?php if(isset($servicio) && $servicio->recipientes==2){ echo "selected"; } ?>>CAT 2</option>
                                        <option value="3" <?php if(isset($servicio) && $servicio->recipientes==3){ echo "selected"; } ?>>CAT 3</option>
                                    </select>
                                </div>

                               <div class="form-group">
                                    <p>Precio por puntos </p>
                                    <select name="por_puntos" id="puntos" class="form-control" >
                                        <option value="0" <?php if(isset($servicio) && $servicio->por_puntos==0){echo "selected";} ?>>No</option>
                                        <option value="1" <?php if(isset($servicio) && $servicio->por_puntos==1){echo "selected";} ?>>Si</option>
                                    </select>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-4">
                                        <p>Aplica Descuento</p>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input name="aplica_desc" value="1" <?php if(isset($servicio) && $servicio->aplica_desc==1){ echo "checked"; } ?> type="checkbox">
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                                
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-12">
                               <div class="form-group">
                                    <p>Descripción </p>
                                    <div class="controls">
                                        <div class="controls">
                                            <textarea  id="summernote" class="form-control" rows="25"><?php 
                                                if(isset($servicio)){ 
                                                $desc2 = str_replace("<br>", "\n", $servicio->descripcion);
                                                $desc = str_replace(" <br> ", "\n", $desc2);
                                                echo $desc; } ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h5><i class="ft-file-text"></i> Documentación</h5>
                        <div style="height: 5px" class="gradient-green-teal"></div><br>
                        <div class="form-group row">
                            <div class="col-sm-4">
                                <select id="documento" class="form-control form-control-sm">
                                <?php
                                foreach ($documentos as $d) {
                                    echo "<option data-name='$d->nombre' value=".$d->id.">$d->nombre</option>";
                                }
                                ?>
                                </select>
                            </div>
                            <button type="button" onclick="add_documento()" class="btn btn-sm btn-raised btn-outline-primary"><i class="ft-plus"></i> Agregar Doc.</button>
                        </div>
                        <hr>
                        <div class="overflow-auto">
                            <table class="table table-sm table-hover table-striped" id="tabla_docs">
                                <thead>
                                    <tr>
                                        <td>Documentos</td>
                                        <td></td>
                                    </tr>
                                </thead>
                                <tbody id="table_documentos">
                                    <?php 
                                    if(isset($docs)){
                                    foreach($docs as $d){ ?>
                                    <tr>
                                        <td>
                                            <?php echo $d->documento; ?>
                                            <input id="id_documento" value="<?php echo $d->id_documento; ?>" hidden />
                                            <input id="id_sd" value="<?php echo $d->id; ?>" hidden />
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-sm btn-raised btn-outline-danger btnRemove"><i class="ft-minus"></i></button>
                                        </td>
                                    </tr>
                                    <?php } } ?>
                                </tbody>
                            </table>
                        </div>
                        <h4 class="form-section"><i class="fa fa-dollar"></i> Precios de Servicio</h4>
                        <?php if($this->session->userdata("empresa")!=4 && $this->session->userdata("empresa")!=5 ){
                            foreach ($sucursales as $s) { ?>

                                <p>Sucursal <?php echo $s->nombre;?></p>

                                <!-- ----------------------------------PRECIOS X SUCURSAL----------------------------------------- -->

                                <?php
                                    //tomamos el precio correspondiente a la sucursal
                                    $precio_sucursal=0;
                                    if(isset($precios_sucursal)){
                                        foreach ($precios_sucursal as $p) {
                                            if($p->sucursal_id==$s->id){
                                                $precio_sucursal=$p->precio;
                                            }
                                        }
                                    }

                                ?>
                                <div class="form-group precio_unico <?php if(isset($servicio) && $servicio->por_puntos==1){echo "d-none";} ?>" id="" >
                                    <p>Precio <span class="required">*</span></p>
                                    <div class="controls">
                                        <input name="sucursal_precio[]" value="<?php echo $s->id; ?>" hidden>
                                        <input type="text" name="precio[]" class="form-control" <?php  echo "value='$precio_sucursal'"; ?>>
                                    </div>
                                </div>

                                <!-- -------------------------------PRECIOS POR PUNTOS X SUCURSAL------------------------------------- -->

                                <div class="precios_varios <?php if((isset($servicio) && $servicio->por_puntos==0) || !isset($servicio)){echo 'd-none';} ?>"  >
                                    <p>Rango de Precios:</p>
                                    <button onclick="add_rango($(this),<?php echo $s->id; ?>)" type="button" class="btn btn-icon btn-sm  btn-success"><i class="ft-plus"></i> Agregar rango</button>
                                    
                                    <?php

                                    //creamos un array temporal con los precios por sucursal tomado del array general de precios de sucursal
                                    $size=0; $i=0; $precios_temp=array();

                                    if(isset($precios_puntos)){
                                        foreach ($precios_puntos as $p) {
                                            if($p->sucursal_id==$s->id){
                                                array_push($precios_temp, $p);
                                                $size++;
                                            }
                                        }
                                    }
                                    do{
                                    ?>
                                        <div class="row">
                                            <div class="col-3">
                                                <input name="sucursal_rango[]" value="<?php echo $s->id; ?>" hidden>
                                                <input type="text" name="rango_inicio[]" placeholder="Desde" class="form-control" <?php if($size>0){ echo "value='".$precios_temp[$i]->rango_inicio."'";} ?>>
                                            </div>
                                            <div class="col-1">A</div>
                                            <div class="col-3">
                                                <input type="text" name="rango_fin[]" placeholder="Hasta" class="form-control" <?php if($size>0){ echo "value='".$precios_temp[$i]->rango_fin."'";} ?>>
                                            </div>
                                            <div class="col-1">=</div>
                                            <div class="col-3">
                                                <input type="text" name="precio_rango[]" class="form-control" <?php if($size>0){ echo "value='".$precios_temp[$i]->precio."'";} ?>>
                                            </div>
                                            <div class="col-1"><button type="button" onclick="del_rango($(this))" class="btn btn-sm btn-danger "><i class="ft-minus"></i></button></div>
                                        </div>
                                    <?php
                                        
                                    $i++;
                                    }
                                    while($i<$size);
                                    ?>
                                    
                                </div>
                        <?php }  

                        }else{ ?>
                            <div class="form-group col-md-6">
                                <p>Precio Consultor <span class="required">*</span></p>
                                <div class="controls">
                                    <input type="text" name="precio_consultor" class="form-control" <?php if(isset($servicio)){ echo "value='$servicio->precio_consultor'";} ?> >
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <p>Precio Directo <span class="required">*</span></p>
                                <div class="controls">
                                    <input type="text" name="precio_directo" class="form-control" <?php if(isset($servicio)){ echo "value='$servicio->precio_directo'";} ?> >
                                </div>
                            </div>
                        <?php } ?>

                        <br><hr>
                        <div class="row">
                            <a href="<?php echo base_url(); ?>index.php/Servicios" class="btn btn-icon btn-secondary"><i class="ft-chevron-left"></i> Regresar</a>
                            <button type="submit" class="btn gradient-green-teal shadow-z-1 white" style="width: 50%; margin-left: 15%;">Guardar <i class="fa fa-save"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="template" class="row d-none">
    <div class="col-3">
        <input name="sucursal_rango[]" class="input-sucursal" hidden>
        <input type="text" name="rango_inicio[]" placeholder="Desde" class="form-control" >
    </div>
    <div class="col-1">A</div>
    <div class="col-3">
        <input type="text" name="rango_fin[]" placeholder="Hasta" class="form-control" >
    </div>
    <div class="col-1">=</div>
    <div class="col-3">
        <input type="text" name="precio_rango[]" class="form-control" >
    </div>
    <div class="col-1"><button type="button" onclick="del_rango($(this))" class="btn btn-sm btn-danger "><i class="ft-minus"></i></button></div>
</div>


<script>

    <?php if(isset($servicio)){
        echo "var id_servicioa = $servicio->id;";
    }?>
 
     $(document).ready(function () {
        // VALIDACION
        
        $("#puntos").on("change", function(){
            $(".precio_unico").toggleClass("d-none");
            $(".precios_varios").toggleClass("d-none");
            $(".rango-added").remove();
        });
        
        $(".toupper").on("change",function(){
            $(this).val($(this).val().toUpperCase());
        });
        
        /*$('#summernote').richText({
              imageUpload: false,
              fileUpload: false,
              videoEmbed: false,
              urls: false,
              code: false
        });*/ //se comenta por temas de espacios y caracteres, tablas, divs, que copian y pegan de otro documento a esta área
        //CKEDITOR.replace('summernote');

        $("table").on("click",".btnRemove",function(){
           $(this).closest("tr").remove();
        });

        $("#form-servicio")
                .on('success.form.fv', function (e) {
                    // Prevent form submission
                    e.preventDefault();
                    var $form = $(e.target);
                    // Use Ajax to submit form data
                    var fd = new FormData($form[0]);
                    <?php if(isset($servicio)){
                        echo "fd.append('id', $servicio->id);";
                    }?>
                    var aplica_desc=0;
                    if($("input:checkbox[name*='aplica_desc']").is(":checked")==true){
                        aplica_desc=1;  
                    }
                    fd.append('aplica_desc', aplica_desc);
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/Servicios/insertUpdateServicio/",
                        cache: false,
                        data: fd,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            var id_serv=data;
                            console.log("id_serv: "+id_serv);
                            if (parseInt(id_serv)>=1)
                            {
                                if ($("#tabla_docs tbody > tr").length>0) {
                                    var DATA  = [];
                                    var TABLA   = $("#tabla_docs tbody > tr");
                                    TABLA.each(function(){         
                                        item = {};
                                        item ["id_documento"] = $(this).find("input[id*='id_documento']").val()
                                        item ["id"] = $(this).find("input[id*='id_sd']").val()
                                        item ["id_servicio"] = id_serv;
                                        DATA.push(item);
                                    });
                                    INFO  = new FormData();
                                    aInfo   = JSON.stringify(DATA);
                                    INFO.append('data', aInfo);
                                    $.ajax({
                                        data: INFO,
                                        type: 'POST',
                                        url : '<?php echo base_url(); ?>index.php/Servicios/submit_docs_serv',
                                        processData: false, 
                                        contentType: false,
                                        async: false,
                                        statusCode:{
                                            404: function(data_docs){
                                                //toastr.error('Error!', 'No Se encuentra el archivo');
                                            },
                                            500: function(){
                                                //toastr.error('Error', '500');
                                            }
                                        },
                                        success: function(data_docs){
                                        }
                                    }); 
                                }

                                var texto="Se insertó el Servicio";
                                var url="<?php echo base_url(); ?>index.php/Servicios/";
                                
                                <?php if(isset($servicio)){ // Si es Edicion
                                    echo "texto='Se actualizó el Servicio'; ";
                                    echo "url='".base_url()."index.php/Servicios/edicion_servicio/".$servicio->id."';";
                                } ?>
                                var id_act = parseInt(id_serv);
                                var datos = $("#summernote").val();
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url(); ?>index.php/Servicios/updateDescrip",
                                    cache: false,
                                    data: { 'id':id_act,'descrip':datos},
                                    success: function (data) {
                                        console.log("descripcion editada");
                                    }
                                }); // fin ajax 
                                swal({
                                    title: 'Exito!',
                                    text: texto,
                                    type: 'success',
                                    showCancelButton: false,
                                    allowOutsideClick: false
                                }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        window.location = "<?php echo base_url(); ?>index.php/Servicios/";
                                    }
                                }).catch(swal.noop);
                            }
                            else{
                                swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error")
                            }
                        }
                    }); // fin ajax
                })
                .formValidation({
                    framework: 'bootstrap',
                    icon: {
                        valid: 'fa fa-check',
                        invalid: 'fa fa-times',
                        validating: 'fa fa-refresh'
                    },
                    fields: {
                        nombre: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese un Nombre"
                                }
                            }
                        },
                        clave: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese un la Clave"
                                }
                            }
                        }
                    }
                });
                // FIN VALIDADOR
    });
    
    function actualizaDescrip(){
        var datos = $("#summernote").val();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>index.php/Servicios/updateDescrip",
            cache: false,
            data: { id:$("#servicio_idin").val(),descrip: $("#summernote").val()},
            /*"data": function(d){
                d.id = $("#servicio_idin").val(),
                d.descrip = $('#summernote').val()
            },*/
            processData: false,
            contentType: false,
            success: function (data) {
                console.log("descripcion editada");
            }
        }); // fin ajax 
    }

    function add_documento(){
        var markupStr = $('#documento option:selected').data("name")+ '<input id="id_documento" value="'+$('#documento').val()+'" hidden /> <input id="id_sd" value="0" hidden />';
        var btn='<button type="button" class="btn btn-sm btn-raised btn-outline-danger btnRemove"><i class="ft-minus"></i></button>';
        $("#table_documentos").append("<tr><td>"+markupStr+"</td><td>"+btn+"</td></tr>");
    }

    function add_rango(btn,sucursal){
        $row=$("#template").clone();
        $row.removeAttr('id').removeClass('d-none');
        $row.find('.input-sucursal').val(sucursal);
        $row.insertAfter(btn);
    }
    
    function del_rango(btn){
        btn.closest(".row").remove();
    }



</script>
