<?php
    $id_usuario = $this->session->userdata("id_usuario");
    $empresa = $this->session->userdata("empresa");
?>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">Catálogo de Servicios</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <h5><i class="ft-file-text"></i> Listado de Servicios</h5><hr>
                        <!--<div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Empresa</p>
                                    <select id="proveedor" class="form-control" onchange="load()">
                                        <option value="0">Todo</option>
                                        <?php 
                                        foreach ($proveedores as $p) {
                                            if($id_usuario=="1" || $id_usuario=="12"){
                                                echo "<option value='$p->id'>$p->nombre</option>";
                                            }
                                            else if($id_usuario!="1" && $empresa=="1" && $p->id==1 || $id_usuario!="12" && $empresa=="1" && $p->id==1
                                                || $id_usuario!="1" && $empresa=="2" && $p->id==2 || $id_usuario!="12" && $empresa=="2" && $p->id==2 
                                                || $id_usuario!="1" && $empresa=="3" && $p->id==3 || $id_usuario!="12" && $empresa=="3" && $p->id==3
                                                || $id_usuario!="1" && $empresa=="6" && $p->id==6 || $id_usuario!="12" && $empresa=="6" && $p->id==6){  //ecose
                                                echo "<option value='$p->id'>$p->nombre</option>";
                                            }else if($id_usuario!="1" && $empresa=="4" && $p->id==4 || $id_usuario!="12" && $empresa=="4" && $p->id==4){ //ahisa
                                                echo "<option value='$p->id'>$p->nombre</option>";
                                            }else if($id_usuario!="1" && $empresa=="5" && $p->id==5 || $id_usuario!="12" && $empresa=="5" && $p->id==5){ //auven
                                                echo "<option value='$p->id'>$p->nombre</option>";
                                            }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>-->
                        <a href="<?php echo base_url(); ?>index.php/servicios/alta_servicio" class=" pull-right btn gradient-green-teal shadow-z-1 white"><i class="fa fa-plus-circle"></i> Agregar Servicio </a><br><br>
                        <table class="table table-striped table-condensed table-hover table-responsive" id="tabla">
                            <thead>
                                <tr>
                                    <th>Clave</th>
                                    <th>Familia</th>
                                    <th>Nombre</th>
                                    <th>Tamaño</th>
                                    <th>Descripcion</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<script>
    var id_usu = <?php echo $this->session->userdata("id_usuario"); ?>;
    $(document).ready(function () {
        table = $('#tabla').DataTable();

        //Listener para edicion
        $('#tabla tbody').on('click', 'button.edit', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = row.data();
            window.location = '<?php echo base_url(); ?>index.php/Servicios/edicion_servicio/'+data.id;
        });

        //Listener para eliminar
        $('#tabla tbody').on('click', 'button.delete', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = row.data();
            delete_record(data.id);
            
        });

        load();
    });

    function load() {
        //table.destroy();
        table = $('#tabla').DataTable({
            destroy:true,
            "ajax": {
                "url": "<?php echo base_url(); ?>index.php/Catalogos/getServicios",
                type: "post",
                data: { id_prov: $("#proveedor option:selected").val() },
            },
            "columns": [
                {"data": "clave"},
                {"data": "familia"},
                {"data": "nombre"},
                {"data": "tamano",
                "render": function ( data, type, row, meta ) {
                    var ret="N/A";
                    switch(data+""){
                        case "1" : ret="CH"; break;
                        case "2" : ret="MD"; break;
                        case "3" : ret="GD"; break;
                    }
                  return ret;}
                  },
                {"data": "descripcion"},
                /*{
                    "data": null,
                    "defaultContent": "<button type='button' class='btn btn-sm btn-icon gradient-green-teal white edit'><i class='ft-edit'></i></button> <button type='button' class='btn btn-sm btn-icon btn-danger white delete'><i class='ft-trash'></i></button>"
                }*/
                {"data": "null", 
                    "render": function ( data, type, row, meta ) {
                        if(id_usu>1){//es admin
                           tipo="<button type='button' class='btn btn-sm btn-icon gradient-green-teal white edit'><i class='ft-edit'></i></button> <button type='button' disabled class='btn btn-sm btn-icon btn-danger white delete'><i class='ft-trash'></i></button"; 
                        }
                        else{ //es usuario no admin
                            tipo="<button type='button' class='btn btn-sm btn-icon gradient-green-teal white edit'><i class='ft-edit'></i></button> <button type='button' class='btn btn-sm btn-icon btn-danger white delete'><i class='ft-trash'></i></button";
                        }
                        return tipo;
                    }
                },
            ]
        });

    }

    function delete_record(id) {
        swal({
            title: "Eliminar",
            text: "¿Desea eliminar este elemento?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Eliminar"
        }).then(function(t){t &&
            $.ajax({
                     type: "POST",
                     url: '<?php echo base_url(); ?>index.php/Servicios/eliminar_servicio/'+id,
                     success: function (result) {
                        console.log(result);
                        if(result==0){
                            swal("Error!", "No se puede realizar esta acción porque no cuenta con los permisos", "success");   
                        }
                        else{
                            table.ajax.reload();
                            swal("Exito!", "Se ha eliminado correctamente", "success");    
                        }
                        
                     }
                 });
        });
    }

</script>