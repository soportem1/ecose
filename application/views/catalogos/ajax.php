<script>
    function getEstadosAjax() {
        $.ajax({
            type: "POST",
            traditional: true,
            url: "<?php echo base_url(); ?>index.php/catalogos/getEstados",
            data: {pais: $("#slcPais").val()},
            success: function (data) {
                $("#slcEstados").empty();
                $("#slcEstados").append(data).trigger('chosen:updated');
            }
        });
    }
</script>
