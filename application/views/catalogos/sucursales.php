<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">Catálogo de Sucursales</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <h5><i class="ft-file-text"></i> Listado de Sucursales</h5><hr>
                        <a href="<?php echo base_url(); ?>index.php/catalogos/alta_sucursal" class=" pull-right btn gradient-green-teal shadow-z-1 white"><i class="fa fa-plus-circle"></i> Agregar Sucursal </a><br><br>
                        <table class="table table-striped table-condensed table-hover table-responsive" id="tabla">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>% de variación</th>
                                    <th>Teléfono</th>
                                    <th>Dirección</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>  

<script>
    function load() {
        table.destroy();
        table = $('#tabla').DataTable({
            "ajax": {
                "url": "<?php echo base_url(); ?>index.php/catalogos/getSucursales"
            },
            "columns": [
                {"data": "id"},
                {"data": "nombre"},
                {"data": "variacion"},
                {"data": "telefono"},
                {"data": "direccion"},
                {
                    "data": null,
                    "defaultContent": "<button type='button' class='btn btn-sm btn-icon gradient-green-teal white edit'><i class='ft-edit'></i></button><button type='button' class='btn btn-sm btn-icon btn-danger white delete'><i class='ft-trash'></i></button>"
                }
            ]
        });

    }
    $(document).ready(function () {
        table = $('#tabla').DataTable();

        //Listener para edicion
        $('#tabla tbody').on('click', 'button.edit', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = row.data();
            window.location = '<?php echo base_url(); ?>index.php/catalogos/edicion_sucursal/'+data.id;
        });
        $('#tabla tbody').on('click', 'button.delete', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = row.data();
            delete_record(data.id);
            
        });

        load();
    });

    function delete_record(id) {
        swal({
            title: "Eliminar",
            text: "¿Desea eliminar este elemento?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Eliminar"
        }).then(function(t){t &&
            $.ajax({
                     type: "POST",
                     url: '<?php echo base_url(); ?>index.php/Catalogos/eliminar_suc/',
                     data: { id: id }, 
                     success: function (result) {
                        //console.log(result);
                        /*if(result==0){
                            swal("Error!", "No se puede realizar esta acción porque no cuenta con los permisos", "success");   
                        }
                        else{*/
                            load();
                            swal("Exito!", "Se ha eliminado correctamente", "success");    
                        //}
                        
                     }
                 });
        });
    }
</script>