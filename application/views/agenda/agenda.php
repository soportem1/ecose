<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">Agenda</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <h5><i class="ft-calendar"></i> Agenda de Visitas</h5><hr>
                        <div class="row">
                            <!--<button data-toggle="modal" data-target="#modal_evento" style="margin-left: 77%;" class="btn btn-success btn-icon"><i class="ft-plus"></i> Agregar Evento</button>-->
                        </div>
                        <div style="margin-left: 5%; width:90%; height: 50%" id='calendar'></div>
                        <br>
                        <h5><i class="ft-calendar"></i> Información de Evento</h5><hr>
                        <div class="row">
                            <div class="col-md-6">
                                <ul class="list-group">
                                    <li class="list-group-item" id="lb_clie"><strong>Cliente:</strong> </li>
                                    <li class="list-group-item" id="lb_dir"><strong>Dirección:</strong> </li>
                                    <li class="list-group-item" id="lb_tec"><strong>Técnico:</strong> </li>
                                    <li class="list-group-item" id="lb_evento"><strong>Evento:</strong> </li>
                                    <li class="list-group-item" id="lb_hora"><strong>Horario:</strong> </li>
                                    <li class="list-group-item" id="lb_observaciones"><strong>Observaciones:</strong> </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<!--MODAL DE Nuevo Evento ---------------------------------------------------------------------->
<div class="modal fade text-left" id="modal_evento" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label class="modal-title text-text-bold-600">Nuevo Evento</label>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <p>Nombre del evento:</p>
                    <input id="nombre" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <p>Selección de color:</p>
                    <input id="color" width="100px" />
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <p>Inicio:</p>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar-o"></span>
                                </span>
                                <input type='text' id="fecha_inicio" class="form-control form-control-sm pickadate" placeholder="Selecciona la fecha" />
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                                <input type='text' id="hora_inicio" class="form-control form-control-sm timepicker" placeholder="Selecciona la hora" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <p>Fin:</p>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar-o"></span>
                                </span>
                                <input type='text' id="fecha_fin" class="form-control form-control-sm pickadate" placeholder="Selecciona la fecha" />
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                                <input type='text' id="hora_fin" class="form-control form-control-sm timepicker" placeholder="Selecciona la hora" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <p>Observaciones:</p>
                    <textarea rows="2" type="text" id="observaciones" class="form-control"></textarea>
                </div>
                <div class="modal-footer">
                    <button onclick="guardar_evento()" class="btn btn-success">Guardar Evento</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {

        $("#color").spectrum({
            color: '#6aa84f',
            flat: false,
            showInput: true,
            preferredFormat: "hex",
            showPalette: true,
            showPaletteOnly: true,
            palette: [["#f00", "#f90", "#ff0", "#0f0", "#0ff", "#00f", "#90f", "#f0f"], ["#f4cccc", "#fce5cd", "#fff2cc", "#d9ead3", "#d0e0e3", "#cfe2f3", "#d9d2e9", "#ead1dc"], ["#ea9999", "#f9cb9c", "#ffe599", "#b6d7a8", "#a2c4c9", "#9fc5e8", "#b4a7d6", "#d5a6bd"], ["#e06666", "#f6b26b", "#ffd966", "#93c47d", "#76a5af", "#6fa8dc", "#8e7cc3", "#c27ba0"], ["#c00", "#e69138", "#f1c232", "#6aa84f", "#45818e", "#3d85c6", "#674ea7", "#a64d79"], ["#900", "#b45f06", "#bf9000", "#38761d", "#134f5c", "#0b5394", "#351c75", "#741b47"], ["#600", "#783f04", "#7f6000", "#274e13", "#0c343d", "#073763", "#20124d", "#4c1130"]]
        });

        $('.pickadate').pickadate({
            monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dec'],
            weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
            today: 'Hoy',
            clear: 'Limpiar',
            close: 'Cerrar',
            format: "yyyy-mm-dd"
        });

        $('.timepicker').pickatime({
            format: "HH:i:00",
            formatLabel: "HH:i a ",
            min: [7, 0],
            max: [20, 0]
        });

        $('#calendar').fullCalendar({
            lang: 'es',
            header: {
                center: 'month,agendaWeek,agendaDay' // buttons for switching between views
            },
            views: {
                month: {buttonText: 'Mes'},
                agendaWeek: {buttonText: 'Semana'},
                agendaDay: {buttonText: 'Día'}
            },
            defaultDate: '<?php echo date("Y-m-d"); ?>',
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            events: '<?php echo base_url(); ?>index.php/agenda/eventos',
            eventClick: function (calEvent) {
                informacion_evento(calEvent.id_ultimo,calEvent.tabla);
            }
        });
    });

    function guardar_evento() {
        var params = {
            nombre: $("#nombre").val(),
            color: $("#color").val(),
            fecha_inicio: $("#fecha_inicio").val(),
            hora_inicio: $("#hora_inicio").val(),
            fecha_fin: $("#fecha_fin").val(),
            hora_fin: $("#hora_fin").val(),
            observaciones: $("#observaciones").val()
        };

        if (params.fecha_inicio != "" && params.fecha_fin != "" && params.hora_inicio != "" && params.hora_fin != "") {
            $.ajax({
                type: "POST",
                traditional: true,
                url: "<?php echo base_url(); ?>index.php/agenda/save_evento/",
                data: params,
                success: function (data) {
                    if (data > 0) {
                        swal('Exito!', 'Se dio de alta el evento en el calendario', 'success');

                        $('#calendar').fullCalendar('refetchEvents');
                        $('#modal_evento').modal("hide");
                    }
                }
            });
        } else {
            swal('', 'Ingrese los valores de Fecha y Hora', 'warning');
        }
    }

    function informacion_evento(id,tabla) {
        //console.log("id cs: "+id);
        $.ajax({
            type: "POST",
            traditional: true,
            url: "<?php echo base_url(); ?>index.php/agenda/get_evento/"+id+"/"+tabla,
            success: function (data) {
                var json = JSON.parse(data);
                $("#lb_clie").html("<strong>Cliente:</strong> "+json.info["empresa"]);
                $("#lb_dir").html("<strong>Dirección:</strong> "+json.info["calle"]+" "+json.info["no_ext"]+", "+json.info["colonia"]+", C.P. "+json.info["cp"]+", "+json.info["poblacion"]+", "+json.info["estado"]);
                $("#lb_tec").html("<strong>Técnico:</strong> "+json.info["nombre"]+ ", Teléfono: " +json.info["telefono"]+ ", Email: " +json.info["correo"]);
                if(json.info["id_empresa_serv"]!=4 && json.info["id_empresa_serv"]!=5){
                    $("#lb_evento").html("<strong>Evento:</strong> "+json.info["evento"]);
                }else if(json.info["id_empresa_serv"]==4){
                    $("#lb_evento").html("<strong>Evento:</strong> "+json.info["evento2"]);
                }else if(json.info["id_empresa_serv"]==5){
                    $("#lb_evento").html("<strong>Evento:</strong> "+json.info["evento3"]);
                }

                $("#lb_hora").html("<strong>Hora:</strong> "+json.info["horario"]);
                $("#lb_observaciones").html("<strong>Observaciones:</strong> "+json.info["observaciones"]);
                
            }
        });
    }
</script>