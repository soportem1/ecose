<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/chartist.min.css">
<script src="<?php echo base_url(); ?>app-assets/vendors/js/chartist.min.js" type="text/javascript"></script>
<?php
    function mes($val){
        switch ($val) {
            case 1:
                return "Ene"; break;
            case 2:
                return "Feb"; break;
            case 3:
                return "Mar"; break;
            case 4:
                return "Abr"; break;
            case 5:
                return "May"; break;
            case 6:
                return "Jun"; break;
            case 7:
                return "Jul"; break;
             case 8:
                return "Ago"; break;
            case 9:
                return "Sep"; break;
            case 10:
                return "Oct"; break;
            case 11:
                return "Nov"; break;
            case 12:
                return "Dic"; break;
            
            default:
                return "Ene";
                break;
        }
    }
?>
<div class="main-content">
    <div class="content-wrapper"><section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <!--<div class="content-header mb-3">ERP Ecose</div>-->
                    <?php $logo =  base_url()."app-assets/img/logo.png";
                    if($this->session->userdata("empresa")=="1" || $this->session->userdata("empresa")=="2" || $this->session->userdata("empresa")=="3" || $this->session->userdata("empresa")=="6") {
                        $logo =  base_url()."app-assets/img/logo.png"; 
                        $color='rgba(0, 201, 255, 1)';
                    }
                    else if($this->session->userdata("empresa")=="4") {
                        $logo = base_url()."app-assets/img/logo_ahisa.png"; 
                        $color = "rgba(232, 99, 0, 1)"; 
                    }
                    else if($this->session->userdata("empresa")=="5") {
                        $logo =  base_url()."app-assets/img/logo_auven2.png";
                        $color = "rgba(14, 50, 100, 1)"; 
                    }
                    ?>
                    <p class="text-center"><img width="200px" src="<?php echo $logo; ?>" /></p>
                </div>
            </div>
            

            <div class="row">
            <div class="col-sm-6">
                <?php 
                    $logueo = $this->session->userdata();
                    $url="#";
                    if($logueo['creacion'] || $logueo['edicion'] || $logueo['estatus'] || $logueo['envio']){
                        $url=base_url()."index.php/cotizaciones";
                    }
                    
                ?>
                <a href="<?php echo $url; ?>">
                    <div class="card">
                        <div class="card-body">
                            <div class="px-3 py-4">
                                <div class="media">
                                    <div class="media-body text-left">
                                        <h3 class="mb-1 black">Cotizaciones</h3>
                                    </div>
                                    <div class="media-right align-self-center">
                                        <i class="ft-layers success font-large-2 float-right"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-6">
                <?php 
                    $url="#";
                    if($logueo['datos'] || $logueo['cancelacion'] || $logueo['programacion'] || $logueo['estatus_servicio'] || $logueo['pdf']){
                        $url=base_url()."index.php/ordenes";
                    }
                ?>
                <a href="<?php echo $url; ?>">
                    <div class="card">
                        <div class="card-body">
                            <div class="px-3 py-4">
                                <div class="media">
                                    <div class="media-body text-left">
                                        <h3 class="mb-1 black">Ordenes de Trabajo</h3>
                                    </div>
                                    <div class="media-right align-self-center">
                                        <i class="ft-clipboard success font-large-2 float-right"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <?php 
            if($this->session->userdata("administrador")){
        ?>
        <!--Line with Area Chart 1 Starts-->
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">COTIZACIONES Realizadas</h4>
                            <small class="text-muted">Ultimos 6 meses</small>
                        </div>
                        <div class="card-body">
                            <div class="card-block">
                                <div id="line-area" class="height-350 lineArea">                        
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <?php 
           } 
        ?>
        </section>
    </div>
</div>

<script>
    $(window).on("load", function () {
    // Line with Area Chart Starts

    var lineArea = new Chartist.Line('#line-area', {
        labels: [
        <?php
            foreach ($grafica as $g) {
                echo "'".mes($g->mes)."',";
            }
        ?>
        ''],
        series: [
            [
            <?php
                foreach ($grafica as $g) {
                    echo "$g->no_cotizaciones,";
                }
            ?>
            0]
        ]
    }, {
            low: 0,
            showArea: true,
            fullWidth: true,
            onlyInteger: true,
            axisY: {
                low: 0,
                scaleMinSpace: 50,
            },
            axisX: {
                showGrid: false
            }
        });

    lineArea.on('created', function (data) {
        var defs = data.svg.elem('defs');
        defs.elem('linearGradient', {
            id: 'gradient1',
            x1: 0,
            y1: 1,
            x2: 1,
            y2: 0
        }).elem('stop', {
            offset: 0,
            'stop-color': '<?php echo $color; ?>'
        }).parent().elem('stop', {
            offset: 1,
            'stop-color': '<?php echo $color; ?>'
        });


    });
    // Line with Area Chart Ends
})
</script>
