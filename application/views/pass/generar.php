<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">Password Dinámicas</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <h5><i class="ft-gears"></i> Generación de contraseñas dinámicas</h5><hr>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <p>Contraseña generada: </p>
                                    <div class="controls">
                                        <input type="text" readonly id="pass_created" class="toupper form-control" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label style="color: transparent;">genera pass para desc</label>
                                <button id="generar" type="button" class="btn btn-success btn-icon"><i class="ft-gear"></i> Generar Contraseña</button>
                            </div>
                            <div class="col-md-6">
      
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="tipo">Tipo</label>
                                <select id="tipo" class="form-control">
                                    <option value="1">Descuentos</option>
                                    <option value="2">Autorizar Cambios (cotizaciones acaptadas)</option>
                                </select>
                            </div>
                            <div class="col-md-4" style="display:none" id="div_num_cot">
                                <div class="form-group">
                                    <label for="id_cot"># Cotización:</label>
                                    <div class="controls">
                                        <input type="text" id="id_cot" class="toupper form-control" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="Vendedor">Vendedor</label>
                                <select id="vendedor" class="form-control">
                                    <option value=""> </option>
                                    <?php foreach ($vendedores as $key) { ?>
                                    <option value="<?php echo $key->id; ?>"><?php echo $key->nombre; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label style="color: transparent;">asignar pass vendedor</label>
                                <button title="Asignar contraseña a vendedor" type="button" id="asignar" class="btn btn-success btn-icon"><i class="ft-gear"></i> Asignar Contraseña</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $("#tipo").on("change",function(){
            if($("#tipo option:selected").val()==2)
                $("#div_num_cot").show();
            else
                $("#div_num_cot").hide();
        });
        $('#vendedor').select2({ width:"98%"});
        $('#generar').click(function(event) {
            var d = new Date();
            var dia=d.getDate();//1 31
            var dias=d.getDay();//0 6
            var mes = d.getMonth();//0 11
            var yy = d.getFullYear();//9999
            var hr = d.getHours();//0 24
            var min = d.getMinutes();//0 59
            var seg = d.getSeconds();//0 59
            var yyy = 18;
            var ram = Math.floor((Math.random() * 30) + 1);

            const  generateRandomString = (num) => {
                const characters ='ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                let result1= Math.random().toString(36).substring(0,num);       
                return result1;
            }
            var codigo=seg+''+min+''+hr+''+yyy+''+ram+''+mes+''+dia+''+dias+generateRandomString(7);
            $('#pass_created').val(codigo);
        });

        $("#asignar").on("click",function(){
            if($("#vendedor option:selected").val()!="" && $("#pass_created").val()!=""){
                var id_coti = 0;
                if($("#tipo option:selected").val()==2){
                    id_coti = $("#id_cot").val();
                }
                $.ajax({
                    type:'POST',
                    url: '<?php echo base_url(); ?>index.php/GenerarPass/asigna_pass_vende',
                    data: { pass: $("#pass_created").val(), id_vendedor: $("#vendedor option:selected").val(), tipo: $("#tipo option:selected").val(), id_cotizacion: $("#id_cot").val() },
                    success:function(data){
                        swal("Éxito", "Asignada Correctamente", "success");
                        $("#pass_created").val("");        
                        $("#id_cot").val("");  
                        $("#vendedor").val("");            
                    }
                });
            }else{
                swal("Error", "Elija un vendedor y/o contraseña", "warning");
            }
        });
    });
</script>