<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">Encuestas de Cliente</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <br>
                        <h5><i class="ft-file-text"></i> Encuestas de <?php echo $cliente->alias; ?></h5><hr>
                        <a href="#" class=" pull-right btn gradient-green-teal shadow-z-1 white" onclick="enviarEncuesta('<?php echo $idcliente; ?>')" title="Enviar mail de encuesta"><i class="fa fa-user-circle"></i> Enviar encuesta a Cliente </a><br><br>
                        <table class="table table-striped table-condensed table-hover table-responsive" id="tabla">
                            <thead>
                                <tr>
                                    <th width="1%"></th>
                                    <th width="59%">Pregunta</th>
                                    <th width="10%">Respuesta</th>
                                    <th width="20%">Fecha de Respuestas</th>
                                    <th width="10%">Ver Respuestas</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="modal_info" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title text-text-bold-600">Información extra de encuesta</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
        
                    </div>
                    <div class="col-md-4"></div>
                    <div class="col-md-4" id="fecha_modal">
                        Fecha:
                    </div>
                </div>
                <table class="table table-condensed table-striped" id="tabla">
                    <?php  $i=0; foreach ($resp as $d) {  ?>
                        <tr id="por_preg">
                            <td width="70%"> 
                                <?php echo $d->pregunta; ?>   
                            </td>
                            <?php if($d->tipo==1){ ?>
                                <td width="30%" style="text-align:center"> <p style="font-weight: bold;"> Escala </p>
                                    <label class="radio-inline"><input disabled type="radio" <?php if($d->respuesta==4) echo "checked"; ?>> 4</label>
                                    <label class="radio-inline"><input disabled type="radio" <?php if($d->respuesta==3) echo "checked"; ?>> 3</label>
                                    <label class="radio-inline"><input disabled type="radio" <?php if($d->respuesta==2) echo "checked"; ?>> 2</label>
                                    <label class="radio-inline"><input disabled type="radio" <?php if($d->respuesta==1) echo "checked"; ?>> 1</label>
                                </td>
                            <?php }
                            else if($d->tipo==2){ ?>
                                <td width="30%" style="text-align:center">
                                    <label class="radio-inline"><input disabled type="radio" <?php if($d->respuesta==1) echo "checked"; ?>> SI</label> 
                                    <label class="radio-inline"><input disabled type="radio" <?php if($d->respuesta==2) echo "checked"; ?>> NO</label> 
                                </td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td width="100%" colspan="3">
                            <div class="form-group">
                                <p>Sugerencias:</p>
                                <textarea readonly rows="2" type="text" id="sugerencias" class="form-control"></textarea>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%" colspan="3">
                            <p>¿Qué servicio quisiera que incluyéramos? </p>
                            <input readonly type="text" id="serv_inclu" class="form-control form-control-sm">
                        </td>
                    </tr>
                    <tr>
                        <hr>
                        <th width="100%" colspan="3" style="text-align: center;">
                            <p> DATOS DEL CLIENTE</p>
                        </th>
                    </tr>
                    <tr>
                        <td width="15%">
                            <p> Empresa:</p>
                        </td>
                        <td width="90%" colspan="2">
                            <input readonly type="text" id="empresa" class="form-control form-control-sm">
                        </td>
                    </tr>
                    <tr>
                        <td width="15%">
                            <p> Nombre:</p>
                        </td>
                        <td width="90%" colspan="2">
                            <input readonly type="text" id="nombre" class="form-control form-control-sm">
                        </td>
                    </tr>
                    <tr>
                        <td width="15%">
                            <p> Servicio contratado:</p>
                        </td>
                        <td width="90%" colspan="2">
                            <input readonly type="text" id="serv" class="form-control form-control-sm">
                        </td>
                    </tr>
                    <tr>
                        <td width="15%">
                            <p> Área a la que pertenece:</p>
                        </td>
                        <td width="90%" colspan="2">
                            <input readonly type="text" id="area" class="form-control form-control-sm">
                        </td>
                    </tr>
                    <tr>
                        <td width="15%">
                            <p> Teléfono:</p>
                        </td>
                        <td width="90%" colspan="2">
                            <input readonly type="text" id="tel" class="form-control form-control-sm">
                        </td>
                    </tr>
                    <tr>
                        <td width="15%">
                            <p> Correo:</p>
                        </td>
                        <td width="90%" colspan="2">
                            <input readonly type="text" id="correo" class="form-control form-control-sm">
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script>
    var base_url = $('#base_url').val();
    function load() {
        table.destroy();
        table = $('#tabla').DataTable({
            "ajax": {
                "url": "<?php echo base_url(); ?>index.php/Cliente/getEncuestas",
                type: "post",
                data: { id: <?php echo $idcliente; ?>}
            },
            "columns": [
                {"data": "id"},
                {"data": "pregunta"},
                //{"data": "respuesta"},
                {"data": null,
                    "render": function ( data, type, row, meta) {
                        var resp="";
                        if(row.tipo==1){
                            resp="Escala: "+row.respuesta;

                        }else{
                            if(row.respuesta==1)
                                resp="Si";
                            else
                                resp="No";
                        }
                        return resp;
                    }
                },
                {"data": "fecha_reg"},
                {"data": null,
                    "render": function ( data, type, row, meta) {
                        var btn="<button title='Información extra' type='button' class='btn btn-info mas_info'><i class='fa fa-eye'></i></button>";
                        return btn;
                    }
                },
            ],
            "columnDefs": [
                {
                    "targets": [0],
                    "visible": false,
                    "searchable": false
                },
            ]
        });
        $('#tabla').on('click', 'button.mas_info', function () {
            var tr = $(this).closest('tr');
            var data = table.row(tr).data();
            $("#modal_info").modal();
            $("#sugerencias").val(data.sugerencias);
            $("#serv_inclu").val(data.serv_inclu);
            $("#empresa").val(data.empresa);
            $("#nombre").val(data.nombre);
            $("#serv").val(data.serv);
            $("#area").val(data.area);
            $("#tel").val(data.tel);
            $("#correo").val(data.correo);
            $("#fecha_modal").html("Fecha: "+data.fecha_reg);
        });
    }
    function enviarEncuesta(id) {
        console.log("enviar encuesta cliente");
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>index.php/Cliente/encuesta/"+id,
          success: function(data)
              {
                swal("Correcto!", "Encuesta enviada", "success");
                setTimeout(function () { window.location.href = base_url+"index.php/catalogos/clientes" }, 1500);
              },//Cierra el success
              error: function(error, xhr){
                 swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error");
              }
        });
    } 

    
    $(document).ready(function () {
        table = $('#tabla').DataTable();

        //Listener para edicion
        $('#tabla tbody').on('click', 'button.edit', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = row.data();
            window.location = '<?php echo base_url(); ?>index.php/catalogos/edicion_familia/'+data.id;
        });

        load();
    });
</script>