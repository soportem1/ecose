<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">Facturas de Cliente</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <br>
                        <h5><i class="ft-file-text"></i> Facturas de <?php echo $cliente->alias; ?></h5><hr>
                        <a data-toggle="modal" data-target="#modal_factura" class=" pull-right btn gradient-green-teal shadow-z-1 white"><i class="fa fa-plus-circle"></i> Agregar Nuevo Comprobante </a><br><br>
                        <table class="table table-striped table-condensed table-hover table-responsive" id="tabla">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Descripción</th>
                                    <th>Fecha de Carga</th>
                                    <th>Ver Documento</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<!-- Modal Facturas-->
        <div class="modal fade text-left" id="modal_factura" tabindex="-1" role="dialog"  aria-hidden="true">
             <form class="fv-form fv-form-bootstrap" id="form-comprobante" method="post" enctype="multipart/form-data">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header" >
                      <h5 class="modal-title" >Subir comprobante de pago</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <p>Descripción</p>
                            <textarea name="descripcion" class="form-control" rows="3"></textarea>
                        </div>
                    </div>
                     <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <p>Archivo XML (v3.3) <span>*</span></p>
                                    <input type='file' name='xml' accept="application/xml"  class="form-control"  />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <p>Archivo PDF <span>*</span></p>
                                    <input type='file' name='pdf' accept="application/pdf"  class="form-control"  />
                                </div>
                            </div>
                        </div>
                        <input name="cliente_id" value="<?php echo $cliente->id; ?>" hidden />
                    
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success" >Cargar a Sistema</button>
                  <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                  
                </div>
                </form>
            </div>
        </div>
    </div>

<script>
    function load() {
        table.destroy();
        table = $('#tabla').DataTable({
            "ajax": {
                "url": "<?php echo base_url(); ?>index.php/cliente/getFacturas/<?php echo $cliente->id; ?>"
            },
            "columns": [
                {"data": "id"},
                {"data": "descripcion"},
                {"data": "fecha_carga"},
                {
                    "data": null,
                    "defaultContent": "<button type='button' class='mb-0 btn btn-sm btn-primary  white xml'><i class='fa fa-file-code-o'></i></button>\
                                            <button type='button' class='mb-0 btn btn-sm btn-danger white pdf'><i class='fa fa-file-pdf-o'></i></button>"
                }
            ]
        });

    }
    $(document).ready(function () {
        table = $('#tabla').DataTable();

        $('#tabla tbody').on('click', 'button.xml', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = row.data();
            window.open('<?php echo base_url(); ?>uploads/cfdis_cliente_<?php echo $cliente->id; ?>/'+data.xml, 
                   'documento', 
                   'width=600,height=500');
        });
        $('#tabla tbody').on('click', 'button.pdf', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = row.data();
            window.open('<?php echo base_url(); ?>uploads/cfdis_cliente_<?php echo $cliente->id; ?>/'+data.pdf, 
                   'documento', 
                   'width=600,height=500');
        });
        
        
        $("#form-comprobante")
                .on('success.form.fv', function (e) {
                   // Prevent form submission
                    e.preventDefault();
                    var $form    = $(e.target),
                        formData = new FormData(),
                        params   = $form.serializeArray(),
                        xml    = $form.find('[name="xml"]')[0].files,
                        pdf    = $form.find('[name="pdf"]')[0].files;
                     
                     /*$.each(files, function(i, file) {
                        // Prefix the name of uploaded files with "uploadedFiles-"
                        // Of course, you can change it to any string
                        
                        formData.append('uploadedFiles-' + i, file);
                    });*/
                    formData.append('xml',xml[0]);
                    formData.append('pdf',pdf[0]);
                    $.each(params, function(i, val) {
                        formData.append(val.name, val.value);
                    });
                    
                    $("#lock-refresh").show();
                    $("#info_carga").html("");
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/cliente/subir_comprobante/",
                        cache: false,
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            if(data=="OK"){
                                
                                $('[name="xml"]').val("");
                                $('[name="pdf"]').val("");
                                table.ajax.reload();
                                $("#modal_aceptar").modal("hide");
                                swal("","Se realizó el envio de comprobante","success");
                            }
                            else{
                                swal("",data,"error");
                            }
                            $("#lock-refresh").hide();
                        }
                    }); // fin ajax
                })
                .formValidation({
                    framework: 'bootstrap',
                    icon: {
                        valid: '',
                        invalid: '',
                        validating: 'fa fa-refresh'
                    },
                    fields: {
                        
                        pdf: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese un archivo PDF"
                                }
                            }
                        },
                        xml: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese un archivo XML"
                                }
                            }
                        }
                        
                    }
                });
        //fin validador

        load();
    });
</script>