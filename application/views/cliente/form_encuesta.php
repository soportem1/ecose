<!DOCTYPE html>
<html lang="en" class="loading">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Grupo Ecose</title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>app-assets/img/icon.png">
        <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>app-assets/img/icon.png">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="default">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900|Montserrat:300,400,500,600,700,800,900" rel="stylesheet">
        <!-- BEGIN VENDOR CSS-->
        <!-- font icons-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/feather/style.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/simple-line-icons/style.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/perfect-scrollbar.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/prism.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/sweetalert2.min.css">
        <!-- END VENDOR CSS-->
        <!-- BEGIN APEX CSS-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/app.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/js/formvalidation/formValidation.min3f0d.css?v2.2.0">
        <!-- END APEX CSS-->
        <!-- BEGIN Page Level CSS-->
        <!-- END Page Level CSS-->
        <!-- BEGIN Custom CSS-->
        <!-- END Custom CSS-->

        <script src="<?php echo base_url(); ?>app-assets/vendors/js/core/jquery-3.2.1.min.js" type="text/javascript"></script>
    </head>
    <body data-col="1-column" class=" 1-column  blank-page blank-page">
        <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
        <!-- ////////////////////////////////////////////////////////////////////////////-->
        <div class="wrapper">
            <div class="main-panel">

                <div class="main-content">
                    <div class="content-wrapper">
                        <section class="color-palette">
                            <div class="row">
                                <div class="col-sm-1"></div>
                                <div class="col-sm-10">
                                    
                                    <div class="mt-3 pull-right">
                                        <span class="black mr-3 align-bottom">Soluciones Ambientales y en Seguridad e Higiene S.A de C.V</span>
                                        <img src="<?php echo base_url(); ?>app-assets/img/logo.png"  height="80">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-1"></div>
                                <div class="col-sm-10">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="card-block">
                                                <form id="insert-encuesta" data-parsley-validate method="post" class="form-horizontal form-label-left" required>
                                                <br>
                                                <h3>Encuesta de Conformidad</h3><hr><br>
                                                <p class="font-medium-1">
                                                    En grupo ECOSE, nos interesa conocer su opinión respecto a la calidad de nuestros servicios, por lo que le pedimos conteste las siguientes preguntas:
                                                </p>
                                                <p class="font-medium-1">
                                                    Se valora en una escala de 1 a 4, donde 4 es "Siempre", 3 "Frecuentemente", 2 "Ocasionalmente" y 1 "Nunca"
                                                </p>
                                                <br>
                                                <table class="table table-sm">
                                                    <?php 
                                                       //if(isset($contacto)){
                                                        foreach ($contacto as $item){
                                                            echo "<input name='email' hidden value='$item->email'>";
                                                        }
                                                        //}
                                                    ?>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Pregunta</th>
                                                        <th>Escala<br><span style="letter-spacing: 4px" class="black">1 2 3 4</span></th>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>¿La actitud del asesor de ventas fue la adecuada durante todo el servicio?</td>
                                                        <td>
                                                            <label class="radio-inline"><input type="radio" value="1" name="q1"></label> 
                                                            <label class="radio-inline"><input type="radio" value="2" name="q1"></label> 
                                                            <label class="radio-inline"><input type="radio" value="3" name="q1"></label> 
                                                            <label class="radio-inline"><input type="radio" value="4" name="q1"></label> 
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>¿Los tiempos de respuesta para el servicio fueron los acordados?</td>
                                                        <td>
                                                            <label class="radio-inline"><input type="radio" value="1" name="q2"></label> 
                                                            <label class="radio-inline"><input type="radio" value="2" name="q2"></label> 
                                                            <label class="radio-inline"><input type="radio" value="3" name="q2"></label> 
                                                            <label class="radio-inline"><input type="radio" value="4" name="q2"></label> 
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>¿El servicio otorgado cumplió con  las necesidades y expectativas solicitadas?</td>
                                                        <td>
                                                            <label class="radio-inline"><input type="radio" value="1" name="q3"></label> 
                                                            <label class="radio-inline"><input type="radio" value="2" name="q3"></label> 
                                                            <label class="radio-inline"><input type="radio" value="3" name="q3"></label> 
                                                            <label class="radio-inline"><input type="radio" value="4" name="q3"></label> 
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>4</td>
                                                        <td>¿Sus dudas respecto al servicio impartido fueron resueltas de manera satisfactoría?</td>
                                                        <td>
                                                            <label class="radio-inline"><input type="radio" value="1" name="q4"></label> 
                                                            <label class="radio-inline"><input type="radio" value="2" name="q4"></label> 
                                                            <label class="radio-inline"><input type="radio" value="3" name="q4"></label> 
                                                            <label class="radio-inline"><input type="radio" value="4" name="q4"></label> 
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>5</td>
                                                        <td>¿Recomendaría nuestros servicios?</td>
                                                        <td>
                                                            <label class="radio-inline"><input type="radio" value="1" name="q5"></label> 
                                                            <label class="radio-inline"><input type="radio" value="2" name="q5"></label> 
                                                            <label class="radio-inline"><input type="radio" value="3" name="q5"></label> 
                                                            <label class="radio-inline"><input type="radio" value="4" name="q5"></label> 
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br><hr>
                                                <div class="text-center">
                                                    <button class="btn btn-success width-250">Enviar Encuesta</button>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </section>
                    </div>
                </div>

            </div>
        </div>
        <!-- ////////////////////////////////////////////////////////////////////////////-->

        <!-- BEGIN VENDOR JS-->

        <script src="<?php echo base_url(); ?>app-assets/vendors/js/core/popper.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>app-assets/vendors/js/core/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>app-assets/vendors/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>app-assets/vendors/js/prism.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>app-assets/vendors/js/jquery.matchHeight-min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>app-assets/vendors/js/pace/pace.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>app-assets/vendors/js/sweetalert2.min.js" type="text/javascript"></script>
        <!-- BEGIN VENDOR JS-->
        <script src="<?php echo base_url(); ?>app-assets/vendors/js/formvalidation/formValidation.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>app-assets/vendors/js/formvalidation/framework/bootstrap.min.js"></script>
        <!-- BEGIN PAGE VENDOR JS-->
        <!-- END PAGE VENDOR JS-->
        <!-- BEGIN APEX JS-->
        <script src="<?php echo base_url(); ?>app-assets/js/notification-sidebar.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>app-assets/js/customizer.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/js/encuesta.js" type="text/javascript"></script>
        <!-- END APEX JS-->
        <!-- BEGIN PAGE LEVEL JS-->

        <!-- END PAGE LEVEL JS-->

    </body>
</html>