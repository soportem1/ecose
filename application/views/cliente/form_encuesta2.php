<!DOCTYPE html>
<html lang="en" class="loading">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Grupo Ecose</title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>app-assets/img/icon.png">
        <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>app-assets/img/icon.png">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="default">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900|Montserrat:300,400,500,600,700,800,900" rel="stylesheet">
        <!-- BEGIN VENDOR CSS-->
        <!-- font icons-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/feather/style.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/simple-line-icons/style.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/perfect-scrollbar.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/prism.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/sweetalert2.min.css">
        <!-- END VENDOR CSS-->
        <!-- BEGIN APEX CSS-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/app.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/js/formvalidation/formValidation.min3f0d.css?v2.2.0">
        <!-- END APEX CSS-->
        <!-- BEGIN Page Level CSS-->
        <!-- END Page Level CSS-->
        <!-- BEGIN Custom CSS-->
        <!-- END Custom CSS-->

        <script src="<?php echo base_url(); ?>app-assets/vendors/js/core/jquery-3.2.1.min.js" type="text/javascript"></script>
    </head>
    <?php 
        if($emp=="1" || $emp=="2" || $emp=="3" || $emp=="6") {
            $logo =  base_url()."app-assets/img/logo.png";
            $name_firma ="Grupo Ecose"; 
            $razon = "Soluciones Ambientales y en Seguridad e Higiene S.A. de C.V.";
        }
        else if($emp=="4") {
            $logo =  base_url()."app-assets/img/logo_ahisa.png";
            $name_firma ="Grupo Ahisa"; 
            $razon = "AHISA Laboratorio de Pruebas S. de R.L. de C.V.";
        }
        else if($emp=="5") {
            $logo =  base_url()."app-assets/img/logo_auven.png";
            $name_firma ="Grupo Auven";
            $razon = "AUVEN S. de R.L. de C.V.";
        }
    ?>
    <body data-col="1-column" class=" 1-column  blank-page blank-page">
        <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
        <input type="hidden" id="idemp" value="<?php echo $emp; ?>">
        <!-- ////////////////////////////////////////////////////////////////////////////-->
        <div class="wrapper">
            <div class="main-panel">

                <div class="main-content">
                    <div class="content-wrapper">
                        <section class="color-palette">
                            <div class="row">
                                <div class="col-sm-1"></div>
                                <div class="col-sm-10">
                                    
                                    <div class="mt-3 pull-right">
                                        <span class="black mr-3 align-bottom"><?php echo $razon; ?> </span>
                                        <img src="<?php echo $logo;?>" height="80">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-1"></div>
                                <div class="col-sm-10">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="card-block">
                                                <form id="insert_encuesta" data-parsley-validate method="post" class="form-horizontal form-label-left" required>
                                                    <input type="hidden" id="id_cli" value="<?php echo $id_cli; ?>"> 
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <h3>ENCUESTA DE SATISFACCIÓN</h3>
                                                    </div>
                                                    <div class="col-md-4"></div>
                                                    <div class="col-md-2">
                                                        Fecha: <?php echo date("d-m-Y"); ?>
                                                    </div>
                                                </div>

                                                <div style="background-color: #12d31a; height:3px"></div><br><br>
                                                <p class="font-medium-1">
                                                    En Grupo ECOSE, nos interesa conocer su opinión y satisfacción respecto a la calidad de nuestros servicios, por lo que le pedimos conteste las siguientes preguntas:
                                                </p>
                                                <p class="font-medium-1">
                                                    Se valora en una escala de 1 a 4, donde 4 corresponde a "Siempre", 3 a “Frecuentemente”, 2 a “Ocasionalmente” y 1 "Nunca".
                                                </p>
                                                <br>
                                                <table class="table table-condensed table-striped" id="tabla_pregs">
                                                    <thead>
                                                        <tr>
                                                            <th width="2%">No.</th>
                                                            <th width="68%">Pregunta</th>
                                                            <th width="30%"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="table_pre">
                                                    <?php  $i=0; foreach ($pregs->result() as $d) { $i++; ?>
                                                        <tr id="por_preg">
                                                            <td width="2%">
                                                                <?php echo $i; ?>
                                                            </td>
                                                            <td width="68%">
                                                                <input type="hidden" id="id_preg" value="<?php echo $d->id; ?>"> 
                                                                <input type="hidden" id="tipo" value="<?php echo $d->tipo; ?>"> 
                                                                <?php echo $d->pregunta; ?>   
                                                            </td>
                                                            <?php if($d->tipo==1){ ?>
                                                                <td width="30%" style="text-align:center"> <p style="font-weight: bold;"> Escala </p>
                                                                    <label class="radio-inline"><input type="radio" value="4" id="resp<?php echo $d->id; ?>_4" name="resp<?php echo $d->id; ?>"> 4</label>
                                                                    <label class="radio-inline"><input type="radio" value="3" id="resp<?php echo $d->id; ?>_3" name="resp<?php echo $d->id; ?>"> 3</label>
                                                                    <label class="radio-inline"><input type="radio" value="2" id="resp<?php echo $d->id; ?>_2" name="resp<?php echo $d->id; ?>"> 2</label>
                                                                    <label class="radio-inline"><input type="radio" value="1" id="resp<?php echo $d->id; ?>_1" name="resp<?php echo $d->id; ?>"> 1</label>
                                                                </td>
                                                            <?php }
                                                            else if($d->tipo==2){ ?>
                                                                <td width="30%" style="text-align:center">
                                                                    <label class="radio-inline"><input type="radio" value="1" id="resp<?php echo $d->id; ?>_1" name="resp<?php echo $d->id; ?>"> SI</label> 
                                                                    <label class="radio-inline"><input type="radio" value="2" id="resp<?php echo $d->id; ?>_2" name="resp<?php echo $d->id; ?>"> NO</label> 
                                                                </td>
                                                            <?php } ?>
                                                        </tr>
                                                    <?php } ?>
                                                    </table>
                                                    <table class="table table-condensed table-striped" id="tabla">
                                                        <tr>
                                                            <td width="100%" colspan="3">
                                                                <div class="form-group">
                                                                    <p>SUGERENCIAS:</p>
                                                                    Sus comentarios son importantes para nosotros, ya que nos ayudarán a seguir brindando un servicio de calidad:
                                                                    <textarea rows="2" type="text" id="sugerencias" class="form-control"></textarea>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="100%" colspan="3">
                                                                <p>¿Qué servicio quisiera que incluyéramos? </p>
                                                                <input type="text" id="serv_inclu" class="form-control form-control-sm">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <hr>
                                                            <th width="100%" colspan="3" style="text-align: center;">
                                                                <p> DATOS DEL CLIENTE</p>
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <td width="15%">
                                                                <p> Empresa:</p>
                                                            </td>
                                                            <td width="90%" colspan="2">
                                                                <input type="text" id="empresa" class="form-control form-control-sm">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="15%">
                                                                <p> Nombre:</p>
                                                            </td>
                                                            <td width="90%" colspan="2">
                                                                <input type="text" id="nombre" class="form-control form-control-sm">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="15%">
                                                                <p> Servicio contratado:</p>
                                                            </td>
                                                            <td width="90%" colspan="2">
                                                                <input type="text" id="serv" class="form-control form-control-sm">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="15%">
                                                                <p> Área a la que pertenece:</p>
                                                            </td>
                                                            <td width="90%" colspan="2">
                                                                <input type="text" id="area" class="form-control form-control-sm">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="15%">
                                                                <p> Teléfono:</p>
                                                            </td>
                                                            <td width="90%" colspan="2">
                                                                <input type="text" id="tel" class="form-control form-control-sm">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="15%">
                                                                <p> Correo:</p>
                                                            </td>
                                                            <td width="90%" colspan="2">
                                                                <input type="text" id="correo" class="form-control form-control-sm">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                
                                                <br><hr>
                                                <div class="text-center">
                                                    <button type="button" id="save" class="btn btn-success width-250">Enviar Encuesta</button>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </section>
                    </div>
                </div>

            </div>
        </div>
        <!-- ////////////////////////////////////////////////////////////////////////////-->

        <!-- BEGIN VENDOR JS-->

        <script src="<?php echo base_url(); ?>app-assets/vendors/js/core/popper.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>app-assets/vendors/js/core/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>app-assets/vendors/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>app-assets/vendors/js/prism.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>app-assets/vendors/js/jquery.matchHeight-min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>app-assets/vendors/js/pace/pace.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>app-assets/vendors/js/sweetalert2.min.js" type="text/javascript"></script>
        <!-- BEGIN VENDOR JS-->
        <!--<script src="<?php echo base_url(); ?>app-assets/vendors/js/formvalidation/formValidation.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>app-assets/vendors/js/formvalidation/framework/bootstrap.min.js"></script>-->

        <script src="<?php echo base_url(); ?>plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
        <!-- BEGIN PAGE VENDOR JS-->
        <!-- END PAGE VENDOR JS-->
        <!-- BEGIN APEX JS-->
        <script src="<?php echo base_url(); ?>app-assets/js/notification-sidebar.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>app-assets/js/customizer.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/js/encuesta.js?v=<?php echo date("Ymdgis"); ?>" type="text/javascript"></script>
        <!-- END APEX JS-->
        <!-- BEGIN PAGE LEVEL JS-->

        <!-- END PAGE LEVEL JS-->

    </body>
</html>