<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">Cotizaciones</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                       <?php //echo $this->session->userdata("perfil"); ?>
                        <h5><i class="ft-mail"></i> Listado de Cotizaciones</h5><hr>
                        <div class="col-md-3">
                            <label class="modal-title text-text-bold-600">Busqueda por estatus</label>
                            <div class="position-relative has-icon-right">
                                <select class="form-control" id="tipostock" name="tipostock" onchange="loadTable(this.value)">
                                    <option value="0">Todas:</option>
                                    <option value="1">Pendiente</option>
                                    <option value="2">Aceptada</option>
                                    <option value="3">Rechazada</option>
                                    <option value="4">Modificada</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <a href="<?php echo base_url(); ?>index.php/cotizaciones/nueva_cotizacion" class="btn btn-success  pull-right">Nueva Cotización</a>
                            </div>
                            
                        </div>
                        <div class="table-responsive" style="overflow: scroll;">
                            <table class="table table-striped" id="tabla">
                                <thead>
                                    <tr>
                                        <th>Folio</th>
                                        <th>Cliente</th>
                                        <th>Fecha</th>
                                        <th>Importe</th>
                                        <th>Descuento%</th>
                                        <th>Forma de Pago</th>
                                        <th>Vendedor</th>
                                        <th>Estatus</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<!--MODAL DE PRODUCTOs ---------------------------------------------------------------------->
<div class="modal fade text-left" id="modal_productos" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label class="modal-title text-text-bold-600">Productos</label>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Descripción</th>
                                <th>Cantidad</th>
                                <th>Precio</th>
                                <th>Importe</th>
                            </tr>
                        </thead>
                        <tbody id="tabla_resultados">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!--MODAL DE PRODUCTOs ---------------------------------------------------------------------->

<div class="modal fade text-left" id="modal_pass" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label class="modal-title text-text-bold-600">Autorización de Modificación</label>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <input type="hidden" id="id_cotiza_auto" class="form-control" >
                    <div class="col-md-8">
                        <div class="form-group">
                            <p>Contraseña: </p>
                            <div class="controls">
                                <input type="text" id="pass_insert" class="form-control" >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="aceptar_pass"> Aceptar</button>
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<script>
    var table="";
    $(document).ready(function () {
        loadTable($("#tipostock option:selected").val());

        $("#aceptar_pass").on("click",function(){
            //validaPassDesc();
            modal_autoriza();
        }); 

    });

    function validaPassDesc(){
        if($("#pass_insert").val()!=""){
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php/Cotizaciones/validaPass",
                data: {pass: $("#pass_insert").val()},
                success: function (data) {
                    var array = $.parseJSON(data);
                    if(array.usada==0 && array.exist>0){
                        swal("Éxito", "Contraseña correcta, descuento activado", "success");
                        $("#modal_pass").modal("hide");
                        $("#descuento").attr("disabled",false);
                        $("#aux_desc").val(1);
                        $("#id_pass").val(array.id);
                    }else{
                        swal("Error", "Contraseña invalida", "warning");
                    }
                }
            });
        }else{
            swal("Error", "Ingrese una contraseña", "warning");
        }
    }

    function loadTable(status){
        table = $('#tabla').DataTable({
            responsive: true,
            "bProcessing": true,
            "serverSide": true,
            destroy:true,
            "order": [[ 0, "desc" ]],
            "ajax": {
                url: "<?php echo base_url(); ?>index.php/cotizaciones/getData_cotizaciones",
                type: "post",
                data: { status:status },
                error: function () {
                    $("#tabla").css("display", "none");
                }
            },
            "columns": [
                {"data": "folio"},
                {"data": "alias"},
                {"data": "fecha_creacion"},
                //{"data": "importe"},
                {"data": null,
                    "render" : function ( url, type, full) {
                        var msj='';
                        if(full["descuento"]>0){
                            var desc =parseFloat(full["subtotal"])*(parseInt(full["descuento"])/100);
                            var importe = parseFloat(full["subtotal"])-parseFloat(desc);
                            msj+=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(importe);  
                            msj+='<!-- importe:'+full["subtotal"]+' descuento:'+desc+'-->';
                        }else{
                            msj+="$" +full["importe"];    
                        }
                        
                        return msj;
                    }
                },
                //{"data": "descuento"},
                {"data": null,
                    "render" : function ( url, type, full) {
                        var msj='';
                        if(full["descuento"]!=null)
                            msj+=full["descuento"] +" %";
                        else
                            msj+="0"+" %";

                        return msj;
                    }
                }, 
                {"data": "forma_pago",
                    "render": function (data, type, row, meta) {
                        return forma_pago(data);
                    }
                },
                {"data": "nombre"},
                {"data": "status",
                    "render": function (data, type, row, meta) {
                        return estatus(data);
                    }
                },
                {"data": "status",
                    "render": function (data, type, row, meta) {
                        return acciones(data);
                    }
                }
            ],
        });

        $('#tabla').on('click', 'button.aceptar', function () {
            var tr = $(this).closest('tr');
            var data = table.row(tr).data();
            estatus_cotizacion(data.id, 2,data.id_empresa);
        });
        $('#tabla').on('click', 'button.rechazar', function () {
            var tr = $(this).closest('tr');
            var data = table.row(tr).data();
            estatus_cotizacion(data.id, 3,data.id_empresa);
        });
        $('#tabla').on('click', 'button.imprimir', function () {
            var tr = $(this).closest('tr');
            var data = table.row(tr).data();
            imprimir(data.id);
        });
        $('#tabla').on('click', 'a.mail', function () {
            var tr = $(this).closest('tr');
            var data = table.row(tr).data();
            mail(data.id);
        });
        $('#tabla').on('click', 'a.delete', function () {
            var tr = $(this).closest('tr');
            var data = table.row(tr).data();
            eliminar(data.id);
        });
        $('#tabla').on('click', 'a.modificar', function () {
            var tr = $(this).closest('tr');
            var data = table.row(tr).data();
            window.location.href = "<?php echo base_url(); ?>index.php/cotizaciones/modificar_cotizacion/" + data.id;
        });
        $('#tabla').on('click', 'button.modificar_aceptada', function () {
            var tr = $(this).closest('tr');
            var data = table.row(tr).data();
            //modal_autoriza(data.id);
            $("#modal_pass").modal();
            $("#id_cotiza_auto").val(data.id);
        });
    }


    function acciones(data, type, row, meta) {    
        var btn = "<button class='btn btn-sm mb-0 btn-success imprimir'><i class='fa fa-print'></i></button>";
        btn += '<div class="btn-group ml-1 mb-0">\
                <button type="button" class="btn btn-sm btn-raised mb-0 btn-dark dropdown-toggle" data-toggle="dropdown" ><i class="fa fa-bars"></i></button>\
                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">'
        if (data != "2" && data != "3") {
            btn += '<button class="dropdown-item aceptar" type="button">Aceptar</button>\
                    <button class="dropdown-item rechazar" type="button">Rechazar</button>';
        }else{
           btn += '<button class="dropdown-item modificar_aceptada" type="button">Solicitar Modificación</button>'; 
        }
        btn += '<a class="dropdown-item mail" href="#">Envíar por email</a>';
        <?php if($this->session->userdata("administrador")){ ?>
            btn += '<a class="dropdown-item delete" href="#">Eliminar</a>';
        <?php } ?>
        if (data != "2" && data != "3") {
            btn += '<div class="dropdown-divider"></div><a class="dropdown-item modificar" href="#">Modificar</a>';
        }

        btn += '</div></div>';
        return btn;
    }

    function imprimir(id) {
        window.open('<?php echo base_url(); ?>index.php/Formatos/cotizacion/' + id,
                'imprimir',
                'width=700,height=800');
    }
    
    function mail(id) {
        $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php/Formatos/email_cotizacion/"+id,
                success: function (data) {
                    //console.log(data);
                    swal("", "Se ha enviado el correo de cotización", "success");
                }
            });
    }

    function eliminar(id){
        swal({
            title: "Eliminar",
            text: "¿Desea eliminar este elemento?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Eliminar"
        }).then(function(t){t &&
            $.ajax({
                     type: "POST",
                     url: '<?php echo base_url(); ?>index.php/Cotizaciones/cambiarEstatus/',
                     data: {id:id, status: 0},
                     success: function (result) {
                        console.log(result);
                        if($("#tipostock").val()=="0"){
                            table.ajax.reload();
                        }else{
                            loadTable($("#tipostock").val());   
                        }
                        swal("Exito!", "Se ha eliminado correctamente", "success");
                     }
                 });
        });
    }

    function forma_pago(data, type, row, meta) {
        var forma = "50% de anticipo y 50% contraentrega";
        switch (data) {
            case "2":
                forma = "Crédito 30 días";
                break;
            case "3":
                forma = "Crédito 60 días";
                break;
            case "4":
                forma = "100% contraentrega";
                break;
            case "5":
                forma = "En común acuerdo con el cliente";
                break;
        }

        return forma;
    }

    function estatus(data, type, row, meta) {
        var stat = "<span class='badge badge-secondary width-100'>Pendiente</span>";
        switch (data) {
            case "2":
                stat = "<span class='badge badge-success width-100'>Aceptada</span>";
                break;
            case "3":
                stat = "<span class='badge badge-danger width-100'>Rechazada</span>";
                break;
            case "4":
                stat = "<span class='badge badge-warning width-100'>Modificada</span>";
                break;
        }

        return stat;
    }

    function estatus_cotizacion(id, estatus, id_empresa) {
        var conf=0;
        console.log("estatus: "+estatus);
        console.log("id_empresa: "+id_empresa);
        if(estatus==2){
            swal({
                title: '¿Desea aceptar la cotización?',
                html:
                    '<div class="row">\
                        <div class="col-md-12">\
                            <label>Persona(s) a quien(es) va dirigido el informe:</label>\
                        </div>\
                        <div class="col-md-12">\
                            <input id="swal-input1" type="text" class="swal2-input" required>\
                        </div>\
                        <div class="col-md-12">\
                            <label>Nombre y cargo a quien se dirigirán los técnicos:</label>\
                        </div>\
                        <div class="col-md-12">\
                            <input id="swal-input2" type="text" class="swal2-input" required>\
                        </div>\
                    </div>',
                  preConfirm: function () {
                    return new Promise(function (resolve) {
                      resolve([
                        $('#swal-input1').val()
                      ])
                    })
                  },
                text: "¿Desea cambiar el estatus de la Orden?",
                type: 'info',
                showCancelButton: true,
                allowOutsideClick: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Aceptar",
                closeOnConfirm: false,
            }).then(function (isConfirm) {
                if (isConfirm && $("#swal-input1").val()!="") {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/Cotizaciones/cambiarEstatus",
                        data: {id: id, status: estatus, id_empresa:id_empresa, recibe: $("#swal-input1").val(), nom_tec: $("#swal-input2").val()},
                        beforeSend: function() {
                            $(".aceptar").attr('disabled',true); 
                            $(".swal2-confirm").attr('disabled',true); 
                        },
                        success: function (data) {
                            swal("", "Se cambió el estatus de la cotización", "success");
                            if($("#tipostock").val()=="0"){
                                table.ajax.reload();
                            }else{
                                loadTable($("#tipostock").val());   
                            }
                        }
                    });
                }else{
                    swal("", "Ingresa a quien va dirigido", "warning");
                }
            }).catch(swal.noop);
        }else{ //rechazar
            swal({
                title: '',
                text: "¿Desea cambiar el estatus de la Orden?",
                type: 'info',
                showCancelButton: true,
                allowOutsideClick: false,
            }).then(function (isConfirm) {
                conf++;
                if (isConfirm && conf==1) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/cotizaciones/cambiarEstatus",
                        data: {id: id, status: estatus},
                        beforeSend: function() {
                            $(".rechazar").attr('disabled',true); 
                        },
                        success: function (data) {
                            swal("", "Se cambió el estatus de la cotización", "success");
                            if($("#tipostock").val()=="0"){
                                table.ajax.reload();
                            }else{
                                loadTable($("#tipostock").val());   
                            }
                        }
                    });
                }
            }).catch(swal.noop);
        }
    }

    function modal_autoriza(id=0){
        var id_mod = id;
        if($("#pass_insert").val()!=""){
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php/Cotizaciones/autorizaModifica",
                data: {pass: $("#pass_insert").val(), id_cot: $("#id_cotiza_auto").val()},
                success: function (data) {
                    var array = $.parseJSON(data);
                    if(array.usada==0 && array.exist>0){
                        swal("Éxito", "Contraseña correcta", "success");
                         $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>index.php/Cotizaciones/changeStatusPass2",
                            data: {id:array.id },
                            success: function (data2) {
                                $("#pass_insert").val("");
                            }
                        });
                        setTimeout(function () { history.back() 
                            window.location.href = "<?php echo base_url(); ?>index.php/cotizaciones/modificar_cotizacion/"+$("#id_cotiza_auto").val()+"/"+1;
                        }, 1500);
                    }else{
                        swal("Error", "Contraseña invalida", "warning");
                    }
                }
            });
        }else{
            swal("Error", "Ingrese una contraseña", "warning");
        }
    }

</script>