<script>
    $(document).ready(function () {
         $('.select2').select2();
        
        $("#slc_tamaño").on("change",function(){
            buscar_familias();
        });
        
        $("#slc_familia").on("change",function(){
            buscar_servicios($("#slc_familia option:selected").data("emp"));
        });
        
        $("#table_servicios").on("change",".cant",function(){
            cambia_cantidad($(this));
        });
        
        $("#cliente").on("change",function(){
            cambia_contacto();
            if($("#contacto option:selected").val()!="" && $("#contacto option:selected").val()!=undefined){
                $("#savecot").attr("disabled",false);
            }
        });
        $("#contacto").on("change",function(){
            <?php
                /*if(isset($cotizacion)){
                }
                else{
                    echo "cambia_contacto()";
                }*/
            ?>
            //cambia_contacto();
            if(this.value!="" && this.value!=undefined){
                $("#savecot").attr("disabled",false);
            }
        });

        $("#descuento").on("change",function(){
            //if($("#descuento").val()>0)
                valida_descuentos(1,0);
            /*else
                $("#table_descuentos").html('');*/
        });

        if($("#cliente").val()!=""){
            cambia_contacto();
        }
        
        //cambia_contacto();
        buscar_familias();
        
        <?php
            if(isset($cotizacion)){
                echo "load_list();";
            }
            else{
                echo "clear_list();";
            }
        ?>
        
        $("#ingresar_pass").on("click",function(){
            $("#modal_pass").modal();
        });
        $("#aceptar_pass").on("click",function(){
            validaPassDesc();
        }); 
    });

    function validaPassDesc(){
        if($("#pass_insert").val()!=""){
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php/Cotizaciones/validaPass",
                data: {pass: $("#pass_insert").val()},
                success: function (data) {
                    var array = $.parseJSON(data);
                    if(array.usada==0 && array.exist>0){
                        swal("Éxito", "Contraseña correcta, descuento activado", "success");
                        $("#modal_pass").modal("hide");
                        $("#descuento").attr("disabled",false);
                        $("#aux_desc").val(1);
                        $("#id_pass").val(array.id);
                    }else{
                        swal("Error", "Contraseña invalida", "warning");
                    }
                }
            });
        }else{
            swal("Error", "Ingrese una contraseña", "warning");
        }
    }
    
    function cambia_contacto(){
        //console.log("contacto: "+$("#contacto").val());
        $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php/cotizaciones/searchContacto",
                data: {cliente: $("#cliente").val(), contacto: $("#contacto").val()},
                //async: false,
                //dataType: "JSON",
                success: function (data) {
                    $("#contacto").empty();
                    $("#contacto").append(data);
                }
            });
    }
    
    
    function buscar_familias(){
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>index.php/cotizaciones/searchFamilias",
            data: {tamaño: $("#slc_tamaño").val()},
            //async: false,
            //dataType: "JSON",
            success: function (data) {
                $("#slc_familia").empty();
                $("#slc_familia").append(data);
                var id_emp = $("#slc_familia option:selected").data("emp");
                buscar_servicios(id_emp);
            }
        });
    }
    
    function buscar_servicios(id_emp){
        //console.log("id_emp: "+id_emp);
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>index.php/cotizaciones/searchServicios",
            data: {familia: $("#slc_familia").val(),tamaño: $("#slc_tamaño").val(), id_emp:id_emp},
            //async: false,
            //dataType: "JSON",
            success: function (data) {
                $("#slc_servicio").empty();
                $("#slc_servicio").append(data);
            }
        });
    }
    
    function save_cotizacion(btn){
        btn.attr("disabled",true);
        <?php
            if(isset($cotizacion)){
                echo "var id=$cotizacion->id;";
            }
            else{
                echo "var id=0;";
            }
        ?>
        //console.log("id cotizacion: "+id);
        //console.log("descuento:" +$("#descuento").val());
        if($("#contacto option:selected").val()!=undefined){
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php/cotizaciones/saveCotizacion",
                data: {cliente: $("#cliente option:selected").val(),contacto: $("#contacto option:selected").val(),forma_pago: $('#forma_pago option:selected').val(), otro: "",descuento: $("#descuento").val(),desc_aplicado: $("#desc_aplicado").val(),observaciones: $('#observaciones').val(), id: id, aux_auto: $('#aux_auto').val(), id_empresa: $("#id_empresa").val() },
                //async: false,
                //dataType: "JSON",
                success: function (data) {
                    if($("#aux_desc").val()!="0"){
                        cambiaEstatusPass(data);
                    }
                    swal({
                            title: 'Exito!',
                            text: "Se guardo la cotización con el No."+data,
                            type: 'success',
                            showCancelButton: false,
                            allowOutsideClick: false
                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                window.location = "<?php echo base_url(); ?>index.php/cotizaciones";
                            }
                        }).catch(swal.noop);
                }
            });
        }else{
            swal("Error", "Elegir un contacto, cliente no cuenta con uno", "warning");
        }
    }
    
    function cambiaEstatusPass(id_cotiza){
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>index.php/Cotizaciones/changeStatusPass",
            data: {id: $("#id_pass").val(), id_cotiza:id_cotiza},
            success: function (data) {

            }
        });
    }

    function agregar_servicio(){
        var id_cli = $("#cliente option:selected").val();
        var id_emp = $("#slc_familia option:selected").data("emp");
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>index.php/cotizaciones/agregarServicio/"+$("#slc_servicio").val()+"/1/1/0/0/"+id_cli,
            data: { id_emp: id_emp },
            //async: false,
            //dataType: "JSON",
            success: function (data) {
                $("#table_servicios").html(data);
                valida_descuentos(1,0);
            }
        });
    }
    
    function cambia_cantidad(input){
        
        if(!$.isNumeric(input.val()) && input.val()<1){
            swal("","Por favor ingrese un valor positivo","warning");
            input.val(1);
        }
        var row=input.closest("tr").find("td").eq(0).html();
        //console.log("row: "+row);
        //se debe mandar el di de empresa de acuerdo al servicio seleccionado
        id_emp = input.data("idemp");
        //console.log("id_emp: "+id_emp);
        var id_cli = $("#cliente option:selected").val();
        $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php/cotizaciones/cambiaCantidad",
                data: {cantidad: input.val(),row: row, id_cli:id_cli, id_empresa:id_emp, sucursal: $("#sucursal").val() },
                //async: false,
                //dataType: "JSON",
                success: function (data) {
                    $("#table_servicios").html(data);
                    valida_descuentos(1,0);
                }
            });
        
    }

let band_eli=0;
    function elimina_servicio(btn,id,partida){
        //btn.closest(".ord_"+partida+"").remove();
        var row=btn.closest("tr").find("td").eq(0).html();
        //console.log("row: "+row);
        band_eli++;
        //console.log("band_eli: "+band_eli);
        $(".ord_"+partida+"").remove();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>index.php/cotizaciones/eliminaServicio",
            data: {row: row, eli: id, band_eli,band_eli},
            async: false,
            //dataType: "JSON",
            success: function (data) {
                $("#table_servicios").html(data);
                valida_descuentos(1,0);
                //setTimeout(function () {  load_list(); }, 1000);
                
            }
        });
    }
    function valida_descuentos(tipo,band){
        console.log("tipo: "+tipo);
        <?php
            if(isset($cotizacion)){
                echo "var id=$cotizacion->id;";
                echo "var id_emp_cot=$cotizacion->id_empresa;";
            }
            else{
                echo "var id=0;";
                echo "var id_emp_cot=0;";
            }
        ?>
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>index.php/cotizaciones/validaDescuentos",
            data: {descuento: $("#descuento").val(), id_empresa:$("#id_empresa").val(), sucursal: $("#sucursal").val(), tipo_cli:$("#cliente option:selected").data("tipoc"), id:id, id_emp_cot:id_emp_cot },
            success: function (data) {
                $("#table_descuentos").html(data);
                desc = $("#desc_aplicadoprecio").text().replace("%", "").replace(" ", "");
                if(tipo==1){
                    //desc = $("#desc_aplicadoprecio").text().replace("%", "").replace(" ", "");
                    //$("#descuento").val(desc);
                    //$("#descuento").text(desc);
                }
                if(tipo!=1){
                    //desc = $("#desc_aplicadoprecio").text().replace("%", "").replace(" ", "");
                    //$("#descuento").val(desc);
                    //$("#descuento").text(desc);
                }
                if(desc!="" && desc!=" " && tipo!=1){
                    $("#descuento").val(desc);
                    //console.log("cambia desc");
                    //$("#desc_aplicado").val(desc);
                }else if(desc=="" && tipo!=1 || desc==" " && tipo!=1){
                    $("#descuento").val("");
                }
                if($("#desc_guarda").val()>0 && band>0){
                    $("#descuento").val($("#desc_guarda").val());
                }
                //console.log("descuento sobre precio: "+desc);
                
            }
        });
    
    }

    function clear_list(){
        $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php/cotizaciones/limpiaServicios",
                success: function (data) {
                    $("#table_servicios").html("");
                }
            });
        
    }
    
    function load_list(){
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>index.php/cotizaciones/cargaServicios",
            async:false,
            success: function (data) {
                $("#table_servicios").html(data);
                valida_descuentos(1,0);
            }
        });
        
    }
    
    
    
    
 </script>