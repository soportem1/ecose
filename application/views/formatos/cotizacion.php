<?php
$color = "#CCFF99";
function mes($m){
    $mes="enero";
    switch ($m) {
        case 2: $mes="febrero"; break;
        case 3: $mes="marzo"; break;
        case 4: $mes="abril"; break;
        case 5: $mes="mayo"; break;
        case 6: $mes="junio"; break;
        case 7: $mes="julio"; break;
        case 8: $mes="agosto"; break;
        case 9: $mes="septiembre"; break;
        case 10: $mes="octubre"; break;
        case 11: $mes="noviembre"; break;
        case 12: $mes="diciembre"; break;
    }
    return $mes;
}

$time = strtotime($cotizacion->fecha_creacion);

$dia=date('d',$time);
$mes=mes(date('m',$time));
$anio=date('Y',$time);
$logo =  FCPATH."app-assets/img/logo.png"; $name_firma ="Grupo Ecose"; $razon="";
$color = "background-color: #13bf0d";
if($cotizacion->id_empresa=="1" || $cotizacion->id_empresa=="2" || $cotizacion->id_empresa=="3" || $cotizacion->id_empresa=="6") {
    $logo =  FCPATH."app-assets/img/logo.png";
    $name_firma ="Grupo Ecose"; 
    $razon = "Soluciones Ambientales y en Seguridad e Higiene S.A. de C.V.";
    $color = "background-color: #13bf0d";
    $color_tr="#13bf0d";
}
else if($cotizacion->id_empresa=="4") {
    $logo =  FCPATH."app-assets/img/ahisa.png";
    $name_firma ="Grupo Ahisa"; 
    $razon = "AHISA Laboratorio de Pruebas S. de R.L. de C.V.";
    $color = "background-color: #e86300";
    $color_tr="#e86300";
}
else if($cotizacion->id_empresa=="5") {
    $logo =  FCPATH."app-assets/img/logo_auven2.png";
    $name_firma ="Grupo Auven";
    $razon = "AUVEN S. de R.L. de C.V.";
    $color = "background-color: #0a3971";
    $color_tr="#0a3971";
}
?>

<style>
    .tr_table{
        color:white;
        background-color: <?php echo $color_tr; ?>;
    }
    .tr-sv {
        color: white;
        background-color: #4672c2;
    }
    .tr-sv_22 {
        background-color: #c1cbe5;
    }
    .tr-sv2 {
        background-color: #13bf0d;
    }

    .tr-svAh {
        background-color: #e86300;
    }
    .tr-svAu {
        background-color: #0a3971;
    }
    
    .firma{
        width: 10%;
        margin-top: 100px;
        
        margin-left: 25%;
    }
</style>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<div style="font-size: 11px">
    <table>
        <tr>
            <td width="30%" align="center"><img height="60px" src="<?php echo $logo;?>"><br></td>
            <td width="70%" align="right" style="font-size: 12px"><br><br>
                <strong><?php echo $razon; ?></strong>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center" style="font-size: 14px"><br><strong>COTIZACIÓN DE SERVICIO</strong></td>
        </tr>
        <tr>
            <td align="right" colspan="2"><strong>Fecha: <?php echo $dia." de ".$mes." de ".$anio; ?>
                    <br>Folio Cotización: 
                    <?php echo $cotizacion->folio; ?> 
                    <br>No.Cotización: 
                    <?php echo $cotizacion->id; ?>
                    </strong>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left"><strong>
                <br><br><?php echo $cotizacion->empresa; ?>
                <br><br><?php echo $cotizacion->calle." ".$cotizacion->no_ext; ?>,  
                <br><?php echo $cotizacion->colonia; ?>,  
                <br><?php echo $cotizacion->poblacion; ?>, <?php echo $cotizacion->estado; ?>, C.P. <?php echo $cotizacion->cp; ?> 
                <br><br>AT´N: <?php echo $contacto->nombre; ?> / <?php echo strtoupper($contacto->puesto); ?> 
                </strong>
            </td>
        </tr>
        <tr>
            <?php if($cotizacion->id_empresa=="5"){ //auven ?>
                <td colspan="2" align="justify">
                    <br><br>Nos complace presentar a su consideración nuestra cotización para satisfacer los requerimientos escritos y/o verbales proporcionados por ustedes.
                    <br><br>Para cualquier aclaración y en espera de ser favorecidos por su elección, quedamos a sus órdenes.
                </td>
            <?php } else{ ?>
                <td colspan="2" align="justify">
                    <br><br>En atención a su amable solicitud de cotización, le presento nuestra propuesta técnico-económica, conforme a los lineamientos establecidos por las Normas Oficiales Mexicanas correspondientes. Quedamos a sus órdenes para cualquier duda o aclaración, esperando ser favorecidos con su elección. 
                    <br><br><strong>Objetivo.-</strong> Cumplir las medidas Ambientales y de Seguridad e Higiene que establecen las Normas Mexicanas para los centros de trabajo, a fin de proveer un ambiente seguro y saludable en la realización de las tareas que desarrollen los trabajadores. 
                    <br><br><strong>Beneficio.-</strong> El cliente estará dando cumplimiento a sus obligaciones en materia de Seguridad Industrial, así como a los requerimientos que establecen las diferentes dependencias. 
                </td>
            <?php } ?>
        </tr>
        <tr>
            <td colspan="2">
                <div style="border-bottom: 5px solid <?php echo $color_tr; ?>"></div>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="justify"><b>COMUNICADO: COMPROMETIDOS CON EL MEDIO AMBIENTE, A PARTIR DE ENERO 2024 NUESTROS INFORMES SERÁN DIGITALES, CON EL OBJETIVO DE LA REDUCCION DEL USO DE PAPEL, EN CASO QUE REQUIERA EL INFORME EN FÍSICO SE CARGARÁ UN VALOR ECONÓMICO ADICIONAL MISMO QUE SERÁ ANEXADO A LA PROPUESTA ECONÓMICA.</b></td>
        </tr>
        <tr>
            <td colspan="2">
                <div style="border-bottom: 5px solid <?php echo $color_tr; ?>"></div>
            </td>
        </tr>
    </table>
    <br><br><br>
    <table align="center" nobr="true" style="border: 1px solid black">
        <tr align="center" valign="middle" class="tr_table">
            <td rowspan="2" width="10%">PARTIDA</td>
            <td rowspan="2" width="52%">DESCRIPCIÓN</td>
            <td rowspan="2" width="10%">CANT.</td>
            <td colspan="2" width="28%">COSTOS</td>
        </tr>
        <tr class="tr_table">
            <td>UNITARIO</td>
            <td>TOTAL</td>
        </tr>
    </table>
        <?php 
        $i=1; $total=0;

        $vowels = array(
                     '<div style="text-align: center;"><span style="mso-bidi-font-size:12.0pt;line-height:115%;font-family:" century="" gothic",sans-serif;="" mso-bidi-font-family:arial"=""><font face="Helvetica"></font></span></div>',
                    '<div><div><span style="font-family:" century="" gothic",sans-serif;mso-bidi-font-family:arial"=""><font face="Arial"></font></span></div></div>',
                    '<div><font face="Arial"><span style="font-size:14px;"></span></font><div><span style="font-size: 9pt; letter-spacing: 0.3px;"><font face="Arial Black"></font></span></div><div><span style="font-family: Arial, sans-serif; font-size: 9pt; letter-spacing: 0.3px;"><br></span></div><div><span lang="EN-US"><o:p></o:p></span></div></div>',
                    '<div style="font-size: 14px; text-align: left;"><span style="font-size: 10.5pt;"><span style="font-size:12px;"><font face="Arial"><br></font></span></span></div>',
                    '<div><font face="Arial"><span style="color: rgb(0, 0, 0); font-size: medium;"><b></b></span></font></div>',
                    '<div><span style="font-size:10.0pt;line-height:107%;font-family:
" century="" gothic",sans-serif"=""><font face="Arial"></font></span></div>',
                    '<span style="font-size:12px;"></span>',
                    '<span lang="EN-US"><o:p></o:p></span>',
                    
                    '<font face="Arial"><br></font>',
                    '<span style="font-size: 10.5pt;"><b><br></b></span>',
                    '<div style="font-size: 14px; text-align: left;"><br></div>',
                    '<div style="font-size: 14px; text-align: left;"><br></div><div style="font-size: 14px;"><span style="font-size: 10.5pt;"><b><br></b></span></div></div>',
                    '<div style="font-size: 14px;"></div>',
                    '<span style="font-family: Arial; font-size: 12px;"><br></span>',
                    '<span style="font-size: 12px;"><br></span>',
                    '<span style="font-family:" century="" gothic",sans-serif;="" mso-bidi-font-family:arial"=""><font face="Tahoma"></font></span>',
                    '<span style="font-size:10.0pt;line-height:107%;font-family:
" century="" gothic",sans-serif"=""><b><font face="Arial"></font></b></span>',
                    '<span style="font-size:12px;"><br></span>',
                    '<div><br></div>',
                    '<div><div></div></div>',
                    '<div></div>',
                    '<b></b>',
                    '<div><br></div>

                    <span arial",sans-serif;="" mso-fareast-font-family:"times="" new="" roman";mso-fareast-language:es-mx"="" style="font-size: 10pt;"><span style="font-weight: bolder;"></span><span style="font-weight: bolder;"></span></span><span times="" new="" roman",serif;="" mso-fareast-font-family:"times="" roman";mso-fareast-language:es-mx"="" style="font-size: 10pt;"><o:p></o:p></span><br><span arial",sans-serif;="" mso-fareast-font-family:"times="" new="" roman";mso-fareast-language:es-mx"="" style="font-size: 10pt;"><span style="font-weight: bolder;"></span></span>

                    <span style="font-size: 10pt; font-family: Arial, sans-serif;"><span style="font-weight: bolder;"></span>
</span><span style="font-size: 10pt; font-family: Arial, sans-serif; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span style="font-size: 10pt; line-height: 14.2667px;"></span></span>

<span style="font-weight: bolder;"><span style="font-size: 10pt; font-family: Arial, sans-serif; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"></span></span><span style="font-size: 10pt; font-family: Arial, sans-serif; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"></span><u><span style="font-size: 10pt; font-family: Arial, sans-serif;"></span></u><span style="font-size: 10pt;"><o:p></o:p></span><span arial",sans-serif;="" mso-fareast-font-family:"times="" new="" roman";mso-fareast-language:es-mx"="" style="font-size: 10pt;"></span>',
                );
        $rowfila=1;
        foreach ($servicios as $s) { 
            if($rowfila>1){
                $nobrrow='nobr="true"';
            }else{
                $nobrrow='';
            }
            ?>
            <table border="1" <?php echo $nobrrow;?> align="center" style="font-size: 10.5px !important;">
                <tr nobr='true' style="border: 1px solid black; white-space: pre" >
                    <td width="10%"><?php echo $i ?></td>
                    <td width="52%" style="font-family: Verdana !important; font-size: 9.5px; text-align: left; text-align: justify;"><?php //echo $s->nombre." ".str_replace($vowels, "", $s->descripcion); 
                        if($s->id_empresa_serv==1 || $s->id_empresa_serv==2 || $s->id_empresa_serv==3 || $s->id_empresa_serv==6){
                            //str_replace("<br>","", $s->nombre);
                            str_replace("<div>","", $s->descripcion); str_replace("</div>","", $s->descripcion);
                            /*str_replace("<br>","", $s->descripcion); str_replace("<p>","", $s->descripcion);*/
                            /*echo '<FONT FACE="arial">'.preg_replace("/[\r\n|\n|\r]+/","", $s->nombre).". <br>".preg_replace("/[\r\n|\n|\r]+/","", $s->descripcion).'</FONT>';*/
                            //log_message('error', 'descripcion'.$s->descripcion);
                            //echo "<b>".$s->nombre.".</b><br><br>".htmlspecialchars($s->descripcion);
                            echo "<b>".$s->nombre.".</b><br><br>".str_replace($vowels, "", $s->descripcion); 
                        }else if($s->id_empresa_serv==4){
                            //str_replace("<br>","", $s->nombre2);
                            str_replace("<div>","", $s->descripcion2); str_replace("</div>","", $s->descripcion2);
                            /*str_replace("<br>","", $s->descripcion2); str_replace("<p>","", $s->descripcion2);*/
                            /*echo '<FONT FACE="arial">'.preg_replace("/[\r\n|\n|\r]+/","", $s->nombre2).". <br>".preg_replace("/[\r\n|\n|\r]+/","", $s->descripcion2).'</FONT>';*/
                            //log_message('error', 'descrip'.$s->descrip2);
                            //echo "<b>".$s->nombre2.".</b><br><br>".htmlspecialchars($s->descripcion2);
                            //log_message('error', 'descrip'.str_replace($vowels, "", $s->descripcion2));
                            echo "<b>".$s->nombre2.".</b><br><br>".str_replace($vowels, "", $s->descripcion2);
                        }
                        else if($s->id_empresa_serv==5){
                            //str_replace("<br>","", $s->nombre3);
                            str_replace("<div>","", $s->descripcion3); str_replace("</div>","", $s->descripcion3);
                            /*str_replace("<br>","", $s->descripcion3); str_replace("<p>","", $s->descripcion3);*/
                            /*echo '<FONT FACE="arial">'.preg_replace("/[\r\n|\n|\r]+/","", $s->nombre3).". <br>".preg_replace("/[\r\n|\n|\r]+/","", $s->descripcion3).'</FONT>';*/
                            //log_message('error', 'descrip'.$s->descrip3);
                            //echo "<b>".$s->nombre3.".</b><br><br>".htmlspecialchars($s->descripcion3);
                            echo "<b>".$s->nombre3.".</b><br><br>".str_replace($vowels, "", $s->descripcion3);
                        }
                        ?>      
                    </td>
                    <td width="10%"><?php echo $s->cantidad; ?></td>
                    <td width="14%">$ <?php echo number_format($s->precio,2); ?></td>
                    <td width="14%">$ <?php echo number_format($s->precio*$s->cantidad,2) ?></td>
                </tr>
            </table>
            <?php 
            $total+=$s->precio*$s->cantidad;
            $i++;
            $rowfila++;
        }
        ?> 
        <table border="1" nobr="true" align="center">
        <tr>
            <td width="72%"></td>
            <td width="14%" class="tr_table">TOTAL</td>
            <td width="14%" class="tr_table">$<?php echo number_format($cotizacion->subtotal,2); ?></td>
        </tr>
        </table>
        <?php if($cotizacion->descuento!=0){ 
            $desc_cantp = $cotizacion->subtotal*$cotizacion->descuento/100;
            $desc_cant = $cotizacion->subtotal - $desc_cantp; ?>
            <table border="1" nobr="true" align="center">
                <tr>
                    <td width="10%"></td>
                    <td width="52%">Descuento sobre el total<br>*puede no aplicar sobre todos los servicios</td>
                    <td width="10%"></td>
                    <td width="14%">%<?php echo $cotizacion->descuento; ?></td>
                    <td width="14%">$<?php echo number_format($desc_cantp,2);?></td>
                </tr>
                 <tr>
                    <td colspan="3"></td><td class="tr_table">CON DESCUENTO</td><td class="tr_table">$<?php echo number_format($desc_cant,2); ?></td>
                </tr>
            </table>
        <?php
            
        }
        
        ?>

       
    
    <!--
    <table border="1" nobr="true">
        <tr>
            <td width="10%"></td>
            <td width="40%"></td>
            <td width="10%"></td>
            <td width="20%"></td>
            <td width="20%"></td>
        </tr>
    </table>-->
    <br><br>
    <h3>Notas: Los precios NO incluyen el 16% de I.V.A</h3>
    <p><?php echo $cotizacion->observaciones; ?></p>
    <?php if($cotizacion->id_empresa=="4" || $cotizacion->id_empresa=="5"){ ?>
        <table align="center">
            <tr nobr='true' class="tr-sv"><td>1.Fecha de programación</td></tr>
            <tr nobr='true' class="tr-sv_22"><td>Dos días hábiles para dar respuesta desde que se acepta el servicio o se otorga la orden de compra.</td></tr>
            <tr nobr='true'><td><img height="20px" width="35px" src="/app-assets/img/down-arrow.png"></td></tr>
            
            <tr nobr='true' class="tr-sv"><td>2.Trabajo de campo</td></tr>
            <tr nobr='true' class="tr-sv_22"><td>Dependerá del alcance y/o magnitud de los trabajos</td></tr>
            <tr nobr='true'><td><img height="20px" width="35px" src="/app-assets/img/down-arrow.png"></td></tr>
            
            <tr nobr='true' class="tr-sv"><td>3.Elaboración de dictamen y entrega</td></tr>
            <tr nobr='true' class="tr-sv_22"><td>20 días hábiles posteriores a los trabajos de campo, siempre y cuando la información solicitada al cliente esté completa, de lo contrario contarán a partir de que este punto esté cubierto.</td></tr>

            <?php if($cotizacion->id_empresa=="4"){ //ahisa ?>
                <tr nobr='true'><td><img height="20px" width="35px" src="/app-assets/img/down-arrow.png"></td></tr>
                <tr nobr='true' class="tr-sv"><td>4. Entrega de carpeta de resultados al cliente</td></tr>
                <?php if($cotizacion->id_empresa=="1" || $cotizacion->id_empresa=="2" || $cotizacion->id_empresa=="3" || $cotizacion->id_empresa=="6") { ?>
                    <tr nobr='true' class="tr-sv_22"><td>De 20 a 35 días hábiles</td></tr>
                <?php } ?>
                <?php if($cotizacion->id_empresa=="4" || $cotizacion->id_empresa=="5") { ?>
                    <tr nobr='true' class="tr-sv_22"><td>De 20 días hábiles</td></tr>
                <?php } 
            } ?>    
        </table>
    <?php } ?>
    
    <p align="center">Nota: Los tiempos pueden variar con algunos proyectos, en ese caso se indicará desde la programación. <br> <?php //echo $cotizacion->observaciones;?></p>
    <br>
    <h4>Forma de Pago:</h4>
    <p>
        <?php
            switch($cotizacion->forma_pago){
                case 1: echo "50% de anticipo y 50% contraentrega"; break;
                case 2: echo "Crédito 30 días"; break;
                case 3: echo "Crédito 60 días"; break;
                case 4: echo "100% contraentrega"; break;
                case 5: echo "En común acuerdo con el cliente"; break;
            }
        ?> 
    </p>
    <br><br>
    <h4>Aclaraciones generales:</h4>
    <ul>
        <li>Tiempos de entrega serán de 20 días hábiles posteriores a la realización de los trabajos y entrega de toda la información requerida y 60 días hábiles para los reconocimientos y evaluación de la NOM-010-STPS y servicios sub contratados.</li>
        <li>Los precios NO incluyen el 16% de I.V.A.</li>
        <li>Después de los 20 días de la realización de los trabajos, si el cliente no proporciona
        la información y el servicio se demora, se enviará la factura correspondiente para
        su trámite de pago.</li>
        <li>Si se realizara un viaje en falso, es decir que el cliente no tenga las condiciones
        para la realización de los trabajos y estos no se puedan realizar, se hará un cobro de $ 5,000 por la visita.
        En caso de cancelaciones o cambios de fecha, estos deberán ser con 7 días hábiles de anticipación para que no se realice el cobro de viaje en falso.</li>
        <li>Cualquier cambio que se realice después de entregadas las carpetas y/o dictámenes, tendrá un costo adicional de $ 2,000.00</li>
        <li>La vigencia de la presente cotización es de 30 días naturales posteriores a la fecha de elaboración.</li>
        <li>En caso de aceptación de la propuesta favor de remitir a <?php echo $name_firma; ?> ya sea por medio electrónico o bien por medio físico, la presente cotización firmada, o bien orden de compra mediante la cual se consideran aceptados los términos y clausulas especificadas en la presente cotización.</li>
        <li>En caso de tener alguna queja o corrección sobre los servicios entregados, el responsable del proyecto por parte del cliente debe de realizar una carta membretada dirigida a: Área de Calidad de <?php echo $name_firma; ?>, explicando la situación del porqué de su inconformidad junto con la evidencia del hecho hasta 5 días hábiles después de la entrega de la documentación de los servicios, para que en caso de realizar algún cambio en su informe, dictamen o constancias de capacitaciones, se analice la situación y se lleven a cabo las acciones pertinentes.</li>
    </ul>
    <p align="justify">Sin otro particular, agradecemos la oportunidad que nos brindan de ofrecerles nuestra propuesta misma que esperamos cumpla con sus expectativas para la realización de este trabajo. </p>
    <br><br><br><br><br><br>
    <table align="center">
        <tr nobr='true'>
            <td width="5%"></td>
            <td width="42%"><br><br><br><br><strong>ATENTAMENTE</strong></td>
            <td width="6%"></td>
            <td width="42%"><br><br><br><br><strong>ACEPTO COTIZACIÓN</strong></td>
            <td width="5%"></td>
        </tr>
        <tr nobr='true'>
            <td></td>
            <td><br><br><p style="border-bottom: solid 2px black; "></p><br><strong><?php echo $cotizacion->vendedor;?><br><?php echo $name_firma; ?></strong></td>
            <td></td>
            <td><br><br><p style="border-bottom: solid 2px black; "></p><br><strong><?php echo $contacto->nombre."<br>".$cotizacion->empresa; ?></strong></td>
            <td></td>
        </tr>
    </table>
</div>