<?php
$color = "#CCFF99";
$mes = $orden->mesn;
//function mes(){
    //$mes="enero";
    

    switch ($mes) {
        case 1: $mes="Enero"; break;
        case 2: $mes="Febrero"; break;
        case 3: $mes="Marzo"; break;
        case 4: $mes="Abril"; break;
        case 5: $mes="Mayo"; break;
        case 6: $mes="Junio"; break;
        case 7: $mes="Julio"; break;
        case '8': $mes="Agosto"; break;
        case 9: $mes="Septiembre"; break;
        case 10: $mes="Octubre"; break;
        case 11: $mes="Noviembre"; break;
        case 12: $mes="Diciembre"; break;
    }
  //  return $mes;
//}
$razon="";
if($cotizacion->id_empresa=="1" || $cotizacion->id_empresa=="2" || $cotizacion->id_empresa=="3" || $cotizacion->id_empresa=="6") {
    $logo =  FCPATH."app-assets/img/logo.png";
    $razon = "Soluciones Ambientales y en Seguridad e Higiene S.A. de C.V.";
}
else if($cotizacion->id_empresa=="4") {
    $logo =  FCPATH."app-assets/img/ahisa.png";
    $razon = "AHISA Laboratorio de Pruebas S. de R.L. de C.V."; 
}
else if($cotizacion->id_empresa=="5") {
    $logo =  FCPATH."app-assets/img/logo_auven2.png";
    $razon = "AUVEN S. de R.L. de C.V.";
}
?>

<style>
    .tr-sv {
        color: white;
        background-color: #404244;
    }
    .tr-sv2 {
        background-color: #13bf0d;
    }
    
    .firma{
        width: 10%;
        margin-top: 100px;
        
        margin-left: 25%;
    }
</style>


<div style="font-size: 11px">
    <table>
        <tr>
            <td width="30%" align="center"><img height="60px" src="<?php echo $logo;?>"><br></td>
            <td width="70%" align="right" style="font-size: 12px"><br><br>
                <strong><?php echo $razon; ?></strong>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center" style="font-size: 14px"><br><strong>ORDEN DE TRABAJO</strong></td>
        </tr>
        <tr>
            <td align="right" colspan="2"><strong>Emisión: <?php echo $orden->dia." de ".$mes." de ".$orden->anio; ?></strong>
            </td>
        </tr>
    </table>
    <br><br><p></p>
    <table border="1">
        <tr>
            <td><strong> (1) No.Cotización:</strong></td>
            <td> <?php echo $orden->cotizacion_id; ?></td>
            <td><strong> (2) No.Orden de trabajo:</strong> <?php echo $orden->id; ?></td>
        </tr>
        <tr>
            <td><strong> (3) Fecha de aceptación:</strong></td>
            <td> <?php echo $orden->fecha_creacion; ?></td>
            <td rowspan="2"></td>
        </tr>
        <tr>
            <td><strong> (4) Vendedor:</strong></td>
            <td> <?php echo $orden->vendedor; ?></td>
        </tr>
    </table>
    <br>
    <h3>DATOS DE LA EMPRESA</h3>
    <table border="1">
        <tr>
            <td width="30%"><strong> (5)Razón Social y RFC:</strong></td>
            <td width="70%"> <?php echo $cotizacion->empresa; ?></td>
        </tr>
        <tr>
            <td><strong> (6)Dirección completa:</strong></td>
            <td> <?php echo $cotizacion->calle." ".$cotizacion->no_ext."  ".$cotizacion->colonia; ?>
                <br> <?php echo $cotizacion->poblacion.", ".strtoupper($cotizacion->estado)."  CP ".$cotizacion->cp; ?>
            </td>
        </tr>
        <tr>
            <td><strong> (7)Referencia de ubicación:</strong></td>
            <td> <?php echo $cliente->referencia; ?></td>
        </tr>
        <tr>
            <td><strong> (8)Giro de la empresa:</strong></td>
            <td> <?php echo $cliente->giro; ?></td>
        </tr>
        <tr>
            <td><strong> (9)Representante legal:</strong></td>
            <td> <?php echo $cliente->representa; ?></td>
        </tr>
    </table>
    <br>
    <h3>CONTACTO</h3>
    <table border="1">
        <tr>
            <td><strong> Nombre</strong></td>
            <td><strong> Teléfono</strong></td>
            <td><strong> Correo electrónico</strong></td>
        </tr>
        <?php foreach ($contactos as $k) { 
            if($k->orden=="1"){ ?>
                <tr>
                    <td> <?php echo $k->nombre; ?></td>
                    <td> <?php echo $k->telefono; ?></td>
                    <td> <?php echo str_replace("<", "", $k->email); ?></td>
                </tr>
            <?php }
         } ?>
    </table>

    <br>
    <h3>A QUIEN VA DIRIGIDO</h3>
    <table border="1">
        <tr>
            <td><strong> Nombre ATN</strong></td>
        </tr>
        <tr>
            <td> <?php echo $orden->nombre_atn; ?></td>
        </tr>
    </table>
   
    <br>
    <h3>REQUISITOS DE INGRESO</h3>
    <table border="1">
        <tr>
            <td width="30%"><strong> (13)¿Curso de seguridad?</strong></td>

            <?php if ($orden->curso_seguridad==1) {?>

            <td width="10%"><strong> Si:</strong> <?php echo 'X' ?></td>
            <td width="10%"><strong> No:</strong></td>
            <?php } else { ?>
            <td width="10%"><strong> Si:</strong></td>   
            <td width="10%"><strong> No:</strong> <?php echo 'X' ?></td>
            <?php } ?> 
            <td width="25%"><strong> Hora de Inicio:</strong></td>
            <td width="25%"> <?php echo $orden->hora_inicio; ?></td>
        </tr>
        <tr>
            <td><strong> (14)Formatos de Ingreso</strong><br><br></td>
            <td colspan="4"> <?php echo $orden->formato_ingreso; ?></td>
        </tr>
        <tr>
            <td rowspan="2"><strong> (15)Equipo de seguridad</strong></td>
            <td colspan="4"><strong> Básico (calzado de seguridad, lentes, tapones auditivos, casco, chaleco reflejante):</strong><br></td>
        </tr>
        <tr>
            <td colspan="4"><strong> Especial (especificar):</strong><br><br></td>
        </tr>
    </table>
    <table border="1" align="center">
        <tr><td colspan="3"><h3>(16)TURNOS DE TRABAJO</h3></td></tr>
        <tr>
            <td><strong>TURNO</strong></td>
            <td><strong>INICIO</strong></td>
            <td><strong>FIN</strong></td>
        </tr>
        <tr>
            <td><strong>1er Turno</strong></td>
            <td> <?php echo $cliente->turno1_inicio; ?></td>
            <td> <?php echo $cliente->turno1_fin; ?></td>
        </tr>
        <tr>
            <td><strong>2do Turno</strong></td>
            <td> <?php echo $cliente->turno2_inicio; ?></td>
            <td> <?php echo $cliente->turno2_fin; ?></td>
        </tr>
        <tr>
            <td><strong>3er Turno</strong></td>
            <td> <?php echo $cliente->turno3_inicio; ?></td>
            <td> <?php echo $cliente->turno3_fin; ?></td>
        </tr>
    </table>
    <br>
    <p><span style="background-color: yellow"><strong>*Indispensable enviar en digital, y llevar impreso IMSS y SUA a todos los trabajos</strong></span></p>
    <br>
    <h3>DESCRIPCIÓN DEL TRABAJO A REALIZAR</h3>
    <table border="1">
        <tr>
            <td width="20%"><strong> (17)No. De partida</strong></td>
            <td width="20%"><strong> (18)Cantidad de análisis</strong></td>
            <td width="30%"><strong> (19)Nombre</strong></td>
            <td width="30%"><strong> (20)Descripción</strong></td>
        </tr>
<?php $i = 1;
         foreach ($servicios as $s) { ?>
         <tr>
          <td> <?php echo $i; ?></td>
          <td> <?php echo $s->cantidad; ?></td>
          <td> <?php if($s->id_empresa_serv==1 || $s->id_empresa_serv==2 || $s->id_empresa_serv==3 || $s->id_empresa_serv==6){ 
                        echo $s->nombre; 
                    }
                    else if($s->id_empresa_serv==4){
                        echo $s->nombre2; 
                    }else if($s->id_empresa_serv==5){
                        echo $s->nombre3; 
                    }

            ?></td>
          <td> <?php if($s->id_empresa_serv==1 || $s->id_empresa_serv==2 || $s->id_empresa_serv==3 || $s->id_empresa_serv==6){ 
                    echo $s->descripcion; 
                }else if($s->id_empresa_serv==4){
                    echo $s->descripcion2; 
                }else if($s->id_empresa_serv==5){
                    echo $s->descripcion3; 
                }
            ?></td>
         </tr>
    <?php $i++;
} ?>
       
    </table>
    <p><strong>(21)Nombre y cargo a quien se dirigirán los técnicos el día del muestreo:</strong><br><br> 
        <?php echo $orden->nombre_cargo; ?>
        <hr>
    </p>
    <p><strong>(22)Nombre y cargo a quien se dirigirá el informe:</strong><br><br><?php echo $orden->nombre_atn; ?><hr></p>
    <br>
    <!--<h3>OBSERVACIONES PARA RECIPIENTES SUJETOS A PRESIÓN</h3>
    <table border="1">
        <tr>
            <td><strong>(23) Nombre de equipo || No. de identificación</strong></td>
            <td><strong>(24) Capacidad</strong></td>
            <td><strong>(25) Categoría</strong></td>
            <td><strong>(26) Zona de ubicación</strong></td>
        </tr>
        <?php $i = 1;
         foreach ($servicios as $s) { 
             if($s->equipos>0){
             ?>
        <tr>
            <td></td>
            <td></td>
            <td><?php echo $s->equipos; ?></td>
            <td></td>
        </tr>
        <?php 
             }
        $i++;
        } ?>
        <tr>
            <td colspan="4"><strong>(28)Total de equipos:</strong></td>
        </tr>
    </table>-->
    <br>
    <h4>(23) OBSERVACIONES:</h4>
    <br><br><?php echo $orden->observaciones; ?><hr></p>
    <p><strong>Acepto que la información asentada es veraz y correcta:</strong></p>
    <br>
    <p align="center">
        <span style="border-top: 1px black solid"><strong>(24)Nombre y Firma</strong></span>
    </p>
    <p align="center">
        <span style="border-top: 1px black solid"><?php if($orden->contacto!=""){ echo $orden->contacto; } else echo $orden->cliente ?></span>
    </p>
</div>