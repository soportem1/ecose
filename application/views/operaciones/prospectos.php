<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">Listado de Envíos</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <h5><i class="ft-mail"></i> Listado de Envíos</h5><hr>
                        <a href="<?php echo base_url(); ?>index.php/operaciones/nuevo_envio" class="btn btn-warning">Nuevo Envío</a>
                        <div style="overflow: scroll;">
                            <table class="table table-striped table-responsive" id="tabla">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>#</th>
                                        <th>Sucursal</th>
                                        <th>Piezas</th>
                                        <th>Fecha</th>
                                        <th>Destinatario</th>
                                        <th>Peso Total</th>
                                        <th>Precio Total</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<script>

    function load() {
        table.destroy();
        table = $('#tabla').DataTable({
            "ajax": {
                "url": "<?php echo base_url(); ?>index.php/operaciones/getEnvios"
            },
            "columns": [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": "<i class='ft-chevron-down' aria-hidden='true'></i>"
                },
                {"data": "id_envio", "sType": 'numeric'},
                {"data": "sucursal"},
                {"data": "no_piezas"},
                {"data": "fecha"},
                {"data": "cliente_r"},
                {"data": "total_peso"},
                {"data": "total_precio"},
                {
                    "data": null,
                    "defaultContent": "<button type='button' class='btn btn-sm btn-icon btn-success print1'><i class='ft-file-text'></i></button>\
                                       <button type='button' class='btn btn-sm btn-icon btn-warning print2'><i class='ft-tag'></i></button>"
                }
            ],
            "order": [[ 1, "asc" ]]
        });

    }
    
    function imprimir(tipo,id){
        window.open('<?php echo base_url(); ?>index.php/formatos/'+tipo+'/' +id, 
                         'imprimir', 
                         'width=600,height=500');
    }
    
    $(document).ready(function () {
        table = $('#tabla').DataTable();

        //Listener para impresion
        $('#tabla tbody').on('click', 'button.print1', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = row.data();
            imprimir("ticket",data.id_envio);
        });
        //Listener para impresion
        $('#tabla tbody').on('click', 'button.print2', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = row.data();
            imprimir("etiqueta",data.id_envio);
        });
        
        // Listener para informacion detalla de fila
        $('#tabla tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        });

        function format(d) {
            // 'd' son los datos originales de la tabla (json)
            return d.info_secundaria;
        }

        load();
    });
</script>