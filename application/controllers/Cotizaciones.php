<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cotizaciones extends CI_Controller {
    //estatus de la cotizacion:
    // 0-> eliminada
    // 1-> creada /pendiente
    // 2-> aceptada
    // 3-> rechazada
    // 4-> modificada

    public function __construct() {
        parent::__construct();
        //error_reporting(0);
        $logueo = $this->session->userdata('logeado_ecose');
        if($logueo!=1){
            redirect(base_url(), 'refresh');
        }
        date_default_timezone_set('America/Mexico_City');
        $this->load->model('Cotizaciones_model', 'model');
        $this->load->model('Catalogos_model');
    }

    public function index() {
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('cotizaciones/cotizaciones');
        $this->load->view('footer');
    }

    public function nueva_cotizacion() {
        $data['clientes'] = $this->Catalogos_model->getDataClientes();
        $data['aux_auto'] = 0;
        $data["id_empresa"]=$this->session->userdata("empresa");
        $data["sucursal"]=$this->session->userdata("sucursal");
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('cotizaciones/form_cotizacion',$data);
        $this->load->view('footer');
    }
    
    public function modificar_cotizacion($id,$auto=0) {
        $logueo = $this->session->userdata('logeado_ecose');
        if($logueo!=1){
            redirect(base_url(), 'refresh');
        }

        $data['clientes'] = $this->model->getselectwheren("clientes",array("status"=>1));  //ok 
        $data['cotizacion']=$this->model->getCotizacion($id); //ok
        $data['persona_contac']=$this->model->getContactoCotizaciones($data['cotizacion']->contacto_id); //ok
        $this->limpiaServicios();
        $servicios=$this->model->getServiciosCotizacion($id,$data['cotizacion']->id_empresa);   //ok
        $id_cs=0;        
        foreach($servicios as $s){
            $id_cs = $s->id;
            //log_message('error', 'id_cs: '.$id_cs);
            $this->agregarServicio($s->servicio_id, $s->cantidad,0,$data['cotizacion']->sucursal,$id_cs,$data['cotizacion']->cliente_id,$s->id_empresa_serv);
        }
        $data['cotizacion']->id=$id;
        $data['aux_auto']=$auto;
        /*if($data['cotizacion']->vendedor_id!=$this->session->userdata("id_usuario")&&$this->session->userdata("id_usuario")>1){
            redirect('cotizaciones');
        }else{*/

        $data["id_empresa"]=$data['cotizacion']->id_empresa;
        $data["sucursal"]=$data['cotizacion']->sucursal;
            $this->load->view('header');
            $this->load->view('menu');
            $this->load->view('cotizaciones/form_cotizacion',$data);
            $this->load->view('footer');
        //}
    }

    public function getCotizaciones() {
        $cotizaciones = $this->model_cotiza->getCotizaciones();
        $json_data = array("data" => $cotizaciones);
        echo json_encode($json_data);
    }
    
    public function searchContacto(){
        $id_cliente= $this->input->post("cliente");
        $contacto= $this->input->post("contacto");
        $contactos=$this->model->searchContactos($id_cliente);
        $result="";
        $sel="";
        foreach($contactos as $c){
            if($contacto==$c->id) {
                $sel="selected"; 
            }
            else{
                $sel="";
            }
            $result.="<option $sel value='$c->id'>$c->nombre</option>";
        }
        echo $result;
    }
   
    public function searchFamilias(){
        $tamaño=$this->input->post("tamaño");
        $id_emp = $this->session->userdata("empresa");
        if($this->session->userdata("id_usuario")!='1' /*&& $this->session->userdata("id_usuario")!='12'*/){
           $familias=$this->model->searchFamilias($tamaño,$id_emp);
            $result="";
            $emp ="";
            foreach($familias as $f){
                $result.="<option value='$f->id' data-emp='$id_emp'>$f->nombre</option>";
            } 
        }
        if($this->session->userdata("id_usuario")=='1' /*|| $this->session->userdata("id_usuario")=='12'*/){
            $familias=$this->model->searchFamiliasUnion($tamaño);
            $result="";
            $emp ="";
            foreach($familias as $f){
                if($f->empresa==1 || $f->empresa==2 || $f->empresa==3 || $f->empresa==6){
                    $emp="ECOSE";
                }else if($f->empresa==4){
                    $emp="AHISA";
                }else if($f->empresa==5){
                    $emp="AUVEN";
                }
                $result.="<option value='$f->id' data-emp='$f->empresa'>$f->nombre - $emp</option>";
            }
        }/*else{
            $familias=$this->model->searchFamilias($tamaño,$id_emp);
            $result="";
            $emp ="";
            foreach($familias as $f){
                $result.="<option value='$f->id' data-emp='$id_emp'>$f->nombre</option>";
            }
        }*/
        echo $result;
    }
    
    public function searchServicios(){
        $familia=$this->input->post("familia");
        $tamaño=$this->input->post("tamaño");
        $empresa=$this->input->post("id_emp");
        //$empresa = $this->session->userdata("empresa");
        $result="";
        if($familia!=""){
            $servicios=$this->model->searchServicios($familia,$tamaño,$empresa);
            foreach($servicios as $s){
                $rec="";
                switch ($s->recipientes){
                    case 1: $rec="CAT1"; break;
                    case 2: $rec="CAT2"; break;
                    case 3: $rec="CAT3"; break;
                }
                $result.="<option value='$s->id'>$s->nombre $rec</option>";
            }
        }
        
        echo $result;
    }
    
    public function searchProducto() {
        $modelo = $this->input->post("modelo");
        $tipo = $this->input->post("tipo");
        $data["producto"]=$this->model->searchProducto($modelo);
        //print_r($data); die;
        $data["producto"]->precio=$this->calculaPrecio($data["producto"]->precio, $tipo);
        
        echo json_encode($data);
    }
    
    public function agregarServicio($id_servicio,$cantidad,$print,$sucursal,$id,$id_cli,$id_emp_serv=0){ //agregamos servicio en la vista de cotizacion
        $logueo = $this->session->userdata('logeado_ecose');
        if($logueo!=1){
            redirect(base_url(), 'refresh');
        }
        $cotiza=$this->session->userdata('cotiza');
        $id_emp = $this->input->post("id_emp");
        $getsuc = $this->Catalogos_model->getItemCatalogo("clientes",$id_cli);
        $id_emp_ses = $this->session->userdata("empresa");
        $tipo_cli = $getsuc->tipo_cli;
        $nva_suc = 0; $suc_aux=0;
        //if($id_emp_ses==1 || $id_emp_ses==2 || $id_emp_ses==3 || $id_emp_ses==6){ //cotizacion para ecose
        if($id_emp==1 || $id_emp==2 || $id_emp==3 || $id_emp==6){ //cotizacion para ecose
            //$nva_suc = 1;
            $suc_aux=1;
            if($sucursal==0)
                $nva_suc = $this->session->userdata("sucursal");
            else
                $nva_suc = $sucursal;
        }
        else{ //empresa ahisa o auven
            if($sucursal==0)
                $nva_suc = $id_emp;
            else
                $nva_suc = $sucursal;
        }

        $result=""; 
        /*log_message('error', 'id_emp_ses: '.$id_emp_ses);
        log_message('error', 'nva_suc: '.$nva_suc);*/
        //log_message('error', 'id desde agregarServicio: '.$id);
        //log_message('error', 'id_servicio agregarServicio: '.$id_servicio);
        //log_message('error', 'id_emp : '.$id_emp);
        if($id_servicio!=""){ //existe servicio asginado-
            if(isset($id_emp)){
                if($id_emp==1 || $id_emp==2 || $id_emp==3 || $id_emp==6){
                    $suc_aux=1;
                    $servicio=$this->model->getNRow("servicios",$id_servicio); 
                }else if($id_emp==4){
                    $suc_aux=4;
                    $servicio=$this->model->getNRow("servicios_ahisa",$id_servicio); 
                }else if($id_emp==5){
                    $suc_aux=5;
                    $servicio=$this->model->getNRow("servicios_auven",$id_servicio); 
                }
            }else{
                //$id_emp = $this->session->userdata("empresa");
                $id_emp = $id_emp_serv;
                log_message('error', 'id_emp : '.$id_emp);
                if($id_emp==1 || $id_emp==2 || $id_emp==3 || $id_emp==6){
                    $suc_aux=1;
                    $servicio=$this->model->getNRow("servicios",$id_servicio); 
                }else if($id_emp==4){
                    $suc_aux=4;
                    $servicio=$this->model->getNRow("servicios_ahisa",$id_servicio); 
                }else if($id_emp==5){
                    $suc_aux=5;
                    $servicio=$this->model->getNRow("servicios_auven",$id_servicio); 
                }
            }

            $coincidencia=0;  $i=1;
            //log_message('error', 'id desde agregarServicio: '.$id);
            foreach($cotiza as &$c){
                if($c["id"]==$servicio->id && $id_emp==$c["id_emp"]){
                    $c["cantidad"]=$c["cantidad"]+1;
                    $coincidencia=1;
                    //$c["id_cs"]=$id;
                    log_message('error', 'id_cs desde agregarServicio: '.$id);
                    log_message('error', 'suc_aux : '.$suc_aux);
                    if($suc_aux==1){
                        $c["precio"]=$this->validaPrecio($c["id"],$c["cantidad"],$nva_suc,$id_emp); //debemos mandar id_suc si e logueado es admin
                    }else{ //traer precio de servicio si el cliente es directo o consultor
                        $c["precio"]=$this->validaPrecioOtra($c["id"],$c["cantidad"],$tipo_cli,$id_emp);
                    }
                }
                $result.=$this->rowString($i, $c["clave"], $c["descripcion"], $c["precio"], $c["cantidad"], $c["id_cs"], $c["id_emp"]);
                $i++;
            }
            if($coincidencia==0){
                if($suc_aux==1){
                    $precio=$this->validaPrecio($servicio->id,$cantidad,$nva_suc,$id_emp); //aqui manda a validar precio por sucursal
                }
                else{ //traer precio de servicio si el cliente es directo o consultor
                    $precio=$this->validaPrecioOtra($servicio->id,$cantidad,$tipo_cli,$id_emp);
                }
                array_push($cotiza,array("id"=>$servicio->id,"clave"=>$servicio->clave,"descripcion"=>$servicio->descripcion,"cantidad"=>$cantidad,"precio"=>$precio, "categoria"=>$servicio->familia, "aplicaDesc"=>$servicio->aplica_desc,"id_cs"=>$id,"id_emp"=>$id_emp));
                $result.=$this->rowString($i, $servicio->clave, $servicio->descripcion, $precio,1,$servicio->id,$id_emp);
            }
            $this->session->set_userdata('cotiza',$cotiza);
        }
        if($print){
            echo $result;
        }
    }
    
    public function cambiaCantidad(){ //cambia la cantidad de servicios en la vista de cotizacion
        $fila=$this->input->post("row")-1;
        $cantidad=$this->input->post("cantidad");
        $cotiza=$this->session->userdata('cotiza');
        $id_cli=$this->input->post("id_cli");

        $id_empresa=$this->input->post('id_empresa');
        $sucursal=$this->input->post("sucursal");
        
        $result=""; $i=0; $iaux=1;
        /*log_message('error', 'cantidad: '.$cantidad);
        log_message('error', 'i: '.$i);
        log_message('error', 'iaux: '.$iaux);
        log_message('error', 'fila: '.$fila);*/
        log_message('error', 'id_empresa: '.$id_empresa);
        $getsuc = $this->Catalogos_model->getItemCatalogo("clientes",$id_cli);
        //$id_emp_ses = $this->session->userdata("empresa");
        //$sucursal = $this->session->userdata("sucursal");
        $tipo_cli = $getsuc->tipo_cli;

        foreach($cotiza as &$c){
            if($i==$fila){
                $c["cantidad"]=$cantidad;
                //$getsuc = $this->Catalogos_model->getItemCatalogo("clientes",$id_cli);
                //$id_emp_ses = $this->session->userdata("empresa");
                $tipo_cli = $getsuc->tipo_cli;
                if($id_empresa==1 || $id_empresa==2 || $id_empresa==3 || $id_empresa==6){ //cotizacion para ecose
                    $c["precio"]=$this->validaPrecio($c["id"],$c["cantidad"],$sucursal,$id_empresa);
                }else{ //traer precio de servicio si el cliente es directo o consultor
                    //$nva_suc = $this->session->userdata("sucursal");
                    $c["precio"]=$this->validaPrecioOtra($c["id"],$cantidad,$tipo_cli,$id_empresa);
                }
                
            }
            $result.=$this->rowString($iaux, $c["clave"], $c["descripcion"], $c["precio"], $c["cantidad"], $c["id_cs"], $c["id_emp"]);
            
            $i++;
            $iaux++;
        }
        $this->session->set_userdata('cotiza',$cotiza);
        echo $result;
    }

    public function eliminaServicio2(){ //elimina un servicio en la vista de cotizacion
        $row=$this->input->post("row");
        $eli=$this->input->post("eli"); //id del servicio
        log_message('error', 'eli: '.$eli);
        $cotiza=$this->session->userdata('cotiza');
        $i=0;
        foreach($cotiza as &$c){
            $i++;
            //log_message('error', 'id_cs: '.$c["id_cs"]);
            if($eli>0 && $c["id_cs"]==$eli){
                //unset($cotiza[$elim]);
                log_message('error', 'elimino al servi id: '.$c["id_cs"]);
                log_message('error', 'valor de i: '.$i);
                //unset($cotiza['id_cs'][$eli]);
                //unset($cotiza[$i]);
                //unset($cotiza[$eli]);
                //unset($cotiza["id_cs"]);
                unset($cotiza[$row]);
            }else if($c["id_cs"]==0){
                //$pos = key($cotiza);
                //unset($cotiza[$pos]);
                log_message('error', 'pos: '.$pos);
            }
        }
        $this->session->set_userdata('cotiza',$cotiza);
    }
    
    public function eliminaServicio(){ //elimina un servicio en la vista de cotizacion
        $eli=$this->input->post("eli");
        $band_eli=$this->input->post("band_eli");
        //log_message('error', 'eli: '.$eli);
        
        $fila=$this->input->post("row")-1;
        if($band_eli>1){
            //$fila=$fila-1;
        }
        $ban_eli = 0; $elim=0; $cont_reg2=-1;
        
        $cotiza=$this->session->userdata('cotiza');
        $this->session->set_userdata('cotiza',$cotiza);
        //log_message('error', 'fila: '.$fila);
        //log_message('error', 'eli: '.$eli);
        //unset($cotiza[$fila]);
        $result=""; $i=1; $pos=1; $tot_serv=0;

        //log_message('error', 'tot_serv: '.$tot_serv);
        $nvocont=-1; $aux=0; $cont_reg=0; $pos_eli=0;
        /*$clave = array_search($eli, $ );
        log_message('error', 'clave: '.$clave);
        unset($cotiza[$clave]);*/

        foreach($cotiza as &$c){             
            //log_message('error', 'id_cs: '.$c["id_cs"]);
            if(intval($c["id_cs"])==intval($eli)){
                //log_message('error', 'valor de pos: '.$nvocont);
                //log_message('error', 'nvocont: '.$nvocont);
                //log_message('error', 'eli: '.$eli);
                log_message('error', 'id_cs: '.$c["id_cs"]);
                log_message('error', 'eli: '.$eli);
                log_message('error', 'aux: '.$aux);
                //log_message('error', 'cont_reg: '.$cont_reg);
                //log_message('error', 'elim: '.$elim);
                //unset($cotiza[$elim]);
                $pos_eli=$aux;
                //$pos = key($cotiza);
                unset($cotiza[$aux]);
                $this->session->set_userdata('cotiza',$cotiza);
                //log_message('error', 'count(cotiza): '.count($cotiza));
            }else if(intval($c["id_cs"])==0){
                $pos = key($cotiza);
                unset($cotiza[$pos]);
                //log_message('error', 'pos: '.$pos);
            }
            $aux++;
            
        }

        //unset($cotiza[$pos_eli]);
        //$this->session->set_userdata('cotiza',$cotiza);

        $cotiza=$this->session->userdata('cotiza');
        foreach($cotiza as &$c){
            $result.=$this->rowString($i, $c["clave"], $c["descripcion"], $c["precio"], $c["cantidad"], $c["id_cs"], $c["id_emp"]);
            $i++;
        }
        $this->Catalogos_model->updateCatalogo(array("status"=>0),$eli,"cotizaciones_has_servicios");
        echo $result;
        //log_message('error', 'result: '.$result);
    }

    
    private function rowString($partida,$clave,$desc,$precio,$cant,$id,$id_emp){ //string de row para un servicio
        //if(!$this->session->userdata("administrador")&&$clave!=""){
            $btonE = "<td><button type='button'  onclick='elimina_servicio($(this),".$id.",".$partida.")' class='btn btn-sm mb-0 btn-outline-danger'><i class='ft-minus'></i></button></td>";
        /*}
        else{
            $btonE = "<td><button type='button' disabled onclick='elimina_servicio($(this))' class='btn btn-sm mb-0 btn-outline-danger'><i class='ft-minus'></i></button></td>";
        }*/
        return "<tr class=ord_".$partida.">"
                    . "<td>$partida</td>"
                    . "<td><input class='id_serv_".$id."' id='id_serv_add' type='hidden' value='$id'>$clave</td>"
                    . "<td>$desc</td>"
                    . "<td><input data-idemp='".$id_emp."' onchange='valida_descuentos();' class='cant form-control form-control-sm width-150' value='$cant'></td>"
                    . "<td>".number_format($precio,2)."</td>"
                    . "<td>".number_format($precio*$cant,2)."</td>"
                    . ".$btonE."                 
                . "</tr>";
    }
    
    
    private function validaPrecio($id, $cantidad, $sucursal,$id_emp){
        $precio=$this->model->getPrecio($id,$cantidad,$sucursal,$id_emp); //aqui manda a traer el precio por el usuario y sucursal logueado
        return $precio;
    }

    private function validaPrecioOtra($id, $cantidad, $tipo_cli,$id_emp){
        $precio=$this->model->getPrecioOtra($id,$cantidad,$tipo_cli,$id_emp); //aqui manda a traer el precio por el usuario y sucursal logueado
        return $precio;
    }
    
    /*public function validaDescuentos(){ //verificar el % de descuento por monto 
        $descuento=$this->input->post("descuento");
        $result="";
        //$desc=$this->model->getDescuentos();
        $cotiza=$this->session->userdata('cotiza');
        log_message('error', 'descuento: '.$descuento);
        // $result.=$this->descuentoCategorias($cotiza, $desc);
        // $result.=$this->descuentoPuntos($cotiza, $desc);
        // $result.=$this->descuentosRecipientes($cotiza, $desc);
        if($descuento>0){
            $result.=$this->descuentoEspecial($cotiza,$descuento);
        }
        else if($descuento>0 || $descuento=="" || $descuento==0){
            $total=0;
            foreach($cotiza as $c){
                if($c["aplicaDesc"]==1){
                    $total+=$c["precio"]*$c["cantidad"];
                }   
            }
            $descuento=$this->model->getDescuentoPrecio($total);
            log_message('error', 'descuento: '.$descuento);
            if(isset($descuento)){
                $result.=$this->descuentoSobrePrecio($cotiza);
            }
        }
        echo $result;
        
    }*/

    public function validaDescuentos(){ //verificar el % de descuento por monto 
        $descuento=$this->input->post("descuento");
        $id_empresa=$this->input->post("id_empresa"); //id de empresa que se recibe del form, es la de sesion
        $sucursal=$this->input->post("sucursal");
        $tipo_cli=$this->input->post("tipo_cli");
        $id=$this->input->post("id");
        $id_emp_cot=$this->input->post("id_emp_cot");
        $result="";
        //$desc=$this->model->getDescuentos();
        $cotiza=$this->session->userdata('cotiza');
        //log_message('error', 'descuento: '.$descuento);
        // $result.=$this->descuentoCategorias($cotiza, $desc);
        // $result.=$this->descuentoPuntos($cotiza, $desc);
        // $result.=$this->descuentosRecipientes($cotiza, $desc);

        $total=0;
        foreach($cotiza as $c){
            if($c["aplicaDesc"]==1){
                if($id_empresa!=4 && $id_empresa!=5){ //ni ahisa ni auven
                    $total+=$c["precio"]*$c["cantidad"];
                }else{
                    if($tipo_cli==1){
                        $total+=$c["precio"]*$c["cantidad"];
                        //$total+=$c["precio_consultor"]*$c["cantidad"];
                    }
                    if($tipo_cli==2){
                        $total+=$c["precio"]*$c["cantidad"];
                        //$total+=$c["precio_directo"]*$c["cantidad"];
                    }
                }
            }   
        }
        $descuentog=$this->model->getDescuentoPrecio($total); //descuento por monto inf 
        //log_message('error', 'descuentog: '.$descuentog);
        log_message('error', 'descuento post: '.$descuento);
        log_message('error', 'id_empresa post: '.$id_empresa);
        log_message('error', 'id post: '.$id);

        if($id==0 && $id_empresa!=4 && $id_empresa!=5 || $id>0 && $id_emp_cot!=4 && $id_emp_cot!=5){ //solo para ecose 
            if(isset($descuentog) && $descuentog!="" && $descuento==0 || isset($descuentog) && $descuentog!="" && $descuento==""){
                $result.=$this->descuentoSobrePrecio($cotiza,$id_empresa,$tipo_cli,1); //aplica descuento
            }
        }
        if($id==0 && $id_empresa==4 || $id==0 && $id_empresa==5 || $id>0 && $id_emp_cot==4 || $id>0 && $id_emp_cot==5){ //no para ecose 
            if(isset($descuentog) && $descuentog!="" && $descuento==0 || isset($descuentog) && $descuentog!="" && $descuento==""){
                $result.=$this->descuentoSobrePrecio($cotiza,$id_empresa,$tipo_cli,0); //no aplica descuento
            }
        }
        if(isset($descuento) && $descuento>=0){
           $result.=$this->descuentoEspecial($cotiza,$descuento,$id_empresa,$tipo_cli); 
        }

        echo $result;
    }

    private function descuentoEspecial($cotiza,$descuento,$id_empresa,$tipo_cli){
        $total=0;
        foreach($cotiza as $c){
            //log_message('error', 'aplicaDesc: '.$c["aplicaDesc"]);
            /*if($c["aplicaDesc"]==1){
                if($id_empresa!=4 && $id_empresa!=5){ //ni ahisa ni auven
                    $total+=$c["precio"]*$c["cantidad"];
                }else{
                    if($tipo_cli==1){
                        $total+=$c["precio"]*$c["cantidad"];
                        //$total+=$c["precio_consultor"]*$c["cantidad"];
                    }
                    if($tipo_cli==2){
                        $total+=$c["precio"]*$c["cantidad"];
                        //$total+=$c["precio_directo"]*$c["cantidad"];
                    }
                }
            }*/
            $total+=$c["precio"]*$c["cantidad"];
            //log_message('error', 'descuento: '.$descuento);
        }
        if($descuento>0){
            return "<tr>"
                        . "<td>Descuento sobre el total:</td>"
                        . "<td></td>"
                        . "<td>$descuento %</td>"
                        . "<td>$".number_format(($descuento*$total)/100,2)."</td></tr>";
        }else{
            return "";
        }
    }

    private function descuentoSobrePrecio($cotiza,$id_empresa,$tipo_cli,$apli){
        $total=0;
        foreach($cotiza as $c){
            if($c["aplicaDesc"]==1){
                if($id_empresa!=4 && $id_empresa!=5){ //ni ahisa ni auven
                    $total+=$c["precio"]*$c["cantidad"];
                }else{
                    if($tipo_cli==1){
                        $total+=$c["precio"]*$c["cantidad"];
                        //$total+=$c["precio_consultor"]*$c["cantidad"];
                    }
                    if($tipo_cli==2){
                        $total+=$c["precio"]*$c["cantidad"];
                        //$total+=$c["precio_directo"]*$c["cantidad"];
                    }
                }
            }   
        }
        $descuento=$this->model->getDescuentoPrecio($total);
        if(isset($descuento)){
            if($apli==1){
                $des_tot=number_format(($descuento*$total)/100,2);
            }else{
                $descuento=0;
                $des_tot=number_format((0),2);
            }
            if($total>0){
                return "<tr>"
                        . "<td>Descuento sobre el total:</td>"
                        . "<td></td>"
                        . "<td id='desc_aplicadoprecio'>$descuento %</td>"
                        . "<td>$".$des_tot."</td></tr>";
            }else{
                return "";
            }
            
        }
        else{
            return "";
        }
       
    }
    
    private function descuentoCategorias($cotiza,$desc){ //deprecated
        $result="";
        $cat=array();
        foreach($cotiza as $c){
            array_push($cat, $c["categoria"]);
        }
        $valores = array_count_values($cat); //obtiene el conteo de categorias vg -> (1,2) categoria 1 encontrada 2 veces
        foreach($valores as $key => $v){
            $categoria=$this->model->getCategoria($key);
            if($v>1 && $v<3){ // si hay mas de un servicio de una categoria se aplica descuento
                
                $precio=0;
                foreach($cotiza as $co){
                    if($co["categoria"]==$key){
                        $precio+=($co["precio"]*$co["cantidad"]);
                    }
                }
                $descuento=$precio*$desc->a_proveedor1/100;
                $result.="<tr>"
                        . "<td>Descuento sobre categoría: $categoria->nombre</td>"
                        . "<td>$v</td>"
                        . "<td>$desc->a_proveedor1 %</td>"
                        . "<td>$".number_format($descuento,2)."</td></tr>"; 
            }
            elseif($v>=3) {
                $precio=0;
                foreach($cotiza as $co){
                    if($co["categoria"]==$key){
                        $precio+=($co["precio"]*$co["cantidad"]);
                    }
                }
                $descuento=$precio*$desc->a_proveedor2/100;
                $result.="<tr>"
                        . "<td>Descuento sobre categoría: $categoria->nombre</td>"
                        . "<td>$v</td>"
                        . "<td>$desc->a_proveedor2 %</td>"
                        . "<td>$".number_format($descuento,2)."</td></tr>"; 
            }
               
        }
        return $result;
    }
    
    private function descuentoPuntos($cotiza,$desc){ //deprecated
        $result="";
        
        foreach($cotiza as $c){
            $servicio=$this->model->getServicio($c["id"]);
            //print_r($servicio); die;
            if($servicio->por_puntos>0){
                if($c["cantidad"]>0 && $c["cantidad"]<51){
                    $descuento=$c["precio"]*$desc->por_puntos1/100;
                    $result.="<tr>"
                        . "<td>Descuento por puntos: ".$c["descripcion"]."</td>"
                        . "<td>".$c["cantidad"]."</td>"
                        . "<td>$desc->por_puntos1 %</td>"
                        . "<td>$".number_format($descuento,2)."</td></tr>"; 
                }
                if($c["cantidad"]>50 && $c["cantidad"]<101){
                    $descuento=$c["precio"]*$desc->por_puntos2/100;
                    $result.="<tr>"
                        . "<td>Descuento por puntos: ".$c["descripcion"]."</td>"
                        . "<td>".$c["cantidad"]."</td>"
                        . "<td>$desc->por_puntos2 %</td>"
                        . "<td>$".number_format($descuento,2)."</td></tr>"; 
                }
                if($c["cantidad"]>100){
                    $descuento=$c["precio"]*$desc->por_puntos3/100;
                    $result.="<tr>"
                        . "<td>Descuento por puntos: ".$c["descripcion"]."</td>"
                        . "<td>".$c["cantidad"]."</td>"
                        . "<td>$desc->por_puntos3 %</td>"
                        . "<td>$".number_format($descuento,2)."</td></tr>"; 
                }
            }
        }
        return $result;
    }
    
    private function descuentosRecipientes($cotiza,$desc){ //deprecated
        $result="";
        
        foreach($cotiza as $c){
            $servicio=$this->model->getServicio($c["id"]);
            
            if($servicio->recipientes){
                if($c["cantidad"]>1 && $c["cantidad"]<6){
                    $descuento=$c["precio"]*$desc->por_equipo1/100;
                    $result.="<tr>"
                        . "<td>Descuento equipo a presión: ".$c["descripcion"]."</td>"
                        . "<td>".$c["cantidad"]."</td>"
                        . "<td>$desc->por_equipo1 %</td>"
                        . "<td>$".number_format($descuento,2)."</td></tr>"; 
                }
                if($c["cantidad"]>5 && $c["cantidad"]<11){
                    $descuento=$c["precio"]*$desc->por_equipo2/100;
                    $result.="<tr>"
                        . "<td>Descuento equipo a presión: ".$c["descripcion"]."</td>"
                        . "<td>".$c["cantidad"]."</td>"
                        . "<td>$desc->por_equipo2 %</td>"
                        . "<td>$".number_format($descuento,2)."</td></tr>"; 
                }
                if($c["cantidad"]>10 && $c["cantidad"]<51){
                    $descuento=$c["precio"]*$desc->por_equipo3/100;
                    $result.="<tr>"
                        . "<td>Descuento equipo a presión: ".$c["descripcion"]."</td>"
                        . "<td>".$c["cantidad"]."</td>"
                        . "<td>$desc->por_equipo3 %</td>"
                        . "<td>$".number_format($descuento,2)."</td></tr>"; 
                }
                if($c["cantidad"]>50 ){
                    $descuento=$c["precio"]*$desc->por_equipo4/100;
                    $result.="<tr>"
                        . "<td>Descuento equipo a presión: ".$c["descripcion"]."</td>"
                        . "<td>".$c["cantidad"]."</td>"
                        . "<td>$desc->por_equipo4 %</td>"
                        . "<td>$".number_format($descuento,2)."</td></tr>"; 
                }
                
            }
        }
        return $result;
    }
    
    public function saveCotizacion(){
        
        $cliente=$this->input->post("cliente");
        $contacto=$this->input->post("contacto");
        $forma_pago=$this->input->post("forma_pago");
        $observ=$this->input->post("observaciones");
        $descuento=$this->input->post("descuento");
        $desc_aplicado=$this->input->post("desc_aplicado");
        $otro=$this->input->post("otro");
        $id_cotizacion=$this->input->post("id");
        $aux_auto=$this->input->post("aux_auto");
        $id_empresa=$this->input->post("id_empresa");
        //log_message('error', 'descuento: '.$descuento);
        $data=array("cliente_id"=>$cliente,"contacto_id"=>$contacto,"forma_pago"=>$forma_pago,"otra_forma_pago"=>$otro,"observaciones"=>$observ);
        $subtotal=0;
        $total=0; $total2=0; $tot=0; $tot2=0;
        $cotiza=$this->session->userdata('cotiza');
        foreach($cotiza as $c){
            if($c["aplicaDesc"]==1){
                $total+=$c["precio"]*$c["cantidad"];
            }else{
                $total2+=$c["precio"]*$c["cantidad"];
            }
            //log_message('error', 'total de cotizacion: '.$total);
            //log_message('error', 'total2 de cotizacion: '.$total2);
            $subtotal=$subtotal+=$c["precio"]*$c["cantidad"]; 
        }
        $data["subtotal"]=$subtotal; //monto sin desuento
        //log_message('error', 'subtotal de cotizacion: '.$subtotal);
        if($descuento>0){
            $data["descuento"]=$descuento;
        }else{
            $data["descuento"]=0;
        }
        /*else{
            $data["descuento"]=$this->model->getDescuentoPrecio($subtotal);
        }*/

        if($desc_aplicado>0){
            $data["descuento"]=$desc_aplicado;
        }
        
        if($data["descuento"]!=0){
            //log_message('error', 'descuento!= de cotizacion: '.$data["descuento"]);
            $tot=$total-($total*$data["descuento"])/100; //monto al que se le aplica el descuento - motno total con descuento
            $data["total"]=$tot+$total2;
        }else{
            $data["total"] = $subtotal;
        }
        //log_message('error', 'tot de cotizacion: '.$tot);
        
        /*log_message('error', 'total2 de cotizacion: '.$total2);
        log_message('error', 'data["total"]: '.$data["total"]);*/
        
        if($id_cotizacion=="0"){ //es nueva cotizacion
            $data["vendedor_id"]= $this->session->userdata("id_usuario");
            $data["consecutivo"]=$this->model->getConsecutivo();
            //$data["id_empresa"]=$this->session->userdata("empresa"); //ya viene desde el form de guardado
            $data["id_empresa"] = $id_empresa;
            $id_cotizacion=$this->model->insertCotizacion($data);
        }
        else{ //se edita la cotizacion
            if($aux_auto==0){
                $data["status"]=4;
                //$data["fecha_modificacion"]=date("Y-m-d H:n:s.000");
                //$this->model->updateCotizacion($id_cotizacion,$data);

                $cotizacion=$this->model->getCotizacion($id_cotizacion);
                $padre=$id_cotizacion; 
                if($cotizacion->padre!=0){
                    $padre=$cotizacion->padre;
                }
                //log_message('error', 'descuento en data: '.$data["descuento"]);
                $data["no_modificacion"]=$this->model->getModificaciones($padre);
                $data["padre"]=$padre;
                $data["vendedor_id"]=$cotizacion->vendedor_id;
                $data["consecutivo"]=$cotizacion->consecutivo;
            }
            if($aux_auto==0){
                //$data["id_empresa"]=$this->session->userdata("empresa"); //ya viene desde el form de guardado
                $data["id_empresa"] = $id_empresa; 
                $id_cotizacion=$this->model->insertCotizacion($data);
            }else{
                $data["status"]=2;
                $this->Catalogos_model->updateCatalogo($data,$id_cotizacion,"cotizaciones");
                //falta editar los servicios de la cotizacion
                foreach ($cotiza as $c) {
                    $total = $c["cantidad"]*$c["precio"];
                    $get_desc=$this->model->getDescuentoPrecio($total);
                    if($c["aplicaDesc"]==1){
                        $desc_aplicado = $data["descuento"];
                    } 
                    else{
                        $desc_aplicado=0;
                    } 
                    $arra_cs=array(
                        "servicio_id"=>$c["id"],
                        "cantidad"=>$c["cantidad"],
                        "precio" =>$c["precio"],
                        //"descuento"=>0,
                        "descuento"=>$desc_aplicado,
                        "cotizacion_id"=>$id_cotizacion,
                        "id_empresa_serv"=>$c["id_emp"]
                    );
                    if($c["id_cs"]>0)
                        $this->Catalogos_model->updateCatalogo($arra_cs,$c["id_cs"],"cotizaciones_has_servicios");
                    else    
                        $this->model->insertServicioCotiza($arra_cs);
                }
            }
        }
        /*else{ //se edita la cotizacion
            $data["status"]=4;
            //$data["fecha_modificacion"]=date("Y-m-d H:n:s.000");
            $this->model->updateCotizacion($id_cotizacion,$data);

            $cotizacion=$this->model->getCotizacion($id_cotizacion);
            $padre=$id_cotizacion; //781
            if($cotizacion->padre!=0){
                $padre=$cotizacion->padre;
            }
            
            //$data["no_modificacion"]=$this->model->getModificaciones($padre);
            $data["no_modificacion"]=$this->model->getModificaciones($id_cotizacion)+1; //1
            $data["padre"]=$padre; //781
            $data["vendedor_id"]=$cotizacion->vendedor_id;
            $data["consecutivo"]=$cotizacion->consecutivo;

            //$this->model->updateCotizacion($id_cotizacion,$data);
            $id_cotizacion=$this->model->insertCotizacion($data);
        }*/
        
        if($aux_auto==0){
            foreach ($cotiza as $c) {
                $total = $c["cantidad"]*$c["precio"];
                $get_desc=$this->model->getDescuentoPrecio($total);
                /*if(isset($get_desc)){
                    $descuento_get=number_format(($get_desc*$total)/100,2);
                    $desc_fin = $descuento_get;
                }else{
                    $desc_fin = $data["descuento"];
                }*/
               //log_message('error', 'aplicaDesc del array: '.$c["aplicaDesc"]);
                if($c["aplicaDesc"]==1){
                    $desc_aplicado = $data["descuento"];
                } 
                else{
                    $desc_aplicado=0;
                } 
                $temp=array(
                    "servicio_id"=>$c["id"],
                    "cantidad"=>$c["cantidad"],
                    "precio" =>$c["precio"],
                    //"descuento"=>0,
                    "descuento"=>$desc_aplicado,
                    "cotizacion_id"=>$id_cotizacion,
                    "id_empresa_serv"=>$c["id_emp"]
                );
                $this->model->insertServicioCotiza($temp);
            }
        }
        
        echo $id_cotizacion;
    }
    
    public function limpiaServicios(){
        $cotiza=array();
        $this->session->set_userdata('cotiza',$cotiza);
    }
    
    public function cargaServicios() {
        $cotiza=$this->session->userdata('cotiza');
        $result=""; $i=1;
        foreach($cotiza as &$c){
            $result.=$this->rowString($i, $c["clave"], $c["descripcion"], $c["precio"], $c["cantidad"] , $c["id_cs"], $c["id_emp"]);
            $i++;
        }
        $this->session->set_userdata('cotiza',$cotiza);
        echo $result;
    }
    
    public function getData_cotizaciones() {
        $params=$this->input->post();
        $cotizas = $this->model->getCotizaciones($params);
        $totalRecords=$this->model->totalCotizaciones($params);

        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval( $totalRecords ),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $cotizas->result(),
            "query"           => $this->db->last_query()   
            );
        echo json_encode($json_data);
    }

     public function getData_cotizacionesEstatus($status) {

        $params=$this->input->post();
        $cotizas = $this->model->getCotizaciones($params,$status);

        $totalRecords=$this->model->totalCotizacionesEstatus($params,$status);

        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval( $totalRecords ),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $cotizas->result(),
            "query"           => $this->db->last_query()   
            );
        echo json_encode($json_data);
    }

   
    public function cambiarEstatus(){
        $id=$this->input->post("id");
        $status=$this->input->post("status");
        $id_empresa=$this->input->post("id_empresa");
        $recibe=$this->input->post("recibe");
        $nom_tec=$this->input->post("nom_tec");
        //log_message('error', 'status: '.$status);
        //log_message('error', 'id_empresa: '.$id_empresa);
        //if(isset($recibe))
            //log_message('error', 'recibe: '.$recibe);
        if($status==2){
            $this->crearOrdenes($id,$recibe,$nom_tec,$id_empresa); //crea la orden si se acepta 
        }
        $this->db->where("id",$id);
        echo $this->db->update('cotizaciones', array("status"=>$status));
    }
    
    public function crearOrdenes($id_cotizacion,$recibe,$nom_tec,$id_empresa){
        $id_usuario=$this->session->userdata("id_usuario");
        if($id_empresa!="4" && $id_empresa!="5"){
            $table="servicios";
        }if($id_empresa=="4"){
            $table="servicios_ahisa";
        }if($id_empresa=="5"){
            $table="servicios_auven";
        }
        $sql = "SELECT DISTINCT familia as id FROM ".$table." s "
                . "INNER JOIN cotizaciones_has_servicios cs ON s.id=cs.servicio_id "
                . "WHERE cotizacion_id=$id_cotizacion";
        $query = $this->db->query($sql);
        $ordenes= $query->result();
        foreach ($ordenes as $o) {
            $this->db->insert('ordenes', array("cotizacion_id"=>$id_cotizacion, "familia_id"=>$o->id,"id_usuario"=>$id_usuario,"nombre_atn"=>$recibe,"nombre_cargo"=>$nom_tec,"fecha_creacion"=>date("Y-m-d H:i:s"))); 
        }
    }
    
    public function validaPass(){
        $pass=$this->input->post("pass");
        $vende = $this->session->userdata("id_usuario");
        $usada=0; $fecha_uso=""; $id=0; $exist=0;
        $get_pass=$this->model->getselectwheren("pass_creadas",array("pass"=>$pass,"id_vendedor"=>$vende,"tipo"=>"1"));
        foreach ($get_pass as $k) {
            $exist++;
            $usada = $k->usada;
            $fecha_uso = $k->fecha_uso;
            $id=$k->id;
        }
        $return = Array('usada'=>$usada,'fecha_uso'=>$fecha_uso,"id"=>$id,"exist"=>$exist);
        echo json_encode($return);
    }

    public function autorizaModifica(){
        $pass=$this->input->post("pass");
        $id_cot = $this->input->post("id_cot");
        $vende = $this->session->userdata("id_usuario");
        $usada=0; $fecha_uso=""; $id=0; $exist=0;
        $get_pass=$this->model->getselectwheren("pass_creadas",array("id_cotizacion"=>$id_cot,"pass"=>$pass,"id_vendedor"=>$vende,"tipo"=>"2"));
        foreach ($get_pass as $k) {
            $exist++;
            $usada = $k->usada;
            $fecha_uso = $k->fecha_uso;
            $id=$k->id;
        }
        $return = Array('usada'=>$usada,'fecha_uso'=>$fecha_uso,"id"=>$id,"exist"=>$exist);
        echo json_encode($return);
    }
    
    public function changeStatusPass(){
        $id=$this->input->post("id");
        $id_cotiza=$this->input->post("id_cotiza");
        $this->Catalogos_model->updateCatalogo(array("id_cotizacion"=>$id_cotiza,"usada"=>1,"fecha_uso"=>date("Y-m-d H:i:s")),$id,"pass_creadas");
    }

    public function changeStatusPass2(){
        $id=$this->input->post("id");
        $this->Catalogos_model->updateCatalogo(array("usada"=>1,"fecha_uso"=>date("Y-m-d H:i:s")),$id,"pass_creadas");
    }
}
