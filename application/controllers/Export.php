<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Export extends CI_Controller {

    public function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->logueo = $this->session->userdata('logeado_ecose');
        if ($this->logueo!=1) {
            redirect(base_url(), 'refresh');
        }
        $this->load->model('Catalogos_model');
    }
    
    public function index() {
        
    }
    
    public function cliente($estado){
        $id_emp=$this->session->userdata("empresa");
        if($estado!="0"){
            $edo=$this->Catalogos_model->getItemCatalogo('estados',$estado);
            $array = array('status'=>1,"estado"=>$edo->estado); 
        }else{
            $array = array('status'=>1);
        }
        $data['clientesrow']=$this->Catalogos_model->getListaClientes($id_emp,$array);
        $this->load->view('reportes/clientesexport',$data);
    }
    
    
}
