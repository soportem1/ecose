<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Catalogos extends CI_Controller {

    //CONTROLADOR CATALOGOS: listados,altas y edicion

    public function __construct() {
        parent::__construct();
        //error_reporting(0);
        
//        $logueo = $this->session->userdata('logeado_aries');
//        if($logueo!=1){
//            redirect(base_url(), 'refresh');
//        }
        
        $this->load->model('Catalogos_model', 'model');
    }

    //VIEWS - LISTADOS -------------------------------------------------------

    public function familias() {
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/familias');
        $this->load->view('footer');
    }
    
    

    public function empleados() {
        $data['id_usuario'] = $this->session->userdata('id_usuario');
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/empleados',$data);
        $this->load->view('footer');
    }
    
    public function empresas() {
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/proveedores');
        $this->load->view('footer');
    }

    public function clientes() {
        $data["estados"]=$this->model->getCatalogo("estados");
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/clientes',$data);
        $this->load->view('footer');
    }
    
    public function perfiles(){
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/perfiles');
        $this->load->view('footer');
    }

    public function sucursales(){
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/sucursales');
        $this->load->view('footer');
    }


    //VIEWS - ALTAS ----------------------------------------------------------
    public function alta_empleado() {
		$data["proveedores"]=$this->model->getCatalogo("proveedores");
        $data["departamentos"]=$this->model->getCatalogo("departamentos");
        $data["sucursales"]=$this->model->selectWhere("sucursales",array("estatus"=>1));
        $data["perfiles"]=$this->model->getPefil("perfiles");
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/form_empleado',$data);
        $this->load->view('footer');
    }
    
    public function alta_perfil() {
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/form_perfil');
        $this->load->view('footer');
    }

    public function alta_cliente() {
        $data["sucur"]=$this->model->getCatalogoWhere("sucursales","estatus=1");
        $data["estados"]=$this->model->getCatalogo("estados");
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/form_cliente',$data);
        $this->load->view('footer');
    }
    
    
    
    public function alta_familia() {
        $data["documentos"]=$this->model->getCatalogo("documentos");
        $data["proveedores"]=$this->model->getCatalogo("proveedores");
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/form_familia',$data);
        $this->load->view('footer');
    }
    
    public function alta_empresa() {
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/form_proveedor');
        $this->load->view('footer');
    }

    public function alta_sucursal() {
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/form_sucursal');
        $this->load->view('footer');
    }

    //VIEWS - EDICION ----------------------------------------------------------
    public function edicion_perfil($id) {
        $data['perfiles'] = $this->model->getItemCatalogo("perfiles", $id);
        $data['permisos_perfil'] = $this->model->getCatalogoWhere("permisos_perfil", "perfiles_id='$id'")[0];
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/form_perfil', $data);
        $this->load->view('footer');
    }

    public function vista_perfil($id) {
       
        $data['perfiles'] = $this->model->getItemCatalogo("perfiles", $id);
        $data['permisos_perfil'] = $this->model->getCatalogoWhere("permisos_perfil", "perfiles_id='$id'")[0];
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/vista_perfil', $data);
        $this->load->view('footer');
    }

    public function visual_perfil() {
        $id = $this->input->get('id');

        $data = $this->model->getPerfilPermisos($id);
        // $data = $this->model->getvisualpermisos();
        echo json_encode($data);
    }
    public function visual_empleado() {
        $id = $this->input->get('id');

        $data = $this->model->getItemEmpleados($id);
        // $data = $this->model->getvisualpermisos();
        echo json_encode($data);
    }

    public function edicion_empleado($id) {

        $data['empleado'] = $this->model->getItemCatalogo("empleados", $id);
		$data["proveedores"]=$this->model->getCatalogo("proveedores");
        $data["departamentos"]=$this->model->getCatalogo("departamentos");
        $data["sucursales"]=$this->model->selectWhere("sucursales",array("estatus"=>1));
        $data["perfiles"]=$this->model->getPefil("perfiles");
        
        //desencriptar password
        $this->load->library('encrypt');
        $key = 'mangoo-security';
        $data['empleado']->password = $this->encrypt->decode($data['empleado']->password, $key);
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/form_empleado', $data);
        $this->load->view('footer');
    }

    public function edicion_cliente($id) {
        $data['cliente'] = $this->model->getItemCatalogo("clientes", $id);
        $data['contactos'] = $this->model->getCatalogoWhere("personas_contacto", "cliente_id='$id' and estatus=1");
        $data["estados"]=$this->model->getCatalogo("estados");
        $data["sucur"]=$this->model->getCatalogoWhere("sucursales","estatus=1");  

        /*if($data['cliente']->id_usuario!=$this->session->userdata("id_usuario")&&$this->session->userdata("id_usuario")>1){
            redirect('catalogos/clientes');
        }else{*/
            $this->load->view('header');
            $this->load->view('menu');
            $this->load->view('catalogos/form_cliente', $data);
            $this->load->view('footer');
        //}
        
    }
     
    public function edicion_familia($id) {
        $data["documentos"]=$this->model->getCatalogo("documentos");
        $data["docs"]=$this->model->getCatalogoWhere("familias_has_documentos","familia_id=$id");
        $data["alcances"]=$this->model->getCatalogoWhere("alcances","familia_id=$id");
        $data['familia'] = $this->model->getItemCatalogo("familias", $id);
        $data["proveedores"]=$this->model->getCatalogo("proveedores");
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/form_familia', $data);
        $this->load->view('footer');
    }

    public function edicion_empresa($id) {
        $data['proveedor'] = $this->model->getItemCatalogo("proveedores", $id);
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/form_proveedor', $data);
        $this->load->view('footer');
    }

    public function edicion_sucursal($id) {

        $data['sucursal'] = $this->model->getItemCatalogo("sucursales", $id);
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/form_sucursal', $data);
        $this->load->view('footer');
    }

    //Funciones genericas de insercion y edicion ------------------------
    public function insertUpdateToCatalogo($catalogo) {
        $data = $this->input->post();
        $data=array_map('strtoupper', $data);
        if($catalogo=="proveedores"){
            $data["correo"]= strtolower($data["correo"]);
        }
        if (!isset($data['id'])) {
            //insert
            $result = $this->model->insertToCatalogo($data, $catalogo);
        }
        else{
            //update
            $id=$data['id'];
            unset($data['id']);
            $result = $this->model->updateCatalogo($data,$id, $catalogo);
        }
        echo $result;
    }
    
    //Funciones de perfil permisos 
    public function insertUpdatePerfil() {
        $result = 0;
        //arrays de insercion
        $data = $this->input->post();
        $data["nombre"]= strtoupper($data["nombre"]);
        $permisos_perfil = array(
            'permiso_administrador' => $this->input->post('permiso_administrador'),
            'permiso_catalogos' => $this->input->post('permiso_catalogos'),
            'permiso_clientes' => $this->input->post('permiso_clientes'),
            'permiso_configuraciones' => $this->input->post('permiso_configuraciones'),
            'permiso_creacion' => $this->input->post('permiso_creacion'),
            'permiso_edicion' => $this->input->post('permiso_edicion'),
            'permiso_estatus' => $this->input->post('permiso_estatus'),
            'permiso_envio' => $this->input->post('permiso_envio'),
            'permiso_datos' => $this->input->post('permiso_datos'),
            'permiso_cancelacion' => $this->input->post('permiso_cancelacion'),
            'permiso_programacion' => $this->input->post('permiso_programacion'),
            'permiso_program_env' => $this->input->post('permiso_program_env'),
            'permiso_estatus_servicio' => $this->input->post('permiso_estatus_servicio'),
            'permiso_pdf' => $this->input->post('permiso_pdf'),
            'permiso_agenda' => $this->input->post('permiso_agenda'),
            
        ); 
        unset($data['permiso_administrador']);
        unset($data['permiso_catalogos']);
        unset($data['permiso_clientes']);
        unset($data['permiso_configuraciones']);
        unset($data['permiso_creacion']);
        unset($data['permiso_edicion']);
        unset($data['permiso_estatus']);
        unset($data['permiso_envio']);
        unset($data['permiso_datos']);
        unset($data['permiso_cancelacion']);
        unset($data['permiso_programacion']);
        unset($data['permiso_program_env']);
        unset($data['permiso_estatus_servicio']);
        unset($data['permiso_pdf']);
        unset($data['permiso_agenda']);
    
        if (!isset($data['id'])) {
            //insertar
            $perfil_id = $this->model->insertPerfil($data);
            
            if ($perfil_id > 0) { //se inserto el perfil ahora se insertan los permisos
                $permisos_perfil['perfiles_id'] = $perfil_id;
                $result = $this->model->insertToCatalogo($permisos_perfil, "permisos_perfil");
            }
        }
        else{
            //editar
            $result=$this->model->updateCatalogo($data, $data['id'], 'perfiles');
            if($result==1){
                $result=$this->model->updatePermisos_perfil($permisos_perfil,$data['id']);
            }
        }

        echo $result;
    }    
    
    
    //Funciones Alta y Edicion de EMPLEADO ----------------------------
        public function insertUpdateEmpleado() {
        $result = 0;

        //arrays de insercion
        $data = $this->input->post();
        $data["nombre"]= strtoupper($data["nombre"]);
        $data["direccion"]= strtoupper($data["direccion"]);
        
        //encriptar password
        $this->load->library('encrypt');
        $key = 'mangoo-security';
        $data['password'] = $this->encrypt->encode($data['password'], $key);
        if (!isset($data['id'])) {
            //insertar
            $result = $this->model->insertEmpleado($data);        }
        else{
            //editar
            $result=$this->model->updateCatalogo($data, $data['id'], 'empleados');
        }

        echo $result;
    }
/*
    public function insertUpdateEmpleado() {
        $result = 0;

        //arrays de insercion
        $data = $this->input->post();
        $data["nombre"]= strtoupper($data["nombre"]);
        $data["direccion"]= strtoupper($data["direccion"]);
        
        $permisos = array(
            'permiso_personal' => $this->input->post('permiso_personal'),
            'permiso_clientes' => $this->input->post('permiso_clientes'),
            'permiso_productos' => $this->input->post('permiso_productos'),
            'permiso_proveedores' => $this->input->post('permiso_proveedores'),
            'permiso_configuraciones' => $this->input->post('permiso_configuraciones'),
            'permiso_cotizaciones' => $this->input->post('permiso_cotizaciones'),
            'permiso_notas' => $this->input->post('permiso_notas'),
            'permiso_ordenes' => $this->input->post('permiso_ordenes'),
            'permiso_agenda' => $this->input->post('permiso_agenda'),
            'permiso_expedientes' => $this->input->post('permiso_expedientes'),
        ); 

        unset($data['permiso_personal']);
        unset($data['permiso_clientes']);
        unset($data['permiso_productos']);
        unset($data['permiso_proveedores']);
        unset($data['permiso_configuraciones']);
        unset($data['permiso_cotizaciones']);
        unset($data['permiso_notas']);
        unset($data['permiso_ordenes']);
        unset($data['permiso_agenda']);
        unset($data['permiso_expedientes']);

        //encriptar password
        $this->load->library('encrypt');
        $key = 'mangoo-security';
        $data['password'] = $this->encrypt->encode($data['password'], $key);

        if (!isset($data['id'])) {
            //insertar
            $empleado_id = $this->model->insertEmpleado($data);
            if ($empleado_id > 0) { //se inserto el empleado ahora se insertan los permisos
                $permisos['empleados_id'] = $empleado_id;
                $result = $this->model->insertToCatalogo($permisos, "permisos");
            }
        }
        else{
            //editar
            $result=$this->model->updateCatalogo($data, $data['id'], 'empleados');
            if($result==1){
                $result=$this->model->updatePermisos($permisos,$data['id']);
            }
        }

        echo $result;
    }
    */
    
    public function insertUpdateCliente() {
        $data=$this->input->post();
        $tipo_cli=$this->input->post("tipo_cli");
        unset($data["mapa_dir"]);

        //log_message('error', 'tipo_cli'.$tipo_cli);
        //son varios contactos
        $id_contact=$data["id_contact"]; unset($data["id_contact"]);
        $nombre=$data["nombre"]; unset($data["nombre"]);
        $telefono=$data["telefono"]; unset($data["telefono"]);
        $correo=$data["correo"]; unset($data["correo"]);
        $puesto=$data["puesto"]; unset($data["puesto"]);
        $tipo=$data["tipo_contacto"]; unset($data["tipo_contacto"]);

        if (!isset($data['id'])) {
            //insert
            $data["sucursal"]=$this->session->userdata("sucursal");
            $data["empresa_id"]=$this->session->userdata("empresa");
            $data['id_usuario']=$this->session->userdata("id_usuario");
            if($data["usuario"]!="" && $data["password"]!=""){
                $data['password'] = password_hash($data["password"], PASSWORD_BCRYPT);
            }
            $id = $this->model->insertCliente($data);
            $result=$id;
        }  
        else{
            //update
            $id=$data['id'];
            unset($data['id']);
            //$data["empresa_id"]=$this->session->userdata("empresa");
            $data['id_usuario']=$this->session->userdata("id_usuario");
            if($data["password"]!='xxxxxx'){
                $data['password'] = password_hash($data["password"], PASSWORD_BCRYPT);
            }
            $result = $this->model->updateCliente($data,$id);
            //$this->model->deleteContactos($id); 
        }

        if(isset($tipo) && isset($nombre) && $nombre!=""){
            for($i=0; $i< sizeof($nombre); $i++){
                $tmp=explode(",",$tipo[$i]);
                if(sizeof($tmp)>0){
                    if(isset($tmp[0])){
                        $tem_cot=$tmp[0];
                    }else{
                        $tem_cot="";
                    }
                    if(isset($tmp[1])){
                        $tem_fact=$tmp[1];
                    }else{
                        $tem_fact="";
                    }
                    if(isset($tmp[2])){
                        $tem_ord=$tmp[2];
                    }else{
                        $tem_ord="";
                    }
                    $temp=array(
                        "nombre"=>$nombre[$i],
                        "telefono"=>$telefono[$i],
                        "email"=>$correo[$i],
                        "puesto"=>$puesto[$i],
                        "cotizacion"=>$tem_cot,
                        "facturacion"=>$tem_fact,
                        "orden"=>$tem_ord,
                        "cliente_id"=>$id
                        //'id_usuario'=>$this->session->userdata("id_usuario")
                    );
                    if($id_contact[$i]==0){
                        $this->model->insertToCatalogo($temp,"personas_contacto");
                    }else{
                        $this->model->updateCatalogon('personas_contacto',$temp,array('id'=>$id_contact[$i]));
                    }
                }
            }
        }
        echo $result;
    }
    
    public function insertUpdateFamila(){
        $data=$this->input->post();
        $docs= $this->input->post("documentos");
        unset($data["documentos"]);
        $alcances=$this->input->post("alcances");
        unset($data["alcances"]);
        //print_r($data);
        if (!isset($data['id'])) {
            //insert
            $data["id_empresa"] = $this->session->userdata("empresa");
            $result = $this->model->insertToCatalogo($data, "familias");
            $id=$result;  
        }
        else{
            //update
            $id=$data['id'];
            unset($data['id']);
            $result = $this->model->updateCatalogo($data,$id, "familias");
        }
        if($result>0){
            $this->model->removeFamiliaElements($id);
            $this->addFamiliaElements($docs,$alcances,$id);
        }
        echo $id;   
    }
    
    private function addFamiliaElements($docs,$alcances,$id){
        if($docs!=null){
            foreach($docs as $d){
                $temp=array("documento"=>$d,"familia_id"=>$id);
                $this->model->insertToCatalogo($temp,"familias_has_documentos");
            }
        }
        if($alcances!=null){
            foreach($alcances as $a){
                $temp=array("alcance"=>$a,"familia_id"=>$id);
                $this->model->insertToCatalogo($temp,"alcances");
            }  
        }
        
    }

    public function eliminar_fam($id){
        //if($this->session->userdata("id_usuario")>1){
           echo $this->model->deleteCatalogo($id,"familias");
        /*}
        else{
            echo 0;
        }*/
        
    }
    
    
    // Funcion de Eliminar
    public function deletePerfil(){
        $result2 = 0;
        $id= $this->input->post('id');
        $result = $this->model->deletePerfil($id);
        $msg['success'] = false;
        if($result){
            $msg['success'] = true;
        }
        echo json_encode($msg);
        }

         
    
    //Funciones de Obtención de registros --------------------------------

   

    public function getProductos() {
        $productos = $this->model->getProductos();
        $json_data = array("data" => $productos);
        echo json_encode($json_data);
    }
    
    public function getEmpleados() {
        //$empleados = $this->model->getCatalogo("empleados");
        $id_empresa=$this->session->userdata("empresa");
        $empleados = $this->model->getEmpleEmpresa($id_empresa);
        $json_data = array("data" => $empleados);
        echo json_encode($json_data);
    }
    
    public function getPerfiles() {
        $perfiles = $this->model->getData_perfiles("perfiles");
        $json_data = array("data" => $perfiles);
        echo json_encode($json_data);
    }

    public function getClientes() {
        /*$clientes = $this->model->getClientes();
        $json_data = array("data" => $clientes);
        echo json_encode($json_data);*/

        $params = $this->input->post();
        $getdata = $this->model->getClientesData($params);
        $totaldata= $this->model->total_clientes($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);

    }
    
    public function getServicios() {
        $id_prov = $this->input->post("id_prov");
        //log_message('error', 'id_prov'.$id_prov);
        $id = $this->session->userdata("empresa");
        $servicios = $this->model->getServicios($id_prov,$id);
        $json_data = array("data" => $servicios);
        //if($this->session->userdata("id_usuario")>1){
            echo json_encode($json_data);
        /*}
        else{
            echo 0;
        }*/
        
    }
    
    public function getFamilias() {
        $id = $this->session->userdata("empresa");
        $familias = $this->model->getCatalogoEmpresa($id);
        $json_data = array("data" => $familias);
        echo json_encode($json_data);
    }
    
    public function getEmpresas() {
        $proveedores = $this->model->getCatalogo("proveedores");
        $json_data = array("data" => $proveedores);
        echo json_encode($json_data);
    }

    public function getSucursales() {
        $sucursales = $this->model->getCatalogoWhere("sucursales","estatus=1");
        $json_data = array("data" => $sucursales);
        echo json_encode($json_data);
    }
    function eliminaremple(){
        $params = $this->input->post();
        $id=$params['id'];
        $this->model->updateCatalogon('empleados',array('activo'=>0),array('id'=>$id));
    }
    function eliminar_suc(){
        $params = $this->input->post();
        $id=$params['id'];
        $this->model->updateCatalogon('sucursales',array('estatus'=>0),array('id'=>$id));
    }

    function eliminarContacto(){
        $id = $this->input->post('id');
        $this->model->updateCatalogon('personas_contacto',array('estatus'=>0),array('id'=>$id));
    }



}
