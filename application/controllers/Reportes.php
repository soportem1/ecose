<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reportes extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //error_reporting(0);
        $this->load->model('ModeloReportes');
        $this->load->model('Catalogos_model');  
        date_default_timezone_set('America/Mexico_City');      
    }

    function index(){
    	//$data['vendedores']=$this->ModeloReportes->GetVendedores();
    	$data['clientes']=$this->ModeloReportes->GetCientes();
        $data['sucs']=$this->Catalogos_model->selectWhere("sucursales",array("estatus"=>1));
        $data['zonas']=$this->Catalogos_model->selectZona();
    	$this->load->view('header');
        $this->load->view('menu');
        $this->load->view('reportes/lista',$data);
        $this->load->view('footer');
    }

    function GetReportes(){
    	$params=$this->input->post();
    	//$parametros['vendedor']=1;
    	//$parametros['fecha1']= "2018-12-05";
    	//$parametros['fecha2']= "2019-01-03";
    	//$parametros['cliente']="";

        for($i=0; $i<count($params['tiposta']); $i++){
            //log_message('error', 'status: '.$params['tiposta'][$i]);
        }

    	$res=$this->ModeloReportes->GetData($params); //acá me quedo al envio de valore del array
        if($params['fecha1']!="" && $params['fecha2']!=""){
             $totalRecords = $this->ModeloReportes->totalCotizaciones($params,1);
        }
        else{
    	   $totalRecords = $this->ModeloReportes->totalCotizaciones($params,0);
        }

        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval( $totalRecords ),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $res->result(),
            "query"           => $this->db->last_query()   
            );
    	echo json_encode($json_data);
    }

    public function GetVentaSuc(){
        $params=$this->input->post();
        $datas = $this->ModeloReportes->get_total_sucursal($params);
        echo "<table class='table table-striped table-responsive data-tables' id='tabla_por_suc'>
                <thead>
                    <tr>
                        <th>Sucursal</th>
                        <th>Servicios</th>
                        <th>Viaticos</th>
                        <th>Importe vendido</th>
                    </tr>
                </thead>
                <tbody>";
        $tot_gral = 0;
        $sub=0; $viat=0; $suc_ant=0; $cont_suc=0; $cont_suc_aux=0; $aux_pinta=0;

        foreach($datas as $d){
            $cont_suc_aux++;
        }
        foreach($datas as $d){
            $cont_suc++;
            if($d->id_suc!=$suc_ant){ //ok
                $get_viat = $this->ModeloReportes->get_total_viatico_sucu($params,$d->id_suc);
                $viat = $get_viat->total_viat;
                $get_count = $this->ModeloReportes->get_count_sucu($params,$d->id_suc);
                $con_consul=$get_count->tot_chs;
            }            
            //log_message('error', 'suc_ant: '.$suc_ant);
            //log_message('error', 'id_suc: '.$d->id_suc);

            if($d->id_suc==$suc_ant){
                $sub=($sub + $d->total_sucursal)-$d->descuentos;
                $tot = $sub+$viat;
                //$tot_gral = $tot_gral + $tot;
                //log_message('error', 'sub mismo sucursal: '.$sub);
            }else{
                $sub = $d->total_sucursal-$d->descuentos;
                $tot = $sub+$viat; 
                //$tot_gral = $tot_gral + $tot;
                //log_message('error', 'sub diferente sucursal: '.$sub);
            }
            
            /*log_message('error', 'cont_suc: '.$cont_suc);
            log_message('error', 'con_consul: '.$con_consul);
            log_message('error', 'cont_suc_aux: '.$cont_suc_aux);*/

            if($d->id_suc!=$suc_ant && $cont_suc==$con_consul && $cont_suc_aux<=2){ //ok para 2 regs
                $tot_gral = $tot_gral + $tot;
                echo "<tr>   
                        <td>".$d->sucursal."</td>
                        <td>$".number_format($sub,2,'.',',')."</td>
                        <td>$".number_format($viat,2,'.',',')."</td>
                        <td>$".number_format($tot,2,'.',',')."</td>
                    </tr>";  
                $cont_suc=0;  
            }
            if($d->id_suc!=$suc_ant && $cont_suc==$con_consul && $cont_suc_aux>2){
                $tot_gral = $tot_gral + $tot;
                echo "<tr>   
                        <td>".$d->sucursal."</td>
                        <td>$".number_format($sub,2,'.',',')."</td>
                        <td>$".number_format($viat,2,'.',',')."</td>
                        <td>$".number_format($tot,2,'.',',')."</td>
                    </tr>";   
                $cont_suc=0;
            }
            if($d->id_suc==$suc_ant && $cont_suc==$con_consul && $cont_suc_aux>2){
                $tot_gral = $tot_gral + $tot;
                echo "<tr>   
                        <td>".$d->sucursal."</td>
                        <td>$".number_format($sub,2,'.',',')."</td>
                        <td>$".number_format($viat,2,'.',',')."</td>
                        <td>$".number_format($tot,2,'.',',')."</td>
                    </tr>";   
            }
            if($d->id_suc==$suc_ant && $cont_suc==$con_consul && $cont_suc_aux>2){   
                $aux_pinta++;
                $cont_suc=0;
            }
            $suc_ant = $d->id_suc;
        }
        echo "</tbody>
                <tfoot>
                    <tr>
                        <th colspan='3' style='text-align:right'>Total:</th>
                        <th id='tot_vend'>$".number_format($tot_gral,2,'.',',')."</th>
                    </tr>
                </tfoot>
            </table>";

        /*$json_data = array("data" => $datas);
        echo json_encode($json_data);*/
    }

    public function getViaticosSuc(){
        $params=$this->input->post();
        $tot_viat=0;
        $results=$this->ModeloReportes->get_viaticos_sucursal($params);
        foreach ($results as $k) {
            $tot_viat=$k->total_sucursal_viat;
        }
        echo $tot_viat;
    }

    public function GetVentaEmp(){ 
        $params=$this->input->post();
        $datas = $this->ModeloReportes->get_total_empresa($params);
        /*$json_data = array("data" => $datas);
        echo json_encode($json_data);*/
        echo "<table class='table table-striped table-responsive data-tables' id='tabla_por_emp'>
                <thead>
                    <tr>
                        <th>Empresa</th>
                        <th>Servicios</th>
                        <th>Viaticos</th>
                        <th>Importe vendido</th>
                    </tr>
                </thead>
                <tbody>";
        $tot_gral = 0; $cong=0;
        $sub=0; $viat=0; $emp_ant=0; $emp_ant1=0; $aux_emp=0; $emp_ant2=0; $aux_emp2=0; $sub2=0;$cont_em=0;
        foreach($datas as $d){
            $cong++;
        }
        $con_consul=0;
        foreach($datas as $d){
            //log_message('error', 'total_sucursal: '.$d->total_sucursal);
            $cont_em++;
            $emp_ant= $d->id_empresa;
            
            if($cong<=2){
                if($d->id_empresa!=$emp_ant1){ //ok
                    $get_viat = $this->ModeloReportes->get_total_viatico_empre($params,$d->id_empresa);
                    $viat = $get_viat->total_viat;
                    $get_count = $this->ModeloReportes->get_count_empre($params,$d->id_empresa);
                    $con_consul=$get_count->tot_chs;
                    $aux_emp2++;
                    //log_message('error', 'aux_emp2: '.$aux_emp2);
                }
                if($d->id_empresa==$emp_ant1){
                    $sub=($sub + $d->total_sucursal)-$d->descuentos;
                    $tot = $sub+$viat;
                    //$tot_gral = $tot_gral + $tot;
                    //log_message('error', 'sub misma empresa: '.$sub);
                }else{
                    $sub = $d->total_sucursal-$d->descuentos;
                    $tot = $sub+$viat; 
                    //$tot_gral = $tot_gral + $tot;
                    //log_message('error', 'sub diferente empresa: '.$sub);
                }
            }else{
                /*log_message('error', 'd id_empresa: '.$d->id_empresa);
                log_message('error', 'emp_ant: '.$emp_ant);
                log_message('error', 'emp_ant1: '.$emp_ant1);*/
                if($d->id_empresa!=$emp_ant1){ //ok
                    $get_viat = $this->ModeloReportes->get_total_viatico_empre($params,$d->id_empresa);
                    $viat = $get_viat->total_viat;
                    $get_count = $this->ModeloReportes->get_count_empre($params,$d->id_empresa);
                    $con_consul=$get_count->tot_chs;
                    $aux_emp2++;
                    //log_message('error', 'aux_emp2: '.$aux_emp2);
                }
                if($d->id_empresa==$emp_ant && $d->id_empresa!=$emp_ant1 && $emp_ant1==0){
                    $sub=($sub + $d->total_sucursal)-$d->descuentos;
                    $tot = $sub+$viat;
                    //$tot_gral = $tot_gral + $tot;
                    //log_message('error', 'sub misma empresa: '.$sub);
                }else if($d->id_empresa==$emp_ant && $d->id_empresa==$emp_ant1){
                    $sub=($sub + $d->total_sucursal)-$d->descuentos;
                    $tot = $sub+$viat;
                    //$tot_gral = $tot_gral + $tot;
                    //log_message('error', 'sub misma empresa: '.$sub);
                }else{
                    $sub = $d->total_sucursal-$d->descuentos;
                    $tot = $sub+$viat; 
                    //$tot_gral = $tot_gral + $tot;
                    //log_message('error', 'sub diferente empresa: '.$sub);
                } 
                
            }
            
            $emp_ant1= $d->id_empresa;
            //log_message('error', 'cont_em: '.$cont_em);
            //log_message('error', 'con_consul: '.$con_consul);
            if($d->id_empresa!=$emp_ant2 && $cong<=2){
                $tot_gral = $tot_gral + $tot;
                echo "<tr>   
                        <td>".$d->empresa."</td>
                        <td>$".number_format($sub,2,'.',',')."</td>
                        <td>$".number_format($viat,2,'.',',')."</td>
                        <td>$".number_format($tot,2,'.',',')."</td>
                    </tr>";
            }
            if($d->id_empresa!=$emp_ant2 && $cont_em==$con_consul && $cong>2){
                $tot_gral = $tot_gral + $tot;
                echo "<tr>   
                        <td>".$d->empresa."</td>
                        <td>$".number_format($sub,2,'.',',')."</td>
                        <td>$".number_format($viat,2,'.',',')."</td>
                        <td>$".number_format($tot,2,'.',',')."</td>
                    </tr>";
                $cont_em=0;
            }
            if($d->id_empresa==$emp_ant2 && $cont_em==$con_consul && $cong>2){
                $tot_gral = $tot_gral + $tot;
                echo "<tr>   
                        <td>".$d->empresa."</td>
                        <td>$".number_format($sub,2,'.',',')."</td>
                        <td>$".number_format($viat,2,'.',',')."</td>
                        <td>$".number_format($tot,2,'.',',')."</td>
                    </tr>";
                $cont_em=0;
            }
            $emp_ant2= $d->id_empresa;
        }
        echo "</tbody>
                <tfoot>
                    <tr>
                        <th colspan='3' style='text-align:right'>Total:</th>
                        <th id='tot_vend'>$".number_format($tot_gral,2,'.',',')."</th>
                    </tr>
                </tfoot>
            </table>";
        //echo $html;
    }

    public function GetVentaVende(){ //acá me quedo
        $params=$this->input->post();
        $datas = $this->ModeloReportes->get_total_vende($params);

        /*$json_data = array("data" => $datas);
        echo json_encode($json_data);*/
        echo "<table class='table table-striped table-responsive data-tables' id='tabla_por_vende'>
                <thead>
                    <tr>
                        <th>Vendedor</th>
                        <th>Zona</th>
                        <th>Servicios</th>
                        <th>Viaticos</th>
                        <th>Importe vendido</th>
                    </tr>
                </thead>
                <tbody>";
        $tot_gral = 0;
        $sub=0; $viat=0; $ven_ant=0; $cont_em=0; $cont_em_aux=0; $aux_pinta=0;

        foreach($datas as $d){
            $cont_em_aux++;
        }
        foreach($datas as $d){
            $cont_em++;
            if($d->id_emplea!=$ven_ant){ //ok
                $get_viat = $this->ModeloReportes->get_total_viatico_vende($params,$d->id_emplea);
                $viat = $get_viat->total_viat;
                $get_count = $this->ModeloReportes->get_count_vende($params,$d->id_emplea);
                $con_consul=$get_count->tot_chs;
            }            
            //log_message('error', 'ven_ant: '.$ven_ant);
            //log_message('error', 'id_emplea: '.$d->id_emplea);

            if($d->id_emplea==$ven_ant){
                $sub=($sub + $d->total_sucursal)-$d->descuentos;
                $tot = $sub+$viat;
                //$tot_gral = $tot_gral + $tot;
                //log_message('error', 'sub mismo empleado: '.$sub);
            }else{
                $sub = $d->total_sucursal-$d->descuentos;
                $tot = $sub+$viat; 
                //$tot_gral = $tot_gral + $tot;
                //log_message('error', 'sub diferente empleado: '.$sub);
            }
            
            //log_message('error', 'cont_em: '.$cont_em);
            //log_message('error', 'con_consul: '.$con_consul);
            //log_message('error', 'cont_em_aux: '.$cont_em_aux);

            if($d->id_emplea!=$ven_ant && $cont_em==$con_consul && $cont_em_aux<=2){ //ok para 2 regs
                $tot_gral = $tot_gral + $tot;
                echo "<tr>   
                        <td>".$d->empleado."</td>
                        <td>".$d->zona."</td>
                        <td>$".number_format($sub,2,'.',',')."</td>
                        <td>$".number_format($viat,2,'.',',')."</td>
                        <td>$".number_format($tot,2,'.',',')."</td>
                    </tr>";  
                $cont_em=0;  
            }
            if($d->id_emplea!=$ven_ant && $cont_em==$con_consul && $cont_em_aux>2){
                $tot_gral = $tot_gral + $tot;
                echo "<tr>   
                        <td>".$d->empleado."</td>
                        <td>".$d->zona."</td>
                        <td>$".number_format($sub,2,'.',',')."</td>
                        <td>$".number_format($viat,2,'.',',')."</td>
                        <td>$".number_format($tot,2,'.',',')."</td>
                    </tr>";  
                $cont_em=0;  
            }
            if($d->id_emplea==$ven_ant && $cont_em==$con_consul && $cont_em_aux>2){
                $tot_gral = $tot_gral + $tot;
                echo "<tr>   
                        <td>".$d->empleado."</td>
                        <td>".$d->zona."</td>
                        <td>$".number_format($sub,2,'.',',')."</td>
                        <td>$".number_format($viat,2,'.',',')."</td>
                        <td>$".number_format($tot,2,'.',',')."</td>
                    </tr>";  
            }
            if($d->id_emplea==$ven_ant && $cont_em==$con_consul && $cont_em_aux>2){   
                $aux_pinta++;
                $cont_em=0;
            }
 
            //$tot_gral = $tot_gral + $tot;
            $ven_ant = $d->id_emplea;
        }
        echo "</tbody>
                <tfoot>
                    <tr>
                        <th colspan='4' style='text-align:right'>Total:</th>
                        <th id='tot_vend'>$".number_format($tot_gral,2,'.',',')."</th>
                    </tr>
                </tfoot>
            </table>";
        //echo $html;
        //log_message('error', 'html: '.$html);
    }

    public function GraficaVentas(){
        //$params = $this->input->get();
        $params = $this->input->post();
        $records = $this->ModeloReportes->getVentasGral($params);
        //echo json_encode($records);

        $mant=0; $sub=0; $tot=0; $tot_gral=0; $cont_em_aux=0; $cont_em=0; $array = array(); $array2=array(); $arraydata=array(); $auxp=0; $fechasarray=array(); $valoresarray=array();
        //$records2 = $this->ModeloReportes->getVentasGralMes($params);
        foreach($records as $d){
            $cont_em_aux++;
        }
        foreach ($records as $d) {
            $cont_em++;
            if($d->mes!=$mant){ //ok
                $get_count = $this->ModeloReportes->get_count_total_mes($params,$d->mes,date("Y", strtotime($d->fecha_creacion)));
                $con_consul=$get_count->tot_chs;
            } 
            log_message('error', 'mes: '.$d->mes);
            log_message('error', 'mant: '.$mant);
            if($d->mes==$mant){
                $sub=($sub + $d->total_sucursal)-$d->descuentos;
                log_message('error', 'sub mismo mes: '.$sub);
            }else{
                $sub = $d->total_sucursal-$d->descuentos;
                log_message('error', 'sub diferente mes: '.$sub);
            }
            /*log_message('error', 'cont_em: '.$cont_em);
            log_message('error', 'con_consul: '.$con_consul);*/
            log_message('error', 'cont_em_aux: '.$cont_em_aux);
            log_message('error', 'con_consul: '.$con_consul);
            log_message('error', 'cont_em: '.$cont_em);
            //log_message('error', 'auxp: '.$auxp);
       
            if($d->mes!=$mant && $cont_em==$con_consul && $cont_em_aux<=2){
                $tot_gral = $tot_gral + $sub; 
                log_message('error', 'tot_gral: '.$tot_gral); 
                $cont_em=0;  
                $namem= $this->getName($d->mes);
                log_message('error', 'namem1: '.$namem);
                //$arraydata[] = array($namem=>$tot_gral);
                $fechasarray[]=$namem;
                $valoresarray[]=$tot_gral;
            }
            $tot_gral=0;
            if($d->mes!=$mant && $cont_em==$con_consul && $cont_em_aux>2){
                $tot_gral = $tot_gral + $sub;
                log_message('error', 'tot_gral: '.$tot_gral); 
                $cont_em=0;  
                $namem= $this->getName($d->mes);
                $array = array_merge($array,array($namem));
                $array2 = array_merge($array2,array($tot_gral));
                $fechasarray[]=$namem;
                $valoresarray[]=$tot_gral;
            }
            $tot_gral=0;
            if($d->mes==$mant && $cont_em==$con_consul && $cont_em_aux>2){
                $tot_gral = $tot_gral + $sub; 
                log_message('error', 'tot_gral: '.$tot_gral); 
                $namem= $this->getName($d->mes);
                log_message('error', 'namem3: '.$namem);
                $fechasarray[]=$namem;
                $valoresarray[]=$tot_gral;
            }
            //echo json_encode([$array]);
            $tot_gral=0;
            if($d->mes==$mant && $cont_em==$con_consul && $cont_em_aux>2){   
                $cont_em=0;
            }
            $mant = $d->mes;
        }
        //log_message('error', 'array: '.var_dump($array));
        //echo json_encode(array("mes"=>$array,"tot_gral"=>$array2));
        //$arraydata = array('mes'=>$array,'tot_gral'=>$array2);
        $datos=array("fechasarray"=>$fechasarray,"valoresarray"=>$valoresarray);
        echo json_encode($datos);
    }

    public function getName($mes){
        if($mes==1)
            $name="Ene";
        else if($mes==2)
            $name="Feb";
        else if($mes==3)
            $name="Mar";
        else if($mes==4)
            $name="Abr";
        else if($mes==5)
            $name="May";
        else if($mes==6)
            $name="Jun";
        else if($mes==7)
            $name="Jul";
        else if($mes==8)
            $name="Ago";
        else if($mes==9)
            $name="Sep";
        else if($mes==10)
            $name="Oct";
        else if($mes==11)
            $name="Nov";
        else if($mes==12)
            $name="Dic";

        return $name;
    }

    public function getVendedor(){
        $search = $this->input->get('search');
        $sucursal =$this->input->get('sucursal');
        $zonas =$this->input->get('zonas');
        $results=$this->ModeloReportes->get_select_vendedor($search,$sucursal,$zonas);
        echo json_encode($results);    
    }

    function ExportReporte($fecha1,$fecha2,$sucursal,$zona,$cliente,$tiposta,$vendedor){
        if($zona=="M%C3%89RIDA"){
            $zona="MÉRIDA";
        }
        if($sucursal=="M%C3%89RIDA"){
            $sucursal="MÉRIDA";
        }
        //$params=$this->input->post();
        /*log_message('error', 'fecha1: '.$fecha1);
        log_message('error', 'fecha2: '.$fecha2);
        log_message('error', 'sucursal: '.$sucursal);
        log_message('error', 'zona: '.$zona);
        log_message('error', 'cliente: '.$cliente);
        log_message('error', 'tiposta: '.$tiposta);
        log_message('error', 'vendedor: '.$vendedor);*/

        $nvo_ts= array($tiposta);
        //log_message('error', 'nvo_ts: '.print_r($nvo_ts));
        for($i=0; $i<count($nvo_ts); $i++){
            //log_message('error', 'status: '.$nvo_ts[$i]);
        }

        $data["fecha1"] = $fecha1;
        $data["fecha2"] = $fecha2;
        $data["sucursal"] = $sucursal;
        $data["zona"] = $zona;
        $data["cliente"] = $cliente;
        $data["tiposta"] = $nvo_ts;
        $data["vendedor"] = $vendedor;
        $data["r"] = $this->ModeloReportes->get_reportes_data($fecha1,$fecha2,$sucursal,$zona,$cliente,$nvo_ts,$vendedor);
        $this->load->view('reportes/excelReporte',$data);
    }

}

?>