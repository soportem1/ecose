<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GenerarPass extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $perfil = $this->session->userdata("perfil");
        if($perfil!=1){
           redirect(base_url(), 'refresh');
        }
        $this->load->model('Catalogos_model');
        date_default_timezone_set('America/Mexico_City');
    }
    
    public function index() {
        $data['vendedores']=$this->Catalogos_model->selectWhereOrder("empleados",array(/*"perfil"=>2,*/"activo"=>1),"nombre","asc");
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('pass/generar',$data);
        $this->load->view('footer');
    }
    
    public function asigna_pass_vende(){
        $data= $this->input->post();
        $data["fecha_creada"] = date("Y-m-d H:i:s");
        $result=$this->Catalogos_model->insertToCatalogo($data,"pass_creadas");
        echo $result;
    }
    
}
