<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Configuraciones extends CI_Controller {

    //CONTROLADOR CATALOGOS: listados,altas y edicion

    public function __construct() {
        parent::__construct();
        //error_reporting(0);
//        $logueo = $this->session->userdata('logeado_aries');
//        if($logueo!=1){
//            redirect(base_url(), 'refresh');
//        }
//        
        $this->load->model('Catalogos_model', 'model');
    }

    //VIEWS -------------------------------------------------------

    public function departamentos() {
        $data["departamentos"] = $this->getListado("departamentos");
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('configuraciones/departamentos', $data);
        $this->load->view('footer');
    }
    
    public function descuentos() {
        $data["descuentos"] = $this->model->getCatalogo("descuentos")[0];
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('configuraciones/descuentos', $data);
        $this->load->view('footer');
    }
    
    public function documentos() {
        $data["documentos"] = $this->getListado("documentos");
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('configuraciones/documentos', $data);
        $this->load->view('footer');
    }

    public function encuestas() {
        $data["pregs"]=$this->model->getselectwheren('preguntas_encuesta',array('id_empresa'=>1,'estatus'=>1));
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('configuraciones/encuestas',$data);
        $this->load->view('footer');
    }

    public function get_encuestas() {
        $id=$this->input->post('id');
        $pregs=$this->model->getselectwheren('preguntas_encuesta',array('id_empresa'=>$id,'estatus'=>1));
        $html="";
        foreach ($pregs->result() as $d) { 
            $sel="";
            $sel2="";
            if($d->tipo==1){ $sel="selected"; }
            else if($d->tipo==2){ $sel2="selected"; }
            $html.='<tr>
                <td width="90%"><input type="hidden" id="id" value="'.$d->id.'">   
                    <div class="row">
                        <div class="col-md-10">
                            <input class="form-control" type="text" id="pregunta" value="'.$d->pregunta.'">
                        </div>
                        <div class="col-md-2">
                            <select id="tipo" class="form-control">
                                <option '.$sel.' value="1">Escala</option>
                                <option '.$sel2.' value="2">Si/No</option>
                            </select>
                        </div>
                    </div>
                </td>
                <td><button type="button" onclick="eliminarPreg('.$d->id.')" class="btn btn-raised btn-icon btn-pure danger mr-1"><i class="fa fa-times-circle"></i></button></td>
            </tr>';
        }
        echo $html;
    }   

    public function eliminar_preg() {
        $id=$this->input->post('id');
        $result = $this->model->updateCatalogo(array("estatus"=>0), $id,"preguntas_encuesta");
    } 

    public function submit_pregs(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            //$id = $DATA[$i]->id;
            $data['id_empresa'] = $DATA[$i]->id_empresa;
            $data['pregunta'] = $DATA[$i]->pregunta;
            $data['tipo'] = $DATA[$i]->tipo;
            $data['id']= $DATA[$i]->id;
            $id = $DATA[$i]->id; //aca me quedo
            $temp=array("id_empresa"=>$DATA[$i]->id_empresa,"pregunta"=>$DATA[$i]->pregunta,"tipo"=>$DATA[$i]->tipo);
            if($DATA[$i]->id==0){    
                $id=$this->model->insertToCatalogo($temp,"preguntas_encuesta");
            }else{
                $this->model->updateCatalogon("preguntas_encuesta",$temp,array("id"=>$id));
            }
            echo $id;          
        }
    }

    //Funciones genericas de insercion y edicion ------------------------
    public function insertUpdateToCatalogo($catalogo) {
        $data = array(
            "id" => $this->input->post("id"),
            "nombre" => strtoupper($this->input->post("nombre"))
        );
        if($catalogo=="descuentos_act"){
            $data=$this->input->post();
        }
        
        if ($data['id'] == null) {
            //insert
            $result = $this->model->insertToCatalogo($data, $catalogo);
        } else {
            //update
            $id = $data['id'];
            unset($data['id']);
            $result = $this->model->updateCatalogo($data, $id, $catalogo);
        }

        echo $result;
    }

    public function updatePorcentajes() {
        $data = $this->input->post();
        $result = $this->model->insertToCatalogo($data, "porcentajes");
        echo $result;
    }

    public function deleteFromCatalogo($catalogo) {
        $id = $this->input->post("id");
        $result = $this->model->deleteCatalogo($id, $catalogo);
        echo $result;
    }

    public function getListado($catalogo) {
        return $this->model->getCatalogo($catalogo);
    }

    public function getDescuentos() {
        $descuentos = $this->model->getDescuentos();
        $json_data = array("data" => $descuentos);
        echo json_encode($json_data);
    }

    public function deleteDescuento($id){
        echo $this->model->updateCatalogo(array(estatus=>0), $id, "descuentos_act");
        
    }

    

}
