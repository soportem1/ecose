<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //error_reporting(0);
        /*$logueo = $this->session->userdata('logeado_aries');
        if($logueo!=1){
            redirect(base_url(), 'refresh');
        }*/
        
        $this->load->model('Operaciones_model', 'model');
        $this->load->model('Catalogos_model');
        date_default_timezone_set('America/Mexico_City');
        $this->id_usuario=$this->session->userdata("id_usuario");
    }

    private static $Key = "GENERADORDECLAVE";

    public static function encrypt ($string) {
        return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5(Cliente::$Key),
        str_replace("/","",$string), MCRYPT_MODE_CBC, md5(md5(Cliente::$Key))));
    }

    public static function decrypt ($string) {
        return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5(Cliente::$Key), 
        base64_decode($string), MCRYPT_MODE_CBC, md5(md5(Cliente::$Key))), "\0");
    }

    public function facturas($id) {
        $data['cliente'] = $this->model->getItemCatalogo("clientes", $id);
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('cliente/facturas',$data);
        $this->load->view('footer');
    }
    
    public function encuestas($id) {
        $data['cliente'] = $this->model->getItemCatalogo("clientes", $id);
        $data['contacto'] = $this->model->getClienteContac($id);
        $data["resp"] = $this->model->getEncuestasTotal($id);
        $data['idcliente']=$id;
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('cliente/encuestas',$data);
        $this->load->view('footer');
    }
    
    public function encuesta($id) {
        $this->enviarEncuestaMail($id);
    }

    public function enviarEncuestaMail($id) {
        $obtcont = $this->model->getClienteContac($id);
        $data='';
        $empre=0;
        $data = $this->input->post();
    
        $this->load->library('email');
        $config = array();
        $id_emp=$this->session->userdata("empresa");
        $config['protocol'] = 'smtp';
        if($id_emp==1 || $id_emp==2 || $id_emp==3 || $id_emp==6){
            $config['smtp_host'] = 'ecose.mangoo.systems';
            $config['smtp_user'] = 'noreply@ecose.mangoo.systems';
            $config['smtp_pass'] = 'GETJb.W#x#9l';
            $mail_dom = 'noreply@ecose.mangoo.systems';
        }
        if($id_emp==4){
            $config['smtp_host'] = 'ahisa.mangoo.systems';                     
            $config['smtp_user'] = 'noreply@ahisa.mangoo.systems';
            $mail_dom = 'noreply@ahisa.mangoo.systems';
            $config['smtp_pass'] = '^Lh**=+d{_04';
        }
        if($id_emp==5){
            $config['smtp_host'] = 'auven.mangoo.systems';                     
            $config['smtp_user'] = 'noreply@auven.mangoo.systems';
            $mail_dom = 'noreply@auven.mangoo.systems';
            $config['smtp_pass'] = 'zjq!-zhyk%&g';
        }

        $config['smtp_port'] = 465;
        $config["smtp_crypto"] = 'ssl';
        //$config['newline']   = "\r\n";
        //$config["crlf"] = "\r\n";
        $config['mailtype'] = "html";
        $config['charset'] = "utf-8";
        $config['wordwrap'] = TRUE;
        $this->email->initialize($config);

        $this->email->from($mail_dom, 'Encuesta de Calidad');
        if($id=='ecr'){
          $this->email->to("calidad@ecose.com.mx"); 
          $this->email->subject('Re: Encuesta de Calidad de servicio'); 
        }else{
            $array = array();
            foreach ($obtcont as $item) {
                $empre = $item->empresa_id;
                if ($item->email!='') {
                    $array = array_merge($array,array($item->email));
                    $this->email->to($array);
                }
            }
            $this->email->subject('Encuesta de Calidad de servicio');
        }

        $this->load->library('encrypt');
        $key = 'GENERADORDECLAVE';
        $id_enc = $this->encrypt->encode($id, $key);
        //log_message('error', 'id_enc: '.$id_enc);

        $data["url"] = base_url()."cliente/encuestaCliente/?is=".utf8_encode($id_enc); //url de encuesta por cliente
        $data["empre"]=$empre;
        $data["id_emp"] = $this->session->userdata("empresa");
        $msj=$this->load->view('cliente/mail_encuesta', $data,true);

        $this->email->message($msj);
        echo $this->email->send();
    }

    public function encuestaCliente(){
        $id=$_GET['is'];
        $id=utf8_decode($id);
        $id = str_replace(" ","+",$id);
        //log_message('error', 'id_enc2: '.$id);
        $this->load->library('encrypt');
        $key = 'GENERADORDECLAVE';

        $id_desenc=$this->encrypt->decode($id,$key);
        //log_message('error', 'id_desenc: '.$id_desenc);

        $get=$this->model->getItemCatalogo('clientes',$id_desenc);
        if($get->empresa_id=="1" || $get->empresa_id=="2" || $get->empresa_id=="3" || $get->empresa_id=="6")
            $empresa_id=1;
        else
            $empresa_id=$get->empresa_id;

        $data["pregs"]=$this->Catalogos_model->getselectwheren('preguntas_encuesta',array('id_empresa'=>$empresa_id,'estatus'=>1));
        $data["emp"]=$get->empresa_id;
        $data["id_cli"]=$id_desenc;
        $data["id_emp"] = $this->session->userdata("empresa");
        $this->load->view('cliente/form_encuesta2',$data);
    }

    public function submit_pregs(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $id = $DATA[$i]->id; 
            $id_pregunta = $DATA[$i]->id_pregunta;
            $id_cliente = $DATA[$i]->id_cliente;
            $respuesta = $DATA[$i]->respuesta;
            $sugerencias = $DATA[$i]->sugerencias;
            $serv_inclu = $DATA[$i]->serv_inclu;
            $empresa = $DATA[$i]->empresa;
            $nombre = $DATA[$i]->nombre;
            $serv = $DATA[$i]->serv;
            $area = $DATA[$i]->area;
            $tel = $DATA[$i]->tel;
            $correo = $DATA[$i]->correo;

            $temp=array("id_pregunta"=>$id_pregunta,"id_cliente"=>$id_cliente,"respuesta"=>$respuesta, "sugerencias"=>$sugerencias,"serv_inclu"=>$serv_inclu,"empresa"=>$empresa, "nombre"=>$nombre,"serv"=>$serv,"area"=>$area,"tel"=>$tel,"correo"=>$correo,"fecha_reg"=>date("Y-m-d H:i:s"));
            if($DATA[$i]->id==0){    
                $id=$this->model->insertToCatalogo($temp,"respuestas_encuesta");
            }else{
                $this->model->updateCatalogon("respuestas_encuesta",$temp,array("id"=>$id));
            }
            echo $id;          
        }
    }

    public function enviarEncuesta($id) {
        //forma para enviar email al cliente        
        //$mail=$data['contacto']->email;
        $obtcont = $this->model->getClienteContac($id);

        $data='';
        $data = $this->input->post();
        //Envio de Mail
        $this->load->library('email');

        $config = array();
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ecose.mangoo.systems';
        $config['smtp_user'] = 'noreply@ecose.mangoo.systems';
        $config['smtp_pass'] = 'GETJb.W#x#9l';
        $config['smtp_port'] = 465;
        $config["smtp_crypto"] = 'ssl';
        //$config['newline']   = "\r\n";
        //$config["crlf"] = "\r\n";
        $config['mailtype'] = "html";
        $config['charset'] = "utf-8";
        $config['wordwrap'] = TRUE;
        $this->email->initialize($config);

        $this->email->from('ecose-noreply@ecose.mangoo.systems', 'Calidad Grupo Ecose');
        if($id=='ecr'){
            $this->email->to('calidad@ecose.com.mx'); 
            $this->email->subject('Re: Encuesta de Calidad de servicio ecose'); 
        }else{
            foreach ($obtcont as $item) {
                if ($item->email!='') {
                    $this->email->to($item->email);
                }
            }
            $this->email->subject('Encuesta de Calidad de servicio ecose');
        }
        //$this->email->bcc('gerardo@mangoo.com.mx');
        //$this->email->bcc('ecose-noreply@sicoi.net');
        //$this->email->to('jesus@mangoo.com.mx');
        //$this->email->subject('Encuesta de Calidad de servicio ecose');
        
        //$msj=$this->load->view('cliente/form_encuesta', true);
        if($id!='ecr'){
            $msj= "Para terminos de calidad le sugerimos que realice la siguiente encuesta de calidad:
               <br>click en el siguiente link: <a href='http://sicoi.net/ecosepruebas/index.php/Cliente/encuestaCliente/".$id."'>http://ecose.sicoi.net/encuesta/</a> ";
        }
        else{
            $msj= 'Encuesta realizada por el cliente  \n
                   <br><table class="table table-sm">
                    <tr>
                        <th>No.</th>
                        <th>Pregunta</th>
                        <th>Escala<br><span style="letter-spacing: 4px" class="black">1 2 3 4</span></th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>¿La actitud del asesor de ventas fue la adecuada durante todo el servicio?</td>
                        <td>
                            <label>'.$data['q1'].'</label> 
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>¿Los tiempos de respuesta para el servicio fueron los acordados?</td>
                        <td>
                            <label>'.$data['q2'].'</label>  
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>¿El servicio otorgado cumplió con  las necesidades y expectativas solicitadas?</td>
                        <td>
                            <label>'.$data['q3'].'</label>  
                        </td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>¿Sus dudas respecto al servicio impartido fueron resueltas de manera satisfactoría?</td>
                        <td>
                            <label>'.$data['q4'].'</label> 
                        </td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>¿Recomendaría nuestros servicios?</td>
                        <td>
                            <label>'.$data['q5'].'</label> 
                        </td>
                    </tr>
                </table>';
        }
        $msj.= '<table style="width: 100%">
                <tr>
                    <td width="30%" align="center"><img height="60px" src="<?php echo base_url();?>/app-assets/img/logo.png"><br></td>
                    <td width="70%" align="right" style="font-size: 12px"><br><br>
                        <strong>Soluciones Ambientales y en Seguridad e Higiene S.A. de C.V.</strong>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center" style="font-size: 14px; background-color: #058e1c; color: white"><br><strong>ENCUESTA DE CALIDAD</strong></td>
                </tr>
                <tr>
                    <td align="right" colspan="2">
                            
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Buen día,<br>
                        Encuesta de Calidad Grupo Ecose<br><br>
                    </td>
                </tr>
        </table>';
        //echo $msj; die;
        $this->email->message($msj);
        //$this->email->attach('./mail/encuesta.pdf');
        echo 'd';
        echo $this->email->send();
    }
    
    public function getEncuestas() {
        $id = $this->input->post('id');
        $encuestas = $this->model->getEncuestas($id);
        $json_data = array("data" => $encuestas);
        echo json_encode($json_data);
    }
    
    public function getFacturas($id) {
        $facturas = $this->model->getFacturas($id);
        $json_data = array("data" => $facturas);
        echo json_encode($json_data);
    }
    
    public function subir_comprobante() {
        
            $xml = $_FILES["xml"]["name"];
            $xml = str_replace(' ', '_', $xml);
            $pdf = $_FILES["pdf"]["name"];
            $pdf = str_replace(' ', '_', $pdf);
            
            $directorio = 'uploads/cfdis_cliente_'.$this->input->post("cliente_id");
            if (!is_dir("./".$directorio)) {
                mkdir("./".$directorio, 0777);
            }

            $config['upload_path'] = $directorio . "/";
            $config['allowed_types'] = 'xml|pdf';
            $config['overwrite'] = true;
            //$config['file_name']=$xml;

            $this->load->library('upload', $config);

            //cargamos el archivo xml
            
            if ($this->upload->do_upload("xml")) {
                $this->upload->data("xml");
                
                    $this->load->library('upload', $config);
                    if($this->upload->do_upload("pdf")){
                        $this->upload->data("pdf");
                        //cargamos todo en la base
                        $data=$this->input->post();
                        $data["xml"]=$xml;
                        $data["pdf"]=$pdf;
                        $this->model->insertToCatalogo($data,"facturas");
                        echo "OK";
                        
                    }
                    else { 
                        $msg = $this->upload->display_errors('', '');
                        echo $msg;
                    }
                
            } else {
                $msg = $this->upload->display_errors('', '');
                echo $msg;
            }
    }

    public function eliminaCliente() {
        $id=$this->input->post("id");
        $this->model->eliminaCliente($id);
        $this->model->insertToCatalogo(array("id_cliente"=>$id,"id_usuario"=>$this->id_usuario,"fecha"=>date("Y-m-d H:i:s")),"bitacora_eliminacli");
    }

    public function creaPass() {
        $cli=$this->input->post("cli");
        $id=$this->input->post("id");
        $pass = password_hash(date("YmdGis").$cli.$id, PASSWORD_BCRYPT);
        $pass=substr($pass, 0,(strlen($pass)/3));
        echo $pass;
    }

    public function verificarUsuario() {
        $user=$this->input->post("user");
        $get_user = $this->model->getClienteUsuario($user);
        echo $get_user->num_rows();
    }
    
}