<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Servicios extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //error_reporting(0);

        $this->load->model('Catalogos_model', 'model');
        $this->load->model('Servicios_model', 'servimodel');
        
    }

    public function index() {
        $data["proveedores"]=$this->model->getCatalogo("proveedores");
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/servicios',$data);
        $this->load->view('footer');
    }

    public function alta_servicio() {
        $data["sucursales"]=$this->model->getCatalogo("sucursales");
        $data["proveedores"]=$this->model->getCatalogo("proveedores");
        $id_emp = $this->session->userdata("empresa");
        if($id_emp==1 || $id_emp==2 || $id_emp==3 || $id_emp==6){
            $id_emp = 1;
        }
        $data["familias"]=$this->model->selectWhere("familias",array("id_empresa"=>$id_emp));
        $data["documentos"]=$this->model->getCatalogo("documentos");
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/form_servicio',$data);
        $this->load->view('footer');
    }

     public function edicion_servicio($id) {
        $id_emp = $this->session->userdata("empresa");
        if($id_emp==1 || $id_emp==2 || $id_emp==3 || $id_emp==6){
            $from="servicios";
            $id_emp = 1;
        }else if($id_emp==4){
            $from="servicios_ahisa";
        }
        else if($id_emp==5){
            $from="servicios_auven";
        }if(!isset($id_emp)){
            $from="servicios";
            $id_emp = 1;
        }
        $data["sucursales"]=$this->model->getCatalogo("sucursales");
        $data["proveedores"]=$this->model->getCatalogo("proveedores");
        $data["familias"]=$this->model->selectWhere("familias",array("id_empresa"=>$id_emp));
        
        $data['servicio'] = $this->model->getItemCatalogo($from, $id);
        $data["documentos"]=$this->model->getCatalogo("documentos");
        $data["docs"]=$this->model->getDocsServicio(array("id_servicio"=>$id,"estatus"=>1),$id_emp);
        $data['precios_sucursal']= $this->servimodel->getPreciosSucursal($id);
        $data['precios_puntos']= $this->servimodel->getPreciosPuntos($id);
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/form_servicio', $data);
        $this->load->view('footer');
    }

    public function updateDescrip(){
        $id=$this->input->post('id');
        $descrip=$this->input->post('descrip');
        $id_emp = $this->session->userdata("empresa");

        $descrip = str_replace('<div>', '', $descrip);
        $descrip = str_replace('</div>', '', $descrip);
        //$descrip = str_replace('\n', '<br>', $descrip);
        //$descrip = str_replace("/[\r\n|\n|\r]+/", "<br>", $descrip);
        $descrip=preg_replace('/\n+/', ' <br><br> ', $descrip);
        //log_message('error', 'id'.$id);
        //log_message('error', 'descrip'.$descrip);
        $result = $this->model->updateDescrip($descrip,$id,$id_emp);
        //UPDATE `servicios_auven` SET descripcion = REPLACE(descripcion,'<div>','');
        //UPDATE `servicios_auven` SET descripcion = REPLACE(descripcion,'</div>','')
    }

    public function insertUpdateServicio(){
        $data=$this->input->post();
        $docs= $this->input->post("documentos");
        $id_emp = $this->session->userdata("empresa");
        if(isset($data["precio"]))
            $precio_unico=$data["precio"]; 
        if(isset($data["sucursal_precio"]))
            $sucursal_precio=$data["sucursal_precio"];

        if(isset($data["por_puntos"]) && $data["por_puntos"]==1){
            if($id_emp==1 || $id_emp==2 || $id_emp==3 || $id_emp==6){
                $rango_inicio=$data["rango_inicio"]; 
                $rango_fin=$data["rango_fin"]; 
                $precio_rango=$data["precio_rango"]; 
                $sucursal_rango=$data["sucursal_rango"];
                $data["precio"]=0;
            }
        }

        unset($data["rango_inicio"]); unset($data["rango_fin"]); unset($data["precio_rango"]); unset($data["0"]);
        unset($data["precio"]); unset($data["sucursal_precio"]); unset($data["sucursal_rango"]);
        
        if($id_emp==1 || $id_emp==2 || $id_emp==3 || $id_emp==6){
            $from="servicios";
        }else if($id_emp==4){
            $from="servicios_ahisa";
        }
        else if($id_emp==5){
            $from="servicios_auven";
        }
        if (!isset($data['id'])) {
            //insert
            $this->model->insertToCatalogo($data,$from);
            $result=$this->db->insert_id();
            $id=$result;
        }
        else{
            //update
            $id=$data['id'];
            unset($data['id']);
            //print_r($data); die;
            $result = $this->model->updateCatalogo($data,$id,$from);
            if($id_emp==1 || $id_emp==2 || $id_emp==3 || $id_emp==6){
                $this->servimodel->deletePreciosServicio($id);
            }
        }

        if($data["por_puntos"]!=1 && $id_emp==1 || $data["por_puntos"]!=1 && $id_emp==2 || $data["por_puntos"]!=1 && $id_emp==3 || $data["por_puntos"]!=1 && $id_emp==6){
            //se agregan todos los precios normales a la sucursal
            for($i=0; $i< sizeof($precio_unico); $i++){
                $temp = array(
                    "precio" => $precio_unico[$i],
                    "sucursal_id" => $sucursal_precio[$i],
                    "servicio_id" => $id
                );
                $this->model->insertToCatalogo($temp,"servicios_has_sucursales");
            }
        }
        else if($data["por_puntos"]==1 && $id_emp==1 || $data["por_puntos"]==1 && $id_emp==2 || $data["por_puntos"]==1 && $id_emp==3 || $data["por_puntos"]==1 && $id_emp==6){
            //se agregan todos los precios por puntos
            for($i=0; $i< sizeof($rango_inicio); $i++){
                $temp=array(
                    "rango_inicio"=>$rango_inicio[$i],
                    "rango_fin"=>$rango_fin[$i],
                    "precio"=>$precio_rango[$i],
                    "sucursal_id" => $sucursal_rango[$i],
                    "servicio_id"=>$id
                );
                $this->model->insertToCatalogo($temp,"servicios_has_precios");
            }
        }
        echo $id;
    }

    public function submit_docs_serv(){
        $datos = $this->input->post('data');
        $id_emp = $this->session->userdata("empresa");
        if($id_emp==1 || $id_emp==2 || $id_emp==3 || $id_emp==6){
            $from="servicios_documentos";
        }else if($id_emp==4){
            $from="servicios_documentos_ahisa";
        }
        else if($id_emp==5){
            $from="servicios_documentos_auven";
        }
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            //$id = $DATA[$i]->id;
            $data['id_servicio'] = $DATA[$i]->id_servicio;
            $data['id_documento'] = $DATA[$i]->id_documento;
            $data['id']= $DATA[$i]->id;
            $id = $DATA[$i]->id;

            if($DATA[$i]->id==0){
                $temp=array("id_servicio"=>$DATA[$i]->id_servicio,"id_documento"=>$DATA[$i]->id_documento);
                $id=$this->model->insertToCatalogo($temp,$from);
            }else{
                $temp=array("id_servicio"=>$DATA[$i]->id_servicio,"id_documento"=>$DATA[$i]->id_documento);
                $this->model->updateCatalogon($from,$temp,array("id"=>$id));
            }
            echo $id;          
        }
    }

    public function eliminar_servicio($id){
        $id_emp = $this->session->userdata("empresa");
        if($id_emp==1 || $id_emp==2 || $id_emp==3 || $id_emp==6){
            $from="servicios";
        }else if($id_emp==4){
            $from="servicios_ahisa";
        }
        else if($id_emp==5){
            $from="servicios_auven";
        }
        //if($this->session->userdata("id_usuario")>1){
           echo $this->model->updateCatalogo(array("status"=>0),$id,$from);
        /*}
        else{
            echo 0;
        }*/
        
    }
   
    

}
