<?php

defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
class Ordenes extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //error_reporting(0);
        /*$logueo = $this->session->userdata('logeado_ecose');
        if($logueo!=1){
            redirect(base_url(), 'refresh');
        }*/
        date_default_timezone_set('America/Mexico_City');
        
        $this->load->model('Ordenes_model', 'model');
        $this->load->model('Catalogos_model');
    }

    public function index() {
        $data["proveedores"]=$this->model->getCatalogo("proveedores");
        $data["usu"]=$this->Catalogos_model->getUsuarios();
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('ordenes/ordenes',$data);
        $this->load->view('footer');
        $this->load->view('ordenes/ordenes_listjs');
    }
    
    public function orden($id){
        $data["orden"]=$this->model->getOrden($id);
        $data["cliente"]=$this->model->getCliente($data["orden"]->cliente_id);
        $data["servicios"]=$this->model->getServiciosCotizacion($data["orden"]->cotizacion_id);
        
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('ordenes/form_orden',$data);
        $this->load->view('footer');
    }
	
	public function update($id){
		$data=$this->input->post();
        //$data=$this->session->userdata("id_usuario");
		echo $this->model->updateOrden($id,$data);
	}
    
    public function getData_ordenes() { //tabla de ordenes de trabajo
        $params=$this->input->post();
        $ordenes = $this->model->getOrdenes($params);
        $totalRecords=$this->model->getTotalOrdenes($params);

        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval( $totalRecords ),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $ordenes->result(),
            "query"           =>$this->db->last_query()   
            );
        echo json_encode($json_data);
    }
    
    public function getData_ordenesEstatus($status) {
        $params=$this->input->post();
        $ordenes = $this->model->getOrdenes($params,$status);
        $totalRecords=$this->model->getTotalOrdenesEstatus($params,$status);

        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval( $totalRecords ),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $ordenes->result(),
            "query"           => $this->db->last_query()   
            );
        echo json_encode($json_data);
    }

    function ExportOrdenes($fecha1,$fecha2,$estatus,$emp){        
        $data["fecha1"] = $fecha1;
        $data["fecha2"] = $fecha2;
        $data["estatus"] = $estatus;
        $data["emp"] = $emp;

        $data["o"] = $this->Ordenes_model->getOrdenesExcel($fecha1,$fecha2,$estatus,$emp);
        $this->load->view('ordenes/excelReporte',$data);
    }

    public function cambiarEstatus(){
        $id=$this->input->post("id"); //id de orden
        $status=$this->input->post("status");
        
        $get_or=$this->Catalogos_model->getselectwheren("ordenes",array("id"=>$id));
        $get_or=$get_or->row();
        $this->Catalogos_model->updateCatalogon('ordenes',array("status"=>$status),array('cotizacion_id'=>$get_or->cotizacion_id));
        //$this->db->where("id",$id);
        //echo $this->db->update('ordenes', array("status"=>$status));
    }
    
    public function cancelarServicio(){
        $id=$this->input->post("id");
        $this->db->where("id",$id);
        echo $this->db->update('cotizaciones_has_servicios', array("status"=>0));
    }
    
    public function programacion($id,$idcli){
        $data=$this->input->post();
        $tecnico = $data["tecnico"]; unset($data["tecnico"]);
        $this->db->where("cotizacion_id",$id);
        $this->db->update('cotizaciones_has_servicios', $data);

        //actualizar id_tecnico de orden
        $this->Catalogos_model->updateCatalogon("ordenes",array("id_tecnico"=>$tecnico),array("cotizacion_id"=>$id));

        /*$data["servicio"]=$this->model->getServicio($id);
        //$data["documento"]=$this->model->getDocsFam($id);
        $data["documentos"]=$this->model->getDocsConfig();
        $data["servicios"]=$this->model->getServiciosCotizacion($id);*/
        //$data["tecnico"]=$this->model->getEmailTecnico($data["servicio"]->proveedor);
        //$data["tecnico"]=$this->model->getEmailTecnico($servicio->proveedor);
        //$mail= $this->model->getMail($idcli); //email del cliente
        //obtener email del tecnico

        //print_r($data);        echo $mail; die;
        //echo $this->email_horario($data,$mail,$servicio->correo);
    }

    public function avisarMailCli(){
        $data=$this->input->post();
        $idcli=$data["id_cli"]; unset($data["id_cli"]);
        $id=$data["servicio"]; unset($data["servicio"]);
        $getemp=$this->Catalogos_model->getItemCatalogo("cotizaciones",$id);

        //$data["servicio"]=$this->model->getServicio($id,$getemp->id_empresa);
        $data["documentos"]=$this->model->getDocsServicio($id,$getemp->id_empresa);
        $data["servicios"]=$this->model->getServiciosCotizacion($id,$getemp->id_empresa); //ordenes_model
        $mail= $this->model->getMail($id); //email del cliente

        //log_message('error', 'hora_inicio recibida: '.$data["hora_inicio"]);
        //log_message('error', 'mail: '.$mail);
        //log_message('error', 'mail tec: '.$data["servicio"]->correo);
        //log_message('error', 'data : '.var_dump($data));
        //log_message('error', 'mail_tec : '.$data["servicio"]->mail_tec);
        $this->email_horario($data,$mail/*,$data["servicio"]->mail_tec*/,$getemp->id_empresa); 
        /*$get_mail= $this->model->getMailContacto($id); //email del cliente
         
        foreach ($get_mail as $k) {
            echo $this->email_horario($data,$k->mail,$data["servicio"]->correo);   
        }*/
        
    }
    
    public function email_horario($data,$mail/*,$tec*/,$id_emp){
        
        $this->load->library('email');
        $config = array();
        $config['protocol'] = 'smtp';

        /*$config['smtp_host'] = 'reseller1.networksclub.net';                     
        $config['smtp_user'] = 'programaciones@ecose.com.mx';
        $config['smtp_pass'] = '=xu]Wz-a(Q{V';*/

        if($id_emp==1 || $id_emp==2 || $id_emp==3 || $id_emp==6){
            $config['smtp_host'] = 'ecose.mangoo.systems';                     
            $config['smtp_user'] = 'programaciones@ecose.mangoo.systems';
            $mail_dom = 'programaciones@ecose.mangoo.systems';
            $config['smtp_pass'] = 'nouJ*#?$?g$A';
        }

        if($id_emp==4){
            $config['smtp_host'] = 'ahisa.mangoo.systems';                     
            $config['smtp_user'] = 'programaciones@ahisa.mangoo.systems';
            $mail_dom = 'programaciones@ahisa.mangoo.systems';
            $config['smtp_pass'] = 'glUNjFEtR@S$';
        }

        if($id_emp==5){
            $config['smtp_host'] = 'auven.mangoo.systems';                     
            $config['smtp_user'] = 'programaciones@auven.mangoo.systems';
            $mail_dom = 'programaciones@auven.mangoo.systems';
            $config['smtp_pass'] = '6KM#^GISWzP&';
        }
        $config['smtp_port'] = 465;
        $config["smtp_crypto"] = 'ssl';
        //$config['newline']   = "\r\n";
        //$config["crlf"] = "\r\n";
        $config['mailtype'] = "html";
        $config['charset'] = "utf-8";
        $config['wordwrap'] = TRUE;
        $config['validate'] = true;

        $this->email->initialize($config);
        $this->email->from($mail_dom, 'Programación');
        $this->email->to($mail);
        //$this->email->cc($tec);

        $this->email->subject('Programación de servicio');
        $msj=$this->load->view('ordenes/mail', $data, true);
        $this->email->message($msj);
        $this->email->send();
    }
    
    public function verEmpresaOrden(){
        $id=$this->input->post("id");
        $id_emp=$this->input->post("id_emp");
        $getcs=$this->Catalogos_model->getServiciosEmpresa(array("cotizacion_id"=>$id,"cs.status"=>1),$id_emp);
        $cont_serv=0;
        $class="";
        foreach ($getcs as $k) {
            if($k->id==1 || $k->id==2 || $k->id==3 || $k->id==6){
                $class="style='font-size:11px;' class='btn btn-success' title='Empresa del servicio'";
            }else if($k->id==4){
                $class="style='font-size:11px; color:white' class='btn btn-warning' style='border-radius:20px' title='Empresa del servicio'";
            }else if($k->id==5){
                $class="style='font-size:11px;' class='btn btn-info' style='border-radius:20px' title='Empresa del servicio'";
            }
            echo "<p ".$class."> ".$k->nombre." </p>";
        }
    }

    public function accessReport() {
        header('Access-Control-Allow-Origin: *');
    }

    public function creaNameCP1(){
        $id_cli=$this->input->post('id_cli');
        $namecarp1 = password_hash("CRPTACT".$id_cli, PASSWORD_BCRYPT);
        $namecarp1=substr($namecarp1, (strlen($namecarp1)/2),-1);
        echo preg_replace('([^A-Za-z])', '', $namecarp1);
    }

    public function cargaReporte(){
        $id_orden=$_POST['id_orden'];
        $carp=$_POST['carp'];
        $carp_prin=$_POST['carp_prin'];
        $id_cli=$_POST['id_cli'];

        $name=$_FILES['file_repor']['name'];
        //log_message('error', "nnn: ".$nnn);
        //log_message('error', "id_orden: ".$id_orden);
        //log_message('error', "id_cli: ".$id_cli);
        //log_message('error', "carp_prin: ".$carp_prin);
        //log_message('error', "carp: ".$carp);
        $namecarp1 = password_hash("CRPTACT".$id_cli, PASSWORD_BCRYPT);
        $namecarp1=substr($namecarp1, (strlen($namecarp1)/2),-1);
        $namecarp1 = preg_replace('([^A-Za-z])', '', $namecarp1);

        if($carp!="0"){
            $DIR_SUC=FCPATH.'reportes/cliente/'.$carp_prin.'/'.$carp;
        }else{
            //mkdir(FCPATH.'reportes/cliente/'.$namecarp1, 0777, true);
            $get_ord=$this->Catalogos_model->getItemCatalogo("ordenes",$id_orden);
            $namecarp = password_hash($get_ord->cotizacion_id.$id_orden.$id_cli, PASSWORD_BCRYPT);
            $namecarp=substr($namecarp, (strlen($namecarp)/2),-1);
            $namecarp = preg_replace('([^A-Za-z])', '', $namecarp);
            $DIR_SUC=FCPATH.'reportes/cliente/'.$namecarp1.'/'.$namecarp;
            mkdir($DIR_SUC, 0777, true);
        }
        $col="file_repor";
        $config['upload_path']   = $DIR_SUC;
        $config['allowed_types'] = 'pdf|zip|rar';
        $config['max_size']  = 358400; //350 mb
        $date=date('Gis');
        $config['file_name']=$date."_".$id_orden."_".$this->eliminar_acentos($name);      
        
        $output = [];
        $file_name2 = $config['file_name'];
        //log_message('error', "file_name2: ".$file_name2);
        //log_message('error', "config: ".json_encode($config));
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload("file_repor")){
            $data = array('error' => $this->upload->display_errors());
            //log_message('error', json_encode($data));                    
        }else{
            $upload_data = $this->upload->data();
            $file_name = $upload_data['file_name']; //uploded file name
            $extension=$upload_data['file_ext'];    // uploded file extension
            //$this->Catalogos_model->updateCatalogon('ordenes',array($col=>$file_name,"carpeta_prin"=>$namecarp1,"carpeta"=>$namecarp),array('id'=>$id_orden));
            if($carp=="0"){
                $this->Catalogos_model->updateCatalogon('ordenes',array("carpeta_prin"=>$namecarp1,"carpeta"=>$namecarp,"cargado"=>1),array('id'=>$id_orden));
            }
            $this->Catalogos_model->insertToCatalogo(array("id_orden"=>$id_orden,"file_repor"=>$file_name,"reg"=>date("Y-m-d H:i:s")),"ordenes_reportes");
            /*$get_or=$this->Catalogos_model->getselectwheren("ordenes_reportes",array("id_orden"=>$id_orden,"estatus"=>"1"));
            if($get_or->num_rows()==1){
                $this->Catalogos_model->updateCatalogon('ordenes',array("cargado"=>"1"),array('id'=>$id_orden));
            }*/
            $data = array('upload_data' => $this->upload->data());
        }
        echo json_encode($output);
    }

    public function deleteReporte($id,$carp_cli,$carp,$file,$id_orden){
        /*log_message('error', "id: ".$id);
        log_message('error', "carp_cli: ".$carp_cli);
        log_message('error', "carp: ".$carp);
        log_message('error', "file: ".$file);
        log_message('error', "id_orden: ".$id_orden);*/
        $dir=FCPATH.'reportes/cliente/'.$carp_cli.'/'.$carp."/".$file;
        //log_message('error', "dir: ".$dir);
        if (file_exists($dir)) {
            unlink($dir);
        }
        $output = [];
        //$this->Catalogos_model->updateCatalogon('ordenes',array("file_repor"=>"","carpeta"=>""),array('id'=>$id));
        $this->Catalogos_model->updateCatalogon('ordenes_reportes',array("estatus"=>"0"),array('id'=>$id));
        //solo cambiar estatus cuando no exista ningun reporte activo
        $get_or=$this->Catalogos_model->getselectwheren("ordenes_reportes",array("id_orden"=>$id_orden,"estatus"=>"1"));
        if($get_or->num_rows()==0){
            $this->Catalogos_model->updateCatalogon('ordenes',array("cargado"=>"0"),array('id'=>$id_orden));
        }
        echo json_encode($output);
    }

    public function verificarReportes(){
        $id_orden = $this->input->post('id_orden');
        
        $data=$this->model->getOrdenesReportes($id_orden);
        //log_message('error', "id_orden verificarReportes: ".$id_orden);
        $i=0; $html=""; $carpeta_prin=""; $carpeta="";
        if($data){
            foreach ($data as $key) {
                $i++;
                $carpeta_prin=$key->carpeta_prin; $carpeta=$key->carpeta;
                $img = "<img src='".base_url()."reportes/cliente/".$key->carpeta_prin."/".$key->carpeta."/".$key->file_repor."' width='50px' alt='Sin Dato'>";
                $imgdet = "{type: 'image', url: '".base_url()."Ordenes/deleteReporte/".$key->id."/".$key->carpeta_prin."/".$key->carpeta."/".$key->file_repor."/".$id_orden."', caption: '".$key->file_repor."', key:".$i."}";
                $ext = explode('.',$key->file_repor);
                //log_message('error', 'url: '.base_url()."reportes/cliente/".$key->carpeta_prin."/".$key->carpeta."/".$key->file_repor);
                if($ext[1]!="pdf"){
                    $imgp = $key->file_repor;
                    $typePDF = "false";  
                }else{
                    $imgp = "".base_url()."reportes/cliente/".$key->carpeta_prin."/".$key->carpeta."/".$key->file_repor;
                    $imgdet = "{type: 'pdf', url: '".base_url()."Ordenes/deleteReporte/".$key->id."/".$key->carpeta_prin."/".$key->carpeta."/".$key->file_repor."/".$id_orden."', caption: '".$key->file_repor."/".$id_orden."', key:".$i."}";
                    $typePDF = "true";
                }
                $var_icon="'pdf': '<i class="."fas fa-file-pdf text-danger"."></i>',
                           'zip': '<i class="."fas fa-file-archive text-muted"."></i>'";
                $downloadUrl=base_url()."reportes/cliente/".$key->carpeta_prin."/".$key->carpeta."/".$key->file_repor;
                
                $selectchs="<option value=''>Elije una opción:</option>";
                $getchs=$this->model->getServiciosOrden($id_orden,$key->id_serv_repor);
                foreach($getchs as $r){
                    if($key->id_serv_repor==$r->id)
                        $sel="selected";
                    else
                        $sel="";

                    if($r->id_empresa_serv!=4 && $r->id_empresa_serv!=5){
                        $selectchs.="<option ".$sel." value='".$r->id."'>".$r->nombre."</option>";
                    }if($r->id_empresa_serv==4){
                        $selectchs.="<option ".$sel." value='".$r->id."'>".$r->nombre2."</option>";
                    }if($r->id_empresa_serv==5){
                        $selectchs.="<option ".$sel." value='".$r->id."'>".$r->nombre3."</option>";
                    }

                }
                $html.= '<div class="row col-md-12">
                        <div class="col-md-6">
                            <label class="col-md-12"> Archivo: '.$key->file_repor.'</label>
                            <input type="file" name="file_repor" id="file_repor'.$i.'" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <div class="controls">
                                <label>Servicio</label>
                                <select name="id_serv_repor" onchange="asignaServRep('.$key->id.',this.value)" class="form-control form-control-sm">
                                    '.$selectchs.' 
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr style="background-color: red;">
                        </div>
                    </div>

                    <script>
                    $("#file_repor'.$i.'").fileinput({
                        overwriteInitial: false,
                        showDownload: true,
                        showClose: false,
                        showCaption: false,
                        showUploadedThumbs: false,
                        showBrowse: false,';
                $html.=   "downloadUrl: '".$downloadUrl."',
                        initialPreviewDownloadUrl: '".$downloadUrl."',
                        ";
                $html.=   'removeTitle: "Cancel or reset changes",
                        elErrorContainer: "#kv-avatar-errors-1",
                        msgErrorClass: "alert alert-block alert-danger",
                        defaultPreviewContent: "'.$img.'",
                        layoutTemplates: {main2: "{preview} {remove}"},
                        allowedFileExtensions: ["jpg", "png", "gif","pdf"],';

                $html.=    "previewFileIconSettings: { 
                            'pdf': '<i class=";$html.= '"fas fa-file-pdf text-danger"';$html.= "></i>',
                            'zip': '<i class=";$html.= '"fas fa-file-archive text-muted"';$html.= "></i>',
                        },";
                $html.=    'initialPreview: [
                            "'.$imgp.'"        
                        ],
                        initialPreviewAsData: '.$typePDF.',
                        initialPreviewFileType: "image",
                        initialPreviewConfig: [
                            '.$imgdet.'
                        ],
                        previewFileExtSettings: { // configure the logic for determining icon file extensions
                            "zip": function(ext) {
                                return ext.match(/(zip|rar|tar|gzip|gz|7z)$/i);
                            },
                        }
                    }).on("filedeleted", function(event, files, extra) {
                        verificarReportes('.$id_orden.');
                    });
                    $("#inputFile'.$i.'").on("filedeleted", function(event, key, jqXHR, data) {
                        console.log("Key = " + key);
                    });
                    </script>';
            }
        }
        echo json_encode(array("html"=>$html,"carpeta_prin"=>$carpeta_prin,"carpeta"=>$carpeta));
    }

    public function verificarCarpetasOrd(){
        $id_orden = $this->input->post('id_orden');
        $data=$this->model->getOrdenesReportes($id_orden);
        $carpeta_prin=""; $carpeta=""; $cont=0;
        if($data){
            foreach ($data as $key) {
                $cont++;
                $carpeta_prin=$key->carpeta_prin; $carpeta=$key->carpeta;
            }
        }
        echo json_encode(array("cont"=>$cont,"carpeta_prin"=>$carpeta_prin,"carpeta"=>$carpeta,"id_orden"=>$id_orden));
    }

    public function asignarServicioRep(){
        $id_repor=$this->input->post("id_repor"); 
        $id_chs=$this->input->post("id_chs");
        $this->Catalogos_model->updateCatalogon('ordenes_reportes',array("id_serv_repor"=>$id_chs),array('id'=>$id_repor));
    }

    public function eliminar_acentos($cadena){
        //Reemplazamos la A y a
        $cadena = str_replace(
        array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
        array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
        $cadena
        );

        //Reemplazamos la E y e
        $cadena = str_replace(
        array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
        array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
        $cadena );

        //Reemplazamos la I y i
        $cadena = str_replace(
        array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
        array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
        $cadena );

        //Reemplazamos la O y o
        $cadena = str_replace(
        array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
        array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
        $cadena );

        //Reemplazamos la U y u
        $cadena = str_replace(
        array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
        array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
        $cadena );

        //Reemplazamos la N, n, C y c
        $cadena = str_replace(
        array('Ç', 'ç'),
        array('C', 'c'),
        $cadena
        );

        return $cadena;
    }
}