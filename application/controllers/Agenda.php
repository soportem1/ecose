<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Agenda extends CI_Controller {

    public function __construct() {
        parent::__construct();
        error_reporting(0);
//        $logueo = $this->session->userdata('logeado_aries');
//        if($logueo!=1){
//            redirect(base_url(), 'refresh');
//        }
        $this->load->model('Operaciones_model', 'model');
    }
    
    public function index() {
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('agenda/agenda');
        $this->load->view('footer');
    }
    
    public function save_evento(){
        $data= $this->input->post();
        $result=$this->model->insertToCatalogo($data,"eventos");
        echo $result;
    }
    
    public function eventos(){
        $eventos1 = $this->model->getEventos();
        $eventos2 = $this->model->getEventosServicios();
        $eventos= array_merge($eventos1,$eventos2);
        echo json_encode($eventos);
    }
    
    public function get_evento($id,$tabla){
        $data["info"]=$this->model->getEvento($id,$tabla);
        if(isset($data["info"]->cliente)){
            $data["productos"]=$this->model->getProductosEvento($id);
        }
        echo json_encode($data);
    }
    
    
    
    
}
